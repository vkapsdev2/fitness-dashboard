<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'delete users']);
        Permission::create(['name' => 'add users']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'admin']);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        

        $role2 = Role::create(['name' => 'coach']);
        $role2->givePermissionTo('edit users');

        $role3 = Role::create(['name' => 'customer']);

        // create demo users
        $user = Factory(App\User::class)->create([
           
             'name' => 'Ankit User',
            'email' => 'ankit@example.com',
            'password' => bcrypt('123456')
        ]);
        $user->assignRole($role3);

        $user = Factory(App\User::class)->create([
            'name' => 'Swadha Coach User',
            'email' => 'swadha@example.com',
            'password' => bcrypt('123456')
        ]);
        $user->assignRole($role2);

        $user = Factory(App\User::class)->create([
            'name' => 'Admin Admin User',
            'email' => 'admin@example.com',
            'password' => bcrypt('123456')
        ]);
        $user->assignRole($role1);
    }
}
