<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovementSubHomeworkoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movement_sub_homeworkouts', function (Blueprint $table) {
            $table->id();
            $table->integer('homeWorkout_id')->unsigned()->index();
            $table->foreign('homeWorkout_id')->references('id')->on('movement_homeworkouts')->onDelete('cascade');
            $table->string('sub_name'); 
            $table->string('video_file')->nullable();
            $table->string('video_url')->nullable();
            $table->string('sub_banner')->nullable();
            $table->string('workout_timing')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movement_sub_homeworkouts');
    }
}
