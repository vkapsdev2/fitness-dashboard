<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes_log', function (Blueprint $table) {
            $table->id();
            $table->string('customer_id');
            $table->string('hours_sleep')->nullable();
            $table->string('quality_sleep')->nullable();
            $table->string('date');
            $table->string('angry')->nullable();
            $table->string('anxious')->nullable();
            $table->string('calm')->nullable();
            $table->string('depressed')->nullable();
            $table->string('fearful')->nullable();
            $table->string('grateful')->nullable();
            $table->string('happy')->nullable();
            $table->string('helpless')->nullable();
            $table->string('relaxed')->nullable();
            $table->string('sad')->nullable();
            $table->string('stressed')->nullable();
            $table->string('tired')->nullable();
            $table->string('optimistic')->nullable();
            $table->string('something_good')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes_log');
    }
}
