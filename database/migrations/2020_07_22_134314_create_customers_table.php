<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
             $table->increments('id');
            $table->bigInteger('customer_id')->unsigned();
            $table->foreign('customer_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
            $table->string('customer_name');
            $table->string('level')->nullable();
            $table->string('weight')->nullable();
            $table->string('height')->nullable();
            $table->string('age')->nullable();
            $table->string('body_fat_percent')->nullable();
            $table->string('body_fat_mass')->nullable();
            $table->string('skletal_muscle_mass')->nullable();
            $table->string('neck')->nullable();
            $table->string('shoulder')->nullable();
            $table->string('right_bicep')->nullable();
            $table->string('chest')->nullable();
            $table->string('waist')->nullable();
            $table->string('hips')->nullable();
            $table->string('right_thigh')->nullable();
            $table->string('right_calf')->nullable();
            $table->string('career')->nullable();
            $table->string('relationship')->nullable();
             $table->string('joy')->nullable();
            $table->string('health')->nullable();
            $table->string('nutrition')->nullable();
            $table->string('waist_satisfaction')->nullable();
            $table->string('appearance')->nullable();
            $table->string('purpose')->nullable();
            $table->string('sleep')->nullable();
            $table->string('social')->nullable();
            $table->string('spiritual')->nullable();
            $table->string('ready_for_change')->nullable();
            $table->string('social_goal')->nullable();
            $table->string('consistent_physical_activity')->nullable();
            $table->longText('consistent_physical_activity_time')->nullable();
            $table->longText('consistent_physical_activity_description')->nullable();
            $table->longText('customer_occupation')->nullable();
            $table->longText('houldhold')->nullable();
            $table->text('recreational_activity')->nullable();
            $table->text('hobbies')->nullable();
            $table->text('sleep_hours_in_detail')->nullable();
            $table->text('sleep_taking_time')->nullable();
            $table->string('last_physical_examination_time')->nullable();
            $table->string('ever_been_hospitalized')->nullable();
            $table->string('is_pregnant')->nullable();

            $table->string('smoking')->nullable();

            $table->string('alcoholic_quantity_per_week')->nullable();

            $table->string('diagnosed_heart_condition')->nullable();

            $table->string('is_feel_chest_pain')->nullable();

            $table->string('did_feel_chest_pain_when_not_physical')->nullable();

            $table->string('is_feel_loss_consciousness')->nullable();

            $table->string('is_joint_issue')->nullable();

            $table->string('is_take_medications')->nullable();

            $table->string('is_diabetic')->nullable();

            $table->string('is_high_blood_pressure')->nullable();

            $table->string('is_high_cholesterol')->nullable();

            $table->string('is_diagnosed_osteoporosis')->nullable();

            $table->string('is_breathless_to_staris')->nullable();

            $table->string('do_you_have_been_cancer')->nullable();

            $table->string('activity_reson_for_not_engage_physical_activity')->nullable();
            $table->longText('family_disease_detail')->nullable();
            $table->longText('other_family_disease')->nullable();
            $table->string('plan')->nullable();
            $table->dateTime('plan_start_date',0)->nullable();
            $table->dateTime('plan_end_date',0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
