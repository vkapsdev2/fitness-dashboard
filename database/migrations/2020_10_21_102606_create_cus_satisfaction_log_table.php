<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCusSatisfactionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cus_satisfaction_log', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id')->unsigned()->index();
            $table->string('log_date')->nullable();
            $table->string('career')->nullable();
            $table->string('relationship')->nullable();
            $table->string('joy')->nullable();
            $table->string('health')->nullable();
            $table->string('nutrition')->nullable();
            $table->string('sleep')->nullable();
            $table->string('appearance')->nullable();
            $table->string('purpose')->nullable();
            $table->string('social')->nullable();
            $table->string('spiritual')->nullable();
            $table->string('ready_for_change')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cus_satisfaction_log');
    }
}
