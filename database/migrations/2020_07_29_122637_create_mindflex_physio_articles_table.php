<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMindflexPhysioArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mindflex_physio_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('article_img');
            $table->string('author_name');
            $table->longText('description')->nullable();
            $table->string('physio_article_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mindflex_physio_articles');
    }
}
