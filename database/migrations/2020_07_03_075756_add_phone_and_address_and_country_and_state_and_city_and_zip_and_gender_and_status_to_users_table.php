<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneAndAddressAndCountryAndStateAndCityAndZipAndGenderAndStatusToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->after('remember_token');
            $table->string('address')->after('phone');
            $table->string('country')->after('address');
            $table->string('state')->after('country');
            $table->string('city')->after('state');
            $table->string('zip')->after('city');
            $table->string('gender')->after('zip');
            $table->string('status')->after('gender');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('image');
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('country');
            $table->dropColumn('state');
            $table->dropColumn('city');
            $table->dropColumn('zip');
            $table->dropColumn('gender');
            $table->dropColumn('status');
        });
    }
}
