<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuggestedCustomerMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggested_customer_meals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('suggester_coach_id')->nullable();
            $table->bigInteger('suggested_customer_id')->unsigned();
                        $table->foreign('suggested_customer_id')
                        ->references('id')
                        ->on('users')
                        ->onDelete('cascade');   
            $table->text('assign_break_fast_meal')->nullable();
            $table->string('break_fast_days')->nullable();
            $table->text('assign_lunch_meal')->nullable();
            $table->string('lunch_days')->nullable();
            $table->text('assign_dinner_meal')->nullable();
            $table->string('dinner_days')->nullable();
            $table->longText('other_suggest_meal_detail')->nullable();
             $table->date('suggesting_date')->nullable();
            $table->string('suggest_meal_status')->default('1');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggested_customer_meals');
    }
}
