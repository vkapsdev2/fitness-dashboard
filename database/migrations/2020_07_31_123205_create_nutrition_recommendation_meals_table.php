<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNutritionRecommendationMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrition_recommendation_meals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Food_name')->nullable();
            $table->string('meal_type')->nullable();
            $table->string('calorie')->nullable();
            $table->string('fats')->nullable();
            $table->string('carbohydrate')->nullable();
            $table->string('protein')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrition_recommendation_meals');
    }
}
