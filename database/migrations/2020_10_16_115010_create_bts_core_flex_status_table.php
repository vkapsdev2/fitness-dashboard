<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBtsCoreFlexStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bts_core_flex_status', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('coach_id')->unsigned()->index();
            $table->foreign('coach_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('customer_id')->unsigned()->index();
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('exercise_id')->unsigned()->index();
            $table->foreign('exercise_id')->references('id')->on('bts_core_flex')->onDelete('cascade');

            $table->string('rating');
            $table->text('review')->nullable();
            $table->date('review_date');
            $table->string('like_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bts_core_flex_status');
    }
}
