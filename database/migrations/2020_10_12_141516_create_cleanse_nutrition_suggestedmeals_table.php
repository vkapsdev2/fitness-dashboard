<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCleanseNutritionSuggestedmealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cleanse_nutrition_suggestedmeals', function (Blueprint $table) {
            $table->id();
            $table->string('Food_name')->nullable();
            $table->string('meal_type')->nullable();
            $table->string('calorie')->nullable();
            $table->string('fats')->nullable();
            $table->string('carbohydrate')->nullable();
            $table->string('protein')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cleanse_nutrition_suggestedmeals');
    }
}
