<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMindiflexPysioDailyAffirmationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mindiflex_pysio_daily_affirmation', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('video_file')->nullable();
            $table->string('video_url')->nullable();
            $table->text('description')->nullable();
            $table->string('mark_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mindiflex_pysio_daily_affirmation');
    }
}
