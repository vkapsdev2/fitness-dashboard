<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCleanseMindresetStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cleanse_mindreset_status', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('mindreset_id')->unsigned()->index();
            $table->foreign('mindreset_id')->references('id')->on('cleanse_mind_reset')->onDelete('cascade');

            $table->bigInteger('customer_id')->unsigned()->index();
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('cascade');
            $table->string('status');
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cleanse_mindreset_status');
    }
}
