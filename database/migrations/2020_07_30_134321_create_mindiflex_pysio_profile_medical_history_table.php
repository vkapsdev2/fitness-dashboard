<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMindiflexPysioProfileMedicalHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mindiflex_pysio_profile_medical_history', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('medical_hisory_detail');
            $table->bigInteger('customer_id')->unsigned();
            $table->foreign('customer_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
            $table->date('date')->nullable();
            $table->string('time')->nullable();
            $table->string('medical_history_status')->default('1');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mindiflex_pysio_profile_medical_history');
    }
}
