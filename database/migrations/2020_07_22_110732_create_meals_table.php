<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meal_title')->nullable();
            $table->string('meal_image')->nullable();
            $table->string('meal_calories')->nullable();
            $table->string('meal_protein')->nullable();
            $table->string('meal_carbs')->nullable();
            $table->string('meal_fats')->nullable();
            $table->string('meal_fiber')->nullable();
            $table->string('meal_minerals')->nullable();
            $table->string('meal_calcium')->nullable();
            $table->text('meal_vitmins')->nullable();
            $table->string('meal_water')->nullable();
            $table->longText('other_meal_detail')->nullable();
            $table->text('meal_quantity')->nullable();
            $table->string('meal_plan_type')->nullable();
            $table->longText('meal_description')->nullable();
            $table->string('created_by')->nullable();
            $table->string('meal_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meals');
    }
}
