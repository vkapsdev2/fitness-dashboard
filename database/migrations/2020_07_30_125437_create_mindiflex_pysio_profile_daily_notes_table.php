<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMindiflexPysioProfileDailyNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mindiflex_pysio_profile_daily_notes', function (Blueprint $table) {
             $table->increments('id');
            $table->longText('daily_note_detail');
            $table->bigInteger('customer_id')->unsigned();
            $table->foreign('customer_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
            $table->date('daily_note_date')->nullable();
            $table->string('time')->nullable();
            $table->string('daily_note_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mindiflex_pysio_profile_daily_notes');
    }
}
