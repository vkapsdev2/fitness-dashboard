<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFacebookUrlToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
             $table->string('facebook_url')->after('gender')->nullable();
             $table->string('google_url')->after('facebook_url')->nullable();
             $table->string('twitter_url')->after('google_url')->nullable();
             $table->string('youtube_url')->after('twitter_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('facebook_url');
            $table->dropColumn('google_url');
            $table->dropColumn('twitter_url');
            $table->dropColumn('youtube_url');
        });
    }
}
