<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coaches', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('coach_id')->unsigned();
            $table->foreign('coach_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
            $table->string('specializations')->nullable();
            $table->string('certification')->nullable();
            $table->string('awards')->nullable();
            $table->string('publish_articles')->nullable();
            $table->string('client_feedback')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coaches');
    }
}
