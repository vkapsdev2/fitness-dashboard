<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('customer_id')->unsigned(); 
            $table->date('date')->nullable();
            $table->longText('order_detail')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('is_applied_coupon')->nullable();
            $table->string('coupon')->nullable();
            $table->string('total_item')->nullable();
            $table->string('total')->nullable();
            $table->string('discount_rate')->nullable();
            $table->string('grand_total')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('order_status')->default('1');
            $table->timestamps();
            $table->foreign('customer_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
