<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBtsExpressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bts_express', function (Blueprint $table) {
            $table->id();
            $table->string('day');
            $table->bigInteger('coach_id')->unsigned()->index();
            $table->foreign('coach_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('start_time');
            $table->string('end_time');
            $table->string('banner');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bts_express');
    }
}
