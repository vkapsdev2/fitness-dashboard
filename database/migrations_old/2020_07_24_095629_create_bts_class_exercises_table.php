<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBtsClassExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bts_class_exercises', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('class_id')->unsigned()->index();
            $table->foreign('class_id')->references('id')->on('bts_classes')->onDelete('cascade');
            $table->string('exercise_name'); 
            $table->string('video_file')->nullable();
            $table->string('video_url')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bts_class_exercises');
    }
}
