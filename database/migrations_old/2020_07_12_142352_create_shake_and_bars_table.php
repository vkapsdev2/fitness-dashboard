<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShakeAndBarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shake_and_bars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shake_name')->nullable();
            $table->string('shake_img')->nullable();
            $table->string('shake_type')->nullable();
            $table->string('is_shake_customize')->nullable();
            $table->string('shake_ingredients')->nullable();
            $table->text('shake_description')->nullable();
            $table->string('shake_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shake_and_bars');
    }
}
