<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCleanseWeightLossStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cleanse_weight_loss_status', function (Blueprint $table) {
            $table->id();
            $table->integer('weight_loss_id')->unsigned()->index();
            $table->foreign('weight_loss_id')->references('id')->on('cleanse_weight_loss')->onDelete('cascade');

            $table->integer('customer_id')->unsigned()->index();
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');
    
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cleanse_weight_loss_status');
    }
}
