<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoachAssignToCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coach_assign_to_customer', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('coach_assign_id')->unsigned();
            $table->foreign('coach_assign_id')
            ->references('id')
            ->on('users');
            $table->longText('coach_assign_customer_id')->nullable();
            $table->string('coach_assign_customer_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coach_assign_to_customer');
    }
}
