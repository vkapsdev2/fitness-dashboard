<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustRemindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cust_reminders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('coach_id')->unsigned();
            $table->foreign('coach_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('title');
            $table->text('days');
            $table->time('time');
            $table->string('repeat_status');
            $table->string('reminder_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cust_reminders');
    }
}
