<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_title')->nullable();
            $table->string('plan_image')->nullable();
            $table->string('validity_days')->nullable();
            $table->string('plan_type')->nullable();
            $table->string('actual_price')->nullable();
            $table->string('sales_price')->nullable();
            $table->longText('plan_description')->nullable();
            $table->string('plan_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
