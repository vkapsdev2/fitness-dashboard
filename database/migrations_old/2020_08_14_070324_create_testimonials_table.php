<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id');
               $table->string('title')->nullable();
               $table->bigInteger('from_customer')->unsigned();
               $table->foreign('from_customer')->references('id')->on('users')->onDelete('cascade');
               $table->bigInteger('to_coach')->unsigned();
               $table->foreign('to_coach')->references('id')->on('users')->onDelete('cascade');
               $table->string('description')->nullable();
               $table->string('testimonial_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
    }
}
