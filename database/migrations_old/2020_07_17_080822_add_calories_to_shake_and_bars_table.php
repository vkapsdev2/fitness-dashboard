<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCaloriesToShakeAndBarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shake_and_bars', function (Blueprint $table) {
            $table->string('calories')->nullable()->after('shake_type');
            $table->string('protein')->nullable()->after('calories');
            $table->string('carbs')->nullable()->after('protein');
            $table->string('fats')->nullable()->after('carbs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shake_and_bars', function (Blueprint $table) {
            $table->dropColumn('calories');
            $table->dropColumn('protein');
            $table->dropColumn('carbs');
            $table->dropColumn('fats');
        });
    }
}
