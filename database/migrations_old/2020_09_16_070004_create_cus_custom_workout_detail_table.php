<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCusCustomWorkoutDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void Exercises
     */
    public function up()
    {
        Schema::create('cus_custom_workout_detail', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('customer_workout_id')->unsigned();
            $table->foreign('customer_workout_id')->references('id')->on('cus_custom_workout')->onDelete('cascade');

            $table->bigInteger('gym_category_id')->unsigned();
            $table->foreign('gym_category_id')->references('id')->on('movement_gymworkouts')->onDelete('cascade');

             $table->bigInteger('gym_category_exercise_id')->unsigned();
            $table->foreign('gym_category_exercise_id')->references('id')->on('movement_sub_gymworkouts')->onDelete('cascade');

            $table->binary('sets')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cus_custom_workout_detail');
    }
}
