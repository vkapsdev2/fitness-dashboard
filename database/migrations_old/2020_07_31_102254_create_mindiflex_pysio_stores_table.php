<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMindiflexPysioStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mindiflex_pysio_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->string('actual_price')->nullable();
            $table->string('sales_price')->nullable();
            $table->string('total_quantity')->nullable();
            $table->text('buy_now_url')->nullable();
            $table->text('description')->nullable();
            $table->string('remaining_quantity')->nullable();
            $table->string('alert_quantity_stock')->nullable();
            $table->string('store_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mindiflex_pysio_stores');
    }
}
