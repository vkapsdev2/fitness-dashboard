<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovementSubGymworkoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movement_sub_gymworkouts', function (Blueprint $table) {
            $table->id();
            $table->integer('gymWorkout_id')->unsigned()->index();
            $table->foreign('gymWorkout_id')->references('id')->on('movement_gymworkouts')->onDelete('cascade');
            $table->string('sub_name'); 
            $table->string('video_file')->nullable();
            $table->string('video_url')->nullable();
            $table->text('description')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movement_sub_gymworkouts');
    }
}
