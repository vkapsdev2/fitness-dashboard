<?php

use Illuminate\Support\Facades\Route;

/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::post('ajaxLogin','Auth\AjaxAuthenticationController@login')->name('ajax-login');
Route::post('ajaxRegister','Auth\AjaxAuthenticationController@register')->name('ajax-register');

 Route::get('facebook_login','UserController@facebookCheck');

Route::get('/', function () {
    return redirect()->guest('login');
});
Route::get('logout', 'Auth\LoginController@logout'); 

Route::group(['auth'], function () {
  //Route::get('/home', 'PagesController@index');
});



Route::group(['middleware' => ['auth']], function() {
  Route::get('/home', 'PagesController@index');
  
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');

    /*User Crud Ajax*/
    Route::post('users/ajaxList','UserController@ajaxList');
    Route::post('users/ajaxAddUser','UserController@ajaxAdd')->name('ajax-add-user');
    Route::post('users/ajaxUpdateUser/{id}','UserController@ajaxUpdate')->name('ajax-update-user');
    Route::delete('users/ajaxDeleteUser/{id}','UserController@ajaxDelete')->name('ajax-delete-user');
    Route::get('users/ajaxGetUser/{id}','UserController@ajaxGet')->name('ajax-get-user');

    /*End Ajax user Crud*/

    // Route::resource('products','ProductController');

  /*========================Event Module start=======================*/
    Route::get('/eventsList','Admin\EventController@index')->name('event.index'); 
    Route::get('event/create', 'Admin\EventController@create')->name('event.create');
    Route::post('event/save', 'Admin\EventController@save')->name('event.save');

    Route::get('/event/edit/{id}','Admin\EventController@edit')->name('event.edit');
    Route::post('/event/update/{id}','Admin\EventController@update')->name('event.update');
    Route::delete('/event/delete/{id}','Admin\EventController@delete')->name('event.delete');
    Route::get('/event/search', 'Admin\EventController@search')->name('event.search');
  /*=================Event Module end===================================*/

  /*=============Resources Podcast Module start==========================*/
    Route::get('/podcasts','Admin\PodcastResourcesController@index')->name('podcast.index'); 
    Route::get('podcast/create', 'Admin\PodcastResourcesController@create')->name('podcast.create');
    Route::post('podcast/save', 'Admin\PodcastResourcesController@save')->name('podcast.save');
    Route::get('/podcast/edit/{id}','Admin\PodcastResourcesController@edit')->name('podcast.edit');
    Route::post('/podcast/update/{id}','Admin\PodcastResourcesController@update')->name('podcast.update');
    Route::delete('/podcast/delete/{id}','Admin\PodcastResourcesController@delete')->name('podcast.delete');

    Route::get('/podcasts/{podcast_id}/SubPodcasts','Admin\PodcastResourcesController@subPodcastList')->name('subPodcasts.index');

    Route::get('podcast/{podcast_id}/subPodcast/create', 'Admin\PodcastResourcesController@createSubPodcast')->name('subPodcast.create');

    Route::post('podcast/{podcast_id}/subPodcast/save', 'Admin\PodcastResourcesController@saveSubPodcast')->name('subPodcast.save');

    Route::get('podcast/{podcast_id}/subPodcast/edit/{id}','Admin\PodcastResourcesController@editSubPodcast')->name('subPodcast.edit');

    Route::post('podcast/{podcast_id}/subPodcast/update/{id}','Admin\PodcastResourcesController@updateSubPodcast')->name('subPodcast.update');
    
    Route::delete('podcast/{podcast_id}/subPodcast/delete/{id}','Admin\PodcastResourcesController@deleteSubPodcast')->name('subPodcast.delete');
  /*==========Resources Podcast Module end================================*/

  /*=======================Resources Recipies module start==================*/
    Route::get('/recipies','Admin\RecipiesResourcesController@index')->name('recipies.index');
    Route::get('recipies/create', 'Admin\RecipiesResourcesController@create')->name('recipies.create');
    Route::post('recipies/save', 'Admin\RecipiesResourcesController@save')->name('recipies.save'); 
    Route::get('/recipies/edit/{id}','Admin\RecipiesResourcesController@edit')->name('recipies.edit'); 
    Route::post('/recipies/update/{id}','Admin\RecipiesResourcesController@update')->name('recipies.update');
    Route::get('/recipies/detailView/{id}','Admin\RecipiesResourcesController@detailView')->name('recipies.detailView'); 
     
    Route::delete('/recipies/delete/{id}','Admin\RecipiesResourcesController@delete')->name('recipies.delete');
  /*=====================Resources Recipies module end========================*/

  /*===========MindFlex Planner  Movement/Homeworkout module start============*/
    Route::get('/mindflex_planner/movement/home_workout','Admin\MindFlexPlanner\Movement\HomeWorkoutController@index')->name('movement_home_workout.index'); 

    Route::post('/mindflex_planner/movement/ajax_home_list','Admin\MindFlexPlanner\Movement\HomeWorkoutController@ajaxList')->name('ajax_home_list.ajaxList'); 

    Route::get('mindflex_planner/movement/home_workout/create', 'Admin\MindFlexPlanner\Movement\HomeWorkoutController@create')->name('movement_home_workout.create');

    Route::post('mindflex_planner/movement/home_workout/save', 'Admin\MindFlexPlanner\Movement\HomeWorkoutController@save')->name('movement_home_workout.save');

    Route::get('/mindflex_planner/movement/home_workout/edit/{id}','Admin\MindFlexPlanner\Movement\HomeWorkoutController@edit')->name('movement_home_workout.edit');

    Route::post('/mindflex_planner/movement/home_workout/update/{id}','Admin\MindFlexPlanner\Movement\HomeWorkoutController@update')->name('movement_home_workout.update');

    Route::delete('/mindflex_planner/movement/home_workout/delete/{id}','Admin\MindFlexPlanner\Movement\HomeWorkoutController@delete')->name('movement_home_workout.delete');

    Route::get('/mindflex_planner/movement/home_workout/ajax','Admin\MindFlexPlanner\Movement\HomeWorkoutController@ajaxData')->name('movement_home_workout.ajaxData');

    Route::get('/mindflex_planner/movement/home_workout/{homeWorkout_id}/sub_home_workout','Admin\MindFlexPlanner\Movement\HomeWorkoutController@subHomeWorkoutList')->name('movement_sub_home_workout.index');

    Route::get('mindflex_planner/movement/home_workout/{homeWorkout_id}/sub_home_workout/create', 'Admin\MindFlexPlanner\Movement\HomeWorkoutController@createSubHomeWorkout')->name('movement_sub_home_workout.create');

    Route::post('mindflex_planner/movement/home_workout/{homeWorkout_id}/sub_home_workout/save', 'Admin\MindFlexPlanner\Movement\HomeWorkoutController@saveSubHomeWorkout')->name('movement_sub_home_workout.save');

    Route::get('mindflex_planner/movement/home_workout/{homeWorkout_id}/sub_home_workout/edit/{id}','Admin\MindFlexPlanner\Movement\HomeWorkoutController@editSubHomeWorkout')->name('movement_sub_home_workout.edit');

    Route::post('mindflex_planner/movement/home_workout/{homeWorkout_id}/sub_home_workout/update/{id}','Admin\MindFlexPlanner\Movement\HomeWorkoutController@updateSubHomeWorkout')->name('movement_sub_home_workout.update');
      
    Route::delete('mindflex_planner/movement/home_workout/{homeWorkout_id}/sub_home_workout/delete/{id}','Admin\MindFlexPlanner\Movement\HomeWorkoutController@deleteSubHomeWorkout')->name('movement_sub_home_workout.delete');
    
    Route::get('/mindflex_planner/movement/home_workout/{homeWorkout_id}/sub_home_workout/detailView/{id}','Admin\MindFlexPlanner\Movement\HomeWorkoutController@detailView')->name('movement_sub_home_workout.detailView'); 
  /*==========MindFlex Planner  Movement/Homeworkout module end===================*/

  /*===========MindFlex Planner  Movement/Gymworkout module Start================*/
    Route::get('/mindflex_planner/movement/gym_workout','Admin\MindFlexPlanner\Movement\GymWorkoutController@index')->name('movement_gym_workout.index'); 

    Route::get('mindflex_planner/movement/gym_workout/create', 'Admin\MindFlexPlanner\Movement\GymWorkoutController@create')->name('movement_gym_workout.create');

    Route::post('mindflex_planner/movement/gym_workout/save', 'Admin\MindFlexPlanner\Movement\GymWorkoutController@save')->name('movement_gym_workout.save');

    Route::get('/mindflex_planner/movement/gym_workout/edit/{id}','Admin\MindFlexPlanner\Movement\GymWorkoutController@edit')->name('movement_gym_workout.edit');

    Route::post('/mindflex_planner/movement/gym_workout/update/{id}','Admin\MindFlexPlanner\Movement\GymWorkoutController@update')->name('movement_gym_workout.update');

    Route::delete('/mindflex_planner/movement/gym_workout/delete/{id}','Admin\MindFlexPlanner\Movement\GymWorkoutController@delete')->name('movement_gym_workout.delete');

    Route::get('/mindflex_planner/movement/gym_workout/{gymWorkout_id}/sub_gym_workout','Admin\MindFlexPlanner\Movement\GymWorkoutController@subGymWorkoutList')->name('movement_sub_gym_workout.index');

    Route::get('mindflex_planner/movement/gym_workout/{gymWorkout_id}/sub_gym_workout/create', 'Admin\MindFlexPlanner\Movement\GymWorkoutController@createSubGymWorkout')->name('movement_sub_gym_workout.create');

    Route::post('mindflex_planner/movement/gym_workout/{gymWorkout_id}/sub_gym_workout/save', 'Admin\MindFlexPlanner\Movement\GymWorkoutController@saveSubGymWorkout')->name('movement_sub_gym_workout.save');

    Route::get('mindflex_planner/movement/gym_workout/{gymWorkout_id}/sub_gym_workout/edit/{id}','Admin\MindFlexPlanner\Movement\GymWorkoutController@editSubGymWorkout')->name('movement_sub_gym_workout.edit');

    Route::post('mindflex_planner/movement/gym_workout/{gymWorkout_id}/sub_gym_workout/update/{id}','Admin\MindFlexPlanner\Movement\GymWorkoutController@updateSubGymWorkout')->name('movement_sub_gym_workout.update');
      
    Route::delete('mindflex_planner/movement/gym_workout/{gymWorkout_id}/sub_gym_workout/delete/{id}','Admin\MindFlexPlanner\Movement\GymWorkoutController@deleteSubGymWorkout')->name('movement_sub_gym_workout.delete');
    
    Route::get('/mindflex_planner/movement/gym_workout/{gymWorkout_id}/sub_gym_workout/detailView/{id}','Admin\MindFlexPlanner\Movement\GymWorkoutController@detailView')->name('movement_sub_gym_workout.detailView'); 
  /*=============MindFlex Planner  Movement/Gymworkout module end=================*/

    /*=============MindFlex Planner  Movement/HIT module start=================*/
        Route::get('/mindflex_planner/movement/hit','Admin\MindFlexPlanner\Movement\HitController@index')->name('movement_hit.index'); 

        Route::get('mindflex_planner/movement/hit/create', 'Admin\MindFlexPlanner\Movement\HitController@create')->name('movement_hit.create');

        Route::post('mindflex_planner/movement/hit/save', 'Admin\MindFlexPlanner\Movement\HitController@save')->name('movement_hit.save');

        Route::get('/mindflex_planner/movement/hit/edit/{id}','Admin\MindFlexPlanner\Movement\HitController@edit')->name('movement_hit.edit');

        Route::post('/mindflex_planner/movement/hit/update/{id}','Admin\MindFlexPlanner\Movement\HitController@update')->name('movement_hit.update');

        Route::delete('/mindflex_planner/movement/hit/delete/{id}','Admin\MindFlexPlanner\Movement\HitController@delete')->name('movement_hit.delete');

        Route::get('/mindflex_planner/movement/hit/detailView/{id}','Admin\MindFlexPlanner\Movement\HitController@detailView')->name('movement_hit.detailView'); 
    /*=============MindFlex Planner  Movement/HIT module end===================*/

    /*=============MindFlex Planner  Movement/Free Weight module start=================*/
        Route::get('/mindflex_planner/movement/free_weight','Admin\MindFlexPlanner\Movement\FreeWeightController@index')->name('movement_free_weight.index'); 

        Route::get('mindflex_planner/movement/free_weight/create', 'Admin\MindFlexPlanner\Movement\FreeWeightController@create')->name('movement_free_weight.create');

        Route::post('mindflex_planner/movement/free_weight/save', 'Admin\MindFlexPlanner\Movement\FreeWeightController@save')->name('movement_free_weight.save');

        Route::get('/mindflex_planner/movement/free_weight/edit/{id}','Admin\MindFlexPlanner\Movement\FreeWeightController@edit')->name('movement_free_weight.edit');

        Route::post('/mindflex_planner/movement/free_weight/update/{id}','Admin\MindFlexPlanner\Movement\FreeWeightController@update')->name('movement_free_weight.update');

        Route::delete('/mindflex_planner/movement/free_weight/delete/{id}','Admin\MindFlexPlanner\Movement\FreeWeightController@delete')->name('movement_free_weight.delete');

        Route::get('/mindflex_planner/movement/free_weight/detailView/{id}','Admin\MindFlexPlanner\Movement\FreeWeightController@detailView')->name('movement_free_weight.detailView');
    /*=============MindFlex Planner  Movement/Free Weight module end===================*/

    /*=========MindFlex Planner Mindset/Daily Affirmation module start============*/
        Route::get('/mindflex_planner/mindset/daily_affirmation','Admin\MindFlexPlanner\MindSet\DailyAffirmationController@index')->name('daily_affirmation.index'); 

        Route::get('mindflex_planner/mindset/daily_affirmation/create', 'Admin\MindFlexPlanner\MindSet\DailyAffirmationController@create')->name('daily_affirmation.create');

        Route::post('mindflex_planner/mindset/daily_affirmation/save', 'Admin\MindFlexPlanner\MindSet\DailyAffirmationController@save')->name('daily_affirmation.save');

        Route::get('/mindflex_planner/mindset/daily_affirmation/edit/{id}','Admin\MindFlexPlanner\MindSet\DailyAffirmationController@edit')->name('daily_affirmation.edit');

        Route::post('/mindflex_planner/mindset/daily_affirmation/update/{id}','Admin\MindFlexPlanner\MindSet\DailyAffirmationController@update')->name('daily_affirmation.update');

        Route::delete('/mindflex_planner/mindset/daily_affirmation/delete/{id}','Admin\MindFlexPlanner\MindSet\DailyAffirmationController@delete')->name('daily_affirmation.delete');

        Route::get('/mindflex_planner/mindset/daily_affirmation/detailView/{id}','Admin\MindFlexPlanner\MindSet\DailyAffirmationController@detailView')->name('daily_affirmation.detailView');
    /*=======MindFlex Planner  Mindset/Daily Affirmation module end================*/

    /*=========MindFlex Planner Mindset/Question to coaches module start==========*/
        Route::get('/mindflex_planner/mindset/question_to_coaches','Admin\MindFlexPlanner\MindSet\CoachQuestionController@index')->name('question_coach.index'); 

        Route::get('mindflex_planner/mindset/question_to_coaches/create', 'Admin\MindFlexPlanner\MindSet\CoachQuestionController@create')->name('question_coach.create');

        Route::post('mindflex_planner/mindset/question_to_coaches/save', 'Admin\MindFlexPlanner\MindSet\CoachQuestionController@save')->name('question_coach.save');

        Route::get('/mindflex_planner/mindset/question_to_coaches/edit/{id}','Admin\MindFlexPlanner\MindSet\CoachQuestionController@edit')->name('question_coach.edit');

        Route::post('/mindflex_planner/mindset/question_to_coaches/update/{id}','Admin\MindFlexPlanner\MindSet\CoachQuestionController@update')->name('question_coach.update');

        Route::delete('/mindflex_planner/mindset/question_to_coaches/delete/{id}','Admin\MindFlexPlanner\MindSet\CoachQuestionController@delete')->name('question_coach.delete');
    /*=======MindFlex Planner  Mindset/Question to coaches module end==============*/

    /*=========MindFlex Planner Mindset Detail module start===================*/
        Route::get('/mindflex_planner/mindset/mindset_detail','Admin\MindFlexPlanner\MindSet\MindsetDetailController@index')->name('mindset_detail.index');

        /*Route::get('/mindflex_planner/mindset/mindset_detail_create','Admin\MindFlexPlanner\MindSet\MindsetDetailController@create')->name('mindset_detail.create');*/

        Route::post('mindflex_planner/mindset/mindset_detail/save', 'Admin\MindFlexPlanner\MindSet\MindsetDetailController@save')->name('mindset_detail.save');

        Route::get('/mindflex_planner/mindset/mindset_detail/show/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@show')->name('mindset_detail.show');

        Route::get('/mindflex_planner/mindset/mindset_detail/edit/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@edit')->name('mindset_detail.edit');

        Route::post('/mindflex_planner/mindset/mindset_detail/update/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@update')->name('mindset_detail.update');



        Route::get('/mindflex_planner/mindset/mindset_detail/file_list/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@fileList')->name('mindset_detail_file.list');

        Route::get('/mindflex_planner/mindset/mindset_detail_file/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@addFile')->name('mindset_detail_file.add');

        Route::post('mindflex_planner/mindset/mindset_detail/save_file/{id}', 'Admin\MindFlexPlanner\MindSet\MindsetDetailController@saveFile')->name('mindset_detail.save_file');

        Route::delete('mindflex_planner/mindset/mindset_detail_file/delete/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@deleteFile')->name('mindset_detail_file.delete');



        Route::get('/mindflex_planner/mindset/mindset_detail/lesson','Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonList')->name('mindset_detail_lesson.list');

        Route::get('/mindflex_planner/mindset/mindset_detail/lesson/create','Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonCreate')->name('mindset_detail_lesson.create');

        Route::post('mindflex_planner/mindset/mindset_detail/lesson/save', 'Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonSave')->name('mindset_detail_lesson.save');


        Route::get('/mindflex_planner/mindset/mindset_detail/lesson/edit/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@leassonEdit')->name('mindset_detail_lesson.edit');

        Route::post('/mindflex_planner/mindset/mindset_detail/lesson/update/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonUpdate')->name('mindset_detail_lesson.update');

        Route::delete('mindflex_planner/mindset/mindset_detail/lesson/delete/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonDelete')->name('mindset_detail_lesson.delete');

        Route::get('/mindflex_planner/mindset/mindset_detail/lesson_file/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonFileList')->name('mindset_detail_lesson_file.list');

        Route::get('/mindflex_planner/mindset/mindset_detail/lesson_file/{id}/create','Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonFileCreate')->name('mindset_detail_lesson_file.create');

        Route::post('mindflex_planner/mindset/mindset_detail/lesson_file/{id}/save', 'Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonFileSave')->name('mindset_detail_lesson_file.save');

        Route::delete('mindflex_planner/mindset/mindset_detail/lesson_file/delete/{id}','Admin\MindFlexPlanner\MindSet\MindsetDetailController@lessonDeletefile')->name('mindset_detail_lesson_file.delete');

    /*=========MindFlex Planner Mindset Detail module end===================*/
    
    /*=========MindFlex Planner Mindset/Assignment module start============*/
        Route::get('/mindflex_planner/mindset/assignment','Admin\MindFlexPlanner\MindSet\AssignmentController@index')->name('assignment.index'); 
        Route::get('mindflex_planner/mindset/assignment/create', 'Admin\MindFlexPlanner\MindSet\AssignmentController@create')->name('assignment.create');

        Route::post('mindflex_planner/mindset/assignment/save', 'Admin\MindFlexPlanner\MindSet\AssignmentController@save')->name('assignment.save');

        Route::get('/mindflex_planner/mindset/assignment/edit/{id}','Admin\MindFlexPlanner\MindSet\AssignmentController@edit')->name('assignment.edit');

        Route::post('/mindflex_planner/mindset/assignment/update/{id}','Admin\MindFlexPlanner\MindSet\AssignmentController@update')->name('assignment.update');

        Route::delete('/mindflex_planner/mindset/assignment/delete/{id}','Admin\MindFlexPlanner\MindSet\AssignmentController@delete')->name('assignment.delete');

        Route::get('/mindflex_planner/mindset/assignment/show/{id}','Admin\MindFlexPlanner\MindSet\AssignmentController@show')->name('assignment.detailView');





        //Ajax Methods

        Route::get('/mindflex_planner/mindset/home_workout_list','Admin\MindFlexPlanner\MindSet\AssignmentController@homeWorkoutList'); 

        Route::get('/mindflex_planner/mindset/gym_workout_list','Admin\MindFlexPlanner\MindSet\AssignmentController@gymWorkoutList'); 

        Route::get('/mindflex_planner/mindset/home_workout_exercise/','Admin\MindFlexPlanner\MindSet\AssignmentController@homeWorkoutExercise'); 

        Route::get('/mindflex_planner/mindset/gym_workout_exercise/','Admin\MindFlexPlanner\MindSet\AssignmentController@gymWorkoutExercise'); 


    /*=======MindFlex Planner  Mindset/Assignment module end================*/

    /*=========MindFlex Planner Nutrition/Food Log module start==========*/
        Route::get('/mindflex_planner/nutrition/food_log','Admin\MindFlexPlanner\Nutrition\FoodLogController@index')->name('food_log.index'); 

        Route::get('mindflex_planner/nutrition/food_log/create', 'Admin\MindFlexPlanner\Nutrition\FoodLogController@create')->name('food_log.create');

        Route::post('mindflex_planner/nutrition/food_log/save', 'Admin\MindFlexPlanner\Nutrition\FoodLogController@save')->name('food_log.save');

        Route::get('/mindflex_planner/nutrition/food_log/edit/{id}','Admin\MindFlexPlanner\Nutrition\FoodLogController@edit')->name('food_log.edit');

        Route::post('/mindflex_planner/nutrition/food_log/update/{id}','Admin\MindFlexPlanner\Nutrition\FoodLogController@update')->name('food_log.update');

        Route::delete('/mindflex_planner/nutrition/food_log/delete/{id}','Admin\MindFlexPlanner\Nutrition\FoodLogController@delete')->name('food_log.delete');
    /*=======MindFlex Planner  Nutrition/Food Log module end==============*/

    /*=========MindFlex Planner Nutrition/Suggested Meal module start==========*/
        Route::get('/mindflex_planner/nutrition/suggested_meal','Admin\MindFlexPlanner\Nutrition\SuggestedMealController@index')->name('suggested_meal.index'); 

        Route::get('mindflex_planner/nutrition/suggested_meal/create', 'Admin\MindFlexPlanner\Nutrition\SuggestedMealController@create')->name('suggested_meal.create');

        Route::post('mindflex_planner/nutrition/suggested_meal/save', 'Admin\MindFlexPlanner\Nutrition\SuggestedMealController@save')->name('suggested_meal.save');

        Route::get('/mindflex_planner/nutrition/suggested_meal/edit/{id}','Admin\MindFlexPlanner\Nutrition\SuggestedMealController@edit')->name('suggested_meal.edit');

        Route::post('/mindflex_planner/nutrition/suggested_meal/update/{id}','Admin\MindFlexPlanner\Nutrition\SuggestedMealController@update')->name('suggested_meal.update');

        Route::delete('/mindflex_planner/nutrition/suggested_meal/delete/{id}','Admin\MindFlexPlanner\Nutrition\SuggestedMealController@delete')->name('suggested_meal.delete');
    /*=======MindFlex Planner  Nutrition/Suggested Meal module end==============*/

    /*=========MindFlex Planner Nutrition/Tips module start==========*/
        Route::get('/mindflex_planner/nutrition/tip','Admin\MindFlexPlanner\Nutrition\TipController@index')->name('tip.index'); 

        Route::get('mindflex_planner/nutrition/tip/create', 'Admin\MindFlexPlanner\Nutrition\TipController@create')->name('tip.create');

        Route::post('mindflex_planner/nutrition/tip/save', 'Admin\MindFlexPlanner\Nutrition\TipController@save')->name('tip.save');

        Route::get('/mindflex_planner/nutrition/tip/edit/{id}','Admin\MindFlexPlanner\Nutrition\TipController@edit')->name('tip.edit');

        Route::post('/mindflex_planner/nutrition/tip/update/{id}','Admin\MindFlexPlanner\Nutrition\TipController@update')->name('tip.update');

        Route::delete('/mindflex_planner/nutrition/tip/delete/{id}','Admin\MindFlexPlanner\Nutrition\TipController@delete')->name('tip.delete');

        Route::get('/mindflex_planner/nutrition/tip/show/{id}','Admin\MindFlexPlanner\Nutrition\TipController@show')->name('tip.show');
    /*=======MindFlex Planner  Nutrition/Tips Log module end==============*/

    /*========= Start MindFlex Planner Nutrition/FAQ module start==========*/
        Route::get('/mindflex_planner/nutrition/faq','Admin\MindFlexPlanner\Nutrition\FaqController@index')->name('faq.index'); 
        Route::get('/mindflex_planner/nutrition/faq/list','Admin\MindFlexPlanner\Nutrition\FaqController@ListFaq')->name('faq.list'); 

        Route::get('mindflex_planner/nutrition/faq/create', 'Admin\MindFlexPlanner\Nutrition\FaqController@create')->name('faq.create');

        Route::post('mindflex_planner/nutrition/faq/save', 'Admin\MindFlexPlanner\Nutrition\FaqController@save')->name('faq.save');

        Route::get('/mindflex_planner/nutrition/faq/edit/{id}','Admin\MindFlexPlanner\Nutrition\FaqController@edit')->name('faq.edit');

        Route::post('/mindflex_planner/nutrition/faq/update/{id}','Admin\MindFlexPlanner\Nutrition\FaqController@update')->name('faq.update');

        Route::delete('/mindflex_planner/nutrition/faq/delete/{id}','Admin\MindFlexPlanner\Nutrition\FaqController@delete')->name('faq.delete');

        Route::get('/mindflex_planner/nutrition/faq/show/{id}','Admin\MindFlexPlanner\Nutrition\FaqController@show')->name('faq.show');
    /*======= End MindFlex Planner  Nutrition/FAQ module end==============*/

    /*=======MindFlex Planner Supplimentation/Reoccurring Reminder module start===========*/
        Route::get('/mindflex_planner/supplementation/reoccur_reminder','Admin\MindFlexPlanner\Supplimentation\ReoccurReminderController@index')->name('reoccur_reminder.index'); 

        Route::get('mindflex_planner/supplementation/reoccur_reminder/create', 'Admin\MindFlexPlanner\Supplimentation\ReoccurReminderController@create')->name('reoccur_reminder.create');

        Route::post('mindflex_planner/supplementation/reoccur_reminder/save', 'Admin\MindFlexPlanner\Supplimentation\ReoccurReminderController@save')->name('reoccur_reminder.save');

        Route::get('/mindflex_planner/supplementation/reoccur_reminder/edit/{id}','Admin\MindFlexPlanner\Supplimentation\ReoccurReminderController@edit')->name('reoccur_reminder.edit');

        Route::post('/mindflex_planner/supplementation/reoccur_reminder/update/{id}','Admin\MindFlexPlanner\Supplimentation\ReoccurReminderController@update')->name('reoccur_reminder.update');

        Route::delete('/mindflex_planner/supplementation/reoccur_reminder/delete/{id}','Admin\MindFlexPlanner\Supplimentation\ReoccurReminderController@delete')->name('reoccur_reminder.delete');
    /*=======MindFlex Planner Supplimentation/Reoccurring Reminder module ends============*/
    
    /*=======MindFlex Planner  Supplimentation/Guide module Start============*/
        Route::get('/mindflex_planner/supplementation/guide','Admin\MindFlexPlanner\Supplimentation\GuideController@index')->name('guide.index'); 

        Route::get('mindflex_planner/supplementation/guide/create', 'Admin\MindFlexPlanner\Supplimentation\GuideController@create')->name('guide.create');

        Route::post('mindflex_planner/supplementation/guide/save', 'Admin\MindFlexPlanner\Supplimentation\GuideController@save')->name('guide.save');

        Route::get('/mindflex_planner/supplementation/guide/edit/{id}','Admin\MindFlexPlanner\Supplimentation\GuideController@edit')->name('guide.edit');

        Route::post('/mindflex_planner/supplementation/guide/update/{id}','Admin\MindFlexPlanner\Supplimentation\GuideController@update')->name('guide.update');

        Route::delete('/mindflex_planner/supplementation/guide/delete/{id}','Admin\MindFlexPlanner\Supplimentation\GuideController@delete')->name('guide.delete');

        Route::get('/mindflex_planner/supplementation/guide/show/{id}','Admin\MindFlexPlanner\Supplimentation\GuideController@show')->name('guide.show');
    /*=======MindFlex Planner  Supplimenttation/Guide module end==============*/

    /*=======MindFlex Planner  Supplimentation/suppliment module Start============*/
        Route::get('/mindflex_planner/supplementation/supplement','Admin\MindFlexPlanner\Supplimentation\SupplimentController@index')->name('suppliment.index'); 

        Route::get('mindflex_planner/supplementation/supplement/create', 'Admin\MindFlexPlanner\Supplimentation\SupplimentController@create')->name('suppliment.create');

        Route::post('mindflex_planner/supplementation/supplement/save', 'Admin\MindFlexPlanner\Supplimentation\SupplimentController@save')->name('suppliment.save');

        Route::get('/mindflex_planner/supplementation/supplement/edit/{id}','Admin\MindFlexPlanner\Supplimentation\SupplimentController@edit')->name('suppliment.edit');

        Route::post('/mindflex_planner/supplementation/supplement/update/{id}','Admin\MindFlexPlanner\Supplimentation\SupplimentController@update')->name('suppliment.update');

        Route::delete('/mindflex_planner/supplementation/supplement/delete/{id}','Admin\MindFlexPlanner\Supplimentation\SupplimentController@delete')->name('suppliment.delete');
    /*=======MindFlex Planner  Supplimenttation/suppliment module end==============*/

    /*=======Start MindFlex Planner Supplimentation /Store Module*/

        Route::get('/mindflex_planner/supplementation/store','Admin\MindFlexPlanner\Supplimentation\StoreController@index')->name('store.index'); 

        Route::get('mindflex_planner/supplementation/store/create', 'Admin\MindFlexPlanner\Supplimentation\StoreController@create')->name('store.create');

        Route::post('mindflex_planner/supplementation/store/save', 'Admin\MindFlexPlanner\Supplimentation\StoreController@save')->name('store.save');

        Route::get('/mindflex_planner/supplementation/store/edit/{id}','Admin\MindFlexPlanner\Supplimentation\StoreController@edit')->name('store.edit');

        Route::post('/mindflex_planner/supplementation/store/update/{id}','Admin\MindFlexPlanner\Supplimentation\StoreController@update')->name('store.update');

        Route::get('/mindflex_planner/supplementation/store/detailView/{id}','Admin\MindFlexPlanner\Supplimentation\StoreController@detailView')->name('store.detailView');

        Route::delete('/mindflex_planner/supplementation/store/delete/{id}','Admin\MindFlexPlanner\Supplimentation\StoreController@delete')->name('store.delete');

    /*=======End MindFlex Planner Supplimentation /Store Module*/

  
    /*=======MindFlex Planner  Progress Photos module start==============*/
        Route::get('/mindflex_planner/progress_photos','Admin\MindFlexPlanner\ProgressPhotoController@index')->name('progress_photo.index'); 

        Route::get('mindflex_planner/progress_photos/create', 'Admin\MindFlexPlanner\ProgressPhotoController@create')->name('progress_photo.create');

        Route::post('mindflex_planner/progress_photos/save', 'Admin\MindFlexPlanner\ProgressPhotoController@save')->name('progress_photo.save');
        Route::delete('/mindflex_planner/progress_photos/delete/{id}','Admin\MindFlexPlanner\ProgressPhotoController@delete')->name('progress_photo.delete');
    /*=======MindFlex Planner Progress Photos module end==============*/

    /*=======MindFlex Planner MindFlex RX module start==============*/
        Route::get('/mindflex_planner/mindflex_rx','Admin\MindFlexPlanner\MindFlexRxController@index')->name('mindflex_rx.index'); 

        Route::get('mindflex_planner/mindflex_rx/create', 'Admin\MindFlexPlanner\MindFlexRxController@create')->name('mindflex_rx.create');

        Route::post('mindflex_planner/mindflex_rx/save', 'Admin\MindFlexPlanner\MindFlexRxController@save')->name('mindflex_rx.save');

         Route::get('/mindflex_planner/mindflex_rx/edit/{id}','Admin\MindFlexPlanner\MindFlexRxController@edit')->name('mindflex_rx.edit');

        Route::post('/mindflex_planner/mindflex_rx/update/{id}','Admin\MindFlexPlanner\MindFlexRxController@update')->name('mindflex_rx.update');

        Route::get('/mindflex_planner/mindflex_rx/detailView/{id}','Admin\MindFlexPlanner\MindFlexRxController@show')->name('mindflex_rx.show'); 

        Route::delete('/mindflex_planner/mindflex_rx/delete/{id}','Admin\MindFlexPlanner\MindFlexRxController@delete')->name('mindflex_rx.delete');
       
        Route::get('/mindflex_planner/mindflex_rx/ajax_customer','Admin\MindFlexPlanner\MindFlexRxController@ajaxCustomer')->name('mindflex_rx.ajax_customer');

    /*=======MindFlex Planner MindFlex RX module end==============*/

   /*Resources Article Module start*/
  Route::get('/articles','Admin\ArticlesResourcesController@index')->name('article.index');
   Route::get('article/create', 'Admin\ArticlesResourcesController@create')->name('article.create');
    Route::post('article/save', 'Admin\ArticlesResourcesController@save')->name('article.save'); 
   Route::get('/article/edit/{id}','Admin\ArticlesResourcesController@edit')->name('article.edit'); 
   Route::post('/article/update/{id}','Admin\ArticlesResourcesController@update')->name('article.update');
   Route::get('/article/detailView/{id}','Admin\ArticlesResourcesController@detailView')->name('article.detailView'); 
   
    Route::get('/article/delete/{id}','Admin\ArticlesResourcesController@delete')->name('article.delete');
 /*Resources Article Module end*/

 /* Resources Book module start*/

  Route::get('/books','Admin\BooksResourcesController@index')->name('book.index');
   Route::get('book/create', 'Admin\BooksResourcesController@create')->name('book.create');
    Route::post('book/save', 'Admin\BooksResourcesController@save')->name('book.save'); 
   Route::get('/book/edit/{id}','Admin\BooksResourcesController@edit')->name('book.edit'); 
   Route::post('/book/update/{id}','Admin\BooksResourcesController@update')->name('book.update');
   Route::get('/book/detailView/{id}','Admin\BooksResourcesController@detailView')->name('book.detailView'); 
   
    Route::delete('/book/delete/{id}','Admin\BooksResourcesController@delete')->name('book.delete');

/* Resources Book module end*/

/*Admin profile module start*/
 Route::get('/admin/editProfile/{id}','HomeController@editProfile')->name('admin.profile');
 Route::post('/admin/update/{id}','HomeController@updateProfile')->name('admin.update');
 Route::get('/admin/changePassword/{id}','HomeController@changePassword')->name('admin.changePassword');
 Route::post('/admin/updatePaasword','HomeController@updatePassword')->name('admin.updatePassword');
/*Admin profile module end*/

/* Payment Module Start */
Route::get('/stripe','Admin\PaymentController@index')->name('stripe.index');
Route::get('/stripe/create','Admin\PaymentController@create')->name('stripe.create');
Route::post('/stripe/save','Admin\PaymentController@save')->name('stripe.save');
Route::delete('/stripe/delete/{id}','Admin\PaymentController@delete')->name('stripe.delete');
/* Payment Module End */

/* Customers module start*/
  Route::get('/customers','Admin\CustomersController@index')->name('customers.index');
  Route::get('/customers/create','Admin\CustomersController@create')->name('customers.create');
  Route::post('/customers/save','Admin\CustomersController@save')->name('customers.save');
  Route::get('/customers/edit/{id}','Admin\CustomersController@edit')->name('customers.edit');
  Route::get('/customers/detailView/{id}','Admin\CustomersController@detailView')->name('customers.detailView');
  Route::get('/customers/detailView/{id}','Admin\CustomersController@detailView')->name('customers.detailView');
  Route::get('/customers/edit/{id}','Admin\CustomersController@edit')->name('customers.edit'); 
   Route::post('/customers/update/{id}','Admin\CustomersController@update')->name('customers.update');

    Route::delete('/customers/delete/{id}','Admin\CustomersController@delete')->name('customers.delete');

    Route::get('/customers/body_measurement_log','Admin\CustomersController@bodyMeasurementLog')->name('customers.body_measurement_log');

    Route::get('/customers/custom_workout','Admin\CustomersController@customWorkout')->name('customers.custom_workout');

    Route::get('customer/notifications', 'Admin\CustomerNotification@index')->name('cus_notify.index');
    Route::get('customer/notification/create', 'Admin\CustomerNotification@create')->name('cus_notify.create');
    Route::post('/customers/notification/save','Admin\CustomerNotification@save')->name('cus_notify.save');
    Route::get('/customers/notification/show/{id}','Admin\CustomerNotification@show')->name('cus_notify.show');
   
    Route::delete('/customers/notification/delete/{id}','Admin\CustomerNotification@delete')->name('cus_notify.delete');
    
    Route::get('/customers/search','Admin\CustomersController@searchData')->name('customers.search');
    Route::get('/customers/allNum','Admin\CustomersController@allCustomerNum')->name('customers.allNum');
    Route::get('/customers/allCustomers','Admin\CustomersController@getAllCustomer')->name('customers.allCustomers');

    /* Customers module end*/
     
    /*======================== Customers Food Log module Start=======================*/
      Route::get('/customers/food_log','Admin\CustomersFoodLogController@index')->name('cust_food_log.index');

      Route::get('/customers/food_log/create','Admin\CustomersFoodLogController@create')->name('cust_food_log.create');
      Route::post('/customers/food_log/save','Admin\CustomersFoodLogController@save')->name('cust_food_log.save');
   
       Route::get('/customers/food_log/edit/{id}','Admin\CustomersFoodLogController@edit')->name('cust_food_log.edit');
       Route::post('/customers/food_log/update/{id}','Admin\CustomersFoodLogController@update')->name('cust_food_log.update');
      Route::delete('/customers/food_log/delete/{id}','Admin\CustomersFoodLogController@delete')->name('cust_food_log.delete');

    /*======================== Customers Food Log module end=======================*/

    /*============== Customers Workout Reminder module Start=====================*/
      Route::get('/customers/reminder','Admin\CustomersReminderController@index')->name('cust_reminder.index');

      Route::get('/customers/reminder/create','Admin\CustomersReminderController@create')->name('cust_reminder.create');
      Route::post('/customers/reminder/save','Admin\CustomersReminderController@save')->name('cust_reminder.save');
   
       Route::get('/customers/reminder/edit/{id}','Admin\CustomersReminderController@edit')->name('cust_reminder.edit');
       Route::post('/customers/reminder/update/{id}','Admin\CustomersReminderController@update')->name('cust_reminder.update');
      Route::delete('/customers/reminder/delete/{id}','Admin\CustomersReminderController@delete')->name('cust_reminder.delete');

    /*============== Customers Workout Reminder module end=======================*/

/* Coach module start*/
  Route::get('/coach','Admin\CoachesController@index')->name('coach.index');
  Route::get('/coach/create','Admin\CoachesController@create')->name('coach.create');
 Route::post('/coach/save','Admin\CoachesController@save')->name('coach.save');
  Route::get('/coach/edit/{id}','Admin\CoachesController@edit')->name('coach.edit');
  Route::get('/coach/detailView/{id}','Admin\CoachesController@detailView')->name('coach.detailView');
 Route::post('/coach/update/{id}','Admin\CoachesController@update')->name('coach.update');
Route::delete('/coach/delete/{id}','Admin\CoachesController@delete')->name('coach.delete');

Route::get('coach/email_ajax_customer','Admin\CoachesController@emailAjaxCoach');

/* Coach assignment to customer module start*/
  Route::get('/coach/customer_assign/assign/{id}','Admin\CoachesController@CoachWiseList')->name('coach_wise_customer_assign.index');

  Route::get('/coach/customer_assign','Admin\CoachesController@CoachList')->name('customer_assign.index');

  


  Route::get('/coach/customer_assign/create','Admin\CoachesController@AssignCustomerCreate')->name('customer_assign.create');

 Route::post('/coach/customer_assign/save','Admin\CoachesController@AssignCustomerSave')->name('customer_assign.save');

  Route::get('/coach/customer_assign/edit/{id}','Admin\CoachesController@AssignCustomerEdit')->name('customer_assign.edit');


  Route::get('/coach/customers_assign/{id}','Admin\CoachesController@AssignAllCustomerList')->name('customer_assign.allDetailView');

 Route::post('/coach/customer_assign/update/{id}','Admin\CoachesController@AssignCustomerUpdate')->name('customer_assign.update');

Route::delete('/coach/customer_assign/delete/{id}','Admin\CoachesController@AssignCustomerDelete')->name('customer_assign.delete');
/* Coach assignment to customer module end*/


    /* Start Coach Meals module */
      Route::get('/coach/meals','Admin\MealsController@index')->name('meals.index');

      Route::get('/coach/meals/create','Admin\MealsController@create')->name('meals.create');

      Route::post('/coach/meals/save','Admin\MealsController@save')->name('meals.save');

      Route::get('/coach/meals/edit/{id}','Admin\MealsController@edit')->name('meals.edit');

      Route::get('/coach/meals/detailView/{id}','Admin\MealsController@detailView')->name('meals.detailView');

     Route::post('/coach/meals/update/{id}','Admin\MealsController@update')->name('meals.update');
     
    Route::delete('/coach/meals/delete/{id}','Admin\MealsController@delete')->name('meals.delete');

    /* End Coach Meals module start*/


    /* Start Coach suggest Meals to customer module */
      Route::get('/coach/suggest_meals','Admin\MealsController@SuggestedMealList')->name('suggest_meals.index');

      Route::get('/coach/suggest_meals/create','Admin\MealsController@SuggestedMealCreate')->name('suggest_meals.create');

      Route::post('/coach/suggest_meals/save','Admin\MealsController@SuggestedMealSave')->name('suggest_meals.save');

      Route::get('/coach/suggest_meals/edit/{id}','Admin\MealsController@SuggestedMealEdit')->name('suggest_meals.edit');

      Route::get('/coach/suggest_meals/detailView/{id}','Admin\MealsController@SuggestedMealDetailView')->name('suggest_meals.detailView');

     Route::post('/coach/suggest_meals/update/{id}','Admin\MealsController@SuggestedMealUpdate')->name('suggest_meals.update');
     
    Route::delete('/coach/suggest_meals/delete/{id}','Admin\MealsController@SuggestedMealDelete')->name('suggest_meals.delete');

    Route::get('/coach/suggest_meals/getMeal/{id}','Admin\MealsController@GetMealList')->name('suggest_meals.getMealList');

    /* End Coach suggest Meals to customer*/

     /*Coach testimonial start*/
     Route::get('/coach/testimonial','Admin\CoachesController@TestimonialList')->name('coachTestimonial.index');

     Route::get('/coach/testimonial/create','Admin\CoachesController@TestimonialCreate')->name('coachTestimonial.create');

     Route::post('/coach/testimonial/save','Admin\CoachesController@TestimonialSave')->name('coachTestimonial.save');

     Route::get('/coach/testimonial/edit/{id}','Admin\CoachesController@TestimonialEdit')->name('coachTestimonial.edit');

     Route::post('/coach/testimonial/update/{id}','Admin\CoachesController@TestimonialUpdate')->name('coachTestimonial.update');

     Route::get('/coach/testimonial/detailView/{id}','Admin\CoachesController@TestimonialDetailView')->name('coachTestimonial.detailView');

    

      Route::delete('/coach/testimonial/delete/{id}','Admin\CoachesController@TestimonialDelete')->name('coachTestimonial.delete');
/* End Coach module end*/


/* Shake&Bar module start*/
  Route::get('/shakeBar','Admin\ShakeAndBarsController@index')->name('shakeBar.index');

  Route::get('/shakeBar/create','Admin\ShakeAndBarsController@create')->name('shakeBar.create');

  Route::post('/shakeBar/save','Admin\ShakeAndBarsController@save')->name('shakeBar.save');

  Route::get('/shakeBar/edit/{id}','Admin\ShakeAndBarsController@edit')->name('shakeBar.edit');

 Route::post('/shakeBar/update/{id}','Admin\ShakeAndBarsController@update')->name('shakeBar.update');

  Route::get('/shakeBar/detailView/{id}','Admin\ShakeAndBarsController@detailView')->name('shakeBar.detailView');
 

Route::delete('/shakeBar/delete/{id}','Admin\ShakeAndBarsController@delete')->name('shakeBar.delete');
/* End Shake&Bar module */

/* Shake&Bar Order module start*/
  Route::get('/shakeBar/order','Admin\ShakeAndBarsController@ListOrder')->name('order.index');

  Route::get('/shakeBar/order/create','Admin\ShakeAndBarsController@OrderCreate')->name('order.create');

  Route::post('/shakeBar/order/save','Admin\ShakeAndBarsController@OrderSave')->name('order.save');

  Route::get('/shakeBar/order/edit/{id}','Admin\ShakeAndBarsController@OrderEdit')->name('order.edit');

 Route::post('/shakeBar/order/update/{id}','Admin\ShakeAndBarsController@OrderUpdate')->name('order.update');

  Route::get('/shakeBar/order/detailView/{id}','Admin\ShakeAndBarsController@OrderDetailView')->name('order.detailView');
 
 Route::delete('/shakeBar/order/delete/{id}','Admin\ShakeAndBarsController@OrderDelete')->name('order.delete');
/* End Shake&Bar Order module */

/* Start ShakeBar Ingedients*/
Route::get('/shakeBar/ingredients','Admin\ShakeAndBarsController@ListIngredients')->name('shakeBarIngredients.index');

 Route::get('/shakeBar/ingredients/create','Admin\ShakeAndBarsController@IngredientsCreate')->name('shakeBarIngredients.create');

 Route::post('/shakeBar/ingredientscreate/save','Admin\ShakeAndBarsController@IngredientsSave')->name('shakeBarIngredients.save');

 Route::get('/shakeBar/ingredientscreate/edit/{id}','Admin\ShakeAndBarsController@IngredientsEdit')->name('shakeBarIngredients.edit');


Route::post('/shakeBar/ingredientscreate/update/{id}','Admin\ShakeAndBarsController@IngredientsUpdate')->name('shakeBarIngredients.update');

Route::delete('/shakeBar/ingredientscreate/delete/{id}','Admin\ShakeAndBarsController@IngredientsDelete')->name('shakeBarIngredients.delete');

/*=================== EndShakeBar Ingedients===================*/

    /* ===================Start ShakeBar Coupons===================*/
        Route::get('/shakeBar/coupon','Admin\ShakeAndBarsController@ListCoupons')->name('coupon.index');

         Route::get('/shakeBar/coupon/create','Admin\ShakeAndBarsController@CouponCreate')->name('coupon.create');

         Route::post('/shakeBar/coupon/save','Admin\ShakeAndBarsController@CouponSave')->name('coupon.save');

         Route::get('/shakeBar/coupon/edit/{id}','Admin\ShakeAndBarsController@CouponEdit')->name('coupon.edit');


        Route::post('/shakeBar/coupon/update/{id}','Admin\ShakeAndBarsController@CouponUpdate')->name('coupon.update');

        Route::delete('/shakeBar/coupon/delete/{id}','Admin\ShakeAndBarsController@CouponDelete')->name('coupon.delete');

    /* ===================EndShakeBar Coupons===================*/

    /* ===================Badge module start===================*/
       Route::get('/badges','Admin\BadgesController@index')->name('badges.index');

       Route::get('/badges/create','Admin\BadgesController@create')->name('badges.create');

       Route::post('/badges/save','Admin\BadgesController@save')->name('badges.save');

       Route::get('/badges/edit/{id}','Admin\BadgesController@edit')->name('badges.edit');

       Route::post('/badges/update/{id}','Admin\BadgesController@update')->name('badges.update');

       Route::get('/badges/detailView/{id}','Admin\BadgesController@detailView')->name('badges.detailView');
     
       Route::delete('/badges/delete/{id}','Admin\BadgesController@delete')->name('badges.delete');

       Route::get('/customer/badges','Admin\BadgesController@customerAchievementList')->name('customer_badges.index');
       Route::get('/customer/badges/create','Admin\BadgesController@customerAchievement')->name('customer_badges.create');
       Route::post('/customer/badges/save','Admin\BadgesController@customerAchievementSave')->name('customer_badges.save');
       Route::get('/customer/badges/edit/{id}','Admin\BadgesController@customerAchievementEdit')->name('customer_badges.edit');
       Route::post('/customer/badges/update/{id}','Admin\BadgesController@customerAchievementUpdate')->name('customer_badges.update');
       Route::delete('/customer/badges/delete/{id}','Admin\BadgesController@customerAchievementDelete')->name('customer_badges.delete');


    /* ===================End Badge module=================== */

    /* ===================MindFlex Physio Doc start===================*/
        Route::get('/mindflex_physio/doc','Admin\MindFlexPhysio\DocController@index')->name('physio_doc.index');

       Route::get('/mindflex_physio/doc/create','Admin\MindFlexPhysio\DocController@create')->name('physio_doc.create');

       Route::post('/mindflex_physio/doc/save','Admin\MindFlexPhysio\DocController@save')->name('physio_doc.save');

       Route::get('/mindflex_physio/doc/edit/{id}','Admin\MindFlexPhysio\DocController@edit')->name('physio_doc.edit');

       Route::post('/mindflex_physio/doc/update/{id}','Admin\MindFlexPhysio\DocController@update')->name('physio_doc.update');

       Route::get('/mindflex_physio/doc/show/{id}','Admin\MindFlexPhysio\DocController@show')->name('physio_doc.show');
     
       Route::delete('/mindflex_physio/doc/delete/{id}','Admin\MindFlexPhysio\DocController@delete')->name('physio_doc.delete');
    /* ===================MindFlex Physio Doc end===================*/

    /* ===================MindFlex Physio exercise program start===================*/
        Route::get('/mindflex_physio/exercise_program','Admin\MindFlexPhysio\ExerciseController@index')->name('physio_exercise.index');

        Route::get('/mindflex_physio/exercise_program/create','Admin\MindFlexPhysio\ExerciseController@create')->name('physio_exercise.create');

        Route::post('/mindflex_physio/exercise_program/save','Admin\MindFlexPhysio\ExerciseController@save')->name('physio_exercise.save');

        Route::get('/mindflex_physio/exercise_program/edit/{id}','Admin\MindFlexPhysio\ExerciseController@edit')->name('physio_exercise.edit');

        Route::post('/mindflex_physio/exercise_program/update/{id}','Admin\MindFlexPhysio\ExerciseController@update')->name('physio_exercise.update');

        Route::get('/mindflex_physio/exercise_program/show/{id}','Admin\MindFlexPhysio\ExerciseController@show')->name('physio_exercise.show');
     
        Route::delete('/mindflex_physio/exercise_program/delete/{id}','Admin\MindFlexPhysio\ExerciseController@delete')->name('physio_exercise.delete');
    /* ===================MindFlex Physio exercise program end===================*/

     /* ===============MindFlex Physio Environment Factor start===============*/
       Route::get('/mindflex_physio/environment','Admin\MindFlexPhysio\EnvironmentController@index')->name('physio_environment.index');

        Route::get('/mindflex_physio/environment/create','Admin\MindFlexPhysio\EnvironmentController@create')->name('physio_environment.create');

        Route::post('/mindflex_physio/environment/save','Admin\MindFlexPhysio\EnvironmentController@save')->name('physio_environment.save');

        Route::get('/mindflex_physio/environment/edit/{id}','Admin\MindFlexPhysio\EnvironmentController@edit')->name('physio_environment.edit');

        Route::post('/mindflex_physio/environment/update/{id}','Admin\MindFlexPhysio\EnvironmentController@update')->name('physio_environment.update');

        Route::get('/mindflex_physio/environment/show/{id}','Admin\MindFlexPhysio\EnvironmentController@show')->name('physio_environment.show');
     
        Route::delete('/mindflex_physio/environment/delete/{id}','Admin\MindFlexPhysio\EnvironmentController@delete')->name('physio_environment.delete');
    /* ===============MindFlex Physio Environment Factor end===============*/

    /* ===================MindFlex Physio Article start===================*/
       Route::get('/mindflex_physio/articles','Admin\MindFlexPhysio\ArticlesPhysioController@index')->name('physio_article.index');

       Route::get('/mindflex_physio/articles/create','Admin\MindFlexPhysio\ArticlesPhysioController@create')->name('physio_article.create');

       Route::post('/mindflex_physio/articles/save','Admin\MindFlexPhysio\ArticlesPhysioController@save')->name('physio_article.save');

       Route::get('/mindflex_physio/articles/edit/{id}','Admin\MindFlexPhysio\ArticlesPhysioController@edit')->name('physio_article.edit');

       Route::post('/mindflex_physio/articles/update/{id}','Admin\MindFlexPhysio\ArticlesPhysioController@update')->name('physio_article.update');

       Route::get('/mindflex_physio/articles/detailView/{id}','Admin\MindFlexPhysio\ArticlesPhysioController@detailView')->name('physio_article.detailView');
     
       Route::delete('/mindflex_physio/articles/delete/{id}','Admin\MindFlexPhysio\ArticlesPhysioController@delete')->name('physio_article.delete');
    /* ===================End MindFlex Physio Article module ===================*/

    /* ==========MindFlex Physio  Profile  Medicat history start===================*/
       Route::get('/mindflex_physio/profile/medical_history','Admin\MindFlexPhysio\Profile\medicalHistoryController@index')->name('medical_history.index');

       Route::get('/mindflex_physio/profile/medical_history/create','Admin\MindFlexPhysio\Profile\medicalHistoryController@create')->name('medical_history.create');

       Route::post('/mindflex_physio/profile/medical_history/save','Admin\MindFlexPhysio\Profile\medicalHistoryController@save')->name('medical_history.save');

       Route::get('/mindflex_physio/profile/medical_history/edit/{id}','Admin\MindFlexPhysio\Profile\medicalHistoryController@edit')->name('medical_history.edit');

       Route::post('/mindflex_physio/profile/medical_history/update/{id}','Admin\MindFlexPhysio\Profile\medicalHistoryController@update')->name('medical_history.update');

       Route::delete('/mindflex_physio/profile/medical_history/delete/{id}','Admin\MindFlexPhysio\Profile\medicalHistoryController@delete')->name('medical_history.delete');
    /* ==========End MindFlex Physio Profile Medicat history module ===================*/


    /*========== MindFlex Physio  Profile  Daily Notes start===================*/
       Route::get('/mindflex_physio/profile/daily_notes','Admin\MindFlexPhysio\Profile\dailyNotesController@index')->name('daily_notes.index');

       Route::get('/mindflex_physio/profile/daily_notes/create','Admin\MindFlexPhysio\Profile\dailyNotesController@create')->name('daily_notes.create');

       Route::post('/mindflex_physio/profile/daily_notes/save','Admin\MindFlexPhysio\Profile\dailyNotesController@save')->name('daily_notes.save');

       Route::get('/mindflex_physio/profile/daily_notes/edit/{id}','Admin\MindFlexPhysio\Profile\dailyNotesController@edit')->name('daily_notes.edit');

       Route::post('/mindflex_physio/profile/daily_notes/update/{id}','Admin\MindFlexPhysio\Profile\dailyNotesController@update')->name('daily_notes.update');

       Route::delete('/mindflex_physio/profile/daily_notes/delete/{id}','Admin\MindFlexPhysio\Profile\dailyNotesController@delete')->name('daily_notes.delete');
    /*========== End MindFlex Physio Profile Daily Notes module ===================*/

    /*========== MindFlex physio Daily Affirmation Module Start ===================*/
        Route::get('/mindflex_physio/daily_affirmation','Admin\MindFlexPhysio\DailyAffirmationController@index')->name('physio_daily_affirmation.index');
        
        Route::get('mindflex_physio/daily_affirmation/create', 'Admin\MindFlexPhysio\DailyAffirmationController@create')->name('physio_daily_affirmation.create');

        Route::post('mindflex_physio/daily_affirmation/save', 'Admin\MindFlexPhysio\DailyAffirmationController@save')->name('physio_daily_affirmation.save');

        Route::get('/mindflex_physio/daily_affirmation/edit/{id}','Admin\MindFlexPhysio\DailyAffirmationController@edit')->name('physio_daily_affirmation.edit');

        Route::post('/mindflex_physio/daily_affirmation/update/{id}','Admin\MindFlexPhysio\DailyAffirmationController@update')->name('physio_daily_affirmation.update');

        Route::delete('/mindflex_physio/daily_affirmation/delete/{id}','Admin\MindFlexPhysio\DailyAffirmationController@delete')->name('physio_daily_affirmation.delete');

        Route::get('/mindflex_physio/daily_affirmation/show/{id}','Admin\MindFlexPhysio\DailyAffirmationController@show')->name('physio_daily_affirmation.show'); 
   /* ==========MindFlex Daily Affirmation  Module end ===================*/

    /*========== MindFlex physio Store Module Start ===================*/
        Route::get('/mindflex_physio/store','Admin\MindFlexPhysio\StoreController@index')->name('physio_store.index');
        
        Route::get('mindflex_physio/store/create', 'Admin\MindFlexPhysio\StoreController@create')->name('physio_store.create');

        Route::post('mindflex_physio/store/save', 'Admin\MindFlexPhysio\StoreController@save')->name('physio_store.save');

        Route::get('/mindflex_physio/store/edit/{id}','Admin\MindFlexPhysio\StoreController@edit')->name('physio_store.edit');

        Route::post('/mindflex_physio/store/update/{id}','Admin\MindFlexPhysio\StoreController@update')->name('physio_store.update');

        Route::delete('/mindflex_physio/store/delete/{id}','Admin\MindFlexPhysio\StoreController@delete')->name('physio_store.delete');

         Route::get('/mindflex_physio/store/detailView/{id}','Admin\MindFlexPhysio\StoreController@detailView')->name('physio_store.detailView');
   /*========== MindFlex Store  Module end ===================*/

   /*========= MindFlex physio Nutrition/Food Log module start ===================*/
        Route::get('/mindflex_physio/nutrition/food_log','Admin\MindFlexPhysio\Nutrition\FoodLogController@index')->name('physio_food_log.index'); 

        Route::get('mindflex_physio/nutrition/food_log/create', 'Admin\MindFlexPhysio\Nutrition\FoodLogController@create')->name('physio_food_log.create');

        Route::post('mindflex_physio/nutrition/food_log/save', 'Admin\MindFlexPhysio\Nutrition\FoodLogController@save')->name('physio_food_log.save');

        Route::get('/mindflex_physio/nutrition/food_log/edit/{id}','Admin\MindFlexPhysio\Nutrition\FoodLogController@edit')->name('physio_food_log.edit');

        Route::post('/mindflex_physio/nutrition/food_log/update/{id}','Admin\MindFlexPhysio\Nutrition\FoodLogController@update')->name('physio_food_log.update');

        Route::delete('/mindflex_physio/nutrition/food_log/delete/{id}','Admin\MindFlexPhysio\Nutrition\FoodLogController@delete')->name('physio_food_log.delete');

    /*========= MindFlex physio Nutrition/Food Log module end ===================*/

    /*========= MindFlex physio Nutrition/Recommendation module start =================*/
            Route::get('mindflex_physio/nutrition/recommendation','Admin\MindFlexPhysio\Nutrition\RecommendationController@index')->name('physio_recommend_meal.index'); 

            Route::get('mindflex_physio/nutrition/recommendation/create', 'Admin\MindFlexPhysio\Nutrition\RecommendationController@create')->name('physio_recommend_meal.create');

            Route::post('mindflex_physio/nutrition/recommendation/save', 'Admin\MindFlexPhysio\Nutrition\RecommendationController@save')->name('physio_recommend_meal.save');

            Route::get('/mindflex_physio/nutrition/recommendation/edit/{id}','Admin\MindFlexPhysio\Nutrition\RecommendationController@edit')->name('physio_recommend_meal.edit');

            Route::post('/mindflex_physio/nutrition/recommendation/update/{id}','Admin\MindFlexPhysio\Nutrition\RecommendationController@update')->name('physio_recommend_meal.update');

            Route::get('/mindflex_physio/nutrition/recommendation/detailView/{id}','Admin\MindFlexPhysio\Nutrition\RecommendationController@detailView')->name('physio_recommend_meal.detailView');

            Route::delete('/mindflex_physio/nutrition/recommendation/delete/{id}','Admin\MindFlexPhysio\Nutrition\RecommendationController@delete')->name('physio_recommend_meal.delete');
    /*========= MindFlex physio Nutrition/Recommendation module end ===================*/



    /*=================== Plan module start===================*/
       Route::get('/plans','Admin\PlansController@index')->name('plans.index');

       Route::get('/plans/create','Admin\PlansController@create')->name('plans.create');

       Route::post('/plans/save','Admin\PlansController@save')->name('plans.save');

       Route::get('/plans/edit/{id}','Admin\PlansController@edit')->name('plans.edit');

       Route::post('/plans/update/{id}','Admin\PlansController@update')->name('plans.update');

       Route::get('/plans/detailView/{id}','Admin\PlansController@detailView')->name('plans.detailView');
     
       Route::delete('/plans/delete/{id}','Admin\PlansController@delete')->name('plans.delete');
    /*=================== End Plan module ===================*/

    /*=================== ContactUS module start===================*/
       Route::get('/others/contact','Admin\OthersController@ContactList')->name('contact.index');

       Route::get('/others/contact/create','Admin\OthersController@ContactCreate')->name('contact.create');
      
      Route::post('/others/contact/save','Admin\OthersController@ContactSave')->name('contact.save');

      Route::delete('/others/contact/{id}','Admin\PlansController@ConactsDelete')->name('contact.delete');

      Route::get('/others/contact/reply/{id}','Admin\OthersController@ContactReply')->name('contact.reply');

      Route::post('/others/contact/sendReply','Admin\OthersController@ContactSendReply')->name('contact.sendReply');
      
    /*=================== End ContactUS module ===================*/

    /*=================== Social module start===================*/
       Route::get('/others/social','Admin\OthersController@SocialList')->name('social.index');

       Route::get('/others/social/edit/{id}','Admin\OthersController@SocialEdit')->name('social.edit');

       Route::post('/others/social/update/{id}','Admin\OthersController@SocialUpdate')->name('social.update');

        Route::delete('/others/social/delete/{id}','Admin\OthersController@SocialDelete')->name('social.delete');
      
    /*=================== End Social module ===================*/

    

    /*=============Better together schedule Express Module start==============================*/
        Route::get('/schedule_classes/express/','Admin\BTSchedule\ExpressController@index')->name('btSchedule_express.index');

        Route::get('schedule_classes/express/create', 'Admin\BTSchedule\ExpressController@create')->name('btSchedule_express.create');

        Route::post('schedule_classes/express/save', 'Admin\BTSchedule\ExpressController@save')->name('btSchedule_express.save');

        Route::get('schedule_classes/express/edit/{id}','Admin\BTSchedule\ExpressController@edit')->name('btSchedule_express.edit');

        Route::post('schedule_classes/express/update/{id}','Admin\BTSchedule\ExpressController@update')->name('btSchedule_express.update');
          
        Route::delete('schedule_classes/express/delete/{id}','Admin\BTSchedule\ExpressController@delete')->name('btSchedule_express.delete');
        
        Route::get('/schedule_classes/express/detailView/{id}','Admin\BTSchedule\ExpressController@show')->name('btSchedule_express.detailView');

        Route::get('/schedule_classes/express/ajax_validation/','Admin\BTSchedule\ExpressController@ajaxValidation');
    /*=============Better together schedule Express Module end==============================*/

    /*=============Better together schedule corner_stone Module start==============================*/
        Route::get('/schedule_classes/corner_stone/','Admin\BTSchedule\CornerStoneController@index')->name('btSchedule_corner_stone.index');

        Route::get('schedule_classes/corner_stone/create', 'Admin\BTSchedule\CornerStoneController@create')->name('btSchedule_corner_stone.create');

        Route::post('schedule_classes/corner_stone/save', 'Admin\BTSchedule\CornerStoneController@save')->name('btSchedule_corner_stone.save');

        Route::get('schedule_classes/corner_stone/edit/{id}','Admin\BTSchedule\CornerStoneController@edit')->name('btSchedule_corner_stone.edit');

        Route::post('schedule_classes/corner_stone/update/{id}','Admin\BTSchedule\CornerStoneController@update')->name('btSchedule_corner_stone.update');
          
        Route::delete('schedule_classes/corner_stone/delete/{id}','Admin\BTSchedule\CornerStoneController@delete')->name('btSchedule_corner_stone.delete');
        
        Route::get('/schedule_classes/corner_stone/detailView/{id}','Admin\BTSchedule\CornerStoneController@show')->name('btSchedule_corner_stone.detailView');

        Route::get('/schedule_classes/corner_stone/ajax_validation/','Admin\BTSchedule\CornerStoneController@ajaxValidation');
    /*=============Better together schedule corner_stone Module end==============================*/

    /*=============Better together schedule FlexFlowController Module start==============================*/
        Route::get('/schedule_classes/flex_flow/','Admin\BTSchedule\FlexFlowController@index')->name('btSchedule_flex_flow.index');

        Route::get('schedule_classes/flex_flow/create', 'Admin\BTSchedule\FlexFlowController@create')->name('btSchedule_flex_flow.create');

        Route::post('schedule_classes/flex_flow/save', 'Admin\BTSchedule\FlexFlowController@save')->name('btSchedule_flex_flow.save');

        Route::get('schedule_classes/flex_flow/edit/{id}','Admin\BTSchedule\FlexFlowController@edit')->name('btSchedule_flex_flow.edit');

        Route::post('schedule_classes/flex_flow/update/{id}','Admin\BTSchedule\FlexFlowController@update')->name('btSchedule_flex_flow.update');
          
        Route::delete('schedule_classes/flex_flow/delete/{id}','Admin\BTSchedule\FlexFlowController@delete')->name('btSchedule_flex_flow.delete');
        
        Route::get('/schedule_classes/flex_flow/detailView/{id}','Admin\BTSchedule\FlexFlowController@show')->name('btSchedule_flex_flow.detailView');

        Route::get('/schedule_classes/flex_flow/ajax_validation/','Admin\BTSchedule\FlexFlowController@ajaxValidation');
    /*=============Better together schedule FlexFlowController Module end==============================*/

    /*=============Better together schedule CoreFlexController Module start==============================*/
        Route::get('/schedule_classes/core_flex/','Admin\BTSchedule\CoreFlexController@index')->name('btSchedule_core_flex.index');

        Route::get('schedule_classes/core_flex/create', 'Admin\BTSchedule\CoreFlexController@create')->name('btSchedule_core_flex.create');

        Route::post('schedule_classes/core_flex/save', 'Admin\BTSchedule\CoreFlexController@save')->name('btSchedule_core_flex.save');

        Route::get('schedule_classes/core_flex/edit/{id}','Admin\BTSchedule\CoreFlexController@edit')->name('btSchedule_core_flex.edit');

        Route::post('schedule_classes/core_flex/update/{id}','Admin\BTSchedule\CoreFlexController@update')->name('btSchedule_core_flex.update');
          
        Route::delete('schedule_classes/core_flex/delete/{id}','Admin\BTSchedule\CoreFlexController@delete')->name('btSchedule_core_flex.delete');
        
        Route::get('/schedule_classes/core_flex/detailView/{id}','Admin\BTSchedule\CoreFlexController@show')->name('btSchedule_core_flex.detailView');

        Route::get('/schedule_classes/core_flex/ajax_validation/','Admin\BTSchedule\CoreFlexController@ajaxValidation');
    /*=============Better together schedule CoreFlexController Module end==============================*/

   /*=============Quote of the day Module start===================================*/
        Route::get('/quote_of_day','Admin\QuoteOfDayController@index')->name('quote_of_day.index'); 
        Route::get('quote_of_day/create', 'Admin\QuoteOfDayController@create')->name('quote_of_day.create');
        Route::post('quote_of_day/save', 'Admin\QuoteOfDayController@save')->name('quote_of_day.save');
        Route::get('/quote_of_day/edit/{id}','Admin\QuoteOfDayController@edit')->name('quote_of_day.edit');
        Route::post('/quote_of_day/update/{id}','Admin\QuoteOfDayController@update')->name('quote_of_day.update');
        Route::delete('/quote_of_day/delete/{id}','Admin\QuoteOfDayController@delete')->name('quote_of_day.delete');
        Route::get('quote_of_day/date_ajax','Admin\QuoteOfDayController@ajaxDate');
   /*=============Quote of the day Module end=====================================*/

   /*============MindFlex kinetics's Daily Strech Module Start=======================*/
        Route::get('/mindflex_kinetics/daily_stretch','Admin\MindFlexKinetics\DailyStretchController@index')->name('kinetics_daily_stretch.index'); 

        Route::get('mindflex_kinetics/daily_stretch/create', 'Admin\MindFlexKinetics\DailyStretchController@create')->name('kinetics_daily_stretch.create');

        Route::post('mindflex_kinetics/daily_stretch/save', 'Admin\MindFlexKinetics\DailyStretchController@save')->name('kinetics_daily_stretch.save');

        Route::get('/mindflex_kinetics/daily_stretch/edit/{id}','Admin\MindFlexKinetics\DailyStretchController@edit')->name('kinetics_daily_stretch.edit');

        Route::post('/mindflex_kinetics/daily_stretch/update/{id}','Admin\MindFlexKinetics\DailyStretchController@update')->name('kinetics_daily_stretch.update');

        Route::delete('/mindflex_kinetics/daily_stretch/delete/{id}','Admin\MindFlexKinetics\DailyStretchController@delete')->name('kinetics_daily_stretch.delete');

        Route::get('/mindflex_kinetics/daily_stretch/show/{id}','Admin\MindFlexKinetics\DailyStretchController@show')->name('kinetics_daily_stretch.show'); 
   /*============MindFlex kinetics's Daily Strech Module end=======================*/
   
   /*=========MindFlex kinetics's begginer Movement Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/beginner/movement','Admin\MindFlexKinetics\WellnessGuide\Beginner\MovementController@index')->name('begin_movement.index'); 

        Route::get('mindflex_kinetics/wellness_guide/beginner/movement/create', 'Admin\MindFlexKinetics\WellnessGuide\Beginner\MovementController@create')->name('begin_movement.create');

        Route::post('mindflex_kinetics/wellness_guide/beginner/movement/save', 'Admin\MindFlexKinetics\WellnessGuide\Beginner\MovementController@save')->name('begin_movement.save');

        Route::get('/mindflex_kinetics/wellness_guide/beginner/movement/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\MovementController@edit')->name('begin_movement.edit');

        Route::post('/mindflex_kinetics/wellness_guide/beginner/movement/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\MovementController@update')->name('begin_movement.update');

        Route::delete('/mindflex_kinetics/wellness_guide/beginner/movement/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\MovementController@delete')->name('begin_movement.delete');

        Route::get('/mindflex_kinetics/wellness_guide/beginner/movement/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\MovementController@show')->name('begin_movement.show'); 
   /*=========MindFlex kinetics's begginer Movement Module end==============*/

   /*=========MindFlex kinetics's begginer Mindset Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/beginner/mindset','Admin\MindFlexKinetics\WellnessGuide\Beginner\MindsetController@index')->name('begin_mindset.index'); 

        Route::get('mindflex_kinetics/wellness_guide/beginner/mindset/create', 'Admin\MindFlexKinetics\WellnessGuide\Beginner\MindsetController@create')->name('begin_mindset.create');

        Route::post('mindflex_kinetics/wellness_guide/beginner/mindset/save', 'Admin\MindFlexKinetics\WellnessGuide\Beginner\MindsetController@save')->name('begin_mindset.save');

        Route::get('/mindflex_kinetics/wellness_guide/beginner/mindset/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\MindsetController@edit')->name('begin_mindset.edit');

        Route::post('/mindflex_kinetics/wellness_guide/beginner/mindset/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\MindsetController@update')->name('begin_mindset.update');

        Route::delete('/mindflex_kinetics/wellness_guide/beginner/mindset/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\MindsetController@delete')->name('begin_mindset.delete');

        Route::get('/mindflex_kinetics/wellness_guide/beginner/mindset/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\MindsetController@show')->name('begin_mindset.show'); 
   /*=========MindFlex kinetics's begginer Mindset Module end==============*/

   /*=========MindFlex kinetics's begginer Nutrition Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/beginner/nutrition','Admin\MindFlexKinetics\WellnessGuide\Beginner\NutritionController@index')->name('begin_nutrition.index'); 

        Route::get('mindflex_kinetics/wellness_guide/beginner/nutrition/create', 'Admin\MindFlexKinetics\WellnessGuide\Beginner\NutritionController@create')->name('begin_nutrition.create');

        Route::post('mindflex_kinetics/wellness_guide/beginner/nutrition/save', 'Admin\MindFlexKinetics\WellnessGuide\Beginner\NutritionController@save')->name('begin_nutrition.save');

        Route::get('/mindflex_kinetics/wellness_guide/beginner/nutrition/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\NutritionController@edit')->name('begin_nutrition.edit');

        Route::post('/mindflex_kinetics/wellness_guide/beginner/nutrition/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\NutritionController@update')->name('begin_nutrition.update');

        Route::delete('/mindflex_kinetics/wellness_guide/beginner/nutrition/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\NutritionController@delete')->name('begin_nutrition.delete');

        Route::get('/mindflex_kinetics/wellness_guide/beginner/nutrition/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\NutritionController@show')->name('begin_nutrition.show'); 
   /*=========MindFlex kinetics's begginer Nutrition Module end==============*/

   /*=========MindFlex kinetics's begginer Supplementation Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/beginner/supplementation','Admin\MindFlexKinetics\WellnessGuide\Beginner\SupplementationController@index')->name('begin_supplementation.index'); 

        Route::get('mindflex_kinetics/wellness_guide/beginner/supplementation/create', 'Admin\MindFlexKinetics\WellnessGuide\Beginner\SupplementationController@create')->name('begin_supplementation.create');

        Route::post('mindflex_kinetics/wellness_guide/beginner/supplementation/save', 'Admin\MindFlexKinetics\WellnessGuide\Beginner\SupplementationController@save')->name('begin_supplementation.save');

        Route::get('/mindflex_kinetics/wellness_guide/beginner/supplementation/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\SupplementationController@edit')->name('begin_supplementation.edit');

        Route::post('/mindflex_kinetics/wellness_guide/beginner/supplementation/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\SupplementationController@update')->name('begin_supplementation.update');

        Route::delete('/mindflex_kinetics/wellness_guide/beginner/supplementation/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\SupplementationController@delete')->name('begin_supplementation.delete');

        Route::get('/mindflex_kinetics/wellness_guide/beginner/supplementation/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Beginner\SupplementationController@show')->name('begin_supplementation.show'); 
   /*=========MindFlex kinetics's begginer Supplementation Module end==============*/

    /*=========MindFlex kinetics's Intermediate Movement Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/intermediate/movement','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MovementController@index')->name('intermediate_movement.index'); 

        Route::get('mindflex_kinetics/wellness_guide/intermediate/movement/create', 'Admin\MindFlexKinetics\WellnessGuide\Intermediate\MovementController@create')->name('intermediate_movement.create');

        Route::post('mindflex_kinetics/wellness_guide/intermediate/movement/save', 'Admin\MindFlexKinetics\WellnessGuide\Intermediate\MovementController@save')->name('intermediate_movement.save');

        Route::get('/mindflex_kinetics/wellness_guide/intermediate/movement/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MovementController@edit')->name('intermediate_movement.edit');

        Route::post('/mindflex_kinetics/wellness_guide/intermediate/movement/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MovementController@update')->name('intermediate_movement.update');

        Route::delete('/mindflex_kinetics/wellness_guide/intermediate/movement/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MovementController@delete')->name('intermediate_movement.delete');

        Route::get('/mindflex_kinetics/wellness_guide/intermediate/movement/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MovementController@show')->name('intermediate_movement.show'); 
    /*=========MindFlex kinetics's Intermediate Movement Module end==============*/

    /*=========MindFlex kinetics's Intermediate Mindset Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/intermediate/mindset','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MindsetController@index')->name('intermediate_mindset.index'); 

        Route::get('mindflex_kinetics/wellness_guide/intermediate/mindset/create', 'Admin\MindFlexKinetics\WellnessGuide\Intermediate\MindsetController@create')->name('intermediate_mindset.create');

        Route::post('mindflex_kinetics/wellness_guide/intermediate/mindset/save', 'Admin\MindFlexKinetics\WellnessGuide\Intermediate\MindsetController@save')->name('intermediate_mindset.save');

        Route::get('/mindflex_kinetics/wellness_guide/intermediate/mindset/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MindsetController@edit')->name('intermediate_mindset.edit');

        Route::post('/mindflex_kinetics/wellness_guide/intermediate/mindset/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MindsetController@update')->name('intermediate_mindset.update');

        Route::delete('/mindflex_kinetics/wellness_guide/intermediate/mindset/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MindsetController@delete')->name('intermediate_mindset.delete');

        Route::get('/mindflex_kinetics/wellness_guide/intermediate/mindset/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\MindsetController@show')->name('intermediate_mindset.show'); 
    /*=========MindFlex kinetics's Intermediate Mindset Module end==============*/

    /*=========MindFlex kinetics's Intermediate Nutrition Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/intermediate/nutrition','Admin\MindFlexKinetics\WellnessGuide\Intermediate\NutritionController@index')->name('intermediate_nutrition.index'); 

        Route::get('mindflex_kinetics/wellness_guide/intermediate/nutrition/create', 'Admin\MindFlexKinetics\WellnessGuide\Intermediate\NutritionController@create')->name('intermediate_nutrition.create');

        Route::post('mindflex_kinetics/wellness_guide/intermediate/nutrition/save', 'Admin\MindFlexKinetics\WellnessGuide\Intermediate\NutritionController@save')->name('intermediate_nutrition.save');

        Route::get('/mindflex_kinetics/wellness_guide/intermediate/nutrition/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\NutritionController@edit')->name('intermediate_nutrition.edit');

        Route::post('/mindflex_kinetics/wellness_guide/intermediate/nutrition/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\NutritionController@update')->name('intermediate_nutrition.update');

        Route::delete('/mindflex_kinetics/wellness_guide/intermediate/nutrition/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\NutritionController@delete')->name('intermediate_nutrition.delete');

        Route::get('/mindflex_kinetics/wellness_guide/intermediate/nutrition/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\NutritionController@show')->name('intermediate_nutrition.show'); 
    /*=========MindFlex kinetics's Intermediate Nutrition Module end==============*/

    /*=========MindFlex kinetics's Intermediate Supplementation Module start===========*/
        Route::get('/mindflex_kinetics/wellness_guide/intermediate/supplementation','Admin\MindFlexKinetics\WellnessGuide\Intermediate\SupplementationController@index')->name('intermediate_supplementation.index'); 

        Route::get('mindflex_kinetics/wellness_guide/intermediate/supplementation/create', 'Admin\MindFlexKinetics\WellnessGuide\Intermediate\SupplementationController@create')->name('intermediate_supplementation.create');

        Route::post('mindflex_kinetics/wellness_guide/intermediate/supplementation/save', 'Admin\MindFlexKinetics\WellnessGuide\Intermediate\SupplementationController@save')->name('intermediate_supplementation.save');

        Route::get('/mindflex_kinetics/wellness_guide/intermediate/supplementation/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\SupplementationController@edit')->name('intermediate_supplementation.edit');

        Route::post('/mindflex_kinetics/wellness_guide/intermediate/supplementation/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\SupplementationController@update')->name('intermediate_supplementation.update');

        Route::delete('/mindflex_kinetics/wellness_guide/intermediate/supplementation/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\SupplementationController@delete')->name('intermediate_supplementation.delete');

        Route::get('/mindflex_kinetics/wellness_guide/intermediate/supplementation/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Intermediate\SupplementationController@show')->name('intermediate_supplementation.show'); 
    /*=========MindFlex kinetics's Intermediate Supplementation Module end==============*/


    /*=========MindFlex kinetics's Advance Movement Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/advance/movement','Admin\MindFlexKinetics\WellnessGuide\Advance\MovementController@index')->name('advance_movement.index'); 

        Route::get('mindflex_kinetics/wellness_guide/advance/movement/create', 'Admin\MindFlexKinetics\WellnessGuide\Advance\MovementController@create')->name('advance_movement.create');

        Route::post('mindflex_kinetics/wellness_guide/advance/movement/save', 'Admin\MindFlexKinetics\WellnessGuide\Advance\MovementController@save')->name('advance_movement.save');

        Route::get('/mindflex_kinetics/wellness_guide/advance/movement/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\MovementController@edit')->name('advance_movement.edit');

        Route::post('/mindflex_kinetics/wellness_guide/advance/movement/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\MovementController@update')->name('advance_movement.update');

        Route::delete('/mindflex_kinetics/wellness_guide/advance/movement/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\MovementController@delete')->name('advance_movement.delete');

        Route::get('/mindflex_kinetics/wellness_guide/advance/movement/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\MovementController@show')->name('advance_movement.show'); 
    /*=========MindFlex kinetics's Advance Movement Module end==============*/

    /*=========MindFlex kinetics's Advance Mindset Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/advance/mindset','Admin\MindFlexKinetics\WellnessGuide\Advance\MindsetController@index')->name('advance_mindset.index'); 

        Route::get('mindflex_kinetics/wellness_guide/advance/mindset/create', 'Admin\MindFlexKinetics\WellnessGuide\Advance\MindsetController@create')->name('advance_mindset.create');

        Route::post('mindflex_kinetics/wellness_guide/advance/mindset/save', 'Admin\MindFlexKinetics\WellnessGuide\Advance\MindsetController@save')->name('advance_mindset.save');

        Route::get('/mindflex_kinetics/wellness_guide/advance/mindset/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\MindsetController@edit')->name('advance_mindset.edit');

        Route::post('/mindflex_kinetics/wellness_guide/advance/mindset/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\MindsetController@update')->name('advance_mindset.update');

        Route::delete('/mindflex_kinetics/wellness_guide/advance/mindset/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\MindsetController@delete')->name('advance_mindset.delete');

        Route::get('/mindflex_kinetics/wellness_guide/advance/mindset/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\MindsetController@show')->name('advance_mindset.show'); 
    /*=========MindFlex kinetics's Advance Mindset Module end==============*/

    /*=========MindFlex kinetics's Advance Nutrition Module start==============*/
        Route::get('/mindflex_kinetics/wellness_guide/advance/nutrition','Admin\MindFlexKinetics\WellnessGuide\Advance\NutritionController@index')->name('advance_nutrition.index'); 

        Route::get('mindflex_kinetics/wellness_guide/advance/nutrition/create', 'Admin\MindFlexKinetics\WellnessGuide\Advance\NutritionController@create')->name('advance_nutrition.create');

        Route::post('mindflex_kinetics/wellness_guide/advance/nutrition/save', 'Admin\MindFlexKinetics\WellnessGuide\Advance\NutritionController@save')->name('advance_nutrition.save');

        Route::get('/mindflex_kinetics/wellness_guide/advance/nutrition/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\NutritionController@edit')->name('advance_nutrition.edit');

        Route::post('/mindflex_kinetics/wellness_guide/advance/nutrition/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\NutritionController@update')->name('advance_nutrition.update');

        Route::delete('/mindflex_kinetics/wellness_guide/advance/nutrition/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\NutritionController@delete')->name('advance_nutrition.delete');

        Route::get('/mindflex_kinetics/wellness_guide/advance/nutrition/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\NutritionController@show')->name('advance_nutrition.show'); 
    /*=========MindFlex kinetics's Advance Nutrition Module end==============*/

    /*=========MindFlex kinetics's Advance Supplementation Module start===========*/
        Route::get('/mindflex_kinetics/wellness_guide/advance/supplementation','Admin\MindFlexKinetics\WellnessGuide\Advance\SupplementationController@index')->name('advance_supplementation.index'); 

        Route::get('mindflex_kinetics/wellness_guide/advance/supplementation/create', 'Admin\MindFlexKinetics\WellnessGuide\Advance\SupplementationController@create')->name('advance_supplementation.create');

        Route::post('mindflex_kinetics/wellness_guide/advance/supplementation/save', 'Admin\MindFlexKinetics\WellnessGuide\Advance\SupplementationController@save')->name('advance_supplementation.save');

        Route::get('/mindflex_kinetics/wellness_guide/advance/supplementation/edit/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\SupplementationController@edit')->name('advance_supplementation.edit');

        Route::post('/mindflex_kinetics/wellness_guide/advance/supplementation/update/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\SupplementationController@update')->name('advance_supplementation.update');

        Route::delete('/mindflex_kinetics/wellness_guide/advance/supplementation/delete/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\SupplementationController@delete')->name('advance_supplementation.delete');

        Route::get('/mindflex_kinetics/wellness_guide/advance/supplementation/show/{id}','Admin\MindFlexKinetics\WellnessGuide\Advance\SupplementationController@show')->name('advance_supplementation.show'); 
    /*=========MindFlex kinetics's Advance Supplementation Module end==============*/

    /*============MindFlex kinetics's Breathing Module Start=======================*/
        Route::get('/mindflex_kinetics/breathing','Admin\MindFlexKinetics\BreathingController@index')->name('kinetics_breathing.index'); 

        Route::get('mindflex_kinetics/breathing/create', 'Admin\MindFlexKinetics\BreathingController@create')->name('kinetics_breathing.create');

        Route::post('mindflex_kinetics/breathing/save', 'Admin\MindFlexKinetics\BreathingController@save')->name('kinetics_breathing.save');

        Route::get('/mindflex_kinetics/breathing/edit/{id}','Admin\MindFlexKinetics\BreathingController@edit')->name('kinetics_breathing.edit');

        Route::post('/mindflex_kinetics/breathing/update/{id}','Admin\MindFlexKinetics\BreathingController@update')->name('kinetics_breathing.update');

        Route::delete('/mindflex_kinetics/breathing/delete/{id}','Admin\MindFlexKinetics\BreathingController@delete')->name('kinetics_breathing.delete');

        Route::get('/mindflex_kinetics/breathing/show/{id}','Admin\MindFlexKinetics\BreathingController@show')->name('kinetics_breathing.show'); 
    /*============MindFlex kinetics's Breathing Module end======================= */

   /*============MindFlex kinetics's Daily Affirmation Module Start============*/
        Route::get('/mindflex_kinetics/daily_affirmation','Admin\MindFlexKinetics\DailyAffirmationController@index')->name('kinetics_daily_affirmation.index');
        
        Route::get('mindflex_kinetics/daily_affirmation/create', 'Admin\MindFlexKinetics\DailyAffirmationController@create')->name('kinetics_daily_affirmation.create');

        Route::post('mindflex_kinetics/daily_affirmation/save', 'Admin\MindFlexKinetics\DailyAffirmationController@save')->name('kinetics_daily_affirmation.save');

        Route::get('/mindflex_kinetics/daily_affirmation/edit/{id}','Admin\MindFlexKinetics\DailyAffirmationController@edit')->name('kinetics_daily_affirmation.edit');

        Route::post('/mindflex_kinetics/daily_affirmation/update/{id}','Admin\MindFlexKinetics\DailyAffirmationController@update')->name('kinetics_daily_affirmation.update');

        Route::delete('/mindflex_kinetics/daily_affirmation/delete/{id}','Admin\MindFlexKinetics\DailyAffirmationController@delete')->name('kinetics_daily_affirmation.delete');

        Route::get('/mindflex_kinetics/daily_affirmation/show/{id}','Admin\MindFlexKinetics\DailyAffirmationController@show')->name('kinetics_daily_affirmation.show'); 
   /*============MindFlex kinetics's Daily Affirmation Module end==============*/

   /*============MindFlex kinetics's Store Module Start============*/
        Route::get('/mindflex_kinetics/store','Admin\MindFlexKinetics\StoreController@index')->name('kinetics_store.index');
        
        Route::get('mindflex_kinetics/store/create', 'Admin\MindFlexKinetics\StoreController@create')->name('kinetics_store.create');

        Route::post('mindflex_kinetics/store/save', 'Admin\MindFlexKinetics\StoreController@save')->name('kinetics_store.save');

        Route::get('/mindflex_kinetics/store/edit/{id}','Admin\MindFlexKinetics\StoreController@edit')->name('kinetics_store.edit');

        Route::post('/mindflex_kinetics/store/update/{id}','Admin\MindFlexKinetics\StoreController@update')->name('kinetics_store.update');

        Route::delete('/mindflex_kinetics/store/delete/{id}','Admin\MindFlexKinetics\StoreController@delete')->name('kinetics_store.delete');

        Route::get('/mindflex_kinetics/store/show/{id}','Admin\MindFlexKinetics\StoreController@show')->name('kinetics_store.show'); 
   /*============MindFlex kinetics's Store Module end==============*/

    /*============MindFlex Cleanse's weight loss Module start==============*/
        Route::get('/mindflex_cleanse/weight_loss','Admin\MindFlexCleanse\WeightLossController@index')->name('cleanse_weight_loss.index');
        
        Route::get('mindflex_cleanse/weight_loss/create', 'Admin\MindFlexCleanse\WeightLossController@create')->name('cleanse_weight_loss.create');

        Route::post('mindflex_cleanse/weight_loss/save', 'Admin\MindFlexCleanse\WeightLossController@save')->name('cleanse_weight_loss.save');

        Route::get('/mindflex_cleanse/weight_loss/edit/{id}','Admin\MindFlexCleanse\WeightLossController@edit')->name('cleanse_weight_loss.edit');

        Route::post('/mindflex_cleanse/weight_loss/update/{id}','Admin\MindFlexCleanse\WeightLossController@update')->name('cleanse_weight_loss.update');

        Route::delete('/mindflex_cleanse/weight_loss/delete/{id}','Admin\MindFlexCleanse\WeightLossController@delete')->name('cleanse_weight_loss.delete');

        Route::get('/mindflex_cleanse/weight_loss/show/{id}','Admin\MindFlexCleanse\WeightLossController@show')->name('cleanse_weight_loss.show'); 

        Route::get('/mindflex_cleanse/ranking','Admin\MindFlexCleanse\WeightLossController@rankingList'); 
    /*============MindFlex Cleanse's weight loss Module end================*/

    /*============MindFlex Cleanse's mind reset Module start==============*/
        Route::get('/mindflex_cleanse/mind_reset','Admin\MindFlexCleanse\MindResetController@index')->name('cleanse_mind_reset.index');
        
        Route::get('mindflex_cleanse/mind_reset/create', 'Admin\MindFlexCleanse\MindResetController@create')->name('cleanse_mind_reset.create');

        Route::post('mindflex_cleanse/mind_reset/save', 'Admin\MindFlexCleanse\MindResetController@save')->name('cleanse_mind_reset.save');

        Route::get('/mindflex_cleanse/mind_reset/edit/{id}','Admin\MindFlexCleanse\MindResetController@edit')->name('cleanse_mind_reset.edit');

        Route::post('/mindflex_cleanse/mind_reset/update/{id}','Admin\MindFlexCleanse\MindResetController@update')->name('cleanse_mind_reset.update');

        Route::delete('/mindflex_cleanse/mind_reset/delete/{id}','Admin\MindFlexCleanse\MindResetController@delete')->name('cleanse_mind_reset.delete');

        Route::get('/mindflex_cleanse/mind_reset/show/{id}','Admin\MindFlexCleanse\MindResetController@show')->name('cleanse_mind_reset.show'); 
    /*============MindFlex Cleanse's mind reset Module end================*/

    /*============MindFlex Cleanse's Nutrition\Food Log Module start==============*/
        Route::get('/mindflex_cleanse/nutrition/food_log','Admin\MindFlexCleanse\Nutrition\FoodLogController@index')->name('cleanse_food_log.index'); 

        Route::get('mindflex_cleanse/nutrition/food_log/create', 'Admin\MindFlexCleanse\Nutrition\FoodLogController@create')->name('cleanse_food_log.create');

        Route::post('mindflex_cleanse/nutrition/food_log/save', 'Admin\MindFlexCleanse\Nutrition\FoodLogController@save')->name('cleanse_food_log.save');

        Route::get('/mindflex_cleanse/nutrition/food_log/edit/{id}','Admin\MindFlexCleanse\Nutrition\FoodLogController@edit')->name('cleanse_food_log.edit');

        Route::post('/mindflex_cleanse/nutrition/food_log/update/{id}','Admin\MindFlexCleanse\Nutrition\FoodLogController@update')->name('cleanse_food_log.update');

        Route::delete('/mindflex_cleanse/nutrition/food_log/delete/{id}','Admin\MindFlexCleanse\Nutrition\FoodLogController@delete')->name('cleanse_food_log.delete');
    /*============MindFlex Cleanse's Nutrition\Food Log Module end================*/

    /*=========MindFlex Cleanse's Nutrition/Suggested Meal module start==========*/
        Route::get('/mindflex_cleanse/nutrition/suggested_meal','Admin\MindFlexCleanse\Nutrition\SuggestedMealController@index')->name('cleanse_suggested_meal.index'); 

        Route::get('mindflex_cleanse/nutrition/suggested_meal/create', 'Admin\MindFlexCleanse\Nutrition\SuggestedMealController@create')->name('cleanse_suggested_meal.create');

        Route::post('mindflex_cleanse/nutrition/suggested_meal/save', 'Admin\MindFlexCleanse\Nutrition\SuggestedMealController@save')->name('cleanse_suggested_meal.save');

        Route::get('/mindflex_cleanse/nutrition/suggested_meal/edit/{id}','Admin\MindFlexCleanse\Nutrition\SuggestedMealController@edit')->name('cleanse_suggested_meal.edit');

        Route::post('/mindflex_cleanse/nutrition/suggested_meal/update/{id}','Admin\MindFlexCleanse\Nutrition\SuggestedMealController@update')->name('cleanse_suggested_meal.update');

        Route::delete('/mindflex_cleanse/nutrition/suggested_meal/delete/{id}','Admin\MindFlexCleanse\Nutrition\SuggestedMealController@delete')->name('cleanse_suggested_meal.delete');
    /*=======MindFlex Cleanse's  Nutrition/Suggested Meal module end==============*/

    /*=========MindFlex Cleanse's Nutrition/Tips module start==========*/
        Route::get('/mindflex_cleanse/nutrition/tip','Admin\MindFlexCleanse\Nutrition\TipController@index')->name('cleanse_tip.index'); 

        Route::get('mindflex_cleanse/nutrition/tip/create', 'Admin\MindFlexCleanse\Nutrition\TipController@create')->name('cleanse_tip.create');

        Route::post('mindflex_cleanse/nutrition/tip/save', 'Admin\MindFlexCleanse\Nutrition\TipController@save')->name('cleanse_tip.save');

        Route::get('/mindflex_cleanse/nutrition/tip/edit/{id}','Admin\MindFlexCleanse\Nutrition\TipController@edit')->name('cleanse_tip.edit');

        Route::post('/mindflex_cleanse/nutrition/tip/update/{id}','Admin\MindFlexCleanse\Nutrition\TipController@update')->name('cleanse_tip.update');

        Route::delete('/mindflex_cleanse/nutrition/tip/delete/{id}','Admin\MindFlexCleanse\Nutrition\TipController@delete')->name('cleanse_tip.delete');

        Route::get('/mindflex_cleanse/nutrition/tip/show/{id}','Admin\MindFlexCleanse\Nutrition\TipController@show')->name('cleanse_tip.show');
    /*=======MindFlex Cleanse's  Nutrition/Tips Log module end==============*/

    /*============MindFlex Cleanse's Community Log Module start==============*/
        Route::get('/mindflex_cleanse/community','Admin\MindFlexCleanse\CommunityController@index');
    /*============MindFlex Cleanse's Community Module end==============*/

});

// Demo routes
  Route::get('/datatables', 'PagesController@datatables');
  Route::get('/ktdatatables', 'PagesController@ktDatatables');
  Route::get('/select2', 'PagesController@select2');
  Route::get('/icons/custom-icons', 'PagesController@customIcons');
  Route::get('/icons/flaticon', 'PagesController@flaticon');
  Route::get('/icons/fontawesome', 'PagesController@fontawesome');
  Route::get('/icons/lineawesome', 'PagesController@lineawesome');
  Route::get('/icons/socicons', 'PagesController@socicons');
  Route::get('/icons/svg', 'PagesController@svg');

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');
