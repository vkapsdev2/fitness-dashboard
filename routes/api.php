<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login', 'API\UserController@login');

Route::post('facebook_login', 'API\UserController@facebookLogin'); 

Route::post('signup', 'API\UserController@signup');

Route::post('phone_no_check', 'API\UserController@checkUserPhone');

//Route::post('social_login','API\CustomerController@testSocialLogin');

Route::post('check-google-auth','API\UserController@checkGoogleAuth');

/*Forgot Password*/
Route::post('check_email','API\ForgotPassword@checkEmail');
Route::post('reset_password','API\ForgotPassword@checkOTP');


Route::group(['middleware' => 'auth:api'], function(){

	/* PAYMENT KEY STRIPE ACCOUNT */
	Route::get('stripe_key_list','API\PaymentController@stripeKeyList');

	/* Customer personal info save/update */
	Route::post('customer_personal_info', 'API\UserController@customerPersonalInformation');
	Route::post('customer_body_measurement', 'API\UserController@customerBodyMeasurement');
	Route::post('customer_satisfaction', 'API\UserController@customerSatisfaction');
	Route::post('customer_personal_info_question', 'API\UserController@customerPersonalInfoQuestion');
	Route::post('customer_medical_profile', 'API\UserController@customerMedicalProfile');
	Route::post('customer_family_detail', 'API\UserController@customerFamilyDetail');
	

	/* Login Customer personal info GET */
	Route::post('signup_detail_get', 'API\UserController@signupDetailGet');
	Route::post('signup_detail_update', 'API\UserController@signupDetailUpdate');
	

	Route::post('cus_personal_info_detail', 'API\UserController@cusPersonalInformationDetail');
	Route::post('cus_body_measurement_detail', 'API\UserController@custBodyMeasurementDetail');
	Route::post('cus_satisfaction_detail', 'API\UserController@cusSatisfactionDetail');
	Route::post('cus_satisfaction_history', 'API\UserController@cusSatisfactionHistory');
	Route::delete('cus_satisfaction_log_delete', 'API\UserController@cusSatisfactionLogDelete');
	Route::post('cus_personal_info_question_detail', 'API\UserController@cusPersonalInfoQuestionDetail');
	Route::post('cus_medical_profile_detail', 'API\UserController@cusMedicalProfileDetail');
	Route::post('cus_family_detail_get', 'API\UserController@cusFamilyDetailGet');

	/* Customer body measurement log */
	Route::post('customer_body_measurement_log', 'API\UserController@customerBodyMeasurementLog');
	Route::post('measurement_log_detail', 'API\UserController@measurementLogDetail');
	Route::post('cus_body_meas_log_delete', 'API\UserController@measurementLogDelete');
	Route::post('cus_body_meas_log_update', 'API\UserController@measurementLogupdate');
	
	Route::post('cus_body_meas_log_graph', 'API\UserController@bodyMeasurementGraph');
	Route::post('cus_body_satisfaction_log_graph', 'API\UserController@satisfactionGraph');

	Route::get('logout', 'API\UserController@logout');

	/* Contact us */
	Route::post('contact_us', 'API\ContactUsController@contactUs');

	/* -- Coach -- */
	Route::get('coach_detail', 'API\CoachController@coachDetails');

	/* -- Customers -- */
	Route::get('customer_detail', 'API\CustomerController@customerDetails');
	Route::post('customer_attendance', 'API\CustomerController@attendence');
	Route::get('customer_food_log', 'API\UserController@customerFoodLog');

	Route::get('events','API\EventController@eventList');

	Route::get('resource/podcasts','API\ResourceController@podcastList'); //correct and ready for test
	Route::get('resource/articles','API\ResourceController@articleList');
	Route::get('resource/books','API\ResourceController@bookList');
	Route::get('resource/recipes','API\ResourceController@recipesList');

	/* -----=====----- SHAKE BAR -----=====----- */
	Route::get('shake_bar_list','API\ShakeBarController@shakeBarList');
	Route::post('filter_shake_bar_list','API\ShakeBarController@filterShakeBarList');
	//Route::get('shake_bar_detail','API\ShakeBarController@shakeDetail');
	Route::get('shake_bar_discount','API\ShakeBarController@discount');
 
	/* -- Plans -- */
	Route::get('plans_list','API\PlansController@plansList');

	/* -- Quote Of The Day -- */
	Route::get('quote_list','API\QuoteOfTheDayController@quoteList');
	Route::post('add_quote_log','API\QuoteOfTheDayController@addQuoteLog');
	Route::post('list_quote_log','API\QuoteOfTheDayController@listQuoteLog');

	/* -- Badges -- */
	Route::get('badges_list','API\BadgesController@badgesList');
	Route::get('customer_badges_list','API\BadgesController@customerBadgesList');

	/* Customer Reminders Start */
	Route::post('add_reminder','API\CustomerReminderController@addReminder');
	Route::post('reminder_list','API\CustomerReminderController@reminderList');
	Route::post('delete_reminder','API\CustomerReminderController@deleteReminder');
	Route::post('repeat_reminder','API\CustomerReminderController@repeatReminder');
	/* Customer Reminders End */

/*--------------------------------MindFlex Planner--------------------------------*/
	/* -- MindFlex Planner->Movement -- */
	Route::get('mindFlex/movement/home_workouts_list','API\mindFlexPlanner\MovementController@homeWorkoutsList');
	Route::get('mindFlex/movement/gym_workouts_list','API\mindFlexPlanner\MovementController@gymWorkoutsList');
	Route::get('mindFlex/movement/hit_list','API\mindFlexPlanner\MovementController@hitList');
	Route::get('mindFlex/movement/free_weights_list','API\mindFlexPlanner\MovementController@freeWeightsList');

	/* -- MindFlex Planner->MindSet -- */
	Route::get('mindFlex/mindset/daily_affirmations_list','API\mindFlexPlanner\MindSetController@dailyAffirmationsList');

	Route::get('mindFlex/mindset/question_list','API\mindFlexPlanner\MindSetController@questionList');
	Route::post('mindFlex/mindset/assignment_list','API\mindFlexPlanner\MindSetController@assignmentList');

	Route::post('mindFlex/mindset/week_list','API\mindFlexPlanner\MindSetController@weekList');

	Route::post('mindFlex/mindset/day_exercise_list','API\mindFlexPlanner\MindSetController@dayExerciseList');
	Route::post('mindFlex/mindset/mark_complete','API\mindFlexPlanner\MindSetController@markComplete');

	/* -- MindFlex Planner->Nutritions -- */
	Route::get('mindFlex/nutrition/food_logs_list','API\mindFlexPlanner\NutritionController@foodLogsList');
	Route::get('mindFlex/nutrition/suggested_meals_list','API\mindFlexPlanner\NutritionController@suggestedMealsList');
	Route::get('mindFlex/nutrition/suggested_meals_type','API\mindFlexPlanner\NutritionController@suggestedMealType');
	Route::get('mindFlex/nutrition/tips_list','API\mindFlexPlanner\NutritionController@tipsList');

	/* -- MindFlex Planner->Supplementation -- */
	Route::get('mindFlex/supplementation/reoccurring_reminders_list','API\mindFlexPlanner\SupplementationController@reoccurringRemindersList');

	Route::delete('mindFlex/supplementation/reoccurring_reminder_delete','API\mindFlexPlanner\SupplementationController@reminderDelete');

	Route::get('mindFlex/supplementation/guide_books_list','API\mindFlexPlanner\SupplementationController@guideBooksList');

	Route::get('mindFlex/supplementation/my_supplements_list','API\mindFlexPlanner\SupplementationController@mySupplementsList');

	Route::post('mindFlex/supplementation/my_suppliment_add','API\mindFlexPlanner\SupplementationController@mySupplimentAdd');

	Route::get('mindFlex/supplementation/store_list','API\mindFlexPlanner\SupplementationController@storeList');

	/* -- MindFlex Planner->Progress Photos -- */
	Route::get('mindFlex/progress_photos_list','API\mindFlexPlanner\ProgressPhotosController@progressPhotosList');

	Route::post('mindFlex/progress_photos_upload','API\mindFlexPlanner\ProgressPhotosController@addPhoto');

	Route::delete('mindFlex/progress_photos_delete','API\mindFlexPlanner\ProgressPhotosController@photoDelete');

	/* -- MindFlex Planner->List MindFlex RX -- */
	Route::post('mindFlex/mindFlex_rx_list','API\mindFlexPlanner\MindFlexRxController@mindFlexRxList');

	Route::post('mindFlex/accountability_add','API\mindFlexPlanner\MindFlexRxController@accountabilityAdd');
/*-------------------------------------MindFlex Kinetics-------------------------------------*/
	/* -- MindFlex Kinetics->Daily Stretches -- */
	Route::get('mindFlex_kinetics/daily_stretches_list','API\mindFlexKinetics\DailyStretchesController@dailyStretchesList');	
	Route::post('mindFlex_kinetics/daily_stretches_status_add','API\mindFlexKinetics\DailyStretchesController@dailyStretchesStatusAdd');
	Route::post('mindFlex_kinetics/daily_stretches_status_get','API\mindFlexKinetics\DailyStretchesController@dailyStretchesStatusGet');


	/* -- MindFlex Kinetics->4 Week Wellness Guide -- */
	/* Beginner => movement mindset nutrition supplementation */
	Route::get('mindFlex_kinetics/four_week_wellness_guide/beginner_level/{action}','API\mindFlexKinetics\FourWeekWellnessGuideController@beginnerLevel');
	/* Intermediate => movement mindset nutrition supplementation */
	Route::get('mindFlex_kinetics/four_week_wellness_guide/intermediate_level/{action}','API\mindFlexKinetics\FourWeekWellnessGuideController@intermediateLevel');
	/* Advance => movement mindset nutrition supplementation */
	Route::get('mindFlex_kinetics/four_week_wellness_guide/advance_level/{action}','API\mindFlexKinetics\FourWeekWellnessGuideController@advanceLevel');

	/* -- MindFlex Kinetics->Breathings -- */
	Route::get('mindFlex_kinetics/breathings_list','API\mindFlexKinetics\BreathingsController@breathingsList');

	/* -- MindFlex Kinetics->Daily Affirmations -- */
	Route::get('mindFlex_kinetics/daily_affirmations_list','API\mindFlexKinetics\DailyAffirmationsController@dailyAffirmationsList');
	Route::post('mindFlex_kinetics/daily_affirmations_status','API\mindFlexKinetics\DailyAffirmationsController@updateStatus');

	Route::post('mindFlex_kinetics/daily_affirmations_status_add','API\mindFlexKinetics\DailyAffirmationsController@dailyAffirmationsStatusAdd');
	Route::post('mindFlex_kinetics/daily_affirmations_status_get','API\mindFlexKinetics\DailyAffirmationsController@dailyAffirmationsStatusGet');

	/* -- MindFlex Kinetics->Stores -- */
	Route::get('mindFlex_kinetics/stores_list','API\mindFlexKinetics\StoresController@storesList');

/*--------------------------- MindFlex Physio ---------------------------*/ 
	/* --  MindFlex Physio->See A Doc -- */
	Route::get('mindFlex_physio/see_a_doc_list','API\mindFlexPhysio\SeeADocController@seeADocList');
	Route::get('mindFlex_physio/therapist_list','API\mindFlexPhysio\SeeADocController@therapistList');
	Route::post('mindFlex_physio/doc_book_session','API\mindFlexPhysio\SeeADocController@docBookSession');
	Route::post('mindFlex_physio/already_book_session','API\mindFlexPhysio\SeeADocController@alreadyBookSession');

	/* --  MindFlex Physio->Exercise Program -- */
	Route::get('mindFlex_physio/exercise_program_list','API\mindFlexPhysio\ExerciseProgramController@exerciseProgramList');
	Route::post('mindFlex_physio/exercise_program_status_add','API\mindFlexPhysio\ExerciseProgramController@exerciseProgramStatusAdd');
	Route::post('mindFlex_physio/exercise_program_status_get','API\mindFlexPhysio\ExerciseProgramController@exerciseProgramStatusGet');

	/* --  MindFlex Physio->Patient Profile->Medical History -- */
	Route::post('mindFlex_physio/patient_profile/medical_history_list','API\mindFlexPhysio\PatientProfileController@medicalHistoryList');
	Route::post('mindFlex_physio/patient_profile/add_medical_history','API\mindFlexPhysio\PatientProfileController@addMedicalHistory');

	/* --  MindFlex Physio->Patient Profile->Daily Notes -- */
	Route::post('mindFlex_physio/patient_profile/daily_notes_list','API\mindFlexPhysio\PatientProfileController@dailyNotesList');
	Route::post('mindFlex_physio/patient_profile/add_daily_notes','API\mindFlexPhysio\PatientProfileController@addDailyNotes');

	/* --  MindFlex Physio->Article -- */
	Route::get('mindFlex_physio/article_list','API\mindFlexPhysio\ArticleController@articleList');

	/* --  MindFlex Physio->Nutritions->Food Log -- */
	Route::post('mindFlex_physio/nutritions/food_log_list','API\mindFlexPhysio\NutritionsController@foodLogList');
	Route::post('mindFlex_physio/nutritions/add_food_log','API\mindFlexPhysio\NutritionsController@addFoodLog');

	/* --  MindFlex Physio->Nutritions->Recommendation Meal -- */
	Route::get('mindFlex_physio/nutritions/recommendation_meal_type','API\mindFlexPhysio\NutritionsController@recommendationMealType');
	Route::post('mindFlex_physio/nutritions/recommendation_meals_list','API\mindFlexPhysio\NutritionsController@recommendationMealsList');

	/* --  MindFlex Physio->Store -- */
	Route::get('mindFlex_physio/store_list','API\mindFlexPhysio\StoreController@storeList');

	/* --  MindFlex Physio->Environment Factor -- */
	Route::post('mindFlex_physio/environment_factor_list','API\mindFlexPhysio\EnvironmentFactorController@environmentFactorList');
	Route::post('mindFlex_physio/add_environment_factor','API\mindFlexPhysio\EnvironmentFactorController@addEnvironmentFactor');

	/* --  MindFlex Physio->Daily Affirmations -- */
	Route::get('mindFlex_physio/daily_affirmations_list','API\mindFlexPhysio\DailyAffirmationsController@dailyAffirmationsList');



	//Customer Custom Workout 
	Route::get('customer/custom_workout_list','API\CustomerController@customWorkoutList');

	Route::get('customer/custom_workouts','API\CustomerController@customWorkouts');

	Route::post('customer/custom_workout_add','API\CustomerController@customWorkoutAdd');

	Route::post('customer/custom_workout_detail_add','API\CustomerController@customWorkoutDetailAdd');

	Route::post('customer/custom_workout_sets_add','API\CustomerController@customWorkoutSets');

	Route::get('customer/custom_workout_detail_list','API\CustomerController@customWorkoutDetailList');

	Route::delete('customer/custom_workout_exercise_delete','API\CustomerController@customWorkoutExerciseDelete');

	Route::post('customer/fav_exercise','API\CustomerController@customerFavExercise'); 

	Route::post('customer/fav_exercise_list','API\CustomerController@customerFavList');

	
	Route::post('mindset_detail', 'API\mindFlexPlanner\MindsetDetailController@mindsetDetail');
	Route::get('mindset_detail/lesson', 'API\mindFlexPlanner\MindsetDetailController@lesson');
	Route::post('mindset_detail/lesson/file', 'API\mindFlexPlanner\MindsetDetailController@lessonFile');

	/*===================MindFlex Cleanse Module =======================*/
	Route::get('mindflex_cleanse/weight_loss_list','API\mindFlexCleanse\WeightLossController@list');
	Route::post('mindflex_cleanse/weight_loss','API\mindFlexCleanse\WeightLossController@exerciseData');
	Route::post('mindflex_cleanse/weight_loss_status','API\mindFlexCleanse\WeightLossController@weightLossStatus');

	Route::get('mindflex_cleanse/mind_reset_list','API\mindFlexCleanse\MindResetController@list');
	Route::post('mindflex_cleanse/mind_reset','API\mindFlexCleanse\MindResetController@exerciseData');
	Route::post('mindflex_cleanse/mind_reset_status','API\mindFlexCleanse\MindResetController@mindResetStatus');

	Route::get('mindflex_cleanse/ranking','API\mindFlexCleanse\WeightLossController@ranking');


	Route::post('mindFlex_cleanse/nutrition/food_logs_list','API\mindFlexCleanse\NutritionController@foodLogsList');

	Route::post('mindFlex_cleanse/nutrition/suggested_meals_list','API\mindFlexCleanse\NutritionController@suggestedMealsList');

	Route::get('mindFlex_cleanse/nutrition/suggested_meals_type','API\mindFlexCleanse\NutritionController@suggestedMealType');

	Route::get('mindFlex_cleanse/nutrition/tips_list','API\mindFlexCleanse\NutritionController@tipsList');

	/* Community */
	Route::post('mindFlex_cleanse/add_community','API\mindFlexCleanse\CommunityController@addCommunity');
	Route::get('mindFlex_cleanse/get_community','API\mindFlexCleanse\CommunityController@getCommunity');
	Route::post('mindFlex_cleanse/get_customer_community','API\mindFlexCleanse\CommunityController@getCustomerCommunity');
	Route::post('mindFlex_cleanse/customer_community_delete','API\mindFlexCleanse\CommunityController@customerCommunityDelete');


	/*===================== Betther Together Schedule Express Start ====================*/
		Route::get('bt_schedule/express','API\BTSchedule\ExpressController@list');
		Route::post('bt_schedule/express/day_exercise','API\BTSchedule\ExpressController@dayExercise');
		Route::post('bt_schedule/express/exercise_detail','API\BTSchedule\ExpressController@exerciseDetail');
		Route::post('bt_schedule/express/review','API\BTSchedule\ExpressController@review');
		Route::post('bt_schedule/express/like_status','API\BTSchedule\ExpressController@likeStatus');
		Route::get('bt_schedule/express/reviews','API\BTSchedule\ExpressController@allReview');
	/*===================== Betther Together Schedule Express End ======================*/

	/*===================== Betther Together Schedule CoreFlex Start ====================*/
		Route::get('bt_schedule/core_flex','API\BTSchedule\CoreFlexController@list');
		Route::post('bt_schedule/core_flex/day_exercise','API\BTSchedule\CoreFlexController@dayExercise');
		Route::post('bt_schedule/core_flex/exercise_detail','API\BTSchedule\CoreFlexController@exerciseDetail');
		Route::post('bt_schedule/core_flex/review','API\BTSchedule\CoreFlexController@review');
		Route::post('bt_schedule/core_flex/like_status','API\BTSchedule\CoreFlexController@likeStatus');
		Route::get('bt_schedule/core_flex/reviews','API\BTSchedule\CoreFlexController@allReview');
	/*===================== Betther Together Schedule CoreFlex End ======================*/
 
	/*===================== Betther Together Schedule CornerStone Start ====================*/
		Route::get('bt_schedule/corner_stone','API\BTSchedule\CornerStoneController@list');
		Route::post('bt_schedule/corner_stone/day_exercise','API\BTSchedule\CornerStoneController@dayExercise');
		Route::post('bt_schedule/corner_stone/exercise_detail','API\BTSchedule\CornerStoneController@exerciseDetail');
		Route::post('bt_schedule/corner_stone/review','API\BTSchedule\CornerStoneController@review');
		Route::post('bt_schedule/corner_stone/like_status','API\BTSchedule\CornerStoneController@likeStatus');
		Route::get('bt_schedule/corner_stone/reviews','API\BTSchedule\CornerStoneController@allReview');
	/*===================== Betther Together Schedule CornerStone End ======================*/

	/*===================== Betther Together Schedule FlowFlex Start ====================*/
		Route::get('bt_schedule/flow_flex','API\BTSchedule\FlowFlexController@list');
		Route::post('bt_schedule/flow_flex/day_exercise','API\BTSchedule\FlowFlexController@dayExercise');
		Route::post('bt_schedule/flow_flex/exercise_detail','API\BTSchedule\ExpressController@exerciseDetail');
		Route::post('bt_schedule/flow_flex/review','API\BTSchedule\FlowFlexController@review');
		Route::post('bt_schedule/flow_flex/like_status','API\BTSchedule\FlowFlexController@likeStatus');
		Route::get('bt_schedule/flow_flex/reviews','API\BTSchedule\FlowFlexController@allReview');
	/*===================== Betther Together Schedule FlowFlex End ======================*/


/*=================================================================================================*/
/*===================================Coach Module start ===========================================*/


	/*===================== Customer Module Start ======================*/
	Route::post('coach/customers','API\Coach\Customer@customers');
	Route::post('coach/customer_detail','API\Coach\Customer@customerDetail');
	Route::post('coach/customer/questions','API\Coach\Customer@questionToCoach');

	Route::post('coach/reminders','API\Coach\Reminder@list');
	Route::post('coach/reminder/save','API\Coach\Reminder@save');
	Route::delete('coach/reminder/delete','API\Coach\Reminder@delete');

	Route::get('coach/customers/assignment/workout_categories','API\Coach\Assignment@WorkoutCategories');
	Route::get('coach/customers/assignment/category_exercises','API\Coach\Assignment@CategoryExercises');
	Route::post('coach/customers/assignments','API\Coach\Assignment@assignExercise');

	Route::post('coach/customer/mindflex_rx','API\Coach\MindFlexRx@RxLog');

	Route::get('coach/customer/meal/list','API\Coach\MealPlan@list');
	Route::get('coach/customer/meal/meal_type_list','API\Coach\MealPlan@mealTypeList');
	Route::post('coach/customer/meal/save_meal','API\Coach\MealPlan@addmeal');
	/*===================== Customer Module End ======================*/

	Route::get('customer/notification', 'API\CustomerNotification@list');


});
