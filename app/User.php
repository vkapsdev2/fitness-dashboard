<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

use Laravel\Passport\HasApiTokens;

/*use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;*/

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use HasRoles;

    protected $guard_name = 'web';



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image','phone','address','gender','city','state','zip','country','status','facebook_url','google_url','twitter_url','youtube_url','instagram_url','provider'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function assignment()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\MindSet\Assignment','name,id'); 
    }
   

    public function custReminder()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\MindSet\CustomersReminder','name,id'); 
    }
    public function custPhoto()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\ProgressPhoto','name,id'); 
    }

    public function cusmindFlexRx()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\MindFlexRx','name,id'); 
    } 

    public function cusAccountability()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\Accountability','name,id'); 
    }

    public function mindsetCoachQ()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\MindSet\CoachQuestion','name,id'); 
    }

    public function mindsetReoccurR()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\Supplimentation\ReoccurReminder','name,id'); 
    }

    public function dailyRoutine()
    {
     return $this->hasMany('App\Models\DailyRoutine','name,id'); 
    }

    public function customerBadges()
    {
     return $this->hasMany('App\Models\CustomerBadge','name,id'); 
    }

    public function custSupplimentation()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\Supplimentation\Suppliment','name,id'); 
    }

    public function btsExpress()
    {
     return $this->hasMany('App\Models\BTSchedule\Express','name,id'); 
    }

    public function btsCornerStone()
    {
     return $this->hasMany('App\Models\BTSchedule\CornerStone','name,id'); 
    }

    public function btsFlexFlow()
    {
     return $this->hasMany('App\Models\BTSchedule\FlexFlow','name,id'); 
    }

    public function btsCoreFlex()
    {
     return $this->hasMany('App\Models\BTSchedule\CoreFlex','name,id'); 
    }

    /*public function coach(){
        return $this->hasMany('App\Models\Coach','name,id'); 
    }*/
}
