<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'name',
        'book_img',
        'author_name',
        'book_type',
        'description'
     ];
}
