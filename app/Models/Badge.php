<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
  protected  $fillable = [
    	'title',
    	'image',
    	'purpose',
    	'benifit',
    	'description',
    	'badge_status'
    ];

    public function Badges()
    {
     return $this->hasMany('App\Models\CustomerBadge','title,id'); 
    }
}
