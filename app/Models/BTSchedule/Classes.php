<?php

namespace App\Models\BTSchedule;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
	protected $fillable = [
        'class_name',
     ];
      protected $table = 'bts_classes';
}
