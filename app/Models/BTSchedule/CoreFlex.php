<?php

namespace App\Models\BTSchedule;

use Illuminate\Database\Eloquent\Model;

class CoreFlex extends Model
{
    protected $fillable = [
        'day',
        'coach_id',
        'start_time',
        'end_time',
        'banner',
        'description',
     ];
     protected $table = 'bts_core_flex';

     public function coach()
    {
     return $this->belongsTo('App\User','coach_id');
    } 
}
