<?php

namespace App\Models\BTSchedule;

use Illuminate\Database\Eloquent\Model;

class FlexFlow extends Model
{
    protected $fillable = [
        'day',
        'coach_id',
        'start_time',
        'end_time',
        'banner',
        'description',
     ];
     protected $table = 'bts_flex_flow';

     public function coach()
    {
     return $this->belongsTo('App\User','coach_id');
    } 
}
