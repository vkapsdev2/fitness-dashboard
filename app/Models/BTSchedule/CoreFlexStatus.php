<?php

namespace App\Models\BTSchedule;

use Illuminate\Database\Eloquent\Model;

class CoreFlexStatus extends Model
{
    protected $fillable = [
        'coach_id',
        'customer_id',
        'exercise_id',
        'rating',
        'review',
        'review_date',
        'like_status',
     ];
     protected $table = 'bts_core_flex_status';
}
