<?php

namespace App\Models\BTSchedule;

use Illuminate\Database\Eloquent\Model;

class CornerStone extends Model
{
    protected $fillable = [
        'day',
        'coach_id',
        'start_time',
        'end_time',
        'banner',
        'description',
     ];
     protected $table = 'bts_corner_stones';

     public function coach()
    {
     return $this->belongsTo('App\User','coach_id');
    } 
}
