<?php

namespace App\Models\BTSchedule;

use Illuminate\Database\Eloquent\Model;

class Express extends Model
{
    protected $fillable = [
        'day',
        'coach_id',
        'start_time',
        'end_time',
        'banner',
        'video_file',
        'video_url',
        'description',
     ];
     protected $table = 'bts_express';

     public function coach()
    {
     return $this->belongsTo('App\User','coach_id'); 
    }
}
