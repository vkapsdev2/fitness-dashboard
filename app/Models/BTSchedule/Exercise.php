<?php

namespace App\Models\BTSchedule;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $fillable = [
        'exercise_name',
        'class_id',
        'video_file',
        'video_url',
        'description',
     ];
     protected $table = 'bts_class_exercises';
}
