<?php

namespace App\Models\MindFlexPhysio\Nutrition;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model{
    protected $table = 'nutrition_recommendation_meals';
    // protected $fillable = [
    //  'suggester_coach_id',
    //  'suggested_customer_id',
    //  'assign_break_fast_meal',
    //  'break_fast_days',
    //  'assign_lunch_meal',
    //  'lunch_days',
    //  'assign_dinner_meal',
    //  'dinner_days',
    //  'other_suggest_meal_detail',
    //  'suggesting_date',
    //  'suggest_meal_status',
    // ];

    protected $fillable = [
        'Food_name',
        'meal_type',
        'calorie',
        'fats',
        'carbohydrate',
        'protein',
        'description',
    ];
}