<?php

namespace App\Models\MindFlexPhysio\Nutrition;

use Illuminate\Database\Eloquent\Model;

class FoodLog extends Model
{
    protected $fillable = [
        'customer_id',
        'name',
        'quantity',
     ];
      protected $table = 'mindiflex_pysio_nutrition_food_logs';
}
