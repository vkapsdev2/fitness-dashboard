<?php

namespace App\Models\MindFlexPhysio;

use Illuminate\Database\Eloquent\Model;

class PhysioArticle extends Model
{
    protected $fillable = [
    	'id',
    	'name',
    	'article_img',
    	'author_name',
    	'description',
    	'physio_article_status',
    ];
    protected $table = 'mindflex_physio_articles';
}
