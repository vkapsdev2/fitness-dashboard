<?php

namespace App\Models\MindFlexPhysio;

use Illuminate\Database\Eloquent\Model;

class ExerciseMarkAsComplete extends Model
{
    protected $fillable = [
        'exercise_program_id',
        'customer_id',
        'status',
     ];
      protected $table = 'physio_exercise_status';
}
