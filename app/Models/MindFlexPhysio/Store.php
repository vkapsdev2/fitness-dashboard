<?php

namespace App\Models\MindFlexPhysio;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
	protected $table = 'mindiflex_pysio_stores';
    protected $fillable = [
    	'name',
    	'image',
    	'actual_price',
    	'sales_price',
    	'total_quantity',
    	'buy_now_url',
    	'description',
    	'remaining_quantity',
    	'alert_quantity_stock',
    	'store_status',
    ];
}
