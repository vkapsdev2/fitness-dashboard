<?php

namespace App\Models\MindFlexPhysio;

use Illuminate\Database\Eloquent\Model;

class DocBookSession extends Model
{
    protected $fillable = [
        'doc_id',
        'therapist_id',
        'customer_id',
        'date',
        'time',
        'purpose',
     ];
      protected $table = 'physio_docs_book_session';
}

