<?php

namespace App\Models\MindFlexPhysio;

use Illuminate\Database\Eloquent\Model;

class Environment extends Model
{
    protected $fillable = [
        'customer_id',
        'name',
        'image',
        'description',
     ];
      protected $table = 'physio_environment_factor';
}
