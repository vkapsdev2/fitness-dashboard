<?php

namespace App\Models\MindFlexPhysio;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $fillable = [
        'name',
        'banner',
        'video_file',
        'video_url',
        'description',
        'mark_status',
     ];
      protected $table = 'physio_exercise';
}
