<?php

namespace App\Models\MindFlexPhysio;

use Illuminate\Database\Eloquent\Model;

class DailyAffirmation extends Model
{
    protected $fillable = [
        'title',
        'video_file',
        'video_url',
        'description',
        'mark_status',
     ];
      protected $table = 'mindiflex_pysio_daily_affirmation';
}
