<?php

namespace App\Models\MindFlexPhysio;

use Illuminate\Database\Eloquent\Model;

class Doc extends Model
{
    protected $fillable = [
        'title',
        'price',
        'therapist_id',
        'image',
        'booking_date',
        'time_slot',
        'typeVisitSession',
        'description',
     ];
      protected $table = 'physio_docs';
}
