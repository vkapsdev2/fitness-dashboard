<?php

namespace App\Models\MindFlexPhysio\Profile;

use Illuminate\Database\Eloquent\Model;

class DailyNote extends Model
{
    protected $table = 'mindiflex_pysio_profile_daily_notes';
    protected $fillable = [
    	'daily_note_detail',
    	'customer_id',
    	'daily_note_date',
    	'time',
    	'daily_note_status',
    	''
    ];
}
