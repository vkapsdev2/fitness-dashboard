<?php

namespace App\Models\MindFlexPhysio\Profile;

use Illuminate\Database\Eloquent\Model;

class MedicalHistory extends Model
{
    
   protected $table = 'mindiflex_pysio_profile_medical_history';
   protected $fillable = [
   	'medical_hisory_detail',
   	'customer_id',
   	'date',
   	'time',
   	'medical_history_status'
   ];
}
