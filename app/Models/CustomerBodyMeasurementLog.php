<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerBodyMeasurementLog extends Model
{
    protected $fillable = [
    	'customer_id',
        'shoulder',
    	'right_bicep',
    	'chest',
    	'waist',
    	'belly',
    	'hips',
    	'right_thigh',
    	'right_calf',
        'log_date',
    ];

    protected $table = 'cu_body_measurement_log'; 

    
}