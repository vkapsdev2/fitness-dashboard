<?php

namespace App\Models\MindFlexKinetics;

use Illuminate\Database\Eloquent\Model;

class DailyStretchStatus extends Model
{
	protected $fillable = [
		'customer_id',
		'daily_stretch_id',
		'status',
    ];
    protected $table = 'kinetics_daily_stretch_status';
}
