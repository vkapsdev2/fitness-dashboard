<?php

namespace App\Models\MindFlexKinetics\WellnessGuide\Beginner;

use Illuminate\Database\Eloquent\Model;

class Mindset extends Model
{
    protected $fillable = [
        'title',
        'banner',
        'video_file',
        'video_url',
        'description'

    ];
    protected $table = 'wellness_begin_mindset';
}
