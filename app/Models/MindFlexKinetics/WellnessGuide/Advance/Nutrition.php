<?php

namespace App\Models\MindFlexKinetics\WellnessGuide\Advance;

use Illuminate\Database\Eloquent\Model;

class Nutrition extends Model
{
    protected $fillable = [
        'title',
        'banner',
        'video_file',
        'video_url',
        'description'
    ];
    protected $table = 'wellness_advance_nutrition';
}
