<?php

namespace App\Models\MindFlexKinetics\WellnessGuide\Intermediate;

use Illuminate\Database\Eloquent\Model;

class Mindset extends Model
{
    protected $fillable = [
        'title',
        'banner',
        'video_file',
        'video_url',
        'description'
 
    ];
    protected $table = 'wellness_intermediate_mindset';
}
