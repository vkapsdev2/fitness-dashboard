<?php

namespace App\Models\MindFlexKinetics;

use Illuminate\Database\Eloquent\Model;

class DailyStretch extends Model
{
     protected $fillable = [
        'title',
        'video_file',
        'video_url',
        'image',
        'description',
     ];
      protected $table = 'kinetics_daily_stretch';
}
