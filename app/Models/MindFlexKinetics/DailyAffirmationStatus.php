<?php

namespace App\Models\MindFlexKinetics;

use Illuminate\Database\Eloquent\Model;

class DailyAffirmationStatus extends Model
{
	protected $fillable = [
		'customer_id',
		'daily_affirmation_id',
		'status',
    ];
    protected $table = 'kinetics_daily_affirmation_status';

}
