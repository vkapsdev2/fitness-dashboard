<?php

namespace App\Models\MindFlexKinetics;

use Illuminate\Database\Eloquent\Model;

class DailyAffirmation extends Model
{
    protected $fillable = [
        'title',
        'video_file',
        'video_url',
        'image',
        'description',
        'mark_status',
     ];
      protected $table = 'kinetics_daily_affirmation';
}
