<?php

namespace App\Models\MindFlexKinetics;

use Illuminate\Database\Eloquent\Model;

class Breathing extends Model
{
    protected $fillable = [
        'title',
        'video_file',
        'video_url',
        'image',
        'description',
     ];
      protected $table = 'kinetics_breathing'; 
}
