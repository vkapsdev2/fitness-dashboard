<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
    	'customer_id',
    	'date',
    	'order_detail',
    	'payment_method',
    	'is_applied_coupon',
    	'coupon',
    	'total_item',
    	'total',
    	'discount_rate',
    	'grand_total',
    	'payment_status',
    	'order_status',
    	'order_status'
    ];
}
