<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteLog extends Model{
	protected $fillable = [
	'customer_id',
        'date',
        'angry',
        'anxious',
        'calm',
        'depressed',
        'fearful',
        'grateful',
        'happy',
        'helpless',
        'relaxed',
        'sad',
        'stressed',
        'tired',
        'optimistic',
        'something_good',
        'hours_sleep',
        'quality_sleep',
	];
	protected $table = 'quotes_log';
}
