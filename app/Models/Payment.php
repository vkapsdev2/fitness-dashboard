<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	protected $table = 'payment';
	protected $fillable = [
		'id',
		'publishable_key',
		'secret_key',
		'key_status',
	];
}
