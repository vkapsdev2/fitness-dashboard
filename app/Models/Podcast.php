<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Podcast extends Model
{
    protected $fillable = [
        'name',
        'podcast_img',
     ];
     public $timestamps=false;
      public function SubPodcast()  
    {
        return $this->belongsTo('App\Models\SubPodcast' ,'id');
    }
} 
