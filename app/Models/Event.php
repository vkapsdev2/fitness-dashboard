<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'location',
        'banner_img',
        'event_url',
        'start_date',
        'start_time',
        'end_date',
        'end_time',
     ];
}
