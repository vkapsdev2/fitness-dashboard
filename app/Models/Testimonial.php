<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
   protected $fillable = [
   	'title',
   	'to_coach',
   	'from_customer',
   	'description',
   	'testimonial_status',
   ];
}
