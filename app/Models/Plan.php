<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
    	'plan_title',
    	'plan_image',
    	'validity_days',
    	'plan_type',
    	'actual_price',
    	'sales_price',
    	'plan_description',
    	'plan_status'
    ];
}
