<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
    	'name',
    	'method',
    	'discount_rate',
    	'validity',
    	'code',
    	'coupon_status'
    ];
}
