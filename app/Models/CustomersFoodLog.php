<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomersFoodLog extends Model
{
    protected $fillable = [
        'customer_id',
        'first_day_foodlog',
        'second_day_foodlog',
        'third_day_foodlog',
     ];
      protected $table = 'customer_3days_foodlog';
}
