<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
   protected $fillable = [
   	'meal_title',
   	'meal_image',
   	'meal_calories',
   	'meal_protein',
   	'meal_carbs',
   	'meal_fats',
   	'meal_fiber',
   	'meal_minerals',
      'meal_calcium',
   	'meal_vitmins',
   	'meal_water',
   	'other_meal_detail',
   	'meal_quantity',
   	'meal_plan_type',
   	'meal_description',
   	'created_by',
   	'meal_status',
   ];
}
