<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerBadge extends Model
{
    protected $table = 'customer_badges';
   protected $fillable = [
   	'customer_id',
   	'badge_id'
   ];

    public function customer()  
    {
        return $this->belongsTo('App\User', 'customer_id'); 
    }

    public function badge()  
    {
        return $this->belongsTo('App\Models\Badge', 'badge_id'); 
    }

}
