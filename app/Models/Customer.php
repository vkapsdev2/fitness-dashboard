<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'customer_name',
         'level', 
         'weight',
         'height',
         'age',
         'body_fat_percent',
         'body_fat_mass',
         'skletal_muscle_mass',
         'neck',
         'shoulder',
         'right_bicep',
         'chest',
         'waist',
         'hips',
         'right_thigh',
         'right_calf',
         'career',
         'relationship',
         'health',
         'nutrition',
         'waist_satisfaction',
         'appearance',
         'purpose',
         'sleep',
         'social',
         'spiritual',
         'social_goal',
         'joy',
         'ready_for_change',
         'consistent_physical_activity',
         'consistent_physical_activity_time',
         'consistent_physical_activity_description',
         'customer_occupation',
         'houldhold',
         'recreational_activity',
         'hobbies',
         'sleep_hours_in_detail',
         'sleep_taking_time',
         'last_physical_examination_time',
         'ever_been_hospitalized',
         'is_pregnant',
         'smoking',
         'alcoholic_quantity_per_week',
         'diagnosed_heart_condition',
         'is_feel_chest_pain',
         'did_feel_chest_pain_when_not_physical',
         'is_feel_loss_consciousness',
         'is_joint_issue',
         'is_take_medications',
         'is_diabetic',
         'is_high_blood_pressure',
         'is_high_cholesterol',
         'is_diagnosed_osteoporosis',
         'is_breathless_to_staris',
         'do_you_have_been_cancer',
         'activity_reson_for_not_engage_physical_activity',
         'family_disease_detail',
         'other_family_disease',
         'plan',
         'plan_start_date',
         'plan_end_date',
      ];

     
}
