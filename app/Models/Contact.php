<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
    	'name',
    	'email',
    	'query',
    	'date',
    	'replied_by_us',
    	'replied_date',
    	'contact_status'
    ];
}
