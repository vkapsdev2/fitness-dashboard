<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerSatisfactionLog extends Model
{
	protected $table = 'cus_satisfaction_log';
	protected $fillable = [
		'customer_id',
		'log_date',
		'career',
		'relationship',
		'joy',
		'health',
		'nutrition', 
		'sleep',
		'appearance', 
		'purpose',
		'social',
		'spiritual',
		'ready_for_change' 
	];
}