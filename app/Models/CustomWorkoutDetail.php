<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomWorkoutDetail extends Model
{
    protected $table = 'cus_custom_workout_detail';
   protected $fillable = [
   	'customer_workout_id',
   	'gym_category_id',
   	'gym_category_exercise_id',
   	'sets',
   ];
}
