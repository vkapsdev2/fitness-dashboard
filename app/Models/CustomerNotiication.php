<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerNotiication extends Model
{
   protected $table = 'cus_notification';
   protected $fillable = [
   	'customer_id',
   	'title',
   	'description',
   ];

  
}
