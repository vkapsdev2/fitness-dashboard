<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubPodcast extends Model
{
     protected $fillable = [
        'sub_name',
        'podcast_id',
        'audio_file',
        'audio_url',
     ];
     public $timestamps=false;
   public function podcast()
    { 
      return $this->hasMany('App\Models\Podcast','podcast_id'); 
    }
} 
