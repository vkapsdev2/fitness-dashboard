<?php

namespace App\Models\MindFlexCleanse\Nutrition;

use Illuminate\Database\Eloquent\Model;

class SuggestedMeal extends Model
{
	protected $table = 'cleanse_nutrition_suggestedmeals';

	protected $fillable = [
		'Food_name',
		'meal_type',
		'calorie',
		'fats',
		'carbohydrate',
		'protein',
		'description',
	];
}
