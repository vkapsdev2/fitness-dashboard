<?php

namespace App\Models\MindFlexCleanse\Nutrition;

use Illuminate\Database\Eloquent\Model;

class FoodLog extends Model
{
	protected $table = 'cleanse_nutrition_food_log';

	protected $fillable = [
		'name',
		'quantity',
	];
}
