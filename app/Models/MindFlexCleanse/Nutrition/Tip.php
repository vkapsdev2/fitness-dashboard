<?php

namespace App\Models\MindFlexCleanse\Nutrition;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
	protected $table = 'cleanse_nutrition_tips';

	protected $fillable = [
		'title',
		'banner',
		'description',
	];
}
