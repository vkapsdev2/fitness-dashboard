<?php

namespace App\Models\MindFlexCleanse;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
	protected $table = 'cleanse_community';

   	protected $fillable = [
	   	'customer_id',
	   	'community_banner',
	   	'time',
	   	'like',
	   	'title',
	   	'description',
   	];
}
