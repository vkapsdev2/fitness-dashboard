<?php

namespace App\Models\MindFlexCleanse;

use Illuminate\Database\Eloquent\Model;

class MindReset extends Model
{
	protected $table = 'cleanse_mind_reset';

   protected $fillable = [
   	'name',
   	'banner',
   	'video_file',
   	'video_url',
   	'description',
   ];
}
