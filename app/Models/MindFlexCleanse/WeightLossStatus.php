<?php

namespace App\Models\MindFlexCleanse;

use Illuminate\Database\Eloquent\Model;

class WeightLossStatus extends Model
{
    protected $table = 'cleanse_weight_loss_status';

   protected $fillable = [
   	'weight_loss_id',
   	'customer_id',
   	'status',
   ];
}
