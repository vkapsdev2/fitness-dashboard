<?php

namespace App\Models\MindFlexCleanse;

use Illuminate\Database\Eloquent\Model;

class WeightLoss extends Model
{
   protected $table = 'cleanse_weight_loss';

   protected $fillable = [
   	'name',
   	'banner',
   	'video_file',
   	'video_url',
   	'description',
   ];
}
