<?php

namespace App\Models\MindFlexCleanse;

use Illuminate\Database\Eloquent\Model;

class MindResetStatus extends Model
{
    protected $table = 'cleanse_mindreset_status';

   protected $fillable = [
   	'mindreset_id',
   	'customer_id',
   	'status',
   ];
}
