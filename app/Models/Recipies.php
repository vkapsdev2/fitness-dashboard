<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipies extends Model
{
    protected $fillable = [
        'name',
        'video_file',
        'video_url',
        'image',
        'diet_time',
        'discription',
     ];
}
