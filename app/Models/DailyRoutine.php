<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyRoutine extends Model
{
     protected $fillable = [
        'customer_id',
        'weight',
        'sleep',
        'eat',
        'feel',
        'description',
     ];
      protected $table = 'daily_routines';

      public function customer()  
    {
        return $this->belongsTo('App\User', 'customer_id'); 
    }
}
