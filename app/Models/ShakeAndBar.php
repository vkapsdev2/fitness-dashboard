<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShakeAndBar extends Model
{
    protected $fillable = [
    	'shake_name',
        'price',
    	'shake_img',
    	'shake_type',
    	'calories',
    	'protein',
    	'carbs',
    	'fats',
    	'is_shake_customize',
    	'shake_ingredients',
    	'shake_description',
    	'shake_status',
    ];
}
