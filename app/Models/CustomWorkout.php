<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomWorkout extends Model
{
    protected $table = 'cus_custom_workout';
   protected $fillable = [
   	'customer_id',
   	'title',
   ];
}
