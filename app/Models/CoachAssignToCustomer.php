<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoachAssignToCustomer extends Model
{
	protected $table = 'coach_assign_to_customer';
   protected $fillable = [
   	'coach_assign_id',
   	'coach_assign_customer_id',
   	'coach_assign_customer_status',
   ];
}
