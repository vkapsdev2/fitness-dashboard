<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $fillable = [
    	'facebook_url',
    	'google_url',
    	'twitter_url',
    	'youtube_url',
    	'instagram_url',
    	'social_status',
    ];
}
