<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAttendeance extends Model
{
    protected $table = 'customer_attendence';
   protected $fillable = [
   	'customer_id',
   	'name',
   	'email',
   	'QR_id',
   ];
}
