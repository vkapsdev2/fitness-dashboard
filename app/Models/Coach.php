<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coach extends Model
{
    protected $fillable = [
    	'coach_id',
    	'coach_name',
    	'specializations',
    	'certification',
    	'awards',
    	'publish_articles',
    	'client_feedback',
    ];

    /*public function user(){
    	return $this->belongsTo('App\User', 'coach_id');
    }*/
}
