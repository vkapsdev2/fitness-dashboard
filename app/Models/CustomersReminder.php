<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomersReminder extends Model
{
    protected $fillable = [
        'coach_id',
        'customer_id',
        'title',
        'days',
        'time',
        'repeat_status',
        'reminder_name',
    ];
    protected $table = 'cust_reminders';

    public function customer()  
    {
        return $this->belongsTo('App\User', 'customer_id'); 
    }

    public function coach()  
    {
        return $this->belongsTo('App\User', 'coach_id'); 
    }
}
