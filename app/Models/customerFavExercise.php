<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class customerFavExercise extends Model
{
    protected $fillable = [
    	'customer_id',
        'workout_type',
    	'category_id',
    	'exercise_id',
    ];

    protected $table = 'customer_fav_exercise';
}
