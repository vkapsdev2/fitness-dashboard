<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class suggestedCustomerMeals extends Model
{
    protected $table = 'suggested_customer_meals';
    protected $fillable = [
    	'suggester_coach_id',
    	'suggested_customer_id',
    	'assign_break_fast_meal',
    	'break_fast_days',
    	'assign_lunch_meal',
    	'lunch_days',
    	'assign_dinner_meal',
    	'dinner_days',
    	'other_suggest_meal_detail',
    	'suggesting_date',
    	'suggest_meal_status',
    ];
}
