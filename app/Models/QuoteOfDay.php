<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuoteOfDay extends Model
{
     protected $fillable = [
        'title',
        'author',
        'description',
        'quote_date',
     ];
      protected $table = 'quote_of_day';
}
