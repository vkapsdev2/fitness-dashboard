<?php

namespace App\Models\MindFlexPlanner\Nutrition;

use Illuminate\Database\Eloquent\Model;

class FoodLog extends Model
{
    protected $fillable = [
        'name',
        'quantity',
     ];
      protected $table = 'nutrition_foodlogs';
}
