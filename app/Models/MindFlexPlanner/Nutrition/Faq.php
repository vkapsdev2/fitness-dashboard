<?php

namespace App\Models\MindFlexPlanner\Nutrition;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = [
        'faq_title',
        'faq_description',
        'faq_status',
    ];
    protected $table = 'nutrition_faq';
}
