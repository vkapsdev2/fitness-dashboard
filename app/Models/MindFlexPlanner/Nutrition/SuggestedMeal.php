<?php

namespace App\Models\MindFlexPlanner\Nutrition;

use Illuminate\Database\Eloquent\Model;

class SuggestedMeal extends Model
{
    protected $fillable = [
       // 'title',
        'Food_name',
        'meal_type',
        'calorie',
        'fats',
        'carbohydrate',
        'protein',
        'description',
    ];
    protected $table = 'nutrition_suggestedmeals';
}
