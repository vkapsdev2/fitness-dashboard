<?php

namespace App\Models\MindFlexPlanner\Nutrition;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    protected $fillable = [
        'title',
        'banner',
        'description',
    ];
    protected $table = 'nutrition_tips';
}
