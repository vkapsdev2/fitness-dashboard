<?php

namespace App\Models\MindFlexPlanner\MindSet;

use Illuminate\Database\Eloquent\Model;

class MindsetLesson extends Model
{
     protected $fillable = [
    	'name',
        'banner',
        'description'
     ];
      protected $table = 'mindset_lessons';
}
