<?php

namespace App\Models\MindFlexPlanner\MindSet;

use Illuminate\Database\Eloquent\Model;

class CoachQuestion extends Model
{
    protected $fillable = [
        'coach_id',
        'question',
     ];
      protected $table = 'mindset_coachquestions';

      public function coach()  
    {
        return $this->belongsTo('App\User', 'coach_id'); 
    }
}
