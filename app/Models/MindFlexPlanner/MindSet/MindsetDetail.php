<?php

namespace App\Models\MindFlexPlanner\MindSet;

use Illuminate\Database\Eloquent\Model;

class MindsetDetail extends Model
{
    protected $fillable = [
        'title',
        'banner',
        'description',
        'file'
     ];
      protected $table = 'mindset_detail';

     public function file(){
      	return $this->hasMany('App\Models\MindFlexPlanner\MindSet\MindsetResource');
      	//return $this->belongsTo('App\Models\MindFlexPlanner\MindSet\MindsetResource','id');
      }
}
