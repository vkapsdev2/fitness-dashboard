<?php

namespace App\Models\MindFlexPlanner\MindSet;

use Illuminate\Database\Eloquent\Model;

class DailyAffirmation extends Model
{
     protected $fillable = [
        'title',
        'video_file',
        'video_url',
        'description',
     ];
      protected $table = 'mindset_dailyaffirmations';
}
