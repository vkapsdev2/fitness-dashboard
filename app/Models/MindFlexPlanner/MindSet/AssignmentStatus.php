<?php

namespace App\Models\MindFlexPlanner\MindSet;

use Illuminate\Database\Eloquent\Model;

class AssignmentStatus extends Model
{
     protected $fillable = [
        'customer_id',
        'assignment_id',
        'status',
     ];
      protected $table = 'cus_assignment_status';
}
