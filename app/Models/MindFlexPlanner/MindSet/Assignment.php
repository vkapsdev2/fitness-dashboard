<?php

namespace App\Models\MindFlexPlanner\MindSet;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = [
        'customer_id',
        'workout_category',
        'workout_id',
        'exercises_id',
        'week',
        'data',
        'rest_timing',
     ];
      protected $casts = [
        'exercises_id' => 'array',
        'workout_id' => 'array',
    ];
      protected $table = 'assignments';

    public function user()  
    {
        return $this->belongsTo('App\User', 'customer_id'); 
    }
    public function homeWorkout()  
    {
        return $this->belongsTo('App\Models\MindFlexPlanner\Movement\HomeWorkout', 'workout_id'); 
    } 
    public function gymWorkout()  
    {
        return $this->belongsTo('App\Models\MindFlexPlanner\Movement\GymWorkout', 'workout_id'); 
    }
}
