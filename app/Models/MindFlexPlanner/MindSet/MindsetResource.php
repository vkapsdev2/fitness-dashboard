<?php

namespace App\Models\MindFlexPlanner\MindSet;

use Illuminate\Database\Eloquent\Model;

class MindsetResource extends Model
{
    protected $fillable = [
    	'mindset_detail_id',
        'audio_file',
        'audio_url',
        'pdf_file'
     ];
      protected $table = 'mindset_resources';

      

    public function mindsetDetail()
    {
        return $this->belongsTo('App\Models\MindFlexPlanner\MindSet\MindsetDetail');
        //return $this->hasMany('App\Models\MindFlexPlanner\MindSet\MindsetDetail','mindset_detail_id');
        
    }

}
