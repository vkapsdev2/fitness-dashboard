<?php

namespace App\Models\MindFlexPlanner\MindSet;

use Illuminate\Database\Eloquent\Model;

class MindsetLessonFile extends Model
{
     protected $fillable = [
    	'lesson_id',
        'audio_file',
        'audio_url',
        'pdf_file'
     ];
      protected $table = 'mindset_lesson_file';
}
