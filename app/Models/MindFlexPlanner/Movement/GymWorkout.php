<?php

namespace App\Models\MindFlexPlanner\Movement;

use Illuminate\Database\Eloquent\Model;

class GymWorkout extends Model
{
    protected $fillable = [
        'name',
        'banner'
     ];
      protected $table = 'movement_gymworkouts';

    public function assignment()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\MindSet\Assignment','name,id'); 
    }
}
