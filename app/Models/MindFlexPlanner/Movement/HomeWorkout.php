<?php

namespace App\Models\MindFlexPlanner\Movement;

use Illuminate\Database\Eloquent\Model;

class HomeWorkout extends Model
{
     protected $fillable = [
        'name',
        'banner',
     ];
      protected $table = 'movement_homeworkouts';

    public function assignment()
    {
     return $this->hasMany('App\Models\MindFlexPlanner\MindSet\Assignment','name,id'); 
    }

}
