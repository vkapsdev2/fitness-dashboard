<?php

namespace App\Models\MindFlexPlanner\Movement;

use Illuminate\Database\Eloquent\Model;

class SubHomeWorkout extends Model
{
     protected $fillable = [
        'sub_name',
        'homeWorkout_id',
        'video_file',
        'video_url',
        'sub_banner',
        'workout_timing',
        'description',
     ];
     protected $table = 'movement_sub_homeworkouts';
}
