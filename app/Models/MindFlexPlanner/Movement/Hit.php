<?php

namespace App\Models\MindFlexPlanner\Movement;

use Illuminate\Database\Eloquent\Model;

class Hit extends Model
{
    protected $fillable = [
        'name',
        'banner',
        'video_file',
        'video_url',
        'description',
     ];
      protected $table = 'movement_hits';
}
