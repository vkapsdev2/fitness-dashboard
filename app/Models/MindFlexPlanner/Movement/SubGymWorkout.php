<?php

namespace App\Models\MindFlexPlanner\Movement;

use Illuminate\Database\Eloquent\Model;

class SubGymWorkout extends Model
{
     protected $fillable = [
        'sub_name',
        'gymWorkout_id',
        'video_file',
        'video_url',
        'workout_timing',
        'sub_banner',
        'description',
     ];
     protected $table = 'movement_sub_gymworkouts';
}
