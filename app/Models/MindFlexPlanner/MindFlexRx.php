<?php

namespace App\Models\MindFlexPlanner;

use Illuminate\Database\Eloquent\Model;

class MindFlexRx extends Model
{
     protected $fillable = [
        'customer_id',
        'movement',
        'mindset',
        'nutrition',
        'supplimentation',
        'other',
        'log_date',
    ];
    protected $table = 'mindflex_rx';

    public function user()  
    {
        return $this->belongsTo('App\User', 'customer_id')->withDefault([
        	'name' => 'Guest Customer',
    	]);
    }
}
