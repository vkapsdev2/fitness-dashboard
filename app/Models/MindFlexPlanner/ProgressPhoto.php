<?php

namespace App\Models\MindFlexPlanner;

use Illuminate\Database\Eloquent\Model;

class ProgressPhoto extends Model
{
    protected $fillable = [
        'image',
        'customer_id'
    ];
    protected $table = 'progress_photos';

    public function user()  
    {
        //return $this->belongsTo('App\User', 'customer_id'); 
        return $this->belongsTo('App\User', 'customer_id')->withDefault([
        	'name' => 'Guest Customer',
    	]);
    }
}
