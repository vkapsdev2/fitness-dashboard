<?php

namespace App\Models\MindFlexPlanner\Supplimentation;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
     protected $fillable = [
        'name',
        'book_img',
        'author_name',
        'description',
     ];
      protected $table = 'supplimentation_guides';
}
