<?php

namespace App\Models\MindFlexPlanner\Supplimentation;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [
    	'name',
    	'image',
    	'actual_price',
    	'sales_price',
    	'total_quantity',
    	'buy_now_url',
    	'description',
    	'remaining_quantity',
    	'alert_quantity_stock',
    	'store_status',
    ];
}
