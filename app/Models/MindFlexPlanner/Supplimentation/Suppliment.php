<?php

namespace App\Models\MindFlexPlanner\Supplimentation;

use Illuminate\Database\Eloquent\Model;

class Suppliment extends Model
{
     protected $fillable = [
        'name',
        'customer_id',
     ];
      protected $table = 'supplimentation_my_suppliment';

     public function customer()  
    {
        return $this->belongsTo('App\User', 'customer_id'); 
    }
}
