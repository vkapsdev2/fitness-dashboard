<?php

namespace App\Models\MindFlexPlanner\Supplimentation;

use Illuminate\Database\Eloquent\Model;

class ReoccurReminder extends Model
{
    protected $fillable = [
        'coach_id',
        'customer_id',
        'title',
        'days',
        'time',
        'repeat_status',
     ];
      protected $table = 'reoccur_reminder';

    public function coach()  
    {
        return $this->belongsTo('App\User', 'coach_id'); 
    }
    public function customer()  
    {
        return $this->belongsTo('App\User', 'customer_id'); 
    }
}
