<?php

namespace App\Models\MindFlexPlanner;

use Illuminate\Database\Eloquent\Model;

class Accountability extends Model
{
    protected $fillable = [
        'customer_id',
        'goal',
        'goal_2_step',
        'question_to_coach',
        'other',
    ];
    protected $table = 'mindflex_accountability';

    public function user()  
    {
        return $this->belongsTo('App\User', 'customer_id')->withDefault([
        	'name' => 'Guest Customer',
    	]);
    }
}
