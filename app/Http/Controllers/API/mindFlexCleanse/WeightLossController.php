<?php

namespace App\Http\Controllers\API\mindFlexCleanse;
use DB;
use Validator;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\MindFlexCleanse\WeightLoss;
use App\Models\MindFlexCleanse\WeightLossStatus;

class WeightLossController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function list(Request $request){
    	$data = WeightLoss::get();
        $final_data = array();
        $new_data = array();
        foreach ($data as $key => $value) {

            $status = WeightLossStatus::where('weight_loss_id',$value->id)->where('customer_id',$request->customer_id)->get(['status']);

            if (count($status) > 0) {

                

                $new_data['id'] = $value->id;
                $new_data['name'] = $value->name;
                $new_data['banner'] = $value->banner;
                $new_data['video_file'] = $value->video_file;
                $new_data['video_url'] = $value->video_url;
                $new_data['description'] = $value->description;
                $new_data['status'] = $status[0]->status;

               
            } else{
                $new_data['id'] = $value->id;
                $new_data['name'] = $value->name;
                $new_data['banner'] = $value->banner;
                $new_data['video_file'] = $value->video_file;
                $new_data['video_url'] = $value->video_url;
                $new_data['description'] = $value->description;
                $new_data['status'] = 'Incomplete';
            }
             array_push($final_data, $new_data);

        }

       if(count($final_data)>0){
            return response()->json([
               'success' => true,
                'data' => $final_data,
            ], 200);
        }
        else{
            return response()->json([
           'error' => true,
            'message' => 'Data is not available'
        ], 401);
        }

    }

    public function exerciseData(Request $request){
    	$this->validate($request, [
            'id' => 'required',
            'customer_id' => 'required',
        ]);

        /*$data = WeightLoss::where('id',$request->input('id'))->get();

        $result = $this->returnStatus($data);
        return $result;*/

        $status = WeightLossStatus::where('weight_loss_id',$request->id)->where('customer_id',$request->customer_id)->get(['status']);

        if (count($status) > 0) {
            return response()->json([ 'success' => true, 'status' => $status[0]->status],200);
        }
        return response()->json([ 'success' => true, 'status' => 'Incomplete'],200);

    }

    public function weightLossStatus(Request $request){
 
    	$this->validate($request, [
            'id' => 'required',
    		'customer_id' => 'required',
    		'status'=>'required'
        ]);
        
        $exercise = WeightLoss::find($request->id);
        $user = User::find($request->customer_id);
        $status = WeightLossStatus::where('weight_loss_id',$request->id)->where('customer_id',$request->customer_id)->get();
        if($exercise == null){

            return response()->json([
            'error' => true,
            'message' => 'Exercise does not exist!'
        ], 401);

        } 
        else if($user== null){

             return response()->json([
            'error' => true,
            'message' => 'Customer does not exist!'
        ], 401);

        } else{
            
            if (count($status) > 0) {

                WeightLossStatus::where('id',$status[0]->id)->update(['status'=> $request->status]);
                 return response()->json([ 'success' => true, 'message' => "Status updated successfully" ],200);
            }

            $data = new WeightLossStatus([
                'weight_loss_id' => $request->id,
                'customer_id' => $request->customer_id,
                'status' => $request->status
            ]);
            
            $data->save();

            return response()->json([ 'success' => true, 'message' => "Status updated successfully" ],200);
        }

    }
    public function ranking(Request $request){

        $data = DB::select("select `users`.`name`, `users`.`image`,`cleanse_weight_loss_status`.`customer_id`, count(cleanse_weight_loss_status.customer_id) as total_exercise from `cleanse_weight_loss_status` inner join users on `users`.`id` = `cleanse_weight_loss_status`.`customer_id` group by customer_id ORDER BY total_exercise DESC");

        if($data){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }
}
