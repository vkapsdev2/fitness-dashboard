<?php

namespace App\Http\Controllers\API\mindFlexCleanse;

use DB;
use Validator;
use App\Models\MindFlexCleanse\Community;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CommunityController extends Controller{
	public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function getCommunity(Request $request){
    	$data = DB::table('users')
        	->join('cleanse_community',function($join) {$join
        	->on('cleanse_community.customer_id' , '=' , 'users.id'); })
        	->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function getCustomerCommunity(Request $request){
        $this->validate($request, [
            'customer_id' => 'required',
        ]);

        $id = [ 'customer_id' => $request->customer_id ];

        $data = DB::table('users')
            ->join('cleanse_community',function($join) {$join
            ->on('cleanse_community.customer_id' , '=' , 'users.id'); })
            ->where('cleanse_community.customer_id', $id)
            ->get();
        $result = $this->returnStatus($data);
        return $result;
    }

    public function addCommunity(Request $request){
    	$this->validate($request, [
    		'customer_id' => 'required',
    		'time'        => 'required',
    		'like'        => 'required',
            'title'       => 'required',
    		'description' => 'required',
        ]);

        if ($image_64 = $request->community_banner) {

             

            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

            $replace = substr($image_64, 0, strpos($image_64, ',')+1); 
            $image = str_replace($replace, '', $image_64); 
            $image = str_replace(' ', '+', $image); 
            $imageName = date('YmdHis').'.'.$extension;
            \File::put(public_path().'/media/community-image'. '/' .$imageName, base64_decode($image));
            }else{
            $imageName = "";
        }
       // return response()->json([ 'success' => true, 'message' => $imageName ],200);
        $data = new Community([
            'customer_id'      => $request->customer_id,
            'community_banner' => $imageName,
            'time'             => $request->time,
            'like'             => $request->like,
            'title'            => $request->title,
            'description'      => $request->description,
        ]);
        $data->save();
        return response()->json([ 'success' => true, 'message' => "Community added successfull" ],200);
    }

    public function customerCommunityDelete(Request $request){
        $this->validate($request, [
            'id' => 'required',
            'customer_id' => 'required',
        ]);

        $id = $request->id;
        $customer_id = $request->customer_id;

        $res = DB::table('cleanse_community')
            ->where('id', $id)
            ->where('customer_id', $customer_id)
            ->delete();
        if($res){
            return response()->json([ 'success' => true, 'message' => "Community deleted successfully" ],200);
        }else{
            return response()->json([ 'success' => false, 'message' => "Community not found" ],400);
        }
    }
}
