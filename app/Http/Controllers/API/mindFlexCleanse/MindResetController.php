<?php

namespace App\Http\Controllers\API\mindFlexCleanse;

use Validator;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\MindFlexCleanse\MindReset;
use App\Models\MindFlexCleanse\MindResetStatus;  //mindreset_id

class MindResetController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function list(Request $request){
    	$data = MindReset::get(); 
    	$final_data = array();
        $new_data = array();
        foreach ($data as $key => $value) {

            $status = MindResetStatus::where('mindreset_id',$value->id)->where('customer_id',$request->customer_id)->get(['status']);

            if (count($status) > 0) {
                $new_data['id'] = $value->id;
                $new_data['name'] = $value->name;
                $new_data['banner'] = $value->banner;
                $new_data['video_file'] = $value->video_file;
                $new_data['video_url'] = $value->video_url;
                $new_data['description'] = $value->description;
                $new_data['status'] = $status[0]->status;
               
            } else{
                $new_data['id'] = $value->id;
                $new_data['name'] = $value->name;
                $new_data['banner'] = $value->banner;
                $new_data['video_file'] = $value->video_file;
                $new_data['video_url'] = $value->video_url;
                $new_data['description'] = $value->description;
                $new_data['status'] = 'Incomplete';
            }
             array_push($final_data, $new_data);

        }

       if(count($final_data)>0){
            return response()->json([
               'success' => true,
                'data' => $final_data,
            ], 200);
        }
        else{
            return response()->json([
           'error' => true,
            'message' => 'Data is not available'
        ], 401);
        }
    }

    public function exerciseData(Request $request){
    	$this->validate($request, [
            'id' => 'required',
            'customer_id' => 'required',
        ]);

        $status = MindResetStatus::where('mindreset_id',$request->id)->where('customer_id',$request->customer_id)->get(['status']);

        if (count($status) > 0) {
            return response()->json([ 'success' => true, 'status' => $status[0]->status],200);
        }
        return response()->json([ 'success' => true, 'status' => 'Incomplete'],200);
    }

    public function mindResetStatus(Request $request){
 
        $this->validate($request, [
            'id' => 'required',
            'customer_id' => 'required',
            'status'=>'required'
        ]);
        
        $exercise = MindReset::find($request->id);
        $user = User::find($request->customer_id);
         $status = MindResetStatus::where('mindreset_id',$request->id)->where('customer_id',$request->customer_id)->get();
         
        if($exercise == null){

            return response()->json([
            'error' => true,
            'message' => 'Exercise does not exist!'
        ], 401);

        } 
        else if($user== null){

             return response()->json([
            'error' => true,
            'message' => 'Customer does not exist!'
        ], 401);

        } else{
            
            if (count($status) > 0) {
                 MindResetStatus::where('id',$status[0]->id)->update(['status'=> $request->status]);
                return response()->json([ 'success' => true, 'message' => "Status updated successfull" ],200);
            }

            $data = new MindResetStatus([
                'mindreset_id' => $request->id,
                'customer_id' => $request->customer_id,
                'status' => $request->status
            ]);
            
            $data->save();

            return response()->json([ 'success' => true, 'message' => "Status updated successfull" ],200);
        }

    }
}
