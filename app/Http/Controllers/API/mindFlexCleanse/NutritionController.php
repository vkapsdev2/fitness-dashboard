<?php

namespace App\Http\Controllers\API\mindFlexCleanse;

use Validator;
use App\Models\MindFlexCleanse\Nutrition\FoodLog;
use App\Models\MindFlexCleanse\Nutrition\SuggestedMeal;
use App\Models\MindFlexCleanse\Nutrition\Tip;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class NutritionController extends Controller
{
    
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function foodLogsList(Request $request){

        $this->validate($request, [
            'date' => 'required',
        ]);

        $create_at = $request->input('date'); 

    	$data = FoodLog::whereDate('created_at', $create_at)->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function suggestedMealsList(Request $request){
        $this->validate($request, [
            'meal_type' => 'required',
        ]);

        $meal_type = $request->input('meal_type'); 

        $data = SuggestedMeal::where('meal_type',$meal_type)->get();
        $result = $this->returnStatus($data);
        return $result;
    }

    public function suggestedMealType(Request $request){
 

        $data = SuggestedMeal::get();
        $result = $this->returnStatus($data);
        return $result;
    }
    public function tipsList(Request $request){
        $data = Tip::get();
        $result = $this->returnStatus($data);
        return $result;
    }
}
