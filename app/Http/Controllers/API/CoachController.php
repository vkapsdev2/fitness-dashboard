<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Customer;
use App\Models\Coach;
use Spatie\Permission\Models\Role;
use App\Models\CustomersFoodLog;
use Validator;
use Hash;
use DB;
use Carbon\Carbon; 

class CoachController extends Controller{

	public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function coachDetails(Request $request){
        $access_token = $request->header('Authorization');
        $auth_header = explode(' ', $access_token);
        $token = $auth_header[1];
        $token_parts = explode('.', $token);
        $token_header = $token_parts[1];
        $token_header_json = base64_decode($token_header);
        $token_header_array = json_decode($token_header_json, true);
        $user_token = $token_header_array['jti'];
        $id = DB::table('oauth_access_tokens')->where('id', $user_token)->value('user_id');

		$data = DB::table('users')
			->join('coaches', 'coaches.coach_id','users.id')
			->join('model_has_roles',function($join) {
				$join->on('model_has_roles.model_id' , '=' , 'users.id')
				->where('model_has_roles.role_id','=', 2);
			})->where('users.id', $id)->get();

        $result = $this->returnStatus($data);
        return $result;
    }

}