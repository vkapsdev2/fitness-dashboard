<?php

namespace App\Http\Controllers\API\mindFlexKinetics;

use Validator;
use App\Models\MindFlexKinetics\DailyAffirmation;
use App\Models\MindFlexKinetics\DailyAffirmationStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class DailyAffirmationsController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function dailyAffirmationsList(Request $request){
    	$data = DailyAffirmation::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function updateStatus(Request $request){
        $request->validate([
            'id'             => 'required',
            //'mark_status'     => 'required',
        ]); 

        $id = $request->input('id');
        $res = DailyAffirmation::find($id);

        if($res){

             $data = [
                'mark_status' => $request->input('mark_status'),
            ];

            DailyAffirmation::where('id', $id)->update($data);

            return response()->json([ 'success' => true, 'message' => "Affirmation status updated successfully" ],200);
        }else{
             return response()->json([
            'success' => false,
            'message' => 'This Affirmation does not exist'
        ], 401);
        }
    }

    public function dailyAffirmationsStatusAdd(Request $request){
        $this->validate($request, [
            'customer_id' => 'required',
            'daily_affirmation_id' => 'required',
            'status' => 'required',
        ]);

        $check = DailyAffirmationStatus::where(['daily_affirmation_id'=>$request->input('daily_affirmation_id')])->get();
        if($check->toArray()){
            $statusData = [
                'status' => trim($request->input('status')),
            ];
            DailyAffirmationStatus::where(['daily_affirmation_id'=>$request->input('daily_affirmation_id'), 'customer_id'=>$request->input('customer_id')])->update($statusData); 
            return response()->json([ 'success' => true, 'message' => 'Status updated successfully' ], 200);
        }else{
            $data = [
                'customer_id' => trim($request->input('customer_id')),
                'daily_affirmation_id' => trim($request->input('daily_affirmation_id')),
                'status' => trim($request->input('status')),
            ];
            $data = DailyAffirmationStatus::create($data);
            return response()->json([ 'success' => true, 'message' => 'Status added successfully' ], 200);
        }
    }

    public function dailyAffirmationsStatusGet(Request $request){
        $this->validate($request, [
            'customer_id' => 'required',
            'daily_affirmation_id' => 'required',
        ]);
        $data = DailyAffirmationStatus::where(['daily_affirmation_id'=>$request->input('daily_affirmation_id'), 'customer_id'=>$request->input('customer_id')])->get();

        $result = $this->returnStatus($data);
        return $result;
    }
}
