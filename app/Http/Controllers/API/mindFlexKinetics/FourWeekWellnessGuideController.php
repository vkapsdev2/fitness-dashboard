<?php

namespace App\Http\Controllers\API\mindFlexKinetics;

use Validator;
// use App\Models\MindFlexKinetics\WellnessGuide\Beginner\Movement;
// use App\Models\MindFlexKinetics\WellnessGuide\Beginner\Mindset;
// use App\Models\MindFlexKinetics\WellnessGuide\Beginner\Nutrition;
// use App\Models\MindFlexKinetics\WellnessGuide\Beginner\Supplementation;
// use App\Models\MindFlexKinetics\WellnessGuide\Intermediate\Movement;
// use App\Models\MindFlexKinetics\WellnessGuide\Intermediate\Mindset;
// use App\Models\MindFlexKinetics\WellnessGuide\Intermediate\Nutrition;
// use App\Models\MindFlexKinetics\WellnessGuide\Intermediate\Supplementation;
// use App\Models\MindFlexKinetics\WellnessGuide\Advance\Movement;
// use App\Models\MindFlexKinetics\WellnessGuide\Advance\Mindset;
// use App\Models\MindFlexKinetics\WellnessGuide\Advance\Nutrition;
// use App\Models\MindFlexKinetics\WellnessGuide\Advance\Supplementation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class FourWeekWellnessGuideController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function beginnerLevel(Request $request, $action){    
        if($action == 'movement'){
            // $data = Movement::get();
            $data = DB::table('wellness_begin_movements')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'mindset'){
            // $data = Mindset::get();
            $data = DB::table('wellness_begin_mindset')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'nutrition'){
            // $data = Nutrition::get();
            $data = DB::table('wellness_begin_nutrition')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'supplementation'){
            // $data = Supplementation::get();
            $data = DB::table('wellness_begin_supplementations')->get();
            $result = $this->returnStatus($data);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Data Not Found' ], 401);
        }
    	return $result;
    }

    public function intermediateLevel(Request $request, $action){
        if($action == 'movement'){
            // $data = Movement::get();
            $data = DB::table('wellness_intermediate_movements')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'mindset'){
            // $data = Mindset::get();
            $data = DB::table('wellness_intermediate_mindset')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'nutrition'){
            // $data = Nutrition::get();
            $data = DB::table('wellness_intermediate_nutrition')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'supplementation'){
            // $data = Supplementation::get();
            $data = DB::table('wellness_intermediate_supplementation')->get();
            $result = $this->returnStatus($data);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Data Not Found' ], 401);
        }
        return $result;
    }

    public function advanceLevel(Request $request, $action){
        if($action == 'movement'){
            // $data = Movement::get();
            $data = DB::table('wellness_advance_movement')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'mindset'){
            // $data = Mindset::get();
            $data = DB::table('wellness_advance_mindset')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'nutrition'){
            // $data = Nutrition::get();
            $data = DB::table('wellness_advance_nutrition')->get();
            $result = $this->returnStatus($data);
        }else if($action == 'supplementation'){
            // $data = Supplementation::get();
            $data = DB::table('wellness_advance_supplementation')->get();
            $result = $this->returnStatus($data);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Data Not Found' ], 401);
        }
        return $result;
    }
}
