<?php

namespace App\Http\Controllers\API\mindFlexKinetics;

use Validator;
use App\Models\MindFlexKinetics\DailyStretch;
use App\Models\MindFlexKinetics\DailyStretchStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class DailyStretchesController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function dailyStretchesList(Request $request){
    	$data = DailyStretch::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function dailyStretchesStatusAdd(Request $request){
        $this->validate($request, [
            'customer_id' => 'required',
            'daily_stretch_id' => 'required',
            'status' => 'required',
        ]);

        $check = DailyStretchStatus::where(['daily_stretch_id'=>$request->input('daily_stretch_id')])->get();
        if($check->toArray()){
            $statusData = [
                'status' => trim($request->input('status')),
            ];
            DailyStretchStatus::where(['daily_stretch_id'=>$request->input('daily_stretch_id'), 'customer_id'=>$request->input('customer_id')])->update($statusData); 
            return response()->json([ 'success' => true, 'message' => 'Status updated successfully' ], 200);
        }else{
            $data = [
                'customer_id' => trim($request->input('customer_id')),
                'daily_stretch_id' => trim($request->input('daily_stretch_id')),
                'status' => trim($request->input('status')),
            ];
            $data = DailyStretchStatus::create($data);
            return response()->json([ 'success' => true, 'message' => 'Status added successfully' ], 200);
        }
    }

    public function dailyStretchesStatusGet(Request $request){
        $this->validate($request, [
            'customer_id' => 'required',
            'daily_stretch_id' => 'required',
        ]);
        $data = DailyStretchStatus::where(['daily_stretch_id'=>$request->input('daily_stretch_id'), 'customer_id'=>$request->input('customer_id')])->get();

        $result = $this->returnStatus($data);
        return $result;
    }
}
