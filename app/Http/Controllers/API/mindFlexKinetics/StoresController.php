<?php

namespace App\Http\Controllers\API\mindFlexKinetics;

use Validator;
use App\Models\MindFlexKinetics\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class StoresController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function storesList(Request $request){
    	$data = Store::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }
}
