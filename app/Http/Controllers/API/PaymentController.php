<?php

namespace App\Http\Controllers\API;

use App\Models\Payment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
	public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }
    
    public function stripeKeyList(Request $request){
        $data = Payment::where('key_status', 1)->get();
        $result = $this->returnStatus($data);
        return $result;
    }
}
