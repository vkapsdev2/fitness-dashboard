<?php

namespace App\Http\Controllers\API;

use DB;
use Validator; 
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShakeAndBar;
use App\Models\Ingredient;
use App\Models\Coupon;

class ShakeBarController extends Controller
{
	public function returnStatus($data){
		if($data->toArray()){
    		return response()->json([
    	   'success' => true,
            'data' => $data
        ], 200);

    	}else{
    		return response()->json([
    		'success' => false,
            'message' => 'Data Not Found'
        ], 401);
    	}
	}

    public function shakeBarList(Request $request){
    	$data = ShakeAndBar::get();
        $ingredient = Ingredient::get(['id','name','price']);
    	 if(count($data) > 0){
            return response()->json([
               'success' => true,
                'data' => $data,
                'ingredient' => $ingredient
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Not Found'
            ], 401);
        }
    }
  /*  public function shakeDetail(Request $request){
        $request->validate([
            'shake_bar_id'    => 'required',
        ]);

        $data = ShakeAndBar::where('id', $request->shake_bar_id)->get();
        $ingredient = Ingredient::get(['id','name','price']);

        if(count($data) > 0){
            return response()->json([
               'success' => true,
                'data' => $data,
                'ingredient' => $ingredient
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Not Found'
            ], 401);
        }
    }*/

    public function discount(Request $request){
        $request->validate([
            'code'    => 'required',
        ]);
        // get the current time
        $current = Carbon::now()->format('Y-m-d');
        $new_data = array();
        $data = Coupon::where('code', $request->code)->get();
        if($data->toArray()){

            $validity = $data[0]->validity;
            $new_data['id'] = $data[0]->id;
            $new_data['method'] = $data[0]->method;
            $new_data['discount_rate'] = $data[0]->discount_rate;
            $coupon_status = $data[0]->coupon_status;

            if($coupon_status == 1){
                if($current <= $validity){

                    return response()->json([
                   'success' => true,
                    'data' => $new_data,
                ], 200);
             }  
             else{
                return response()->json([
                    'success' => false,
                    'message' => 'Coupon code has expired'
                ], 401);
             }
                 
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Coupon does not active'
                ], 401);
            }

        }else{
            return response()->json([
                'success' => false,
                'message' => 'Invalid Discount Code'
            ], 401);
        }
        

    }
}
