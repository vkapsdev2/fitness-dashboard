<?php

namespace App\Http\Controllers\API;

use Validator;
use Hash;
use DB;
use Carbon\Carbon; 
use App\User;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Customer;
use App\Models\CustomerAttendeance;
use App\Models\customerFavExercise;
use Spatie\Permission\Models\Role;
use App\Models\CustomersFoodLog;

use App\Models\CustomWorkout;
use App\Models\CustomWorkoutDetail;
use App\Models\MindFlexPlanner\Movement\GymWorkout;
use App\Models\MindFlexPlanner\Movement\SubGymWorkout;


class CustomerController extends Controller{

	public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function customerDetails(Request $request){
        $access_token = $request->header('Authorization');
        $auth_header = explode(' ', $access_token);
        $token = $auth_header[1];
        $token_parts = explode('.', $token);
        $token_header = $token_parts[1];
        $token_header_json = base64_decode($token_header);
        $token_header_array = json_decode($token_header_json, true);
        $user_token = $token_header_array['jti'];
        $id = DB::table('oauth_access_tokens')->where('id', $user_token)->value('user_id');

		$data = DB::table('users')
			->join('customers', 'customers.customer_id','users.id')
			->join('model_has_roles',function($join) {
				$join->on('model_has_roles.model_id' , '=' , 'users.id')
				->where('model_has_roles.role_id','=', 3);
			})->where('users.id', $id)->get();

        $result = $this->returnStatus($data);
        return $result;
    }
    
    public function customWorkoutList(Request $request){
        $data = GymWorkout::get();
        foreach ($data as $key => $value) {
            $id = $value->id;
            $data[$key]['movement_sub_gymworkouts'] = DB::table('movement_sub_gymworkouts')->where('gymWorkout_id', '=', $id)->get();
        }

        $result = $this->returnStatus($data);
        return $result;
    }

    public function customWorkouts(Request $request){
        $request->validate([
            'customer_id'    => 'required',
        ]);

        $data = CustomWorkout::where('customer_id',$request->input('customer_id'))->get();

        $result = $this->returnStatus($data);
        return $result;
    }

    public function customWorkoutAdd(Request $request){
        $request->validate([
            'title'          => 'required|string',
            'customer_id'    => 'required',
        ]);

        $customer_id = $request->input('customer_id');

        if(User::find($customer_id)){
             $checkTitle = CustomWorkout::where('title', '=', $request->input('title'))->exists();
            if(!$checkTitle){

                $workout = new CustomWorkout([
                    'customer_id'     => $request->customer_id,
                    'title'           => $request->title
                ]);
                $workout->save();

                return response()->json([ 'success' => true, 'message' => "Customer's custom Workout added successfully",'id'=>$workout['id'] ],200);

            }else{
                return response()->json([ 'error' => true, 'message' => 'Custom Workout already exist' ],400);
            }
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer does not find' ],400);
        }
    }

    public function customWorkoutDetailAdd(Request $request){
        $request->validate([
            'custom_workout_id'          => 'required',
            'gym_category_id'            => 'required',
            'gym_category_exercise_id'   => 'required',
        ]);

        $workout_id = $request->input('custom_workout_id');
        $gym_category_id = $request->input('gym_category_id');
        $gym_category_exercise_id = $request->input('gym_category_exercise_id');
        $custom_workout_detail_ids = array();
        if(CustomWorkout::find($workout_id)){

            if(GymWorkout::find($gym_category_id)){

               if(SubGymWorkout::whereIn('id', $gym_category_exercise_id)){

                foreach ($gym_category_exercise_id as $key => $value) {

                    $workout_detail = new CustomWorkoutDetail([
                    'customer_workout_id'       => $request->custom_workout_id,
                    'gym_category_id'           => $request->gym_category_id,
                    'gym_category_exercise_id'  => $value
                    ]);
                    $workout_detail->save();

                    array_push($custom_workout_detail_ids, $workout_detail['id']);
                }
                return response()->json([ 'success' => true, 'message' => "Customer's custom Workout details added successfully", "ids"=>$custom_workout_detail_ids ],200);  
                }
                else{
                    return response()->json([ 'error' => true, 'message' => 'Gym Category exercises do not exist' ],400);
                 }
            }
            else{
                return response()->json([ 'error' => true, 'message' => 'Gym Category does not exist' ],400);
            }
        }
        else{
            return response()->json([ 'error' => true, 'message' => 'Custom Workout does not find' ],400);
        }
    }

    public function customWorkoutSets(Request $request){
        $request->validate([
            'custom_workout_detail_id'   => 'required',
            'sets'                       => 'required',
        ]);

        $detail_id = $request->input('custom_workout_detail_id');

        if(CustomWorkoutDetail::find($detail_id)){

            $data = json_encode($request->sets);

            $setsArray = [
                 'sets'   => $data
            ];
            DB::table('cus_custom_workout_detail')->where('id', $detail_id)->update($setsArray);
        
        return response()->json([ 'success' => true, 'message' => "custom Workout Sets added successfully" ],200);  
            
        }
        else{
            return response()->json([ 'error' => true, 'message' => 'Custom Workout does not find' ],400);
        }
    }

    public function customWorkoutDetailList(Request $request){

        $request->validate([
            'customer_workout_id'    => 'required',
        ]);

        $data = CustomWorkoutDetail::where('customer_workout_id',$request->input('customer_workout_id'))->get();

        foreach ($data as $key => $value) {
            $customer_workout_id = $value->customer_workout_id;
            $gym_category_id = $value->gym_category_id;
            $gym_category_exercise_id    = $value->gym_category_exercise_id ;

            $data[$key]['custom_workout'] = DB::table('cus_custom_workout')->where('id', '=', $customer_workout_id)->get();

            $data[$key]['category'] = DB::table('movement_gymworkouts')->where('id', '=', $gym_category_id)->get();

            $data[$key]['category_exercise'] = DB::table('movement_sub_gymworkouts')->where('id', '=', $gym_category_exercise_id)->get();
        }

        $result = $this->returnStatus($data);
        return $result;

    }
    public function customWorkoutExerciseDelete(Request $request){
        $request->validate([
            'custom_workout_detail_id'   => 'required',
        ]);

        $detail_id = $request->input('custom_workout_detail_id');

        if(CustomWorkoutDetail::find($detail_id)){

            CustomWorkoutDetail::where('id',$detail_id)->delete();
        
        return response()->json([ 'success' => true, 'message' => "custom Workout Delete successfully" ],200);  
            
        }
        else{
            return response()->json([ 'error' => true, 'message' => 'Custom Workout does not find' ],400);
        }
    }

    public function customerFavExercise(Request $request){

        $request->validate([
            'customer_id'   => 'required',
            'workout_type'  => 'required',
            'category_id'   => 'required',
            'exercise_id'   => 'required',
        ]);

        $data = [
            'customer_id'         => $request->input('customer_id'),
            'workout_type'        => $request->input('workout_type'),
            'category_id'         => $request->input('category_id'),
            'exercise_id'         => $request->input('exercise_id'),
            
        ];
        $data = customerFavExercise::create($data);
        return response()->json([ 'success' => true, 'message' => 'Added Successfully' ], 200); 

    }

    public function customerFavList(Request $request){
        $request->validate([
            'customer_id'   => 'required',
            'workout_type'  => 'required',
            'category_id'   => 'required',
            'exercise_id'   => 'required',
        ]);

        $data = customerFavExercise::where('customer_id',$request->input('customer_id'))->where('workout_type',$request->input('workout_type'))->where('category_id',$request->input('category_id'))->where('exercise_id',$request->input('exercise_id'))->get();

 

        if($data->toArray()){
            return response()->json([
           'success' => true,
            'like' => true
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'like' => false
        ], 401);
        }


    }
    /*public function testSocialLogin(Request $request){

        $data = [
            'data'         => $request->input('data'),
            
        ];
        $data = DB::table('test_social_login')->insert($data);
        return response()->json([ 'success' => true, 'message' => 'Added Successfully' ], 200); 

    }*/

    public function attendence(Request $request){
        $request->validate([
            'customer_id'   => 'required',
        ]);

        $customer_id = $request->customer_id; 

        $res = User::find($customer_id);
        $current = Carbon::now()->format('Y-m-d');

        if($res){
                $check_user = CustomerAttendeance::where('customer_id',$customer_id)->whereDate('created_at', $current)->exists();


                if($check_user){

                     return response()->json(['error' => true, 'Message' => 'Already added' ], 400);
                }else{

                    $data = new CustomerAttendeance([
                        'customer_id'   => $customer_id,
                        'name'          => $request->name,
                        'email'         => $request->email,
                        'QR_id'         => $request->QR_id,
                    ]);
                    $data->save();

                    return response()->json([ 'success' => true, 'message' => 'Submitted successfully', 'name' => $request->name],200);
                }
            /* $res = DB::table('customer_attendence')
                    ->where('customer_id',$customer_id)
                    ->select(DB::raw('count(customer_id) as `total_count`'),
                    DB::raw('YEAR(created_at) year'))
                    ->groupBy(DB::raw('YEAR(created_at)'))
                    ->get();

            return response()->json([ 'success' => true, 'response' => $res ],200);*/
        }
        else{
            return response()->json(['error' => true, 'Message' => 'Customer Does not exists' ], 400);
        }
       

    }
}
