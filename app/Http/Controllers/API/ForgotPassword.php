<?php

namespace App\Http\Controllers\API;

use Mail;
use DB;
use Validator;
use App\User;
use App\Models\ForgotPassword as ForgotPasswordNew;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ForgotPassword extends Controller
{
	public function checkEmail(Request $request){
		$request->validate([
            'email'    => 'required|string|email'
        ]);

        $check = User::where('email', '=', $request->input('email'))->get();
        if($check->toArray()){
        	$email = $check[0]->email;
        	$otp = rand(1000,9999);
        	$addData = [
        		'customer_email' => $request->input('email'),
        		'otp_number'  => $otp,
        		'time'        => date("Y-m-d h:i:s"),
        	];
        	
        	/* Send mail here !! */
        	$message = 'Test';
        	$data = [
                'otp' => $otp,
                'email' => $request->input('email'),
            ];

			\Mail::send('pages.admin.emails.contact-us.forgot-password',['data'=>$data], function($message) use ($data){
	        	$message->to($data['email']);
	        	$message->subject('Password Reset');
	    	});

            ForgotPasswordNew::create($addData);

        	return response()->json([ 'success' => true, 'message' => 'OTP send on your email' ],200);
        }else{
        	return response()->json([ 'error' => true, 'message' => 'Email does not exits' ],400);
        }
	}

	public function checkOTP(Request $request){
		$request->validate([
            'otp_number'  => 'required',
            'email' => 'required',
            'password'    =>'required'
        ]); 
        $data = [
        	"customer_email" => $request->input('email'),
        ];
        $otp_number = $request->input('otp_number');
        $email = $request->input('email');
        $check = ForgotPasswordNew::where($data)->get();
        if($check->toArray()){
        	$oldTime = $check[0]->time;
        	$currentTime = date("Y-m-d h:i:s");
        	// $to_time = strtotime("2008-12-13 10:42:00");
        	$to_time = strtotime($oldTime);
			// $from_time = strtotime("2008-12-13 10:40:00");
			$from_time = strtotime($currentTime);
			$timeDiff = round(abs($to_time - $from_time) / 60,2);
			
        	if($timeDiff <= 5){
        		$otp = $check[0]->otp_number;
        		if($otp == $otp_number){
                    $passwordUpdate = [
                        'password'=> bcrypt($request->password),
                    ];
                    User::where('email',$email)->update($passwordUpdate);
                    DB::table('forgot_password')->where('customer_email', $email)->delete();
        			return response()->json([ 'success' => true, 'message' => 'success' ],200);
        		}else{
        			return response()->json([ 'error' => true, 'message' => 'OTP not matched' ],400);
        		}
        	}else{
        		DB::table('forgot_password')->where('customer_email', $email)->delete();
        		return response()->json([ 'error' => true, 'message' => 'Your OTP is expired' ],400);
        	}
        }else{
        	return response()->json([ 'error' => true, 'message' => 'This OTP not found, please try again' ],400);
        }
	}
}
