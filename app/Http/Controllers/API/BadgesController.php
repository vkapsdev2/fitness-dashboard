<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\Badge;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class BadgesController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }
    
    public function badgesList(Request $request){
        $data = Badge::get();
        $result = $this->returnStatus($data);
        return $result;
    }

    public function customerBadgesList(Request $request){
        $access_token = $request->header('Authorization');
        $auth_header = explode(' ', $access_token);
        $token = $auth_header[1];
        $token_parts = explode('.', $token);
        $token_header = $token_parts[1];
        $token_header_json = base64_decode($token_header);
        $token_header_array = json_decode($token_header_json, true);
        $user_token = $token_header_array['jti'];

        $id = DB::table('oauth_access_tokens')->where('id', $user_token)->value('user_id');

        $data = DB::table('customer_badges')->where('customer_id', $id)->get();

        $id = $data[0]->badge_id;

        $data = DB::table('badges')->where('id', $id)->get();
        $result = $this->returnStatus($data);
        return $result;
    }
}
