<?php

namespace App\Http\Controllers\API;

use DB;
use App\User;
use App\Models\Customer; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerNotification extends Controller
{
    public function list(Request $request){

    	$checkUser = Customer::where('customer_id', $request->customer_id)->first();

    	if($checkUser){

    		$data = DB::table('cus_notification')
                ->join('customers','customers.customer_id','cus_notification.customer_id')
                ->where('cus_notification.customer_id',$request->customer_id)
                ->select('cus_notification.id','cus_notification.title','cus_notification.description','customers.customer_name','customers.customer_id')->get();

	        if(count($data) > 0){
	            return response()->json(['success'=>'true', 'data' => $data], 200);
	        } 
	        return response()->json(['error' =>true, 'message' =>'Data not Found'],201);
    	}

    	return response()->json(['error' =>true, 'message' =>'Customer Does not exist'],401);
    	
    }

    
}
