<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\QuoteOfDay;
use App\Models\QuoteLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuoteOfTheDayController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
	           'success' => true,
	            'data' => $data
	        ], 200);

        }else{
            return response()->json([
	            'success' => false,
	            'message' => 'Data Not Found'
	        ], 401);
        }
    }

    public function quoteList(Request $request){
    	$currentDate =  Date('yy-m-d');
    	$data = QuoteOfDay::whereDate('quote_date','=',$currentDate)->first();
    	if ($data) {
    		return response()->json([
	           'success' => true,
	            'data' => $data
	        ], 200);
    	}else{
    		$data = QuoteOfDay::whereDate('quote_date','<',$currentDate)->first();
    		return response()->json([
	           'success' => true,
	            'data' => $data
	        ], 200);
    	}
    	/*return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);*/

        /*$data = QuoteOfDay::get();
        $result = $this->returnStatus($data);
        return $result;*/
    }

    public function addQuoteLog(Request $request){
   		$this->validate($request, [
            'customer_id' => 'required',
            'angry' => 'required',
	        'anxious' => 'required',
	        'calm' => 'required',
	        'depressed' => 'required',
	        'fearful' => 'required',
	        'grateful' => 'required',
	        'happy' => 'required',
	        'helpless' => 'required',
	        'relaxed' => 'required',
	        'sad' => 'required',
	        'stressed' => 'required',
	        'tired' => 'required',
	        'optimistic' => 'required',
	        'something_good' => 'required',
	        //'hours_sleep' => 'required',
	       // 'quality_sleep' => 'required',
        ]);
        $data = [
        	'customer_id' => $request->input('customer_id'),
        	'date' => date('Y-m-d'),
        	'angry' => $request->input('angry'),
	        'anxious' => $request->input('anxious'),
	        'calm' => $request->input('calm'),
	        'depressed' => $request->input('depressed'),
	        'fearful' => $request->input('fearful'),
	        'grateful' => $request->input('grateful'),
	        'happy' => $request->input('happy'),
	        'helpless' => $request->input('helpless'),
	        'relaxed' => $request->input('relaxed'),
	        'sad' => $request->input('sad'),
	        'stressed' => $request->input('stressed'),
	        'tired' => $request->input('tired'),
	        'optimistic' => $request->input('optimistic'),
	        'something_good' => $request->input('something_good'),
	        'hours_sleep' => $request->input('hours_sleep'),
	        'quality_sleep' => $request->input('quality_sleep'),
        ];
        $data = QuoteLog::create($data);
        return response()->json([ 'success' => true, 'message' => 'Quote log add successfully' ], 200);   
    }

    public function listQuoteLog(Request $request){
   		$this->validate($request, [
            'customer_id' => 'required',
        ]);
        $data = QuoteLog::where(['customer_id'=>$request->input('customer_id')])->get();
        $result = $this->returnStatus($data);
        return $result;
    }
}
