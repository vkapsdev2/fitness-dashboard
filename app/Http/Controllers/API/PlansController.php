<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\Plan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlansController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }
    
    public function plansList(Request $request){
        $data = Plan::get();
        $result = $this->returnStatus($data);
        return $result;
    }
}
