<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function eventList(Request $request){
    	$events = Event::get();
    	if($events->toArray()){
    		return response()->json([
    	   'success' => true,
            'data' => $events
        ], 201);
    	}else{
    		return response()->json([
    		'success' => false,
            'message' => 'No Events Found'
        ], 401);
    	}
    }
}
