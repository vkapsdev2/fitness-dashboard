<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\Contact;
use App\Models\Social;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Mail;
use Mail;

class ContactUsController extends Controller{

    public function contactUs(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email'=> 'required|email',
            'query' => 'required'
        ]);
        $contactData = [
            'name'          => trim($request->input('name')),
            'email'         => trim($request->input('email')),
            'query'         => trim($request->input('query')),
            'date'          => date('Y-m-d'),
            'replied_by_us' => '0',
            'contact_status'=>'1',
            
        ];
        $data = Contact::create($contactData);
        return response()->json([ 'success' => true, 'message' => 'here is your message' ], 200);   
    }
    
}