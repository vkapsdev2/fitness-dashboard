<?php

namespace App\Http\Controllers\API;

use Validator;
use Hash;
use DB;
use Carbon\Carbon; 
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Customer;
use App\Models\Plan;
use App\Models\Coach;
use App\Models\CustomersFoodLog;
use App\Models\CustomerBodyMeasurementLog;
use App\Models\CustomerSatisfactionLog;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{	
	public function headerData($contentType, $xRequestedWith){
        if(($contentType == 'text/plain') && ($xRequestedWith == 'XMLHttpRequest')){
            return response()->json([
                'success' => false,
                'message' => 'header Content-Type = application/json is not found'
            ],401);
        }else if(($contentType == 'application/json') && ($xRequestedWith == '')){
            return response()->json([
                'success' => false,
                'message' => 'header X-Requested-With = XMLHttpRequest is not found'
            ],401);
        }else if(($contentType == 'text/plain') && ($xRequestedWith == '')){
            return response()->json([
                'success' => false,
                'message' => 'header X-Requested-With = XMLHttpRequest and Content-Type = application/json is not found'
            ],401);
        }else{
            return true; 
        }
    }

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request){
        $contentType = $request->header('content-type');
        $xRequestedWith = $request->header('x-requested-with');
        $result = $this->headerData($contentType, $xRequestedWith);

        if($result){
            $request->validate([
                'name'     => 'required|string',
                'email'    => 'required|string|email',
                'password' => 'required|string|confirmed',
                'roles'    => 'required',
            ]); 

            if($request->roles == 3){
                $checkEmail = User::where('email', '=', $request->input('email'))->exists();
                if(!$checkEmail){

                    /*if ($files = $request->file('image')) {
                        $destinationPath = public_path().'/media/customer-image';
                        $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
                        $files->move($destinationPath, $fileName);
                    }else{
                        $fileName = "";
                    }
                    */
                    if ($image_64 = $request->image) {
                        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

                        $replace = substr($image_64, 0, strpos($image_64, ',')+1); 

                       $image = str_replace($replace, '', $image_64); 

                       $image = str_replace(' ', '+', $image); 

                       $imageName = date('YmdHis').'.'.$extension;
                      

                        \File::put(public_path().'/media/customer-image'. '/' . $imageName, base64_decode($image));
                    }else{
                        $imageName = "";
                    }

                    $customer = new User([
                        'name'     => $request->name,
                        'email'    => $request->email,
                        'password' => bcrypt($request->password),
                        'phone'    => $request->input('phone'),
                        'status'   => 1,
                        'image'    => $imageName,
                        'provider' => 'manual',
                    ]);
                    $customer->save();

                    $customer_personal = Customer::create([
                        'customer_id'   => $customer['id'],
                        // 'customer_name' => $request->input('name'),
                        'customer_name' => $request->name,
                        'plan' => '11',
                    ]);
                
                    $customer_role = 3;
                    $customer->assignRole($customer_role);
                    return response()->json([ 'success' => true, 'message' => 'Customer created successfully' ],200);
                }else{
                    return response()->json([ 'error' => true, 'message' => 'Email already exist' ],200);
                }
            }    
        }else{
            return $result;
        }
    }


    public function login(Request $request){
    	$request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            //'provider' => 'required|string',
            'remember_me' => 'boolean',
            'device_token' => 'required'
        ]);
        $checkEmail = User::where('email', '=', $request->input('email'))->exists();
        if(!$checkEmail){
            return response()->json([ 'error' => true, 'message' => 'User does not exist' ],200);
        }else{
           //$check_provider = User::where("provider", $request->input('provider'))->where('email',$request->input('email'))->first();

            $credentials = request(['email', 'password']);

            //$provider = User::where("email", $request->input('email'))->first();
     
            //if($check_provider){

                if(!Auth::attempt($credentials)){
                return response()->json(['error' => true, 'message' => 'Invalid email or password' ], 400);
                }
                else{

                    $user = $request->user();
                    $tokenResult = $user->createToken('Personal Access Token');

                    $token = $tokenResult->token;

                    if ($request->remember_me)
                        $token->expires_at = Carbon::now()->addWeeks(1);

                    $token->save();

                    
                    $role = DB::table('model_has_roles')->where('model_id',$user['id'])->get(['role_id']);

                    User::where('email',$request->email)->update(['device_token' =>$request->device_token]);
                    $plan = Customer::where('customer_id', $user['id'])->get(['plan']);
                    $plan_data = Plan::where('id', 15)->get(['plan_title']);
                    if(count($plan_data) > 0){
                        $plan_name = $plan_data[0]->plan_title;

                    }else{
                        $plan_name = 'Free';
                    }
                    return response()->json([
                        'access_token'  => $tokenResult->accessToken,
                        'token_type'    => 'Bearer',
                        'customer_id'   => $user['id'],
                        'name'          => $user['name'],
                        'phone'         => $user['phone'],
                        'image'         => $user['image'],
                        'plan'          => $plan_name,
                        'roles'         => $role[0]->role_id,
                        'expires_at'    => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString()
                    ]);
                }
            /*} else{

                return response()->json(['error' => true, 'provider' => $provider->provider ], 400);
            } */
        }
        
        
    } 
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request){
        $request->user()->token()->revoke();
        return response()->json([ 'message' => 'Successfully logged out', 'success' => true ]);
    }

    /*public function passwordUpdate(Request $request){

        $request->validate([
            'email' => 'required',
            'password'=>'required'
        ]);

        $checkEmail = User::where('email', $request->email)->exists();

        if(!$checkEmail){
            return response()->json([ 'error' => true, 'message' => 'User does not exist' ],401);
        }else{
           
            $passwordUpdate = [
                'password'=> bcrypt($request->password),
            ];
            User::where('email',$request->email)->update($passwordUpdate);
            return response()->json([ 'success' => true, 'message' => 'password update successfully!' ],200);
        }
    }*/
    public function signupDetailGet(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $userData = DB::Table('users')->select('name', 'email', 'phone', 'status', 'image')->where('id', $customer_id)->get();
            $result = $this->returnStatus($userData);
            return $result;
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        }
    }

    public function signupDetailUpdate(Request $request){
        $request->validate([
            'customer_id' => 'required',
            'name'        => 'required',
            'email'       => 'required|string|email',
            //'password'    => 'required',
        ]);

        $customer_id = $request->input('customer_id');
        $userDetail = User::find($customer_id);
        
        if($userDetail){ 
            if ($image_64 = $request->image) {

                $image_path = public_path().'/media/customer-image'.$userDetail['image'];
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
                
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

                $replace = substr($image_64, 0, strpos($image_64, ',')+1); 

               $image = str_replace($replace, '', $image_64); 

               $image = str_replace(' ', '+', $image); 

               $imageName = date('YmdHis').'.'.$extension;
              

                \File::put(public_path().'/media/customer-image'. '/' . $imageName, base64_decode($image));
            }else{
                $imageName = $userDetail['image'];
            }

            $phone = $request->input('phone');
            if($phone){
                $phone = $request->input('phone');
            }else{
                $phone = '';
            }

            $userArray = [
                'name'      => $request->input('name'),
                'email'     => $request->input('email'),
                'phone'     => $phone,
                'status'    => 1,
                'image'     => $imageName,
                'password'  => bcrypt($request->password),
            ];

            $customerArray = [
                'customer_name' => $request->input('name'),
            ];

            DB::table('users')->where('id', $customer_id)->update($userArray);
            DB::table('customers')->where('customer_id', $customer_id)->update($customerArray);
            return response()->json([ 'success' => true, 'message' => "Customer updated successfull" ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        } 
    }

    public function checkUserPhone(Request $request){
        $request->validate([
            'phone'       => 'required',
        ]);
        $phone_no = $request->input('phone');
        $result  = DB::table('users')->where('phone', $phone_no)->get();

        if($result->toArray())
            return response()->json([ 'success' => false, 'exist' => true ],200);
        else
            return response()->json([ 'success' => true, 'exist' =>false],200);
    }
    public function customerPersonalInformation(Request $request){
        $request->validate([
            'customer_id'   => 'required',
            'gender'        => 'required',
            'level'         => 'required',
            'age'           => 'required',
            'height'        => 'required',
            'weight'        => 'required|min:1|numeric',
            'body_fat_percent'    => 'required|between:0,99.99',
            'skletal_muscle_mass' => 'required|min:1|numeric',
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $userArray = ['gender' => $request->input('gender')];

            $customerArray = [
                'level'         => $request->input('level'),
                'age'           => $request->input('age'),
                'weight'        => $request->input('weight'),
                'height'        => $request->input('height'),
                'body_fat_percent'    => $request->input('body_fat_percent'),
                'skletal_muscle_mass' => $request->input('skletal_muscle_mass'),
                // 'body_fat_mass'        => $request->input('body_fat_mass'),
            ];

            DB::table('users')->where('id', $customer_id)->update($userArray);
            DB::table('customers')->where('customer_id', $customer_id)->update($customerArray);
            return response()->json([ 'success' => true, 'message' => 'Personal information updated successfull' ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        }
    }

    public function cusPersonalInformationDetail(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $userData = DB::Table('users')->select('gender')->where('id', $customer_id)->get();
            $customerData = DB::Table('customers')->select('level', 'age', 'weight', 'height', 'body_fat_percent', 'skletal_muscle_mass')->where('customer_id', $customer_id)->get(); 
            $customerData[0]->gender = $userData[0]->gender;
            $result = $this->returnStatus($customerData);
            return $result;
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        }
    }

    public function customerBodyMeasurement(Request $request){
        $request->validate([
            'customer_id'           => 'required',
            'shoulder'              => 'required|numeric',
            'right_bicep'           => 'required|numeric',
            'chest'                 => 'required|numeric',
            'waist'                 => 'required|numeric',
            'hips'                  => 'required|numeric',
            'right_thigh'           => 'required|numeric',
            'right_calf'            => 'required|numeric',
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){

            $customerArray = [
                'shoulder'        => $request->input('shoulder'),
                'right_bicep'     => $request->input('right_bicep'),
                'chest'           => $request->input('chest'),
                'waist'           => $request->input('waist'),
                'belly'           => $request->input('belly'),
                'hips'            => $request->input('hips'),
                'right_thigh'     => $request->input('right_thigh'),
                'right_calf'      => $request->input('right_calf'),
            ];
            DB::table('customers')->where('customer_id', $customer_id)->update($customerArray);
            return response()->json([ 'success' => true, 'message' => "Customer's Body Measurements updated successfull" ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],200);
        } 
    }

    public function custBodyMeasurementDetail(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $customerData = DB::Table('customers')->select('shoulder', 'right_bicep', 'chest', 'waist', 'belly', 'hips', 'right_thigh', 'right_calf')->where('customer_id', $customer_id)->get(); 
            $result = $this->returnStatus($customerData);
            return $result;
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        }
    }

    public function customerSatisfaction(Request $request){
        $request->validate([
            'customer_id'     => 'required',
            'career'          => 'required',
            'relationship'    => 'required',
            'joy'             => 'required',
            'health'          => 'required',
            'nutrition'       => 'required',
            'sleep'           => 'required',
            'appearance'      => 'required',
        ]);
        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $customerArray = [
               'career'            => $request->input('career'),
               'relationship'      => $request->input('relationship'),
               'joy'               => $request->input('joy'),
               'health'            => $request->input('health'),
               'nutrition'         => $request->input('nutrition'),
               'sleep'             => $request->input('sleep'),
               'appearance'        => $request->input('appearance'),

               'purpose'           => $request->input('purpose'),
               'social'            => $request->input('social'),
               'spiritual'         => $request->input('spiritual'),              
               'ready_for_change'  => $request->input('ready_for_change'),   

            ];
            DB::table('customers')->where('customer_id', $customer_id)->update($customerArray);

            /* Cus Satis Log */
            $satLogArray = [
               'customer_id'       => $request->input('customer_id'),
               'log_date'          => $request->input('log_date'),
               'career'            => $request->input('career'),
               'relationship'      => $request->input('relationship'),
               'joy'               => $request->input('joy'),
               'health'            => $request->input('health'),
               'nutrition'         => $request->input('nutrition'),
               'sleep'             => $request->input('sleep'),
               'appearance'        => $request->input('appearance'),

               'purpose'           => $request->input('purpose'),
               'social'            => $request->input('social'),
               'spiritual'         => $request->input('spiritual'),              
               'ready_for_change'  => $request->input('ready_for_change'),   

            ];
            $check = [
                'customer_id' => $request->input('customer_id'),
                'log_date'    => $request->input('log_date'),
            ];
            $data = CustomerSatisfactionLog::where($check)->get();
            if(sizeof($data) != 0){
                DB::table('cus_satisfaction_log')->where($check)->update($satLogArray);
            }else{
                CustomerSatisfactionLog::create($satLogArray);
            }

            return response()->json([ 'success' => true, 'message' => "Customer's Satisfaction updated successfull" ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],200);
        } 
    }

    public function cusSatisfactionDetail(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $customerData = DB::Table('customers')->select('career', 'relationship', 'joy', 'health', 'nutrition', 'sleep', 'appearance', 'purpose', 'social', 'spiritual', 'ready_for_change')->where('customer_id', $customer_id)->get(); 
            $result = $this->returnStatus($customerData);
            return $result;
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        }
    }

    public function cusSatisfactionHistory(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        
        $satData = DB::Table('cus_satisfaction_log')->where('customer_id', $customer_id)->get();
        $result = $this->returnStatus($satData);
        return $result;
    }

    public function cusSatisfactionLogDelete(Request $request){
        $request->validate([
            'log_id' => 'required',
            'customer_id' => 'required',
            'log_date' =>'required'
        ]);

        $res = CustomerSatisfactionLog::where('id',$request->log_id)->where('customer_id',$request->customer_id)->where('log_date', $request->log_date)->first();

        if($res){
            $res->delete();
            return response()->json(['success' => true, 'message' => 'Log data deleted successfully'], 200);
        }else{
            return response()->json(['error' => true,'message' => 'Data not found'],401);
        }
    }
    public function customerPersonalInfoQuestion(Request $request){
        $request->validate([
            'customer_id'     => 'required',
            'social_goal'     => 'required',
        ]);
        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $customerArray = [
               'social_goal'        => $request->input('social_goal'),
               'consistent_physical_activity' =>$request->input('consistent_physical_activity'),

               'consistent_physical_activity_time' => trim($request->input('consistent_physical_activity_time')),

               'consistent_physical_activity_description' => trim($request->input('consistent_physical_activity_description')),

               'customer_occupation' => trim($request->input('customer_occupation')),
               'houldhold' => trim($request->input('household')),

               'recreational_activity' => trim($request->input('recreational_activity')),
               'hobbies' => trim($request->input('hobbies')),

               'sleep_hours_in_detail' => trim($request->input('sleep_hours_in_detail')),
               'sleep_taking_time' => trim($request->input('sleep_taking_time')),
            ];
            DB::table('customers')->where('customer_id', $customer_id)->update($customerArray);
            return response()->json([ 'success' => true, 'message' => "Customer's Personal Info Questionaries updated successfull" ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],200);
        }
    }

    public function cusPersonalInfoQuestionDetail(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $customerData = DB::Table('customers')->select('social_goal', 'consistent_physical_activity', 'consistent_physical_activity_time', 'consistent_physical_activity_description', 'customer_occupation', 'customers.houldhold AS household', 'recreational_activity', 'hobbies', 'sleep_hours_in_detail', 'sleep_taking_time')->where('customer_id', $customer_id)->get(); 
            $result = $this->returnStatus($customerData);
            return $result;
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        }
    }

    public function customerMedicalProfile(Request $request){
            $request->validate([
                'customer_id'     => 'required',
            ]);
            $customer_id = $request->input('customer_id');
            if(User::find($customer_id)){

                $customerArray = [
                   'last_physical_examination_time' => trim($request->input('last_physical_examination_time')),
                   'ever_been_hospitalized' => trim($request->input('ever_been_hospitalized')),
                   'is_pregnant' => trim($request->input('is_pregnant')),   
                   'smoking' => trim($request->input('smoking')),
                   'alcoholic_quantity_per_week' => trim($request->input('alcoholic_quantity_per_week')),
                   'diagnosed_heart_condition' => trim($request->input('diagnosed_heart_condition')),
                   'is_feel_chest_pain' => trim($request->input('is_feel_chest_pain')),
                   'did_feel_chest_pain_when_not_physical' => trim($request->input('did_feel_chest_pain_when_not_physical')),
                   'is_feel_loss_consciousness' => trim($request->input('is_feel_loss_consciousness')),

                   'is_joint_issue' => trim($request->input('is_joint_issue')),

                   'is_take_medications' => trim($request->input('is_take_medications')),

                   'is_diabetic' =>$request->input('is_diabetic'),
                   'is_high_blood_pressure' =>$request->input('is_high_blood_pressure'),
                   'is_high_cholesterol' =>$request->input('is_high_cholesterol'),
                   'is_diagnosed_osteoporosis' =>$request->input('is_diagnosed_osteoporosis'),

                   'is_breathless_to_staris' =>$request->input('is_breathless_to_staris'),

                   'do_you_have_been_cancer' =>$request->input('do_you_have_been_cancer'),

                   'activity_reson_for_not_engage_physical_activity' =>$request->input('activity_reson_for_not_engage_physical_activity'),
                ];
                DB::table('customers')->where('customer_id', $customer_id)->update($customerArray);
                return response()->json([
                    'message' => "Customer's Medical profile updated successfull"
                ],200);
            }else{
                return response()->json([
                    'error' => true,
                    'message' => 'Customer not found'
                ],200);
            }
    }

    public function cusMedicalProfileDetail(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $customerData = DB::Table('customers')->select('last_physical_examination_time', 'ever_been_hospitalized', 'is_pregnant', 'smoking', 'alcoholic_quantity_per_week', 'diagnosed_heart_condition', 'is_feel_chest_pain', 'did_feel_chest_pain_when_not_physical', 'is_feel_loss_consciousness', 'is_joint_issue', 'is_take_medications', 'is_diabetic', 'is_high_blood_pressure', 'is_high_cholesterol', 'is_diagnosed_osteoporosis', 'is_breathless_to_staris', 'do_you_have_been_cancer', 'activity_reson_for_not_engage_physical_activity')->where('customer_id', $customer_id)->get(); 
            $result = $this->returnStatus($customerData);
            return $result;
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        }
    }

    public function customerFamilyDetail(Request $request){
        $request->validate([
            'customer_id'     => 'required',
        ]);
        // print_r($request->input('family_disease_detail'));die;

        // if($request->input('family_disease_detail')!== null)
        // {
        //     $family_disease_string = implode(",", $request->input('family_disease_detail'));
        // }
        // else
        // {
        //     $family_disease_string = null;
        // }

        $customer_id = $request->input('customer_id');
         if(User::find($customer_id)){

            $customerArray = [
               'family_disease_detail' => $request->input('family_disease_detail'),

               'other_family_disease' => trim($request->input('other_family_disease')),
            ];
            DB::table('customers')->where('customer_id', $customer_id)->update($customerArray);
            return response()->json([
                'message' => "Customer's Family Disease updated successfull"
            ],200);
        }else{
            return response()->json([
                'error' => true,
                'message' => 'Customer not found'
            ],200);
        }
    }

    public function cusFamilyDetailGet(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){
            $customerData = DB::Table('customers')->select('family_disease_detail', 'other_family_disease')->where('customer_id', $customer_id)->get(); 
            $result = $this->returnStatus($customerData);
            return $result;
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        }
    }

    public function customerBodyMeasurementLog(Request $request){
        $request->validate([
            'customer_id'       => 'required',
            'shoulder'          => 'required|numeric',
            'right_bicep'       => 'required|numeric',
            'chest'             => 'required|numeric',
            'waist'             => 'required|numeric',
            'hips'              => 'required|numeric',
            'right_thigh'       => 'required|numeric',
            'right_calf'        => 'required|numeric',
        ]);

        $customer_id = $request->input('customer_id');
        if(User::find($customer_id)){

            $data = DB::table('cu_body_measurement_log')->where('customer_id', $customer_id)->where('log_date', date('d/m/yy'))->get();
           
            if(sizeof($data) == 0){
                $customer = new CustomerBodyMeasurementLog([
                    'customer_id'     => $request->input('customer_id'),
                    'shoulder'        => $request->input('shoulder'),
                    'right_bicep'     => $request->input('right_bicep'),
                    'chest'           => $request->input('chest'),
                    'waist'           => $request->input('waist'),
                    'belly'           => $request->input('belly'),
                    'hips'            => $request->input('hips'),
                    'right_thigh'     => $request->input('right_thigh'),
                    'right_calf'      => $request->input('right_calf'),
                    'log_date'        => date('d/m/yy'),
                ]);
                $customer->save();
                return response()->json([ 'success' => true, 'message' => "Customer's Body Measurements Log created successfull" ],200);
            }else{
                $customerArray = [
                    'customer_id'     => $request->input('customer_id'),
                    'shoulder'        => $request->input('shoulder'),
                    'right_bicep'     => $request->input('right_bicep'),
                    'chest'           => $request->input('chest'),
                    'waist'           => $request->input('waist'),
                    'belly'           => $request->input('belly'),
                    'hips'            => $request->input('hips'),
                    'right_thigh'     => $request->input('right_thigh'),
                    'right_calf'      => $request->input('right_calf'),
                    'log_date'        => date('d/m/yy'),
                ];
                DB::table('cu_body_measurement_log')->where('customer_id', $customer_id)->where('log_date', date('d/m/yy'))->update($customerArray);
                return response()->json([ 'message' => "Customer's Body Measurements Log updated successfull" ],200);
            }   
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        } 
    }

    
    public function measurementLogDetail(Request $request){
        $request->validate([
            'customer_id' => 'required',
            // 'log_date' => 'required',
        ]);

        $customer_id = $request->input('customer_id');
        // $log_date = $request->input('log_date');
        if(User::find($customer_id)){

            // if ( preg_match( '/(0[1-9]|1[0-9]|3[01])\/(0[1-9]|1[012])\/(2[0-9][0-9][0-9]|1[6-9][0-9][0-9])/', $log_date ) == 1 ) {
                // $data = DB::table('cu_body_measurement_log')->where('customer_id', $customer_id)->where('log_date', $log_date)->get();
                $data = DB::table('cu_body_measurement_log')->where('customer_id', $customer_id)->get();

                if(sizeof($data) != 0){
                    return response()->json([ 'success' => true, 'data' => $data ],200); 
                }else{
                    return response()->json([ 'error' => true, 'message' => "Data not found" ],400);   
                }
            // }else{
                // return response()->json([ 'error' => true, 'message' => 'Date format(dd/mm/yyyy) not matched' ],400);    
            // }
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        } 
    }

    public function measurementLogupdate(Request $request){
        $request->validate([
            'id' => 'required',
            'customer_id' => 'required',
            'log_date' => 'required',
        ]);

        $id = $request->id;
        $customer_id = $request->customer_id;
        $log_date = $request->log_date;
        $log = CustomerBodyMeasurementLog::where('customer_id',$customer_id)->where('log_date',$log_date)->where('id', $id)->first();
        if($log){
            $data = [
                "shoulder"=> $request->shoulder,
                "right_bicep"=> $request->right_bicep,
                "chest"=> $request->chest,
                "waist"=> $request->waist,
                "belly"=> $request->belly,
                "hips"=> $request->hips,
                "right_thigh"=> $request->right_thigh,
                "right_calf"=> $request->right_calf,
            ];
            $log->update($data);
            return response()->json([ 'success' => true, 'message' =>  'Data update successfully' ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Data does not exists' ],400);
        }

    }
    public function measurementLogDelete(Request $request){
        $request->validate([
            'id'          => 'required',
            'customer_id' => 'required',
            'log_date'    => 'required',
        ]);

        $id          = $request->input('id');
        $customer_id = $request->input('customer_id');
        $log_date    = $request->input('log_date');
        
        if(User::find($customer_id)){

            if(CustomerBodyMeasurementLog::find($id)){

                DB::table('cu_body_measurement_log')->where('id', $id)->where('log_date', $log_date)->delete();

                return response()->json(['success' => true,'message'=> 'Log detail has been deleted']);
            }else{
                return response()->json([ 'error' => true, 'message' => 'Log Already Deleted' ],401);    
            }
        }else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],400);
        } 
    }

    public function bodyMeasurementGraph(Request $request){
        $request->validate([
            'customer_id' => 'required',
            'body_type_name'    => 'required',
        ]);

        $customer_id = $request->input('customer_id');
        $id          = $request->input('id');
        $body_type_name          = $request->input('body_type_name');

        if(User::find($customer_id)){

            $data =    DB::select('select month(created_at) as month, sum('.$body_type_name.') as total from cu_body_measurement_log where customer_id = '.$customer_id.' group by month(created_at)');

                return response()->json(['success' => true,'data'=> $data]);
        }
        else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],401);
        }
    }

    public function satisfactionGraph(Request $request){
        $request->validate([
            'customer_id' => 'required',
            'name'       => 'required',
        ]);

        $customer_id = $request->input('customer_id');
        $name          = $request->input('name');

        if(User::find($customer_id)){

            $data =    DB::select('select month(created_at) as month, sum('.$name.') as total from cus_satisfaction_log where customer_id = '.$customer_id.' group by month(created_at)');

                return response()->json(['success' => true,'data'=> $data]);
        }
        else{
            return response()->json([ 'error' => true, 'message' => 'Customer not found' ],401);
        }


    }
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'error' => true,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function customerFoodLog(Request $request){
        // $request->validate([ 'customer_id' => 'required' ]);

        $access_token = $request->header('Authorization');
        $auth_header = explode(' ', $access_token);
        $token = $auth_header[1];
        $token_parts = explode('.', $token);
        $token_header = $token_parts[1];
        $token_header_json = base64_decode($token_header);
        $token_header_array = json_decode($token_header_json, true);
        $user_token = $token_header_array['jti'];

        $id = DB::table('oauth_access_tokens')->where('id', $user_token)->value('user_id');
        $data = CustomersFoodLog::where('customer_id', $id)->get();
        $result = $this->returnStatus($data);
        return $result;
    }

    // public function customerFoodLog(Request $request){
        //     $request->validate([
        //         'customer_id'     => 'required',
        //     ]);
        //     $data = CustomersFoodLog::where('customer_id', $request->customer_id)->get();

        //     if($data->toArray()){
        //         return response()->json([
        //        'success' => true,
        //         'data' => $data
        //     ], 200);
        //     }else{
        //         return response()->json([
        //         'success' => false,
        //         'message' => 'No Food Log Found'
        //     ], 401);
        //     }
    // }

    public function checkGoogleAuth(Request $request){

        $request->validate([
            'varify_token'     => 'required',
            'provider'     => 'required',
            'device_token' =>'required'

        ]);

        $varify_token = $request->varify_token;
        $provider = $request->provider;
        $client = new \Google_Client(['client_id' => '200095156584-lalettfo4sjoroh972lk2gcnsp0sss27.apps.googleusercontent.com']);
        $payload = $client->verifyIdToken($varify_token);

        if ($payload) {
            $userid = $payload['sub'];
            $email = $payload['email'];
            $name = $payload['given_name'];
            $image = $payload['picture'];

            $user = User::where("email", $email)->first();

            $provider = User::where("email", $email)->where('provider',$provider)->first();

            if($user){
                if($provider){
                    Auth::loginUsingId($user->id);
                    $tokenResult = $user->createToken('Personal Access Token');

                    $token = $tokenResult->token;

                    if ($request->remember_me)
                        $token->expires_at = Carbon::now()->addWeeks(1);

                    $token->save();

                    User::where('email',$request->email)->update(['device_token' =>$request->device_token]);

                     $plan = Customer::where('customer_id', $user['id'])->get(['plan']);
                    $plan_data = Plan::where('id', 15)->get(['plan_title']);
                    if(count($plan_data) > 0){
                        $plan_name = $plan_data[0]->plan_title;

                    }else{
                        $plan_name = 'Free';
                    }

                    return response()->json([
                        'access_token'  => $tokenResult->accessToken,
                        'token_type'    => 'Bearer',
                        'customer_id'   => $user['id'],
                        'name'          => $user['name'],
                        'email'         => $user['email'],
                        'phone'         => $user['phone'],
                        'image'         => $user['image'],
                        'plan'          => $plan_name,
                        'roles'         => 3,
                        'expires_at'    => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString()
                    ]);
                }else{
                    return response()->json([
                        'error' => true,
                        'provider' => $user->provider,
                 ], 200);
                }
            }
            else{
                $url_arr = explode ('/', $image);
                $ct = count($url_arr);
                $imgName = $url_arr[$ct-1];
                $name_div = explode('.', $imgName);
                $ct_dot = count($name_div);
                $img_type = $name_div[$ct_dot -1];

                // echo $name;die;

                $destinationPath = public_path().'/media/customer-image/'.time().$imgName;
                file_put_contents($destinationPath, file_get_contents($image));

                $user = new User([
                    'name'     => $name,
                    'email'    => $email,
                    'status'   => 1,
                    'image'    => time().$imgName,
                    'provider' => 'google',
                    'device_token' =>$request->device_token
                ]);
                $user->save();

                $customer = Customer::create([
                    'customer_id'   => $user['id'],
                    'customer_name' => $name,
                    'plan' => '11',
                ]);
            
                $customer_role = 3;
                $user->assignRole($customer_role);

                Auth::loginUsingId($user['id']);
                $tokenResult = $user->createToken('Personal Access Token');

                $token = $tokenResult->token;

                if ($request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);

                $token->save();

                return response()->json([
                    'access_token'  => $tokenResult->accessToken,
                    'token_type'    => 'Bearer',
                    'customer_id'   => $user['id'],
                    'name'          => $user['name'],
                    'email'         => $user['email'],
                    'phone'         => $user['phone'],
                    'image'         => $user['image'],
                    'plan'          => 'Free',
                    'roles'         => 3,
                    'expires_at'    => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString()
                ]);
            }
            
        } else {

            return response()->json([
                'error' => true,
                'message' => 'invalid_token'
         ], 400);
        }
    }
    
    public function facebookLogin(Request $request){
        $request->validate([
            'access_token'     => 'required',
            'provider'     => 'required',
            'device_token'     => 'required',

        ]);

        $fb = new \Facebook\Facebook([
          'app_id' => '846858665845822',           //Replace {your-app-id} with your app ID
          'app_secret' => 'bb71850a8f9d074f8e9d9a1657bc2abe',   //Replace {your-app-secret} with your app secret
          'graph_api_version' => 'v5.0',
        ]);

        try {
            // Get your UserNode object, replace {access-token} with your token
              $response = $fb->get('/me?fields=name,first_name,last_name,email', $request->access_token);

        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                // Returns Graph API errors when they occur
             return response()->json([
                'error' => true,
                'message' => 'Graph returned an error: ' . $e->getMessage(),
            ], 400);

          exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                // Returns SDK errors when validation fails or other local issues
            return response()->json([
                'error' => true,
                'message' => 'Facebook SDK returned an error: ' . $e->getMessage(),
            ], 400);
          exit;
        }

        $me = $response->getGraphUser();


        $user = User::where("email", $me->getProperty('email'))->first();

        //$provider = User::where("email", $me->getProperty('email'))->where('provider',$request->provider)->first();

        if($user)
        {
            if($user->provider != "manual"){
                Auth::loginUsingId($user->id);
                $tokenResult = $user->createToken('Personal Access Token');

                $token = $tokenResult->token;

                if ($request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);

                $token->save();

                User::where('email',$request->email)->update(['device_token' =>$request->device_token]);

                $plan = Customer::where('customer_id', $user['id'])->get(['plan']);
                $plan_data = Plan::where('id', 15)->get(['plan_title']);
                if(count($plan_data) > 0){
                    $plan_name = $plan_data[0]->plan_title;

                }else{
                    $plan_name = 'Free';
                }

                return response()->json([
                    'access_token'  => $tokenResult->accessToken,
                    'token_type'    => 'Bearer',
                    'customer_id'   => $user['id'],
                    'name'          => $user['name'],
                    'email'         => $user['email'],
                    'phone'         => $user['phone'],
                    'image'         => $user['image'],
                    'plan'          => $plan_name,
                    'roles'         => 3,
                    'expires_at'    => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString()
                ]);
            }else{
                return response()->json([
                    'error' => true,
                    'provider' => $user->provider,
                 ], 200);
            }
        }
        else
        {

            $user = new User([
                'name'     => $me->getName(),
                'email'    => $me->getProperty('email'),
                'status'   => 1,
                'image'    =>"",
                'provider' => 'facebook',
                'device_token' =>$request->device_token
            ]);
            $user->save();

            $customer = Customer::create([
                'customer_id'   => $user['id'],
                'customer_name' => $me->getName(),
                'plan' => '11',
            ]);
        
            $customer_role = 3;
            $user->assignRole($customer_role);

            Auth::loginUsingId($user['id']);
            $tokenResult = $user->createToken('Personal Access Token');

            $token = $tokenResult->token;

            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);

            $token->save();

            return response()->json([
                'access_token'  => $tokenResult->accessToken,
                'token_type'    => 'Bearer',
                'customer_id'   => $user['id'],
                'name'          => $user['name'],
                'email'         => $user['email'],
                'phone'         => $user['phone'],
                'image'         => $user['image'],
                'plan'          => 'Free',
                'roles'         => 3,
                'expires_at'    => Carbon::parse( $tokenResult->token->expires_at )->toDateTimeString()
            ]);
        }
    }
}
