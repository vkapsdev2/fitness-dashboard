<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\CustomersReminder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerReminderController extends Controller{

	public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
	           'success' => true,
	            'data' => $data
	        ], 200);

        }else{
            return response()->json([
	            'success' => false,
	            'message' => 'Data Not Found'
	        ], 401);
        }
    }

    public function reminderList(Request $request){
    	$this->validate($request, [
	        'customer_id' => 'required',
	    ]);
    	$customer_id = [
	    	'customer_id' => $request->input('customer_id'),
	    ];
        $data = CustomersReminder::where($customer_id)->get();
        $result = $this->returnStatus($data);
        return $result;
    }

	public function addReminder(Request $request){
		$this->validate($request, [
	        'customer_id' => 'required',
	        'title' => 'required',
	        'days' => 'required',
	        'time' => 'required',
	        'repeat_status' => 'required',
	        // 'reminder_name' => 'required',
	    ]);
	    $data = [
	    	// 'coach_id' => $request->input('coach_id'),
	    	'customer_id' => $request->input('customer_id'),
	    	'title' => $request->input('title'),
	        //'days' => json_encode($request->input('days')),
	        'days' => $request->input('days'),
	        'time' => $request->input('time'),
	        'repeat_status' => $request->input('repeat_status'),
	        // 'reminder_name' => $request->input('reminder_name'),
	    ];
	    $data = CustomersReminder::create($data);
	    return response()->json([ 'success' => true, 'message' => 'Reminder added successfully' ], 200);
	}

	public function deleteReminder(Request $request){
    	$this->validate($request, [
	        'customer_id' => 'required',
	        'id' => 'required',
	    ]);
    	$deleteData = [
	    	'customer_id' => $request->input('customer_id'),
	    	'id' => $request->input('id'),
	    ];
        $data = CustomersReminder::where($deleteData)->delete();
        return response()->json([ 'success' => true, 'message' => 'Reminder deleted successfully' ], 200);
    }

    public function repeatReminder(Request $request){
    	$this->validate($request, [
	        'customer_id' => 'required',
	        'id' => 'required',
	        'repeat_status' => 'required',
	    ]);
    	$updateData = [
	    	'customer_id' => $request->input('customer_id'),
	    	'id' => $request->input('id')
	    ];
	    $repeat_status = $request->input('repeat_status');
        $data = CustomersReminder::where($updateData)->update(['repeat_status'=>$repeat_status]);
        return response()->json([ 'success' => true, 'message' => 'Reminder updated successfully' ], 200);
    }
}
