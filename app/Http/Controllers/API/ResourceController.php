<?php

namespace App\Http\Controllers\API;

use DB;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Podcast;
use App\Models\SubPodcast;
use App\Models\Article;
use App\Models\Book;
use App\Models\Recipies;

class ResourceController extends Controller
{
	public function returnStatus($data){
		if($data->toArray()){
    		return response()->json([
    	   'success' => true,
            'data' => $data
        ], 200);
    	}else{
    		return response()->json([
    		'success' => false,
            'message' => 'No Events Found'
        ], 401);
    	}
	}
    public function podcastList(Request $request){
    	// $data = Podcast::with('SubPodcast')->get();
    	/*$data = DB::table('podcasts')->join('sub_podcasts','podcasts.id','sub_podcasts.podcast_id')->select('sub_podcasts.*','podcasts.name as podcast_name')->get();*/
        $data = Podcast::get();
        foreach ($data as $key => $value) {
            $id = $value->id;
            $data[$key]['subPodcast'] = DB::table('sub_podcasts')->where('podcast_id', $id)->get();
        }
        $result = $this->returnStatus($data);
    	return $result;
    }

    public function articleList(Request $request){
    	$data = Article::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function bookList(Request $request){
    	$data = Book::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function recipesList(Request $request){
    	$data = Recipies::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }
}
