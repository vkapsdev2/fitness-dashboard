<?php

namespace App\Http\Controllers\API\BTSchedule;

use DB;
use Validator;
use App\User;
use App\Models\BTSchedule\Express; //bts_express
use App\Models\BTSchedule\ExpressStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExpressController extends Controller
{
    public function list(Request $request){
    	//$data = DB::select("select `users`.`name`, `users`.`image`,`cleanse_weight_loss_status`.`customer_id`, count(cleanse_weight_loss_status.customer_id) as total_exercise from `cleanse_weight_loss_status` inner join users on `users`.`id` = `cleanse_weight_loss_status`.`customer_id` group by customer_id ORDER BY total_exercise DESC");
    	$data = DB::select("select day, count(bts_express.day) as total_exercise from `bts_express` group by day");
 
        foreach ($data as $key => $value) {
            $dbDays[] = $value->day;
        }

        $allDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        $newDays = array_diff($allDays, $dbDays);
        
        $keyDays = array();
        foreach ($newDays as $key => $value) {
            array_push($keyDays,$value);
        }
        
        foreach ($keyDays as $key => $value) {
            $newData[$key]['day'] = $value;
            $newData[$key]['total_exercise'] = 0;
        }
        $newData = json_encode($newData);
        $newData = json_decode($newData, FALSE);
        
        $data = array_merge($data, $newData);

        if($data){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }
    
    public function dayExercise(Request $request){
    	$this->validate($request, [
            'day' => 'required',
        ]);
        $day = $request->day;
        $new_data = array();
        $final_data = array();
    	$data = DB::select("
    		select `users`.`name`, `users`.`image` as coach_image, `users`.`id` as coach_id,`bts_express`.`start_time`,`bts_express`.`end_time` ,`bts_express`.`banner` ,`bts_express`.`video_file` ,`bts_express`.`video_url` ,`bts_express`.`description` ,`bts_express`.`day` as exercise_day, `bts_express`.`id` as exercise_id
    		  from `bts_express` 
    		  inner join users on `users`.`id` = `bts_express`.`coach_id`
    		  where `bts_express`.`day` = '".$day."'
    		  ");

    	
        foreach ($data as $key => $value) {
            
            $coach_id = $value->coach_id;
           // array_push($new_data, $value);
            $coach_review = DB::select("
            select `bts_express_status`.`coach_id`, CAST(ROUND(AVG(`bts_express_status`.`rating`),5) AS DEC(10,1)) AS avg_rating, count(`bts_express_status`.`review`) as total_review
              from `bts_express_status` group by coach_id HAVING coach_id = '".$coach_id."'
              ");
           
            if(count($coach_review) > 0){
                $new_data['coach_id']       = $value->coach_id;
                $new_data['name']           = $value->name;
                $new_data['coach_image']    = $value->coach_image;
                $new_data['start_time']     = $value->start_time;
                $new_data['end_time']       = $value->end_time;
                $new_data['exercise_day']   = $value->exercise_day;
                $new_data['exercise_id']    = $value->exercise_id; 
                $new_data['banner']         = $value->banner; 
                $new_data['video_file']     = $value->video_file; 
                $new_data['video_url']      = $value->video_url; 
                $new_data['description']    = $value->description; 
                $new_data['avg_rating']     = $coach_review[0]->avg_rating;
                $new_data['total_review']   = $coach_review[0]->total_review;
               

            }else{
                $new_data['coach_id']       = $value->coach_id;
                $new_data['name']           = $value->name;
                $new_data['coach_image']    = $value->coach_image;
                $new_data['start_time']     = $value->start_time;
                $new_data['end_time']       = $value->end_time;
                $new_data['exercise_day']   = $value->exercise_day;
                $new_data['exercise_id']    = $value->exercise_id; 
                $new_data['banner']         = $value->banner; 
                $new_data['video_file']     = $value->video_file; 
                $new_data['video_url']      = $value->video_url; 
                $new_data['description']    = $value->description; 
                $new_data['avg_rating']     = 0;
                $new_data['total_review']   = 0;
            }
            array_push($final_data, $new_data);
            
        }
    	if(count($final_data)>0){
    		return response()->json([
	           'success' => true,
	            'data' => $final_data,
        	], 200);
    	}
    	else{
    		return response()->json([
           'error' => true,
            'message' => 'Data is not available'
        ], 401);
    	}
    }

    public function exerciseDetail(Request $request){
    	$this->validate($request, [
            'exercise_id' => 'required',
            'customer_id'=>'required'
        ]);
        $exercise_detail = Express::where('id',$request->exercise_id)->get(['description','banner','video_file','video_url']);

        $like_status = ExpressStatus::where('exercise_id',$request->exercise_id)->where('customer_id',$request->customer_id)->get(['like_status']);

        return response()->json([
           'success' => true,
            'exercise_detail' => $exercise_detail,
            'status' => $like_status,
    	], 200);
    }

    public function likeStatus(Request $request){
    	$this->validate($request,[
    		'exercise_id' => 'required',
            'customer_id'=>'required',
            'coach_id' => 'required',
    	]);
        $customerArray = [
            'like_status' => $request->status
        ];
        $res = ExpressStatus::where('customer_id', $request->customer_id)->where('exercise_id', $request->exercise_id)->get();
        if($res->toArray()){
        	ExpressStatus::where('customer_id', $request->customer_id)->where('exercise_id', $request->exercise_id)->update($customerArray);

	        return response()->json([
	           'success' => true,
	            'message' => 'Exercise Status updated successfull',
	    	], 200);
        }else{

            $insert = new ExpressStatus([
                'customer_id' => $request->customer_id,
                'coach_id'    => $request->coach_id,
                'exercise_id' => $request->exercise_id,
                'like_status' => $request->status,
            ]);

        	$insert->save();

            return response()->json([ 'success' => true, 'message' => "Exercise Status added successfull" ],200);
        }
        
    }

    public function review(Request $request){
    	$this->validate($request, [
            'rating' => 'required',
            'coach_id' => 'required',
            'exercise_id' => 'required',
            'customer_id' => 'required',
            'review_date' => 'required',
        ]);

    	$express_data = Express::where('coach_id',$request->coach_id)->where('id',$request->exercise_id)->first();
    	if($express_data){

    		$data = ExpressStatus::where('customer_id',$request->customer_id)->where('coach_id',$request->coach_id)->where('exercise_id',$request->exercise_id)->first();

    	  	if($data == null){

    	  		$data = new ExpressStatus([
	            'customer_id' => $request->customer_id,
	            'coach_id'    => $request->coach_id,
	            'exercise_id' => $request->exercise_id,
	            'rating'      => $request->rating,
	            'review'      => $request->review,
	            'review_date' => $request->review_date,
	           ]);
		        
		        $data->save();

		        return response()->json([ 'success' => true, 'message' => "Review Added successfull" ],200);	
		   }else{

                $update = [
                    'rating'      => $request->rating,
                    'review'      => $request->review,
                    'review_date' => $request->review_date,
                ];

                ExpressStatus::where('customer_id', $request->customer_id)->where('exercise_id', $request->exercise_id)->update($update);

		   		return response()->json([ 'error' => true, 'message' => "Review updated successfull" ],200);
		   }

    	}else{
    		return response()->json([ 'error' => true, 'message' => "Data does not exist" ],401);
    	}
    	
    }

    public function allReview(Request $request){
  
    	$data = DB::select("
    		select `users`.`name`, `users`.`image` as coach_image,`bts_express_status`.`rating`,`bts_express_status`.`review` ,`bts_express_status`.`review_date`
    		  from `bts_express_status` 
    		  inner join users on `users`.`id` = `bts_express_status`.`coach_id`
    		  ");

    	if($data){
    		return response()->json([
	           'success' => true,
	            'data' => $data,
        	], 200);
    	}
    	else{
    		return response()->json([
           'error' => true,
            'message' => 'Data is not available'
        ], 401);
    	}
    }
}
