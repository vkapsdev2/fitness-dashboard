<?php

namespace App\Http\Controllers\API\Coach;

use App\User;
use App\Models\MindFlexPlanner\MindFlexRx as NewMindFlexRx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MindFlexRx extends Controller
{
    public function RxLog(Request $request){
    	$request->validate([
    		'customer_id' 		=> 'required',
    		'movement' 			=> 'required',
    		'mindset' 			=> 'required',
    		'nutrition'			=> 'required',
    		'supplimentation' 	=> 'required',
    		'other' 			=> 'required',
    		'log_date' 			=> 'required',
    	]);
    	$userCheck = User::find($request->customer_id);
    	if($userCheck){

    		$rx = new NewMindFlexRx([
	    		'customer_id' 		=> $request->customer_id,
	    		'movement' 			=> $request->movement,
	    		'mindset' 			=> $request->mindset,
	    		'nutrition'			=> $request->nutrition,
	    		'supplimentation' 	=> $request->supplimentation,
	    		'other' 			=> $request->other,
	    		'log_date' 			=> $request->log_date
	    	]);
	    	$rx->save();

	    	return response()->json(['success' => true, 'message' => 'Customer Rx Log added successfully'], 200);
    	}
    	else{
    		return response()->json(['error' => true, 'message' =>'Customer does not exist'], 401);
    	}
    	
    }
}
