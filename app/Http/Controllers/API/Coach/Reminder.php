<?php

namespace App\Http\Controllers\API\Coach;

use Hash;
use DB;
use Auth;
use App\User;
use App\Models\Coach;
use App\Models\Customer;
use App\Models\CustomersReminder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Reminder extends Controller
{
    
    public function list(Request $request){
    	$request->validate([
    		'customer_id' => 'required',
    		'coach_id' => 'required'
    	]);

    	$data = CustomersReminder::where('coach_id', $request->coach_id)->where('customer_id', $request->customer_id)->get(['id','coach_id','customer_id', 'title','days','time','repeat_status']);
    	if(count($data) > 0){
    		return response()->json([
		        'success' => true,
		        'data' => $data
		    ], 200);
    	}
    	else{
    		return response()->json(['error' => true, 'Message' => 'data Does not exists' ], 400);
    	}
    	
    }

    public function save(Request $request){
    	$request->validate([
    		'customer_id'	=> 'required',
    		'coach_id' 		=> 'required',
    		'title' 		=> 'required',
    		'days' 			=> 'required',
    		'time' 			=> 'required',
    		'repeat_status' => 'required',
    	]);

    	$reminder = new CustomersReminder([
    		'customer_id'	=> $request->customer_id,
    		'coach_id' 		=> $request->coach_id,
    		'title' 		=> $request->title,
    		'days' 			=> $request->days,
    		'time' 			=> $request->time,
    		'repeat_status' => $request->repeat_status,
    	]);
    	$reminder->save();

    	return response()->json([ 'success' => true, 'message' => "Customer's reminder added successfully" ],200);
    } 

    public function delete(Request $request){
    	$request->validate([
    		'id'	=> 'required',
    		'customer_id'	=> 'required',
    		'coach_id' 		=> 'required',
    	]);
    	$data = CustomersReminder::where('customer_id',$request->customer_id)->where('coach_id',$request->coach_id)->find($request->id);

    	if($data){
    		$data->delete();
    		return response()->json([ 'success' => true, 'Message' => ' Workout delete successfully' ],200);
    	}
    	else{
    		return response()->json(['error' => true, 'Message' => 'This Workout Does not exists' ], 400);
    	}
    	


    }


}
