<?php

namespace App\Http\Controllers\API\Coach;

use App\User;

use App\Models\MindFlexPlanner\MindSet\Assignment as AssignmentNew;

use App\Models\MindFlexPlanner\Movement\HomeWorkout;
use App\Models\MindFlexPlanner\Movement\SubHomeWorkout;

use App\Models\MindFlexPlanner\Movement\GymWorkout;
use App\Models\MindFlexPlanner\Movement\SubGymWorkout;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Assignment extends Controller
{	
	public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function WorkoutCategories(Request $request){
    	

    	if($request->workout_type == 'Home Workout'){
    		$data = HomeWorkout::get();
    		$result = $this->returnStatus($data);
    		return $result;
    	}
    	else if($request->workout_type == 'Gym Workout'){
    		$data = GymWorkout::get();
    		$result = $this->returnStatus($data);
    		return $result;
    	}
    	else{
    		return response()->json(['error' => true, 'Message' => 'Workout type is not valid' ], 400);
    	}
    }

    public function CategoryExercises(Request $request){

    	if($request->workout_type == 'Home Workout' && $request->category_id){
    		$data = SubHomeWorkout::where('homeWorkout_id', $request->category_id)->get(['id','homeWorkout_id as category_id','sub_banner','sub_name']);
    		$result = $this->returnStatus($data);
    		return $result;
    	}
    	else if($request->workout_type == 'Gym Workout' && $request->category_id){
    		$data = SubGymWorkout::where('gymWorkout_id', $request->category_id)->get(['id','gymWorkout_id as category_id','sub_banner','sub_name']);
    		$result = $this->returnStatus($data);
    		return $result;
    	}
    	else{
    		return response()->json(['error' => true, 'Message' => 'Workout type and category_id are not valid' ], 400);
    	} 
    }

    public function assignExercise(Request $request){

    	$request->validate([
    		'customer_id'  =>'required',
    		'workout_type' =>'required', //workout_category
    		'category_id'  =>'required', //workout_id
    		'exercises_id' =>'required', //	exercises_id
    		'week'		   =>'required', 
    	]);
    	
    	/* check user exist code here */
    	$userCheck = User::find($request->customer_id);

    	if($userCheck){
    		 /* $category_ids = array();
    			$exercise_ids = array();
    		 if($request->workout_type == 'Home Workout' ){
    			$data = HomeWorkout::whereIn('id',explode(",",$request->category_id))->get(['id']);

    			if(count($data) > 0){
    				
			       foreach ($data as $key => $value) {
			        	$home_id = $value['id'];
			        	array_push($category_ids, $home_id);

			        	$home_exercise = SubHomeWorkout::where('homeWorkout_id', $home_id)->whereIn('id',explode(",",$request->exercises_id))->get(['id']);

			        	foreach ($home_exercise as $key => $home_exercise_value) {
			        		array_push($exercise_ids, $home_exercise_value['id']);
			        	}
			        	
			        }*/

			        $assign = new AssignmentNew([
			        	'customer_id'=> $request->customer_id,
			        	'workout_id' => explode(",",$request->category_id),
			        	'exercises_id' =>explode(",",$request->exercises_id),
			        	'workout_category' => 1,
			        	'week' => $request->week,
			        	'rest_timing' => $request->rest_timing,
			        	'data' => json_encode($request->sets),
			        ]);
			        $assign->save();

	    			return response()->json([
			           'success' => true,
			            'data' 	=>$assign
			        ], 200);
	    		/*}else{
	    			return response()->json([ 'error' => true, 'message' => 'Workout does not exists' ],400);
	    		}

    		}
    		else{
    			$data = GymWorkout::whereIn('id',explode(",",$request->category_id))->get(['id']);
    		}*/
    		
    	}else{
    		return response()->json([ 'error' => true, 'message' => 'Customer does not find' ],400);
    	}
    }
}




