<?php

namespace App\Http\Controllers\API\Coach;

use App\Models\Meal;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MealPlan extends Controller
{	
	public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }


    public function list(Request $request){

    	$data = Meal::get(['id','meal_title','meal_plan_type']);
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function mealTypeList(Request $request){
    	
    	$meal_type = $request->meal_type;
    	if($meal_type == 'BreakFast' || $meal_type == 'Lunch' || $meal_type == 'Dinner') {

    		if($meal_type == 'BreakFast'){
    			$data = Meal::where('meal_plan_type',1)->get(['id','meal_title','meal_image','meal_calories','meal_protein','meal_carbs','meal_fats']);
    		}else if($meal_type == 'Lunch'){
    			$data = Meal::where('meal_plan_type',2)->get(['id','meal_title','meal_image','meal_calories','meal_protein','meal_carbs','meal_fats']);
    		}else{
    			$data = Meal::where('meal_plan_type',3)->get(['id','meal_title','meal_image','meal_calories','meal_protein','meal_carbs','meal_fats']);
    		}
	    	$result = $this->returnStatus($data);
	    	return $result;
    	}else{
    		return response()->json(['error' => true,'message'=>'Invalid Meal Type'], 401);
    	}
    }

    public function addmeal(Request $request){
    	$request->validate([
    		'title' =>'required',
    		'calories' =>'required',
    		'fat' =>'required',
    		'carbs' =>'required',
    		'protein' =>'required',
    		'Meal_type' =>'required'
    	]);

    	$data = new Meal([
    		'meal_title' 		=> $request->title,
    		'meal_calories' 	=> $request->calories,
    		'meal_fats' 		=> $request->fat,
    		'meal_carbs'		=> $request->carbs,
    		'meal_protein' 		=> $request->protein,
    		'meal_plan_type' 	=> $request->Meal_type,
            'meal_status'       => 1,
            'created_by'        => 2,
    	]);
    	$data->save();

    	return response()->json(['success' => true, 'message' =>'Meal added successfully']);
    }
}
