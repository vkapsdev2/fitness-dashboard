<?php

namespace App\Http\Controllers\API\Coach;

use Hash;
use DB;
use Auth;
use App\User;
use App\Models\Coach;
use App\Models\MindFlexPlanner\Accountability;
use App\Models\MindFlexPlanner\MindFlexRx;
use App\Models\Customer as CustomerModal;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Customer extends Controller
{	
	public function checkCoach($coach_id){

		$coach_role_id = 2;
		$result = DB::table('users')
        ->join('model_has_roles','model_has_roles.model_id','users.id')
        ->select(['users.email'])
        ->where('model_has_roles.role_id', $coach_role_id)
        ->where('users.id', $coach_id)
        ->get();

        return $result;

	}

    public function customers(Request $request){
    	$request->validate([
            'coach_id'    => 'required',
        ]);

        $result = $this->checkCoach($request->coach_id);

        if(count($result) > 0){

        	$customers = DB:: table('coach_assign_to_customer')
		        ->join('customers', 'customers.customer_id','coach_assign_to_customer.coach_assign_customer_id')
		        ->join('coaches', 'coaches.coach_id','coach_assign_to_customer.coach_assign_id')
		        ->join('plans', 'plans.id', 'customers.plan')
		        ->join('users', 'users.id', 'customers.customer_id')
		        ->where('coach_assign_to_customer.coach_assign_id', $request->coach_id)
		        ->select(['coaches.id as coach_id,','customers.customer_id as customer_id','customers.customer_name', 'plans.id As plan_id', 'plans.plan_title','users.image'])
		        ->get();
	        if(count($customers) > 0){
	        	return response()->json([
		        'success' => true,
		        'data' => $customers
		    ], 200);
	        }
        	return response()->json(['error' => true, 'Message' => 'No Customer Assign' ], 400);
        }
          return response()->json(['error' => true, 'Message' => 'Coach Does not exists' ], 400);
    }
         
    public function customerDetail(Request $request){
    	$request->validate([
            'customer_id'    => 'required',
        ]);

    	$result = [];
    	$data = DB::table('customers')
    			->join('users', 'users.id','customers.customer_id')
    			->select('customers.*','users.gender','users.name','users.image','users.phone','users.email')
    			->where('customers.customer_id',$request->customer_id)
    			->get();
    	if(count($data) > 0){
	    	$result['personal Information'] = [
	    		'Gender' 				=> $data[0]->gender,
	    		'Fitness Level' 		=> $data[0]->level,
	    		'Age'				    => $data[0]->age,
	    		'Height' 				=> $data[0]->height,
	    		'Weight'				=> $data[0]->weight,
	    		'Body Fat percentage'	=> $data[0]->body_fat_percent,
	    		'Body Fat Mass' 		=> $data[0]->body_fat_mass,
	    	];

	    	$result['Body Measurement'] = [
	    		'shoulder' 				=> $data[0]->shoulder,
	    		'Right Bicep' 			=> $data[0]->right_bicep,
	    		'Chest'				    => $data[0]->chest,
	    		'Waist' 				=> $data[0]->waist,
	    		'Belly'					=> $data[0]->belly,
	    		'Hips'					=> $data[0]->hips,
	    		'Right Thigh' 		    => $data[0]->right_thigh,
	    		'Right Calf' 		    => $data[0]->right_calf,
	    	];

	    	$result['Satisfaction'] = [
	    		'Career' 				=> $data[0]->career,
	    		'Relationship' 			=> $data[0]->relationship,
	    		'Joy'				    => $data[0]->joy,
	    		'Health' 				=> $data[0]->health,
	    		'Nutrition'			    => $data[0]->nutrition,
	    		'Sleep'					=> $data[0]->sleep,
	    		'Appearance' 		    => $data[0]->appearance,
	    		'Purpose' 		        => $data[0]->purpose,
	    		'Social' 		        => $data[0]->social,
	    		'Spiritual' 		    => $data[0]->spiritual,
	    		'Ready For Change?'     => $data[0]->ready_for_change,
	    	];
	    	$result['Goal'] = $data[0]->social_goal;

	    	$result['Customer Info'] =[
	    		'Name' => $data[0]->name,
	    		'Email' => $data[0]->email,
	    		'Phone' => $data[0]->phone,
	    		'Image' => $data[0]->image,
	    	];

	    	return response()->json([
		        'success' => true,
		        'data' => $result
	    	], 200);
	    }
	    else{
	    	return response()->json(['error' => true, 'Message' => 'Data not found' ], 400);
	    } 
    }

    public function questionToCoach(Request $request){

    	$request->validate([
            'customer_id'    => 'required',
        ]);

        $data = Accountability::where('customer_id', $request->customer_id)->get();

        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

   
}
