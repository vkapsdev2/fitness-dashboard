<?php

namespace App\Http\Controllers\API\mindFlexPhysio;

use Validator;
use App\Models\MindFlexPhysio\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class StoreController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function storeList(Request $request){
        $check = ['store_status' => 1];
    	$data = Store::where($check)->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }
}