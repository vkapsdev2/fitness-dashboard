<?php

namespace App\Http\Controllers\API\mindFlexPhysio;

use Validator;
use App\Models\MindFlexPhysio\Environment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class EnvironmentFactorController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function environmentFactorList(Request $request){
        $this->validate($request, [ 'customer_id' => 'required']);
        $customer_id = $request->customer_id;
    	$data = Environment::where('customer_id', $customer_id)->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function addEnvironmentFactor(Request $request){
        $this->validate($request, [ 'customer_id' => 'required', 'name' => 'required', 'description' => 'required']);

        $data = Environment::create([
            "customer_id" => $request->customer_id,
            "name"        => $request->name,
            // "image"       => $request->image,
            "description" => $request->description
        ]);

        if($data){
            return response()->json([ 'success' => true, 'message' => 'Environment factor add successfully' ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Failed' ],400);
        }
    }
}