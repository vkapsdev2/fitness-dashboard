<?php

namespace App\Http\Controllers\API\mindFlexPhysio;

use Validator;
use App\Models\MindFlexPhysio\Nutrition\FoodLog;
use App\Models\MindFlexPhysio\Nutrition\Recommendation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class NutritionsController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function foodLogList(Request $request){
        $this->validate($request, ['customer_id' => 'required']);
    	$customer_id = $request->customer_id;
        $data = FoodLog::where('customer_id', $customer_id)->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function addFoodLog(Request $request){
        $this->validate($request, [ 'customer_id' => 'required', 'name' => 'required', 'quantity' => 'required']);

        $data = FoodLog::create([
            "customer_id" => $request->customer_id,
            "name"        => $request->name,
            "quantity"    => $request->quantity
        ]);

        if($data){
            return response()->json([ 'success' => true, 'message' => 'Food log add successfully' ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Failed' ],400);
        }
    }

    public function recommendationMealsList(Request $request){
        $this->validate($request, [
            'meal_type' => 'required',
        ]);

        $meal_type = $request->input('meal_type'); 

        $data = Recommendation::where('meal_type',$meal_type)->get();
        $result = $this->returnStatus($data);
        return $result;
    }

    public function recommendationMealType(Request $request){
        $data = Recommendation::get();
        $result = $this->returnStatus($data);
        return $result;
    }

    // public function recommendationMealList(Request $request){
    //     $access_token = $request->header('Authorization');
    //     $auth_header = explode(' ', $access_token);
    //     $token = $auth_header[1];
    //     $token_parts = explode('.', $token);
    //     $token_header = $token_parts[1];
    //     $token_header_json = base64_decode($token_header);
    //     $token_header_array = json_decode($token_header_json, true);
    //     $user_token = $token_header_array['jti'];

    //     $id = DB::table('oauth_access_tokens')->where('id', $user_token)->value('user_id');

    //     $roleID = DB::table('model_has_roles')->where('model_id', $id)->value('role_id');

    //     if($roleID == 3){
    //         /* Customer */
    //         $data = DB::table('nutrition_recommendation_meals')->where('suggested_customer_id', $id)->get();
    //         /* Join with iser table */
    //     }else{
    //         /* Coach */
    //         $data = DB::table('nutrition_recommendation_meals')->where('suggester_coach_id', $id)->get();
    //         /* Join with iser table */
    //     }
    //     $result = $this->returnStatus($data);
    //     return $result;
    // }
}