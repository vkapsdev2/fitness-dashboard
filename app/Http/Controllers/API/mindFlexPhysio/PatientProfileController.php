<?php

namespace App\Http\Controllers\API\mindFlexPhysio;

use Validator;
use App\Models\MindFlexPhysio\Profile\MedicalHistory;
use App\Models\MindFlexPhysio\Profile\DailyNote;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class PatientProfileController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function medicalHistoryList(Request $request){
        $customer_id = $request->customer_id;
    	$data = DB::table('mindiflex_pysio_profile_medical_history')->where('customer_id', $customer_id)->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function addMedicalHistory(Request $request){
        $this->validate($request, [ 'customer_id' => 'required', 'medical_hisory_detail' => 'required', 'medical_history_status' => 'required']);

        $data = MedicalHistory::create([
            "customer_id"            => $request->customer_id,
            "medical_hisory_detail"  => $request->medical_hisory_detail,
            "date"                   => date('Y-m-d'),
            "time"                   => $request->time,
            "medical_history_status" => $request->medical_history_status,
        ]);

        if($data){
            return response()->json([ 'success' => true, 'message' => 'Medical history add successfully' ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Failed' ],400);
        }
    }

    public function dailyNotesList(Request $request){
    	$customer_id = $request->customer_id;
    	$data = DB::table('mindiflex_pysio_profile_daily_notes')->where('customer_id', $customer_id)->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function addDailyNotes(Request $request){
        $this->validate($request, [ 'customer_id' => 'required', 'daily_note_detail' => 'required', 'daily_note_status' => 'required']);

        $data = DailyNote::create([
            "customer_id"        => $request->customer_id,
            "daily_note_detail"  => $request->daily_note_detail,
            "daily_note_date"    => date('Y-m-d'),
            "time"               => $request->time,
            "daily_note_status"  => $request->daily_note_status,
        ]);

        if($data){
            return response()->json([ 'success' => true, 'message' => 'Daily notes add successfully' ],200);
        }else{
            return response()->json([ 'error' => true, 'message' => 'Failed' ],400);
        }
    }
}