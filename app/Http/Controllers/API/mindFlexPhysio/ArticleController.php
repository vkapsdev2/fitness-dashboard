<?php

namespace App\Http\Controllers\API\mindFlexPhysio;

use Validator;
use App\Models\MindFlexPhysio\PhysioArticle;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ArticleController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function articleList(Request $request){
        $check = ["physio_article_status" => 1];
    	$data = PhysioArticle::where($check)->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }
}