<?php

namespace App\Http\Controllers\API\mindFlexPhysio;

// use Validator;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexPhysio\Doc;
use App\Models\MindFlexPhysio\DocBookSession;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SeeADocController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function seeADocList(Request $request){
    	$data = Doc::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function therapistList(Request $request){
        $data = DB::table('users')
            ->join('coaches', 'coaches.coach_id','users.id')
            ->join('model_has_roles',function($join) {$join
            ->on('model_has_roles.model_id' , '=' , 'users.id')
            ->where('model_has_roles.role_id','=', 2);})
            ->get();

        foreach ($data as $key => $value) {
           // $data[$key]->testimonial = DB::table('testimonials')->where('to_coach', $value->coach_id)->get();
            $data[$key]->testimonial = DB::table('testimonials')
                                        ->where('testimonials.to_coach', $value->coach_id)
                                        ->join('customers', 'customers.customer_id','testimonials.from_customer')
                                        ->select('testimonials.*','customers.customer_name as from_customer_name')
                                        ->get();
        }

        $result = $this->returnStatus($data);
        return $result;
    }

    public function docBookSession(Request $request){
        $this->validate($request, [ 'date' => 'required', 'time' => 'required', 'purpose' => 'required', 'doc_id' => 'required', 'therapist_id' => 'required', 'customer_id' => 'required']);

        $data = [
            "date" => $request->date,
            "time" => $request->time,
        ];

        $checkData = DocBookSession::where($data)->exists();
        if(!$checkData){
            $customer_personal = DocBookSession::create([
                "doc_id"        => $request->doc_id,
                "therapist_id"  => $request->therapist_id,
                "customer_id"   => $request->customer_id,
                "date"          => $request->date,
                "time"          => $request->time,
                "purpose"       => $request->purpose,
            ]);

            if($customer_personal){
                return response()->json([ 'success' => true, 'message' => 'Session booked successfully' ],200);
            }else{
                return response()->json([ 'error' => true, 'message' => 'Failed' ],400);
            }
        }else{
            return response()->json([ 'error' => true, 'message' => 'This date and Time booking already exist' ],400);
        }
    }

    public function alreadyBookSession(Request $request){
        $check = ['date' => $request->date];
        $data = DocBookSession::where($check)->get();
        $result = $this->returnStatus($data);
        return $result;
    }
}