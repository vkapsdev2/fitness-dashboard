<?php

namespace App\Http\Controllers\API\mindFlexPhysio;

use Validator;
use App\Models\MindFlexPhysio\Exercise;
use App\Models\MindFlexPhysio\ExerciseMarkAsComplete;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ExerciseProgramController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function exerciseProgramList(Request $request){
    	$data = Exercise::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function exerciseProgramStatusAdd(Request $request){
        $this->validate($request, [
            'customer_id' => 'required',
            'exercise_program_id' => 'required',
            'status' => 'required',
        ]);

        $check = ExerciseMarkAsComplete::where(['exercise_program_id'=>$request->input('exercise_program_id'), 'customer_id'=>$request->input('customer_id')])->get();
        if($check->toArray()){
            $statusData = [
                'status' => trim($request->input('status')),
            ];
            ExerciseMarkAsComplete::where(['exercise_program_id'=>$request->input('exercise_program_id'), 'customer_id'=>$request->input('customer_id')])->update($statusData); 
            return response()->json([ 'success' => true, 'message' => 'Status updated successfully' ], 200);
        }else{
            $data = [
                'customer_id' => trim($request->input('customer_id')),
                'exercise_program_id' => trim($request->input('exercise_program_id')),
                'status' => trim($request->input('status')),
            ];
            $data = ExerciseMarkAsComplete::create($data);
            return response()->json([ 'success' => true, 'message' => 'Status added successfully' ], 200);
        }
    }

    public function exerciseProgramStatusGet(Request $request){
        $this->validate($request, [
            'customer_id' => 'required',
            'exercise_program_id' => 'required',
        ]);
        $data = ExerciseMarkAsComplete::where(['exercise_program_id'=>$request->input('exercise_program_id'), 'customer_id'=>$request->input('customer_id')])->get();

        $result = $this->returnStatus($data);
        return $result;
    }

    

    // public function exerciseProgramStatusAdd(Request $request){
    //     $customer_id = $request->customer_id;

    //     $user = ExerciseMarkAsComplete::where('customer_id', '=', $customer_id)->exists();
    //     if(!$user){
    //         $this->validate($request, ['status' => 'required', 'exercise_program_id' => 'required', 'customer_id' => 'required']);

    //         $data = ExerciseMarkAsComplete::create([
    //             "exercise_program_id"      => $request->doc_id,
    //             "customer_id" => $request->customer_id,
    //             "status"      => $request->status,
    //         ]);

    //         if($data){
    //             return response()->json([ 'success' => true, 'message' => 'Status changed successfully' ],200);
    //         }else{
    //             return response()->json([ 'error' => true, 'message' => 'Failed' ],400);
    //         }
    //     }else{
    //         $this->validate($request, ['status' => 'required', 'exercise_program_id' => 'required', 'customer_id' => 'required']);

    //         $data = [
    //             "status" => $request->status,
    //         ];
    //         $customer_id = $request->customer_id;
    //         $check = DB::table('physio_exercise_status')->where('customer_id', $customer_id)->update($data);
    //         if($check){
    //             return response()->json([ 'success' => true, 'message' => 'Status changed successfully' ],200);
    //         }else{
    //             return response()->json([ 'error' => true, 'message' => 'Failed' ],400);
    //         }
    //     }
    // }

    // public function exerciseProgramStatusGet(Request $request){
    //     $this->validate($request, ['customer_id' => 'required']);
    //     $customer_id = $request->customer_id;
    //     $result = DB::table('physio_exercise_status')->where('customer_id', $customer_id)->get();
    //     $result = $this->returnStatus($result);
    //     return $result;
    // }
}