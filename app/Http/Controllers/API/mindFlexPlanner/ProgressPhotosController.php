<?php

namespace App\Http\Controllers\API\mindFlexPlanner;

use Validator;
use App\Models\MindFlexPlanner\ProgressPhoto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use DB;

class ProgressPhotosController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function progressPhotosList(Request $request){
        $access_token = $request->header('Authorization');
        $auth_header = explode(' ', $access_token);
        $token = $auth_header[1];
        $token_parts = explode('.', $token);
        $token_header = $token_parts[1];
        $token_header_json = base64_decode($token_header);
        $token_header_array = json_decode($token_header_json, true);
        $user_token = $token_header_array['jti'];
        $customer_id = DB::table('oauth_access_tokens')->where('id', $user_token)->value('user_id');

       $orderbydate = ProgressPhoto::where('customer_id', $customer_id)->select('id','image', DB::raw('DATE(created_at) as date'))
          ->get()
          ->groupBy('date');
       // $data = DB::table('progress_photos')->where('customer_id', $customer_id)->get();
        $result = $this->returnStatus($orderbydate);
        return $result;
    }

    public function addPhoto(Request $request){

        $validator = Validator::make($request->all(), [
            'image' => 'required|string',
            'customer_id' => 'required'
        ]);
          

          if ($image_64 = $request->image) {
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

            $replace = substr($image_64, 0, strpos($image_64, ',')+1); 

           $image = str_replace($replace, '', $image_64); 

           $image = str_replace(' ', '+', $image); 

           $imageName = date('YmdHis').'.'.$extension;
          
           $destinationPath = public_path().'/media/progress-photos';

            \File::put(public_path().'/media/progress-photos'. '/' . $imageName, base64_decode($image));

           $photo = new ProgressPhoto([
                'image'     => $imageName,
                'customer_id'    => $request->customer_id,
            ]);
            $photo->save();

            return response()->json([
               'success' => true,
                'message' => "Progess Photo uploaded successfully"
            ], 200);

        }else{
            return response()->json([ 'error' => true, 'message' => 'Something went wrong! Please try again' ],401);
        }
    }

  public function photoDelete(Request $request){
    $validator = Validator::make($request->all(), [
        'customer_id' => 'required',
        'photo_id' => 'required',
    ]);

    $photo_ids = explode(",", $request->photo_id);

    $data = ProgressPhoto::where('customer_id', $request->customer_id)->whereIn('id',$photo_ids)->get();
  
   

    if($data->toArray()){
      foreach ($data as $key => $value) {
        $photos = ProgressPhoto::find($value->id);
        $image_path = public_path().'/media/progress-photos/'.$photos->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $photos->delete();
        
      }

     
      return response()->json([
        'success' => true,
        'message' => "Progess Photo Deleted successfully"
      ], 200);
    }
    return response()->json([ 'error' => true, 'message' => 'This Photo does not exists' ],401);
    
  }
} 
