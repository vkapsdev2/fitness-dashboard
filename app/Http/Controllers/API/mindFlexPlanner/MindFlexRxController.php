<?php

namespace App\Http\Controllers\API\mindFlexPlanner;

use Validator;
use App\Models\MindFlexPlanner\MindFlexRx;
use App\Models\MindFlexPlanner\Accountability;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class MindFlexRxController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function mindFlexRxList(Request $request){

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required'
        ]);
    	$data = MindFlexRx::where('customer_id',$request->input('customer_id'))->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function accountabilityAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id'       => 'required',
            'goal'              => 'required',
            'goal_2_step'       => 'required',
            'question_to_coach' => 'required',
            'other'             => 'required'
        ]);

        $data = [
            'customer_id'       => trim($request->input('customer_id')),
            'goal'              => trim($request->input('goal')),
            'goal_2_step'       => trim($request->input('goal_2_step')),
            'question_to_coach' => trim($request->input('question_to_coach')),
            'other'             => trim($request->input('other')),
            
        ];
        $data = Accountability::create($data);
        return response()->json([ 'success' => true, 'message' => 'Accountability Add Successfully' ], 200); 

    }
}
