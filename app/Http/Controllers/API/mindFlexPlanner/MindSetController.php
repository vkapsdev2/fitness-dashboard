<?php

namespace App\Http\Controllers\API\mindFlexPlanner;

use Validator;
use App\Models\MindFlexPlanner\MindSet\DailyAffirmation;
use App\Models\MindFlexPlanner\MindSet\Assignment;
use App\Models\MindFlexPlanner\MindSet\AssignmentStatus;
use App\Models\MindFlexPlanner\MindSet\CoachQuestion;
use App\Models\MindFlexPlanner\Movement\HomeWorkout;
use App\Models\MindFlexPlanner\Movement\SubHomeWorkout;
use App\Models\MindFlexPlanner\Movement\GymWorkout;
use App\Models\MindFlexPlanner\Movement\SubGymWorkout;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class MindSetController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function dailyAffirmationsList(Request $request){
    	$data = DailyAffirmation::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function questionList(Request $request){
    	$data = CoachQuestion::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public  function assignmentList(Request $request){

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required'
        ]);
     
        $data = Assignment::where('customer_id',$request->input('customer_id'))->get();
        if($data->toArray()){
            $data_arr = array();
            $result = array();
           
            foreach ($data as $key => $value) {
                 $day_count = 0; 
                $workout_category = $value['workout_category'];
                $exercise_id = $value['exercises_id'];
                $id = $value['id'];
                $week = $value['week'];
                $days = json_decode($value['data']);
                if($days->monday_exercises){ $day_count++; } 
                if($days->tuesday_exercises){ $day_count++; }
                if($days->wednesday_exercises){ $day_count++; }
                if($days->thursday_exercises){ $day_count++; }
                if($days->friday_exercises){ $day_count++; }
                if($days->saturday_exercises){ $day_count++; }
                if($days->sunday_exercises){ $day_count++; }
                if($workout_category == '1'){ 
                    $exercise = SubHomeWorkout::whereIn('id',$exercise_id)->get();
                    $result['id'] =$id;
                    $result['week'] =$week;
                    $result['workout_type'] = 'Home Workout';
                    $result['data'] = $exercise;
                    $result['day_per_week'] = $day_count;
                    //$res = array_merge($result, $exercise->toArray());
                }
                else{
                    $exercise = SubGymWorkout::whereIn('id',$exercise_id)->get();

                    $result['id'] = $value['id'];
                    $result['week'] = $value['week'];
                    $result['workout_type']= 'Gym Workout';
                    $result['data'] = $exercise;
                    $result['day_per_week'] = $day_count;
                    //$res = array_merge($result, $exercise->toArray());
                }
                array_push($data_arr, $result);
               
            }
            return response()->json([
               'success' => true,
                'data' => $data_arr
            ], 200);
        }else{
            return response()->json([
           'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function weekList(Request $request){

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'exercises_id' => 'required',
        ]);
        $exercises_id = $request->exercises_id;
        $data = Assignment::where('customer_id',$request->input('customer_id'))->where('exercises_id', 'like', "%\"{$exercises_id}\"%")->get();



        if($data->toArray()){

            $result = array();
            foreach ($data as $key => $value) {
               
                $data = json_decode($value['data'], true);
               // $result['data'] = json_decode($value['data'], true);
                $result['id'] = $value['id'];

                if($data['monday_exercises'] == null)
                    $result['monday_exercise_total'] = 0;
                else
                $result['monday_exercise_total'] = count($data['monday_exercises']);

                if($data['tuesday_exercises'] == null)
                    $result['tuesday_exercise_total'] = 0;
                else
                $result['tuesday_exercise_total'] = count($data['tuesday_exercises']);

                if($data['wednesday_exercises'] == null)
                    $result['wednesday_exercise_total'] = 0;
                else
                $result['wednesday_exercise_total'] = count($data['wednesday_exercises']);

                if($data['thursday_exercises'] == null)
                    $result['thursday_exercise_total'] = 0;
                else
                $result['thursday_exercise_total'] = count($data['thursday_exercises']);


                if($data['friday_exercises'] == null)
                    $result['friday_exercise_total'] = 0;
                else
                    $result['friday_exercise_total'] = count($data['friday_exercises']);

                if($data['saturday_exercises'] == null)
                    $result['saturday_exercise_total'] = 0;
                else
                 $result['saturday_exercise_total'] = count($data['saturday_exercises']);

                if($data['sunday_exercises'] == null)
                    $result['sunday_exercise_total'] = 0;
                else
                $result['sunday_exercise_total'] = count($data['sunday_exercises']);
            }
            return response()->json([
               'success' => true,
                'data' => $result
            ], 200);
        }else{
            return response()->json([
               'success' => false,
                'message' => 'Data Not Found'
            ], 401);  
        }
    }

    public function dayExerciseList(Request $request){

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            //'exercises_id' => 'required',
            'day_name' => 'required'
        ]);
        $day = $request->input('day_name');
       // $exercises_id = $request->exercises_id;
        $data = Assignment::where('customer_id',$request->input('customer_id'))->get();


        if($data->toArray()){
            $result = array();
            $res = array();
            foreach ($data as $key => $value) {
               
                $data = json_decode($value['data'], true);
                
                $workout_category = $value['workout_category'];

                if($data[$day.'_exercises'] == null){

                    return response()->json([
                       'success' => false,
                        'message' => 'Exercise is not available'
                    ], 401);

                }else{
                   
                    $exercise_id = $data[$day.'_exercises'];

                    if($workout_category == '1'){ 
                    $exercise = SubHomeWorkout::whereIn('id',$exercise_id)->get();

                    $res['id'] = $value['id'];
                    $res['rest_timing'] = $value['rest_timing'];
                    $res['workout_type'] = 'Home Workout';
                    $res['exercise'] = $exercise ;
                    $res['sets'] = $data[$day.'_sets'];
                    //$res = array_merge($exercise_result, $exercise->toArray());
                    }
                    else{
                        $exercise = SubGymWorkout::whereIn('id',$exercise_id)->get();
                        $res['id'] = $value['id'];
                        $res['rest_timing'] = $value['rest_timing'];
                        $res['workout_type']= 'Gym Workout';
                        $res['exercise'] = $exercise ;
                        $res['sets'] = $data[$day.'_sets'];
                       // $res = array_merge($exercise_result, $exercise->toArray());
                    }
                    array_push($result, $res);
                }
            }
            
            return response()->json([
               'success' => true,
                'data' =>   $result
            ], 200);
        }else{
            return response()->json([
               'success' => false,
                'message' => 'Data Not Found'
            ], 401);  
        }

    }
    public function markComplete(Request $request){
        $request->validate([
            'assignment_id' =>'required',
            'customer_id' => 'required',
            'status' => 'required',
        ]);
        $res = Assignment::where('customer_id', $request->customer_id)->where('id',$request->assignment_id)->exists();
        if($res){
            
            $statusCheck = AssignmentStatus::where('customer_id', $request->customer_id)->where('assignment_id',$request->assignment_id)->exists();
            if($statusCheck){
               return response()->json([
                   'error' => true,
                    'message' => "Assignment's status already added"
                ], 401);
            }

            $data = new AssignmentStatus([
                'assignment_id' =>$request->assignment_id,
                'customer_id' => $request->customer_id,
                'status' => $request->status,
            ]);
            $data->save();

            return response()->json([
               'success' => true,
                'message' =>'Status added successfully!'
            ], 200);

        }
        return response()->json([
           'error' => true,
            'message' => 'Assignment does not exists'
        ], 401); 
        
    }
}
