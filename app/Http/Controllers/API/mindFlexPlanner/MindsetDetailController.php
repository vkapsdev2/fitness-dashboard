<?php

namespace App\Http\Controllers\API\mindFlexPlanner;

use DB;
use Validator;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\MindFlexPlanner\MindSet\MindsetDetail;
use App\Models\MindFlexPlanner\MindSet\MindsetResource;
use App\Models\MindFlexPlanner\MindSet\MindsetLesson;
use App\Models\MindFlexPlanner\MindSet\MindsetLessonFile;

class MindsetDetailController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

     public function mindsetDetail(Request $request){

        $this->validate($request, [
            'mindset_type' => 'required',
        ]);

        $mindset_id = $request->input('mindset_type'); 

        $mindset_detail = MindsetDetail::find($mindset_id);
    	$mindset_resource = MindsetResource::where('mindset_detail_id',$mindset_id)->get();
        
    	if($mindset_detail){
            return response()->json([
                'success' =>true,
                'detail' =>$mindset_detail,
                'files' =>$mindset_resource,
            ],200);
        }
        else{
            return response()->json([
                'success' => false,
                'message' => 'data Not found'
            ], 401);
        }
    }

    public function lesson(Request $request){
    	$data = MindsetLesson::get();

    	 $result = $this->returnStatus($data);
        return $result;
    }

    public function lessonFile(Request $request){
    	$this->validate($request,[
    		'lesson_id' =>'required'
    	]);

    	$lesson_id = $request->lesson_id;

    	$lesson = MindsetLesson::find($lesson_id);

    	$files = MindsetLessonFile::where('lesson_id',$lesson_id)->get();

    	if($lesson){
    		return response()->json([
    			'success' =>true,
    			'lesson' =>$lesson,
    			'files' =>$files,
    		],200);
    	}
    	else{
            return response()->json([
	            'success' => false,
	            'message' => 'Lesson Not found'
        	], 401);
        }
    	
    }
}
