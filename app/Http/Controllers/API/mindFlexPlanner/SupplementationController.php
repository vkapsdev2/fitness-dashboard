<?php

namespace App\Http\Controllers\API\mindFlexPlanner;

use Validator;
use App\Models\MindFlexPlanner\Supplimentation\ReoccurReminder;
use App\Models\MindFlexPlanner\Supplimentation\Guide;
use App\Models\MindFlexPlanner\Supplimentation\Suppliment;
use App\Models\MindFlexPlanner\Supplimentation\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SupplementationController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }

    public function reoccurringRemindersList(Request $request){
    	/*$access_token = $request->header('Authorization');
        $auth_header = explode(' ', $access_token);
        $token = $auth_header[1];
        $token_parts = explode('.', $token);
        $token_header = $token_parts[1];
        $token_header_json = base64_decode($token_header);
        $token_header_array = json_decode($token_header_json, true);
        $user_token = $token_header_array['jti'];

        $id = DB::table('oauth_access_tokens')->where('id', $user_token)->value('user_id');

        $roleID = DB::table('model_has_roles')->where('model_id', $id)->value('role_id');

        if($roleID == 3){
            /* Customer */
            //$data = DB::table('reoccur_reminder')->where('customer_id', $id)->get();
            /* Join with user/customer table */
        //}else{
            /* Coach */
          //  $data = DB::table('reoccur_reminder')->where('coach_id', $id)->get();
            /* Join with user/customer table */
      //  }*/

        $request->validate([
            'customer_id' => 'required'
        ]);

        $customer_id = $request->input('customer_id');
        $data  = ReoccurReminder::where('customer_id',$customer_id)->get();
        $result = $this->returnStatus($data);
        return $result;
    }
    
    public function reminderDelete(Request $request){
        $request->validate([
            'reminder_id' => 'required'
        ]);

        $id  = $request->input('reminder_id');

        if (ReoccurReminder::find($id)) {
            ReoccurReminder::where('id', $id)->delete();

            return response()->json(['success' => true,'message'=> 'Reminder has been deleted Sucessfully!']);
        }else{
            return response()->json([ 'error' => true, 'message' => 'This reminder does not exist' ],400);
        }
        
    }
    public function guideBooksList(Request $request){
        $data = Guide::get();
        $result = $this->returnStatus($data);
        return $result;
    }

    public function mySupplementsList(Request $request){
        $request->validate([
            'customer_id' => 'required'
        ]);
        $customer_id  = $request->input('customer_id');

    	$data = Suppliment::where('customer_id',$customer_id)->get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function mySupplimentAdd(Request $request){
        $request->validate([
            'customer_id' => 'required',
            'name' => 'required'
        ]);

        $supplimentation = new Suppliment([
            'customer_id'    => $request->input('customer_id'),
            'name'    => $request->input('name'),
        ]);
        $supplimentation->save();

        return response()->json([ 'success' => true, 'message' => "Suppliment Added successfully" ],200);

    }

    public function storeList(Request $request){
    	$data = Store::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }
}
