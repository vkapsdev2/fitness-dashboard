<?php

namespace App\Http\Controllers\API\mindFlexPlanner;

use Validator;
use App\Models\MindFlexPlanner\Movement\HomeWorkout;
use App\Models\MindFlexPlanner\Movement\SubHomeWorkout;

use App\Models\MindFlexPlanner\Movement\GymWorkout;
use App\Models\MindFlexPlanner\Movement\SubGymWorkout;

use App\Models\MindFlexPlanner\Movement\Hit;

use App\Models\MindFlexPlanner\Movement\FreeWeight;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class MovementController extends Controller
{
    public function returnStatus($data){
        if($data->toArray()){
            return response()->json([
           'success' => true,
            'data' => $data
        ], 200);

        }else{
            return response()->json([
            'success' => false,
            'message' => 'Data Not Found'
        ], 401);
        }
    }
    
    public function homeWorkoutsList(Request $request){
        $data = HomeWorkout::get();
        foreach ($data as $key => $value) {
            $id = $value->id;
            $data[$key]['movement_sub_homeworkouts'] = DB::table('movement_sub_homeworkouts')->where('homeWorkout_id', '=', $id)->get();
        }

    	/* $data = DB::table('movement_homeworkouts')
        ->join('movement_sub_homeworkouts',function($join) {
        $join->on('movement_sub_homeworkouts.homeWorkout_id' , '=' , 'movement_homeworkouts.id'); })->get(); */

        $result = $this->returnStatus($data);
        return $result;
    }

    public function gymWorkoutsList(Request $request){
        $data = GymWorkout::get();
        foreach ($data as $key => $value) {
            $id = $value->id;
            $data[$key]['movement_sub_gymworkouts'] = DB::table('movement_sub_gymworkouts')->where('gymWorkout_id', '=', $id)->get();
        }

    	// $data = DB::table('movement_gymworkouts')
     //    ->join('movement_sub_gymworkouts',function($join) {
     //    $join->on('movement_sub_gymworkouts.gymWorkout_id' , '=' , 'movement_gymworkouts.id'); })->get();

        $result = $this->returnStatus($data);
        return $result;
    }

    public function hitList(Request $request){
    	$data = Hit::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }

    public function freeWeightsList(Request $request){
    	$data = FreeWeight::get();
    	$result = $this->returnStatus($data);
    	return $result;
    }
}
