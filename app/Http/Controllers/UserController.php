<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        $roles = Role::pluck('name','name')->all();
        return view('users.index',compact('roles'));
        //
            // ->with('i', ($request->input('page', 1) - 1) * 5)
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();
        $input['password'] = Hash::make($input['password']);


        $user = User::create($input);
        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();


        return view('users.edit',compact('user','roles','userRole'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }


        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();


        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }


    /*$results->count()
$results->currentPage()
$results->firstItem()
$results->hasMorePages()
$results->lastItem()
$results->lastPage() (Not available when using simplePaginate)
$results->nextPageUrl()
$results->perPage()
$results->previousPageUrl()
$results->total() (Not available when using simplePaginate)
$results->url($page)*/

/*https://www.itsolutionstuff.com/post/laravel-58-user-roles-and-permissions-tutorialexample.html*/
// https://keenthemes.com/keen/?page=docs&section=datatable


    /*Ajax List of Users*/
    public function ajaxList(Request $request)
    {
    	$postData = $request->all();
    	$page = (!empty($postData['pagination']['page'])) ? $postData['pagination']['page'] : 1;
    	$perpage = (!empty($postData['pagination']['perpage'])) ? $postData['pagination']['perpage'] : 10;
    	$sort = (!empty($postData['sort']['sort'])) ? $postData['sort']['sort'] : 'asc';
    	$sortField = (!empty($postData['sory']['field'])) ? $postData['sort']['field'] : 'id';


    


        $data = User::orderBy($sortField ,$sort)->paginate($perpage);
        $meta = [
        	'page' => $data->currentPage(),
        	'perpage' => $data->perPage(),
        	'pages' => $data->lastPage(),
        	'page' => $data->currentPage(),
        	'total' => $data->total(),
        	'sort' => $sort,
            'field' => $sortField

        ];
        
        $users = [];


        foreach ($data->items() as $user) {

        	$roles = [];
        	if(!empty($user->getRoleNames())){
        		foreach($user->getRoleNames() as $v){
        			$roles[] = $v;
        		}

        	}
        
           // <label class="badge badge-success">{{ $v }}</label>
       
        	$users[] = [
        		'id' => $user->id,
        		'name' => $user->name,
        		'email' => $user->email,
        		'roles' => implode(',', $roles),
        		'actions' => null
        	];
        }


         return response()->json(['meta' =>$meta,'data'=>$users]);
    }

    
     public function ajaxGet($id)
    {
       $user = User::find($id);
       $roles = Role::pluck('name','name')->all();
       $userRole = $user->roles->pluck('name','name')->all();
       return response()->json(['success' => true,'user' => $user,'roles' => $roles,'userRole' => $userRole]);
    }


    public function ajaxDelete($id)
    {
        User::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'User deleted successfully']);
    }

    public function ajaxUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'required|email|unique:users,email,'.$id,
            // 'password' => 'same:confirm-password',
            'role' => 'required'
        ]);


        $input = $request->all();
        /*if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }*/


        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();


        $user->assignRole($request->input('role'));
        return response()->json(['success' => true,'message'=> 'User updated successfully']);
        
    }

    public function ajaxAdd(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role' => 'required'
        ]);


        $input = $request->all();
        $input['password'] = Hash::make($input['password']);


        $user = User::create($input);
        $user->assignRole($request->input('role'));


        return response()->json(['success' => true,'message'=> 'User added successfully']);
       
    }

    public function facebookCheck(){


        $fb = new \Facebook\Facebook([
          'app_id' => '846858665845822',           //Replace {your-app-id} with your app ID
          'app_secret' => 'bb71850a8f9d074f8e9d9a1657bc2abe',   //Replace {your-app-secret} with your app secret
          'graph_api_version' => 'v5.0',
        ]);

        try {
   
            // Get your UserNode object, replace {access-token} with your token
              $response = $fb->get('/me?fields=name,first_name,last_name,email', 'EAAMCNqXz5D4BAP1VZAu2uvKmxOpPN4R99nzJH4eVrdFUR7ppIHPii4gGiZAfJzXKt7ZBHI7ygYH0Xpe0lvqqu6J2jlF9ClGBybDZCjtDTXHH6SNQ7NRmI3Dnoi6qwGf5ZCPvITeDt3UOKLUU6KyWZBAMCNsleWFZBbnnTWKKqPemjXjpAoYf0fP7fUwP4piQxN8Tvu2soxDTdUXGcx2T8WYoN9pa19KTb6ybSZAZA7e9nDKoutUdjK5ZBnsFRDL7fDTHUZD');

        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                // Returns Graph API errors when they occur
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                // Returns SDK errors when validation fails or other local issues
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }

        $me = $response->getGraphUser();
       
               //All that is returned in the response
        echo 'All the data returned from the Facebook server: ' . $me;

               //Print out my name
        echo 'My name is ' . $me->getName();
        echo $fbemail = $me->getProperty('email');
    }

}