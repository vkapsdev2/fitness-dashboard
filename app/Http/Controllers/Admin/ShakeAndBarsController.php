<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\ShakeAndBar;
use App\Models\Ingredient;
use App\Models\Coupon;
use App\Models\Order;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class ShakeAndBarsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	//echo 'list';die;
    	$shakeAndBars  =   ShakeAndBar::orderBy('id', 'DESC')
    						->get();
    	return View('pages.admin.shakeAndBar.shake.list',compact('shakeAndBars'));

    }

    public function create()
    {

    	//echo 'create'; die;
    	$ingredients  =   Ingredient::orderBy('id', 'ASC')
    									->where('ingredients_status',"1")
    									->get();
    	return View('pages.admin.shakeAndBar.shake.create', compact('ingredients')); 
    	
    }

    public function save(Request $request)
    {

    	$this->validate($request, [
            'shake_name' 		    => 'required',
            'price'             => 'required',
            'shake_type' 		    => 'required',
            'shake_image'   		=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_shake_customize'=> 'required',
            'shake_status' 		  => 'required',
            'calories'          => 'required',
            'protein'           => 'required',
            'carbs'             => 'required',
            'fats'              => 'required',
            
        ]);


    	$shake_type_string = $shake_ingredients_string  = null;
        if($request->input('shake_type')!== null)
      	{
        	$shake_type_string = implode(", ", $request->input('shake_type'));
      	}

      	if($request->input('shake_ingredients')!== null)
      	{
        	$shake_ingredients_string = implode(", ", $request->input('shake_ingredients'));
      	}

      	//Upload Shake image
        if ($files = $request->file('shake_image')) {
           $destinationPath = public_path().'/media/shake'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }

      	if(($request->input('is_shake_customize') == 1))
      	{

          $this->validate($request, [
            'shake_ingredients'    => 'required',
         ]);
      		
      	}
      	
      	$data = [
            'shake_name'     => trim($request->input('shake_name')),
            'price'          => trim($request->input('price')),
            'shake_img'      => $fileName,
            'shake_type'     => $shake_type_string,
            'calories'       => trim($request->input('calories')),
            'protein'        => trim($request->input('protein')),
            'carbs'          => trim($request->input('carbs')),
            'fats'           => trim($request->input('fats')),

            'is_shake_customize'   => trim($request->input('is_shake_customize')),
            'shake_ingredients'    => $shake_ingredients_string,
            'shake_description'    => trim($request->input('shake_description')),
        ];
          $shakeAndBar = ShakeAndBar::create($data);
          return redirect()->route('shakeBar.index')
                        ->with('success','Shake created successfully');
   }

   public function edit($id)
    {
    	//echo 'hello'; die;

    	$shakeAndBar 		= ShakeAndBar::findOrFail($id);
    	$shake_types_arr 	= array();
		$shake_types        = $shakeAndBar->toArray();
        
        if($shake_types){
          $types 			= $shake_types['shake_type'];
          $shake_types_arr  = explode(', ', $types);
        }


        $ingredient_types_arr 	= array();
		$ingredient_types        = $shakeAndBar->toArray();
        
        if($ingredient_types){
          $types_ingred 			= $shake_types['shake_ingredients'];
          $ingredient_types_arr  = explode(', ', $types_ingred);
        }


    	$ingredients  =   Ingredient::orderBy('id', 'ASC')
    									->where('ingredients_status',"1")
    									->get();

    	// $shakeAndBar  =   ShakeAndBar::where('id',$id)->get();
    	
    	return View('pages.admin.shakeAndBar.shake.edit', compact('ingredients', 'shakeAndBar','shake_types_arr', 'ingredient_types_arr'));

    }

    public function update(Request $request, $id)
    {

    	$ShakeAndBar = ShakeAndBar::find($id);

    	$this->validate($request, [
            'shake_name' 		=> 'required',
            'price'         => 'required',
            'shake_type' 		=> 'required',
            'shake_image'   =>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'calories'      => 'required',
            'protein'       => 'required',
            'carbs'         => 'required',
            'fats'          => 'required',
            'shake_status'  => 'required',
            'is_shake_customize'=> 'required',
            
        ]);

        $shake_type_string = $shake_ingredients_string  = null;
        if($request->input('shake_type')!== null)
      	{
        	$shake_type_string = implode(", ", $request->input('shake_type'));
      	}

      	if($request->input('shake_ingredients')!== null)
      	{
        	$shake_ingredients_string = implode(", ", $request->input('shake_ingredients'));
      	}


      	if ($request->hasFile('shake_image')){
      		 
      		
		    $image_path = public_path().'/media/shake/'.$ShakeAndBar->shake_img;
		    if (File::exists($image_path)) {
		        File::delete($image_path);
		    }
		    $bannerImage = $request->file('shake_image');
		    $fileName = $bannerImage->getClientOriginalName();
		    $destinationPath = public_path().'/media/shake';
		    $bannerImage->move($destinationPath, $fileName);
		} else {
		    $fileName = $ShakeAndBar->shake_img;
		}

		$is_shake_customize_val = $request->input('is_shake_customize');

		if($is_shake_customize_val==2)
        {
	       	$shake_ingredients_string  = null;
	    }



		$ShakeAndBar->shake_name    		= trim($request->input('shake_name'));
    $ShakeAndBar->price             = trim($request->input('price'));
		$ShakeAndBar->shake_type     		= $shake_type_string;
		$ShakeAndBar->is_shake_customize= trim($request->input('is_shake_customize'));
		$ShakeAndBar->shake_ingredients = $shake_ingredients_string;
		$ShakeAndBar->shake_description = trim($request->input('shake_description'));
		$ShakeAndBar->shake_status      = $request->input('shake_status');
    $ShakeAndBar->calories          = $request->input('calories');
    $ShakeAndBar->protein           = $request->input('protein');
    $ShakeAndBar->carbs             = $request->input('carbs');
    $ShakeAndBar->fats              = $request->input('fats');
		$ShakeAndBar->shake_img    			= $fileName;

		$ShakeAndBar->save();
		return redirect()->route('shakeBar.index')
                        ->with('success','Shake And Bar updated successfully');
    }

     public function detailView(Request $request, $id)
     {

     	$ingredients  =   Ingredient::orderBy('id', 'ASC')
    									->where('ingredients_status',"1")
    									->get();


		$shakeAndBar 			= ShakeAndBar::findOrFail($id);
		$shake_detail            = $shakeAndBar->toArray();
		$ingredient_types_arr 	= array();
		$ingredient_types       = $shakeAndBar->toArray();
        
        if($ingredient_types){
          $types_ingred 		 = $shake_detail['shake_ingredients'];
          $ingredient_types_arr  = explode(', ', $types_ingred);

        }
       // print_r($ingredient_types_arr);die;
        return View('pages.admin.shakeAndBar.shake.detail', compact( 'shakeAndBar','ingredients','ingredient_types_arr'));
    }

    public function delete($id)
    {
      $data = ShakeAndBar::find($id);
        $image_path = public_path().'/media/shake/'.$data->shake_img;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

    	// ShakeAndBar::where('id',$id)->delete();
      return response()->json(['success' => true,'message'=> 'Shake has been deleted']);

    }

    public function ListIngredients()
    {
    	$ingredients  =   Ingredient::orderBy('id', 'DESC')->get();
    	return View('pages.admin.shakeAndBar.ingredients.list',compact('ingredients'));

    }

    public function IngredientsCreate()
    {
    	return View('pages.admin.shakeAndBar.ingredients.create');
    }

    public function IngredientsSave(Request $request)
    {
    	$this->validate($request, [
            'name' 				 => 'required',
            'ingredients_status' => 'required',
        ]);

        $data = [
            'name'       		=> trim($request->input('name')),
           'price' 		=> trim($request->input('price')),
           'ingredients_status' => trim($request->input('ingredients_status')),
          ];

    	 $Ingredient = Ingredient::create(
         $data);
		return redirect()->route('shakeBarIngredients.index')
                        ->with('success','Ingredients added successfully');

    }

     public function IngredientsEdit($id)
    {
    	 $ingredient = Ingredient::findOrFail($id);
        $data =[
          'ingredient'  => $ingredient,
        ];
        return View('pages.admin.shakeAndBar.ingredients.edit',  compact('ingredient'));
    }

    public function IngredientsUpdate(Request $request, $id)
    {
    	$this->validate($request, [
            'name' 				 => 'required',
            'ingredients_status' => 'required',
        ]);

        $data = [
            'name'       		=> trim($request->input('name')),
           'price' 		=> trim($request->input('price')),
           'ingredients_status' => $request->input('ingredients_status'),
          ];

          $Ingredient = Ingredient::find($id);
          $Ingredient->update($data);

          return redirect()->route('shakeBarIngredients.index')
                        ->with('success','Ingredients updated successfully');
    }

    public function IngredientsDelete($id)
    {
    	Ingredient::where('id',$id)
                  ->delete();
        
            return response()->json(['success' => true,'message'=> 'Ingredients has been deleted']);
    }

    public function ListCoupons()
    {

       $coupons = Coupon::orderBy('id', 'DESC')->get();
      return View('pages.admin.shakeAndBar.coupon.list', compact('coupons'));

    }
    public function CouponCreate()
    {
      return View('pages.admin.shakeAndBar.coupon.create');
    }

    public function CouponSave(Request $request)
    {
      $this->validate($request, [
            'name'          => 'required',
            'method'        => 'required',
            'discount_rate' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'validity'      => 'required',
            'code'          => 'required',
            'coupon_status' => 'required',
        ]);

      $data = [
            'name'          => trim($request->input('name')),
           'method'         => $request->input('method'),
           'discount_rate'  => trim($request->input('discount_rate')),
           'validity'       => trim($request->input('validity')),
           'code'           => $request->input('code'),
           'coupon_status'  => $request->input('coupon_status'),
          ];

          $Coupon = Coupon::create(
         $data);
        return redirect()->route('coupon.index')
                        ->with('success','Coupon added successfully');
    }

    public function CouponEdit($id)
    {
      
      $coupon = Coupon::findOrFail($id);
        $data =[
          'coupon'  => $coupon,
        ];
        return View('pages.admin.shakeAndBar.coupon.edit',  compact('coupon'));
    }
    public function CouponUpdate(Request $request, $id)
    {
       $coupon = Coupon::findOrFail($id);
       $this->validate($request, [
            'name'          => 'required',
            'method'        => 'required',
            'discount_rate' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'validity'      => 'required',
            'code'          => 'required',
            'coupon_status' => 'required',
        ]);

         $data = [
            'name'          => trim($request->input('name')),
           'method'         => $request->input('method'),
           'discount_rate'  => trim($request->input('discount_rate')),
           'validity'       => trim($request->input('validity')),
           'code'           => $request->input('code'),
           'coupon_status'  => $request->input('coupon_status'),
          ];

        $coupon->update($data);
        return redirect()->route('coupon.index')
                        ->with('success','Coupon updated successfully');
    }
   
    public function CouponDelete($id)
    {
      Coupon::where('id',$id)
                  ->delete();
        
            return response()->json(['success' => true,'message'=> 'Coupon has been deleted']);
    }

    public function ListOrder()
    {
      $orders = DB::table('orders')
        ->join('users','users.id','orders.customer_id')
        ->orderBy('id', 'DESC')
        ->select(['users.name', 'orders.*'])
        ->get();
        //print_r($orders);die;
      return View('pages.admin.shakeAndBar.order.list', compact('orders'));

          //echo "order List";
    }
    public function OrderCreate()
    {

      $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $coupons = Coupon::orderBy('id', 'DESC')->get();
      

      return View('pages.admin.shakeAndBar.order.create', compact('customers', 'coupons'));
    }
    public function OrderSave(Request $request)
    {
      
      $orderDate         =  date('Y-m-d');
      $is_applied_coupon = 0;
      $total_item        = 0;
      $total_price       = 0;
      $grandTotal        = 0;
      $discountRate      = 0;
      $payment_status    = 1;
      $coupon_name       = "";
      if(($request->input('coupon')!='')|| ($request->input('coupon')!= null))
      {
         $is_applied_coupon = 1;
      }
     $orderDetail = array();
      //echo "Order Creating";
      $orderDetail[0]=
       array (
               "shake_id" => 1,
               "quantity" => 3,
               "price"    =>100, 
               "extraIngredient" => '3,2,1'
            );
        $orderDetail[1] = 
        array (
               "shake_id" => 2,
               "quantity" => 2, 
               "price"    =>120,
               "extraIngredient" => '4,1'
            );
        $orderDetail[2] =
         array (
               "shake_id" => 3,
               "quantity" => 1, 
               "price"    =>130,
               "extraIngredient" => ''
            );


      for($i = 0; $i<count($orderDetail); $i++)
      {
        $get_all_quantity[] = $orderDetail[$i]['quantity'];
        $get_all_total[]    = $orderDetail[$i]['price'] * $orderDetail[$i]['quantity'];
      }


     $total_item  =  array_sum($get_all_quantity);
     $total_price =  array_sum($get_all_total);

     if($request->input('coupon'))
     {
          $coupon_id   = $request->input('coupon');
          $coupon      =   Coupon::where('id',$coupon_id)
                        ->get();
         $total_discount  = $coupon[0]->discount_rate;
         $discount_method = $coupon[0]->method;
         $coupon_name = $coupon[0]->name;

         if($discount_method ==1) //FixedCost
         {
           $grandTotal = $total_price - $total_discount;
           $discountRate = '$'.$coupon[0]->discount_rate;
         }
         if($discount_method ==2) //Percent
         {
           $total_discount = $total_price *($total_discount / 100);
           $grandTotal =  $total_price - $total_discount;
           $discountRate = $coupon[0]->discount_rate.'%';
         }
      }
      else
      {
        $grandTotal =  $total_price;
      }
    /* Get Discount rate*/
    $data = [
      'customer_id'       => $request->input('customer_id'),
      'date'              => $orderDate,
      'order_detail'      => json_encode($orderDetail),
      'payment_method'    => $request->input('payment_method'),
      'is_applied_coupon' => $is_applied_coupon,
      'coupon'            => $coupon_name,
      'total_item'        => $total_item,
      'total'             => $total_price,
      'discount_rate'     => $discountRate,
      'grand_total'       => $grandTotal,
      'payment_status'    => $payment_status,
      'order_status'      => $request->input('order_status'),
    ];
    
    $Order = Order::create(
         $data);
        return redirect()->route('order.index')
                        ->with('success','Order added successfully');
      
    }
    public function OrderEdit($id)
    {
       echo "Page will be created soon...";
    }
    public function OrderUpdate(Request $request, $id)
    {
      echo "Page will be created soon...";
    }
    public function OrderDetailView($id)
    {
      $checkDetail = Order::findOrFail($id);

      $order = DB::table('orders')
        ->join('users','users.id','orders.customer_id')
        ->orderBy('id', 'DESC')
        ->select(['users.name', 'orders.*'])
        ->where('orders.id', $id)
        ->get();

      $shakes  =   ShakeAndBar::orderBy('id', 'DESC')
                ->get();
      $ingredients  =   Ingredient::orderBy('id', 'DESC')->get();

      return View('pages.admin.shakeAndBar.order.detail', compact('order', 'shakes', 'ingredients'));
      //echo "Page will be created soon...for ID: ".$id;
    }
    public function OrderDelete($id)
    {
      Order::where('id',$id)
                  ->delete();
        
            return response()->json(['success' => true,'message'=> 'Order has been deleted']);
    }

}
