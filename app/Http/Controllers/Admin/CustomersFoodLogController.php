<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use DB;
use Hash; 
use App\User;
use App\Models\CustomersFoodLog;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomersFoodLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        // $food_log = CustomersFoodLog::orderBy('id','DESC')->get();

         $food_logs = DB::table('customer_3days_foodlog')
        ->join('users',function($join) {
        $join->on('users.id' , '=' , 'customer_3days_foodlog.customer_id'); })->select(['customer_3days_foodlog.*', 'users.name as cust_name'])
        ->get();
        return View('pages.admin.customer_foodlog.list',  compact('food_logs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        //print_r($customers->toArray()); dd('fg');
        return view('pages.admin.customer_foodlog.create',  compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'customer_id'       => 'required',
            'first_day_foodlog' => 'required',
            'second_day_foodlog'=> 'required',
            'third_day_foodlog' => 'required',
           
        ]);
       //dd($request->input('start_date_time'));
        $food_log = CustomersFoodLog::create([
                'customer_id'          => $request->input('customer_id'),
                'first_day_foodlog'         => $request->input('first_day_foodlog'),
                'second_day_foodlog'         => $request->input('second_day_foodlog'),
                'third_day_foodlog'         => $request->input('third_day_foodlog'),
            ]);            
        $food_log->save();
       return redirect()->route('cust_food_log.index')
                        ->with('success','Customer Food Log added successfully'); 
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $food_log = CustomersFoodLog::findOrFail($id);
        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $data = [
            'food_log'            => $food_log,
            'customers'            => $customers,
        ];
        return view('pages.admin.customer_foodlog.edit',  compact('food_log','customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $food_log = CustomersFoodLog::find($id);
         $this->validate($request, [
            'customer_id'       => 'required',
            'first_day_foodlog' => 'required',
            'second_day_foodlog'=> 'required',
            'third_day_foodlog' => 'required',
           
        ]);

        $food_log->customer_id = $request->input('customer_id');
        $food_log->first_day_foodlog = $request->input('first_day_foodlog');
        $food_log->second_day_foodlog = $request->input('second_day_foodlog');
        $food_log->third_day_foodlog = $request->input('third_day_foodlog');
        $food_log->save();
        return redirect()->route('cust_food_log.index')
                        ->with('success','Customer Food Log updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        CustomersFoodLog::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Customer Food Log has been deleted']);
    }
}
