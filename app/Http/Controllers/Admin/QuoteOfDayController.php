<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use DB;
use Hash;
use File;
use App\Models\QuoteOfDay;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuoteOfDayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotes = QuoteOfDay::orderBy('id','DESC')->get();
       return View('pages.admin.QuoteOfTheDay.list',  compact('quotes'));
   } 
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.QuoteOfTheDay.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'author' => 'required',
           
        ]);
      
   // dd($request->input('quote_date'));
        $quote = QuoteOfDay::create([
                'title'              => $request->input('title'),
                'author'             => $request->input('author'),
                'description'        => $request->input('description'),
                'quote_date'        => $request->input('quote_date'),
                
            ]);            
        $quote->save();
       return redirect()->route('quote_of_day.index')
                        ->with('success','Quote added successfully');  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quote = QuoteOfDay::findOrFail($id);
        $data = [
            'quote'            => $quote,
        ];
        return View('pages.admin.QuoteOfTheDay.edit',  compact('quote'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $quote = QuoteOfDay::find($id);
         $this->validate($request, [
            'title' => 'required',
            'author' => 'required',
           
        ]);
   
        $quote->title        = $request->input('title');
        $quote->author       = $request->input('author');
        $quote->description  = $request->input('description');
        $quote->quote_date  = $request->input('quote_date');
        $quote->save();
        return redirect()->route('quote_of_day.index')
                        ->with('success','Quote updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        QuoteOfDay::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Quote has been deleted']);
    }

    public function ajaxDate(Request $request){
        $data = QuoteOfDay::whereDate('quote_date',$request->date)->first();
        return response()->json(['status' => true,'success'=>$data]);
    }
}
