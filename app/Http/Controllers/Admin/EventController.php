<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use DB;
use Hash; 
use App\Models\Event;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class EventController extends Controller
{
    public function index(Request $request){
     
       $events = Event::orderBy('id','DESC')->get();
	   return View('pages.admin.event.list',  compact('events'));
   } 

    public function create(){
        return View('pages.admin.event.create');
    }

    public function save(Request $request){

        $this->validate($request, [
            'title' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
            'end_date' => 'required',
            'end_time' => 'required',
            'banner_img' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'event_url' =>'required|url',
           
        ]);
       	if ($files = $request->file('banner_img')) {
           $destinationPath = public_path().'/media/event-banner'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }
       //dd($request->input('start_date_time'));
        $event = Event::create([
                'title'                => $request->input('title'),
                'location'             => $request->input('location'),
                'banner_img'           => $fileName,
                'event_url'            => $request->input('event_url'),
                'start_date'           => $request->input('start_date'),
                'start_time'           => $request->input('start_time'),
                'end_date'             => $request->input('end_date'),
                'end_time'             => $request->input('end_time'),
                
            ]);            
        $event->save();
       return redirect()->route('event.index')
                        ->with('success','Event created successfully');  
    }

    public function edit($id){
        $event = Event::findOrFail($id);
        $data = [
            'event'            => $event,
        ];
        return View('pages.admin.event.edit',  compact('event'));
    }

    public function update(Request $request, $id){
        
        $event = Event::find($id);
         $this->validate($request, [
            'title' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
            'end_date' => 'required',
            'end_time' => 'required',
            'event_url' =>'required|url',
           
        ]);
        if ($request->hasFile('banner_img')){
		    $image_path = public_path().'/media/event-banner/'.$event->banner_img;
		    if (File::exists($image_path)) {
		        File::delete($image_path);
		    }
		    $bannerImage = $request->file('banner_img');
		    $fileName = $bannerImage->getClientOriginalName();
		    $destinationPath = public_path().'/media/event-banner';
		    $bannerImage->move($destinationPath, $fileName);
		} else {
		    $fileName = $event->banner_img;
		}
         
        $event->title = $request->input('title');
        $event->location = $request->input('location');
        $event->banner_img = $fileName;
        $event->event_url = $request->input('event_url');
        $event->start_date = $request->input('start_date');
        $event->start_time = $request->input('start_time');
        $event->end_date = $request->input('end_date');
        $event->end_time = $request->input('end_time');
        $event->save();
        return redirect()->route('event.index')
                        ->with('success','Event updated successfully'); 
        
    }

    public function delete($id){
        $data = Event::find($id);
        $image_path = public_path().'/media/event-banner/'.$data->banner_img;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

        // Event::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Event has been deleted']);
    }

    public function search(Request $request){ 
        if($request->ajax()){
            $output='';
            $events = Event::Where('title', 'LIKE','%'.$request->search.'%')->orwhere('location', 'LIKE','%'.$request->search.'%')->get(); 

            $event = $events->toArray();
            
            if ($event) {
                foreach ($event as $key => $value) {

                    $output.='<tr>'.
                        '<td>'.$value['title'].'</td>'.
                        '<td>'.$value['location'].'</td>'.
                        '<td>'.$value['company_name'].'</td>'.
                        '<td><img style="height: 40px;  width: 80px;" src="'.url('/').'/media/event-banner/'.$value['banner_img'].'" /></td>'.
                        '<td><a href="'.$value['event_url'].'">Event URL</td>'.
                        '<td>'.$value['start_date'].'</td>'.
                        '<td>'.$value['start_time'].'</td>'.
                        '<td>'.$value['end_date'].'</td>'.
                        '<td>'.$value['end_time'].'</td>'.
                        '<td class="text-nowrap"><a href="event/edit/'.$value['id'].'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                        <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelete('.$value['id'].')">  <i class="fa fa-times"></i> </a>
                        </td>'.
                        '</tr>';

                }
                return Response($output);

            }
        }
    }

}
