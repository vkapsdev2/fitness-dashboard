<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Models\Coach;
use App\Models\Testimonial;
use App\Models\CoachAssignToCustomer;
use Hash;
use DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CoachesController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $coach_role_id = 2;
        $coaches = DB::table('users')
            ->join('model_has_roles','model_has_roles.model_id','users.id')
            ->select(['users.*'])
            ->where('model_has_roles.role_id', $coach_role_id)
            ->orderBy('id','DESC')
            ->get();
        return View('pages.admin.coach.list',compact('coaches'));
    }

    public function create(){
    	return view('pages.admin.coach.create');
    }

    public function save(Request $request){
        $this->validate($request, [
            /*basic info*/
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required',
            'phone'                 => 'required|min:10|numeric',
            'address'               => 'required',
            'gender'                => 'required',
            
            'specializations'       => 'required',
            'certification'         => 'required',
            'awards'                => 'required',
            'publish_articles'      => 'required',
            'client_feedback'       => 'required',
            'state'                 => 'required',
            'city'                  => 'required',
            'zip'                   => 'required|numeric',
        ]);
        $coach_role = 2;
        /* Upload Coach image */
        if ($files = $request->file('image')) {
           $destinationPath = public_path().'/media/coach';
           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }
       /* End image upload code. */
        $password = Hash::make($request->input('paasword'));
        $coach = User::create([
            'name'           => $request->input('name'),
            'email'          => $request->input('email'),
            'phone'          => $request->input('phone'),
            'address'        => $request->input('address'),
            'gender'         => $request->input('gender'),
            'city'           => $request->input('city'),
            'state'          => $request->input('state'),
            'zip'            => $request->input('zip'),
            'facebook_url'   => $request->input('facebook_url'),
            'google_url'     => $request->input('google_url'),
            'twitter_url'    => $request->input('twitter_url'),
            'youtube_url'    => $request->input('youtube_url'),
            'instagram_url'  => $request->input('instagram_url'),
            'image'          => $fileName,
            'password'       => $password,
            'country'        => '',
            'status'         => 1,
            'provider'       => "manual",
        ]);
        /* Add  details in Coach table*/
        if(isset($coach['id'])){ 
            $coach_detail = Coach::create([
                'coach_id'              => $coach['id'],
                'coach_name'            => trim($request->input('name')),
                'specializations'       => trim($request->input('specializations')),
                'certification'         => trim($request->input('certification')),
                'awards'                => trim($request->input('awards')),
                'publish_articles'      => trim($request->input('publish_articles')),
                'client_feedback'       => trim($request->input('client_feedback')),

            ]);
            $coach->assignRole($coach_role);
        }
        return redirect()->route('coach.index')->with('success','Coach created successfully');
    }

    public function edit($id){
        $coach = DB::table('users')
            ->join('coaches','coaches.coach_id','users.id')
            ->select(['coaches.*' ,'users.*'])
            ->where('users.id', $id)
            ->get(); 
        if(count($coach) > 0 ){
            $data = [
                'coach'   => $coach,
            ];
            return View('pages.admin.coach.edit', compact('coach'));
        }
        return abort(404);   
    }

    public function update(Request $request, $id){
        $coach = User::find($id);
        $this->validate($request, [
            /*basic info*/
            'name'                  => 'required',
            'phone'                 => 'required|min:10|numeric',
            'address'               => 'required',
            'gender'                => 'required',
            'specializations'       => 'required',
            'certification'         => 'required',
            'awards'                => 'required',
            'publish_articles'      => 'required',
            'client_feedback'       => 'required',
            'state'                 => 'required',
            'city'                  => 'required',
            'zip'                   => 'required|numeric',
        ]);

        if ($request->hasFile('image')){
            $image_path = public_path().'/media/coach/'.$coach->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $coachImage = $request->file('image');
            $fileName = $coachImage->getClientOriginalName();
            $destinationPath = public_path().'/media/coach';
            $coachImage->move($destinationPath, $fileName);
        } else {
            $fileName = $coach->image;
        }
        $coach->name           = $request->input('name');
        $coach->phone          = $request->input('phone');
        $coach->address        = $request->input('address');
        $coach->state          = $request->input('state');
        $coach->city           = $request->input('city');
        $coach->zip            = $request->input('zip');
        $coach->gender         = $request->input('gender');
        $coach->facebook_url   = $request->input('facebook_url');
        $coach->google_url     = $request->input('google_url');
        $coach->twitter_url    = $request->input('twitter_url');
        $coach->youtube_url    = $request->input('youtube_url');
        $coach->instagram_url  = $request->input('instagram_url');
        $coach->image          = $fileName;
        $coach->save();

        $arr = [
            'coach_name'              => trim($request->input('name')),
            'specializations'         => trim($request->input('specializations')),
            'certification'           => trim($request->input('certification')),
            'certification'          => trim($request->input('certification')),
            'awards'                 => trim($request->input('awards')),
            'publish_articles'       => trim($request->input('publish_articles')),
            'client_feedback'       => trim($request->input('client_feedback')),
            
        ];

        $affected = DB::table('coaches')->where('coach_id', $id)->update($arr);

        return redirect()->route('coach.index')->with('success','Coach updated successfully');
    }

    public function detailView($id){
        $coach = DB::table('users')
            ->join('coaches','coaches.coach_id','users.id')
            ->select(['coaches.*' ,'users.*'])
            ->where('users.id', $id)
            ->get();
        if(count($coach) > 0 ){
            return View('pages.admin.coach.detail',  ['coach' => $coach]);
        }
        return abort(404);   
    }

    public function delete($id){
        $data = User::find($id);
        $image_path = public_path().'/media/coach/'.$data->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();
        Coach::where('coach_id', $id)->delete();
        return response()->json(['success' => true,'message'=> 'Coach has been deleted']); 
    }

    public function TestimonialList(Request $request)
    {
        $customer =  DB::table('testimonials')
                        ->join('users', 'users.id','testimonials.from_customer')
                        ->select(['users.name'])
                        ->orderBy('testimonials.id','DESC')
                        ->get();

        $coach =  DB::table('testimonials')
                    ->join('users', 'users.id','testimonials.to_coach')
                    ->select(['users.name'])
                    ->orderBy('testimonials.id','DESC')
                    ->get();
        
        $testimonials = DB::table('testimonials')
                            ->orderBy('id','DESC')
                            ->get();
       
        return view('pages.admin.coach.testimonial.list', compact('testimonials','customer','coach'));
    }
    public function TestimonialCreate()
    {
        $coach_role_id = 2;
        $coaches = DB::table('users')
        ->join('model_has_roles','model_has_roles.model_id','users.id')
        ->select(['users.*'])
        ->where('model_has_roles.role_id', $coach_role_id)
        ->orderBy('id','DESC')
        ->get();

        $customer_role_id = 3;
        $customers = DB::table('users')
        ->join('model_has_roles','model_has_roles.model_id','users.id')
        ->select(['users.*'])
        ->where('model_has_roles.role_id', $customer_role_id)
        ->orderBy('id','DESC')
        ->get();

       return view('pages.admin.coach.testimonial.create', compact('coaches', 'customers'));
    }
    public function TestimonialSave(Request $request)
    {
         

         $this->validate($request, [

            'title'         => 'required',
            'from_customer' => 'required',
            'to_coach'      => 'required',
            'description'   => 'required',
         ]);

         $testimonial_arr = [
            'title'         => $request->input('title'),
            'from_customer' => $request->input('from_customer'),
            'to_coach'      => $request->input('to_coach'),
            'description'   => trim($request->input('description')),
         ];
        $isSaved = Testimonial::create($testimonial_arr);
          if($isSaved)
          {
            return redirect()->route('coachTestimonial.index')
                        ->with('success','Testimonial created successfully');
          }
    }

    public function TestimonialEdit($id)
    {

        /* Get all of coach for coach list */
        $coach_role_id = 2;
        $coaches = DB::table('users')
        ->join('model_has_roles','model_has_roles.model_id','users.id')
        ->select(['users.*'])
        ->where('model_has_roles.role_id', $coach_role_id)
        ->orderBy('id','DESC')
        ->get();

        /* Get all of customer for customer list */
        $customer_role_id = 3;
        $customers = DB::table('users')
        ->join('model_has_roles','model_has_roles.model_id','users.id')
        ->select(['users.*'])
        ->where('model_has_roles.role_id', $customer_role_id)
        ->orderBy('id','DESC')
        ->get();

         $testimonials = DB::table('testimonials')
                            ->where('id',$id)
                            ->get();
        if(count($coaches) > 0 && count($customers) > 0 && count($testimonials) > 0){
            return view('pages.admin.coach.testimonial.edit', compact('testimonials','coaches', 'customers'));
        }
           return abort(404);       
    }

    public function TestimonialUpdate(Request $request, $id)
    {

         $this->validate($request, [

            'title'         => 'required',
            'from_customer' => 'required',
            'to_coach'      => 'required',
            'description'   => 'required',
         ]);
        $testimonial_arr = [
            'title'         => $request->input('title'),
            'from_customer' => $request->input('from_customer'),
            'to_coach'      => $request->input('to_coach'),
            'description'   => trim($request->input('description')),
         ];

         $affected = DB::table('testimonials')
              ->where('id', $id)
              ->update($testimonial_arr);

        return redirect()->route('coachTestimonial.index')
                        ->with('success','Testimonial updated successfully');
    }

    public function TestimonialDetailView($id)
    {

        $testimonial = Testimonial::findOrFail($id);
            

        $customer =  DB::table('testimonials')
                    ->join('users', 'users.id','testimonials.from_customer')
                    ->where('testimonials.from_customer',$testimonial->from_customer)
                    ->where('testimonials.id',$id)
                    ->select(['users.name'])
                    ->get();
    
        $coach =  DB::table('testimonials')
                    ->join('users', 'users.id','testimonials.to_coach')
                    ->where('testimonials.to_coach',$testimonial->to_coach)
                    ->where('testimonials.id',$id)
                    ->select(['users.name'])
                    ->get();
        $testimonials = DB::table('testimonials')
                            ->where('id',$id)
                            ->get();
        
            return View('pages.admin.coach.testimonial.detail',  ['coach' => $coach, 'customer' => $customer, 'testimonials' => $testimonials]);
       

    }
    public function TestimonialDelete($id)
    {
        Testimonial::where('id',$id)
                  ->delete();
        
            return response()->json(['success' => true,'message'=> 'Testimonial has been deleted']);
      
    }

    public function CoachList() 
    {
        $assign_coach_to_customers =   DB::select("select `coaches`.`coach_name`, `coach_assign_to_customer`.`coach_assign_id` as coach_assign_id, count(coach_assign_to_customer.coach_assign_id) as total from `coach_assign_to_customer` inner join coaches on `coaches`.`coach_id` = `coach_assign_to_customer`.`coach_assign_id` 
            join customers on `customers`.`customer_id` = `coach_assign_to_customer`.`coach_assign_customer_id`
            group by coach_assign_id", []);
       
        return view('pages.admin.coach.assignCoachToCustomer.list_assignment', compact('assign_coach_to_customers'));
    }

    public function AssignAllCustomerList($id)
    { 


        $assign_coach_to_customers = DB:: table('coach_assign_to_customer')
        ->join('customers', 'customers.customer_id','coach_assign_to_customer.coach_assign_customer_id')
        ->join('coaches', 'coaches.coach_id','coach_assign_to_customer.coach_assign_id')
        ->join('plans', 'plans.id', 'customers.plan')
        ->where('coach_assign_to_customer.coach_assign_id', $id)
        ->select(['coach_assign_to_customer.*','customers.plan','customers.customer_name','customers.id As CusID','coaches.id As CoachID','coaches.coach_name', 'plans.id As PID', 'plans.plan_title'])
        ->get();
       
      return view('pages.admin.coach.assignCoachToCustomer.list', compact('assign_coach_to_customers'));

       
    }

    public function AssignCustomerCreate()
    {  
        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();

        //print_r($customers); die;
        return view('pages.admin.coach.assignCoachToCustomer.create', compact('customers', 'coaches'));
    }
    public function AssignCustomerSave(Request $request)
    {
       

       $rules = [
        'coach_assign_id'    =>'required',
        'coach_assign_customer_id' => 'required|unique:coach_assign_to_customer,coach_assign_customer_id',
    ];
       
       $customMessages = [
        'coach_assign_customer_id.unique' => 'Coach for customer has already been assigned.',
    ];
    $this->validate($request, $rules, $customMessages);
    $data = [
            'coach_assign_id'=> $request->input('coach_assign_id'),
            'coach_assign_customer_id' => $request->input('coach_assign_customer_id'),

        ];
         $assignCoachToCustomer = CoachAssignToCustomer::create(
         $data);
        return redirect()->route('customer_assign.index')
                        ->with('success','Coach assign to customers successfully');
        
    }

   
    public function AssignCustomerEdit($id)
    {
        $checkAssignDetail = CoachAssignToCustomer::findOrFail($id);



        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();
        return view('pages.admin.coach.assignCoachToCustomer.edit', compact('customers', 'coaches', 'checkAssignDetail'));
    }
    public function AssignCustomerUpdate(Request $request, $id)
    {
         
    $this->validate($request, [
        'coach_assign_id'    =>'required',
        'coach_assign_customer_id' => 'required',
    ]);

    $data = 
    [
        'coach_assign_id'          => $request->input('coach_assign_id'),
        'coach_assign_customer_id' => $request->input('coach_assign_customer_id'),
    ];

    $checkAssignDetail = CoachAssignToCustomer::findOrFail($id);
    if(($checkAssignDetail->coach_assign_customer_id == $request->input('coach_assign_customer_id')) && ($checkAssignDetail->coach_assign_id == $request->input('coach_assign_id')))
    {
        $checkAssignDetail->update($data);
        return redirect()->route('customer_assign.index')
                        ->with('success','Coach assign to customers updated successfully');
    }
    elseif (($checkAssignDetail->coach_assign_id != $request->input('coach_assign_id')) && ($checkAssignDetail->coach_assign_customer_id == $request->input('coach_assign_customer_id'))) 
    {
       

       $checkAssignDetail->update($data);
        return redirect()->route('customer_assign.index')
                        ->with('success','Coach assign to customers updated successfully');
       // $rules = [
           
       //      'coach_assign_customer_id' => 'required|unique:coach_assign_to_customer,coach_assign_customer_id',
       //  ];
           
       //     $customMessages = [
       //      'coach_assign_customer_id.unique' => 'Please add new record.',
       //  ];
       //  $this->validate($request, $rules, $customMessages);

    }
    
    else
    {

            $rules = [
           
            'coach_assign_customer_id' => 'required|unique:coach_assign_to_customer,coach_assign_customer_id',
        ];
           
           $customMessages = [
            'coach_assign_customer_id.unique' => 'Coach for customer has already been assigned.',
        ];
        $this->validate($request, $rules, $customMessages);

    }

    }
    /*public function AssignCustomerDetailView($id, $customer_id, $coach_id)
    {   
        $customer = DB::table('users')
        ->join('customers','customers.customer_id','users.id')
        ->join('plans','plans.id','customers.plan')
        ->select(['customers.*' ,'users.*', 'plans.*'])
        ->where('users.id', $customer_id)
        ->get();
    return view('pages.admin.coach.assignCoachToCustomer.detail', compact('customer', 'coach'));
    }*/
    public function AssignCustomerDelete($id)
    {
        CoachAssignToCustomer::where('id',$id)
                  ->delete();
        
            return response()->json(['success' => true,'message'=> 'Coach assginment for customers has been deleted']);
    }

    public function emailAjaxCoach(Request $request){
        $data = User::where('email',$request->email)->first();
        return response()->json(['status' => true,'success'=>$data]);
    }

}
