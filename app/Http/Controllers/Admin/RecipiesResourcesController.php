<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use DB;
use Hash; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Recipies;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class RecipiesResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $recipies = Recipies::orderBy('id','DESC')->get();
       //dd('Working');
       return View('pages.admin.recipies.list',  compact('recipies'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.recipies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'diet' => 'required',
           // 'banner_img' =>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //'video_file' =>'nullable|mimes:mp4,mov,ogg,qt|max:20000',
           
        ]); 
        //Upload Banner image
        if ($files = $request->file('banner_img')) {
           $destinationPath = public_path().'/media/recipies-banner'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = null;
        }

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/recipies-videos'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
                $this->validate($request, [
                    'video_file' =>'required|mimes:mp4,mov,ogg,qt|max:20000',
                 ]);
            }
            
        }

       //dd($request->input('start_date_time'));
        $recipies = Recipies::create([
                'name'                => $request->input('name'),
                'diet_time'           => $request->input('diet'),
                'image'               => $fileName,
                'video_file'          => $video_file,
                'video_url'           => trim($video_url),
                'discription'         => $request->input('discription')
                
            ]);            
        $recipies->save();
        return redirect()->route('recipies.index')
                        ->with('success','Recipes created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailView($id)
    {
        $recipies = Recipies::findOrFail($id);
       return View('pages.admin.recipies.detail',  compact('recipies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipies = Recipies::findOrFail($id);
        $data = [
            'recipies'            => $recipies,
        ];
        return View('pages.admin.recipies.edit',  compact('recipies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $recipies = Recipies::find($id);
        $this->validate($request, [
            'name' => 'required',
            'diet' => 'required',
            'banner_img' =>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',  
        ]); 

        if ($request->hasFile('banner_img')){
            $image_path = public_path().'/media/recipies-banner/'.$recipies->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner_img');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/recipies-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $recipies->image;
        }

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/recipies-videos'.$recipies->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/recipies-videos';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = $recipies->video_file;
                $video_url = null;
            }
           
        }
         //print_r($fileName);
         // dd($request->input('name'));
        $recipies->name = $request->input('name');
        $recipies->diet_time = $request->input('diet');
        $recipies->discription = $request->input('discription');
        $recipies->image = $fileName;
        $recipies->video_file = $video_file;
        $recipies->video_url = trim($video_url);
        $recipies->save();
        return redirect()->route('recipies.index')
                        ->with('success','Recipes updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Recipies::find($id);

        $image_path = public_path().'/media/recipies-banner/'.$data->image;
        if (($data->image) && (File::exists($image_path))) {
            File::delete($image_path);
        }

        $video_file =  public_path().'/media/recipies-videos'.$data->video_file;
        if (($data->video_file) && (File::exists($video_file))) {
            File::delete($video_file);
        }
        $data->delete();
        // Recipies::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Recipes has been deleted']); 
    }
}
