<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\Book;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class BooksResourcesController extends Controller
{ 

	 public function index(Request $request)
    {

        $books = Book::orderBy('id','DESC')->get();
      
	   return View('pages.admin.book.list',  compact('books'));
    }

    public function create()
    {
        return View('pages.admin.book.create');
    }

     public function save(Request $request)
    {
    	//print_r($request->input()); die;
    	
        $this->validate($request, [
            'name'       => 'required',
            'book_img'   =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author_name'=> 'required',
            'book_type'   => 'required',
            'description' => 'required'
        ]);

        if ($files = $request->file('book_img')) {
           $destinationPath = public_path().'/media/book-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }
        //$arr = $request->input('author_name');

       $book_type = implode(", ", $request->input('book_type'));
       	$book = Book::create(
         [
            'name'    		=> trim($request->input('name')),
            'book_img'   	=> $banner,
            'book_type'		=> $book_type,
            'author_name'	=> trim($request->input('author_name')),
            'description'	=> trim($request->input('description')),
          ]);
        $book->save();
        return redirect()->route('book.index')
                        ->with('success','Book added successfully');
     
      
    }

    public function edit($id)
    {
        $book 		  = Book::findOrFail($id);
		   $types_arr 	= array();
        $book_types = $book->toArray();
        
        if($book_types){
          $types = $book_types['book_type'];
          $types_arr = explode(', ', $types);
        }

		$data =[
          'book'  		=> $book,
          'types_arr' => $types_arr,
        ];
		return View('pages.admin.book.edit',  compact('book','types_arr'));
    }

    public function update(Request $request, $id){
          
          //print_r($request->input()); die; 
        $book = Book::find($id);
         $this->validate($request, [
            'name' 		    => 'required',
            'author_name' => 'required',
            'description' => 'required',
            'book_type'   => 'required',
        ]);
        
        if ($request->hasFile('book_img')){
		    $image_path = public_path().'/media/book-banner/'.$book->book_img;
		    if (File::exists($image_path)) {
		        File::delete($image_path);
		    }
		    $bannerImage = $request->file('book_img');
		    $fileName = $bannerImage->getClientOriginalName();
		    $destinationPath = public_path().'/media/book-banner';
		    $bannerImage->move($destinationPath, $fileName);
		} else {
		    $fileName = $book->book_img;
		}
        
		$book_type = implode(", ", $request->input('book_type'));
        $book->name        = trim($request->input('name'));
        $book->author_name = trim($request->input('author_name'));
        $book->book_type   = $book_type;
        $book->description = trim($request->input('description'));
		    $book->book_img    = $fileName;

        $book->save();
        return redirect()->route('book.index')
                        ->with('success','Book updated successfully'); 
        
    }
     public function detailView(Request $request, $id)
     {
		    $book = Book::findOrFail($id);
       return View('pages.admin.book.detail',  compact('book'));
    }

    public function delete($id){
      $data = Book::find($id);
      $image_path = public_path().'/media/book-banner/'.$data->book_img;
      if (File::exists($image_path)) {
          File::delete($image_path);
      }
      $data->delete();
      
      // Book::find($id)->delete();
      return response()->json(['success' => true,'message'=> 'Book has been deleted']);
    }
}
