<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\Plan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PlansController extends Controller
{
    public function index(Request $request)
    {
    	$plans = Plan::orderBy('id','DESC')->get();
        return View('pages.admin.plan.list',  compact('plans'));
    }

    public function create()
    {
    	return View('pages.admin.plan.create');
    }

    public function save(Request $request)
    {
    	$this->validate($request, [
            'plan_title'        => 'required',
            'plan_image' 		=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'validity_days'    => 'required|numeric',
            'plan_type'  	   => 'required',
            
            'plan_description' => 'required',
            'plan_status'      => 'required',
        ]);

        if($request->input('plan_type') ==1)
        {
        	$this->validate($request, [

        		'actual_price'     => 'required|not_in:0|regex:/^\d+(\.\d{1,2})?$/',
                'sales_price'      => 'required|not_in:0|regex:/^\d+(\.\d{1,2})?$/',
        	]);

        }

        if($request->input('plan_type') == 1)
        {
        	$actual_price  = $request->input('actual_price');
        	$sales_price   = $request->input('sales_price');
        }
        else
        {
        	 $actual_price = 0;
        	 $sales_price  = 0;
		}

        if ($files = $request->file('plan_image')) {
           $destinationPath = public_path().'/media/plan'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }
         $plans = Plan::create(
         [
            'plan_title'         => trim($request->input('plan_title')),
            'plan_image'         => $fileName,
            'validity_days'      => trim($request->input('validity_days')),
            'plan_type'         => trim($request->input('plan_type')),
            'actual_price'      => $actual_price,
            'sales_price'       => $sales_price,
            'plan_description'  => trim($request->input('plan_description')),
            'plan_status'        => trim($request->input('plan_status'))
          ]);

         return redirect()->route('plans.index')
                        ->with('success','Plan created successfully');
    
    }

    public function edit($id)
    {
    	$plan = Plan::findOrFail($id);
        $data =[
          'plan'  => $plan,
        ];
        return View('pages.admin.plan.edit',  compact('plan'));
    }

    public function update(Request $request, $id)
    {
    	$plan = Plan::find($id);
    	$this->validate($request, [
            'plan_title'        => 'required',
            'plan_image' 		=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'validity_days'    => 'required|numeric',
            'plan_type'  	   => 'required',
            'actual_price'     => 'required|numeric',
            'sales_price'      => 'required|numeric',
            'plan_description' => 'required',
            'plan_status'      => 'required',
        ]);

        if($request->input('plan_type') ==1)
        {
        	$this->validate($request, [

        		'actual_price'     => 'required|not_in:0|regex:/^\d+(\.\d{1,2})?$/',
                'sales_price'      => 'required|not_in:0|regex:/^\d+(\.\d{1,2})?$/',
        	]);
        }

        if($request->input('plan_type') == 1)
        {
        	$actual_price  = $request->input('actual_price');
        	$sales_price   = $request->input('sales_price');
        }
        else
        {
        	 $actual_price = 0;
        	 $sales_price  = 0;
		}

        if ($request->hasFile('plan_image')){
            $image_path = public_path().'/media/plan/'.$plan->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $customerImage = $request->file('plan_image');
            $fileName = $customerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/plan';
            $customerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $plan->plan_image;
        }

         $plan->plan_title 	     = $request->input('plan_title');
         $plan->plan_image 	     = $fileName;
         $plan->validity_days 	 = $request->input('validity_days');
         $plan->plan_type 	     = $request->input('plan_type');
         $plan->actual_price 	 = $actual_price;
         $plan->sales_price 	 = $sales_price;
         $plan->plan_description = $request->input('plan_description');
         $plan->plan_status 	 = $request->input('plan_status');
         $plan->save();
          return redirect()->route('plans.index')
                        ->with('success','Plans updated successfully');
    }

    public function detailView(Request $request, $id)
    {
       $plan = Plan::findOrFail($id);
       return View('pages.admin.plan.detail',  compact('plan'));
    }

    public function delete($id)
    {
        $data = Plan::find($id);
        $image_path = public_path().'/media/plan/'.$data->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

		// Plan::find($id)->delete();
		return response()->json(['success' => true,'message'=> 'Plan has been deleted']); 
	}

}
