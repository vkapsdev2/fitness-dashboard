<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\Article;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class ArticlesResourcesController extends Controller
{

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $articles = Article::orderBy('id','DESC')->get();
       //print_r($articles); die;
	   return View('pages.admin.article.list',  compact('articles'));
    }

 /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.article.create');
    }

    public function save(Request $request)
    {
        //print_r($request->input());
        
        $this->validate($request, [
            'name'        => 'required',
            'article_img' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author_name'  => 'required',
            'description'  => 'required'
        ]);

        if ($files = $request->file('article_img')) {
           $destinationPath = public_path().'/media/article-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }else{
            $banner = "";
        }

        $article = Article::create(
         [
            'name'          => trim($request->input('name')),
            'article_img'   => $banner,
            'author_name'   => trim($request->input('author_name')),
            'description'   => trim($request->input('description')),
          ]);
        $article->save();
        return redirect()->route('article.index')
                        ->with('success','Article added successfully');
     
      
    }

    public function edit($id){
        $article = Article::findOrFail($id);
        $data =[
          'article'  => $article,
        ];
        return View('pages.admin.article.edit',  compact('article'));
    }

     public function update(Request $request, $id){
          
        $article = Article::find($id);
         $this->validate($request, [
            'name'        => 'required',
            'author_name' => 'required',
            'description' => 'required',
        ]);
        
        if ($request->hasFile('article_img')){
            $image_path = public_path().'/media/article-banner/'.$article->article_img;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('article_img');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/article-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $article->article_img;
        }
        
        $article->name        = trim($request->input('name'));
        $article->author_name = trim($request->input('author_name'));
        $article->description = trim($request->input('description'));

        $article->article_img = $fileName;

        $article->save();
        return redirect()->route('article.index')
                        ->with('success','Article updated successfully'); 
        
    }

     public function detailView(Request $request, $id)
     {
        $article = Article::findOrFail($id);
       return View('pages.admin.article.detail',  compact('article'));
    }
    
    public function delete($id){

        $data = Article::find($id);
        $image_path = public_path().'/media/article-banner/'.$data->article_img;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();
        return response()->json(['success' => true,'message'=> 'Article has been deleted']);
    }

}