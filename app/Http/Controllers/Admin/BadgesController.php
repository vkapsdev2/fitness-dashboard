<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\Badge;
use App\Models\CustomerBadge;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class BadgesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$badges = Badge::orderBy('id', 'DESC')->get();

        return View('pages.admin.badges.list',compact('badges'));
    }
    public function create()
    {
    	return View('pages.admin.badges.create');
    }
    public function save(Request $request)
    {
    	$this->validate($request, [
            /*basic info*/
            'title'         => 'required',
            'image' 		=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'purpose'       => 'required',
            'badge_status'  => 'required',
            
         ]);

    	if ($files = $request->file('image')) {
           $destinationPath = public_path().'/media/badge'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }else{
            $banner = "";
        }

         $badge = Badge::create(
         [
            'title'          => trim($request->input('title')),
            'image'   => $banner,
            'purpose'   => trim($request->input('purpose')),
            'benifit'   => trim($request->input('benifit')),
            'description'   => trim($request->input('description')),
            'badge_status'   => trim($request->input('badge_status')),
         ]);
        $badge->save();
        return redirect()->route('badges.index')
                        ->with('success','Badges added successfully');
	}
    public function edit($id)
    {
    	$badge = Badge::findOrFail($id);
        return View('pages.admin.badges.edit',compact('badge'));
    }
    public function update(Request $request, $id)
    {

    	$badge = Badge::find($id);
    	$this->validate($request, [
            /*basic info*/
            'title'         => 'required',
            'image' 		=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'purpose'       => 'required',
            'badge_status'  => 'required',
            
         ]);

    	if ($request->hasFile('image')){
            $image_path = public_path().'/media/badge/'.$badge->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('image');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/badge';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $badge->image;
        }

        $data = [

            'title'        => trim($request->input('title')),
            'image'        => $fileName,
            'purpose'      => trim($request->input('purpose')),
            'benifit'      => trim($request->input('benifit')),
            'description'  => trim($request->input('description')),
            'badge_status' => trim($request->input('badge_status')),
         ];
         $badge->update($data);
         return redirect()->route('badges.index')
                        ->with('success','Badges updated successfully'); 

    }
    public function detailView($id)
    {
    	$badge = Badge::findOrFail($id);
       return View('pages.admin.badges.detail',  compact('badge'));
    }
    public function delete($id)
    {
        $data = Badge::find($id);
        $image_path = public_path().'/media/badge/'.$data->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

    	// Badge::find($id)->delete();
    	return response()->json(['success' => true,'message'=> 'This Badge has been deleted ']);

    }

    public function customerAchievementList(){
        $customer_badges = CustomerBadge::with('customer')->with('badge')->get();
        return View('pages.admin.badges.customerAchievement.list',compact('customer_badges'));
    }

    public function customerAchievement(){
        $badges = Badge::orderBy('id', 'DESC')->get();

        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();
        return View('pages.admin.badges.customerAchievement.create',compact('badges','customers'));
    }

    public function customerAchievementSave(Request $request)
    {
        $this->validate($request, [
            'customer_id'         => 'required',
            'badge_id'           => 'required',
            
         ]);

         $customer_badge = CustomerBadge::create(
         [
            'customer_id'     => trim($request->input('customer_id')),
            'badge_id'        => trim($request->input('badge_id'))
         ]);
        $customer_badge->save();
        return redirect()->route('customer_badges.index')
                        ->with('success','Customer Badge added successfully');
    }

    public function customerAchievementEdit($id){

        $row = CustomerBadge::findOrFail($id);
        $badges = Badge::orderBy('id', 'DESC')->get();
        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();
        return View('pages.admin.badges.customerAchievement.edit',compact('row','badges','customers'));
    }

    public function customerAchievementUpdate(Request $request, $id)
    {

        $result = CustomerBadge::find($id);
        $this->validate($request, [
            'customer_id'         => 'required',
            'badge_id'           => 'required',
            
        ]);

        $data = [
            'customer_id'     => trim($request->input('customer_id')),
            'badge_id'        => trim($request->input('badge_id'))
         ];
         $result->update($data);
         return redirect()->route('customer_badges.index')
                        ->with('success','Customer Badges updated successfully'); 

    }

    public function customerAchievementDelete($id)
    {
        CustomerBadge::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Customer Badge has been deleted ']);

    }
}
