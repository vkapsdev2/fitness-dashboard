<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\Meal;
use App\Models\suggestedCustomerMeals;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class MealsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$meals  =   Meal::orderBy('id', 'DESC')
    						->get();
    	return View('pages.admin.coach.meals.list', compact('meals'));
	}
	public function create()
    {
		return View('pages.admin.coach.meals.create');
    }
    public function save(Request $request)
    {
    	$this->validate($request, [
            'meal_title' 		=>'required',
            'meal_image'   		=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           
            'meal_calories' 	=>'required',
            'meal_protein' 		=>'required',
            'meal_carbs' 		=>'required',
            'meal_fats' 		=>'required',
            'meal_fiber' 		=>'required',
            'meal_minerals' 	=>'required',
            'meal_calcium' 		=>'required',
            'meal_vitmins' 		=>'required',
            'meal_quantity' 	=>'required',
            'meal_plan_type' 	=>'required',
            'meal_status' 		=>'required',
            
        ]);
         

        //Upload Shake image
        if ($files = $request->file('meal_image')) {
           $destinationPath = public_path().'/media/meal'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }
         $created_by = Auth::user()->id;
        $data = [
            'meal_title'       => trim($request->input('meal_title')),
            'meal_image'       => $fileName,
            'meal_calories'    => trim($request->input('meal_calories')),
            'meal_protein'     => trim($request->input('meal_protein')),
            'meal_carbs'       => trim($request->input('meal_carbs')),
            'meal_fats'        => trim($request->input('meal_fats')),
            'meal_fiber'   	   => trim($request->input('meal_fiber')),
            'meal_minerals'    => trim($request->input('meal_minerals')),
            'meal_calcium'    => trim($request->input('meal_calcium')),

            'meal_vitmins'     => trim($request->input('meal_vitmins')),
            'meal_water'       => trim($request->input('meal_water')),
            'other_meal_detail' => trim($request->input('other_meal_detail')),
            'meal_quantity'     => trim($request->input('meal_quantity')),
            'meal_plan_type'    => trim($request->input('meal_plan_type')),
            'meal_description'  => trim($request->input('meal_description')),
            'created_by'        => $created_by,
            'meal_status'       => $request->input('meal_status'),
           
        ];
         $shakeAndBar = meal::create($data);
          return redirect()->route('meals.index')
                        ->with('success','Meal created successfully');
    }

    public function edit($id)
    {
    	$meal = Meal::findOrFail($id);
       return View('pages.admin.coach.meals.edit', compact('meal'));
    }
    public function update(Request $request, $id)
    {
    	$Meal = Meal::find($id);
    	$this->validate($request, [
            'meal_title' 		=>'required',
            'meal_image'   		=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           
            'meal_calories' 	=>'required',
            'meal_protein' 		=>'required',
            'meal_carbs' 		=>'required',
            'meal_fats' 		=>'required',
            'meal_fiber' 		=>'required',
            'meal_minerals' 	=>'required',
            'meal_calcium' 		=>'required',
            'meal_vitmins' 		=>'required',
            'meal_quantity' 	=>'required',
            'meal_plan_type' 	=>'required',
            'meal_status' 		=>'required',
            
        ]);

        if ($request->hasFile('meal_image')){
      		 
      		
		    $image_path = public_path().'/media/meal/'.$Meal->meal_image;
		    if (File::exists($image_path)) {
		        File::delete($image_path);
		    }
		    $bannerImage = $request->file('meal_image');
		    $fileName = $bannerImage->getClientOriginalName();
		    $destinationPath = public_path().'/media/meal';
		    $bannerImage->move($destinationPath, $fileName);
		} else {
		    $fileName = $Meal->meal_image;
		}

		 $data = [
            'meal_title'       => trim($request->input('meal_title')),
            'meal_image'       => $fileName,
            'meal_calories'    => trim($request->input('meal_calories')),
            'meal_protein'     => trim($request->input('meal_protein')),
            'meal_carbs'       => trim($request->input('meal_carbs')),
            'meal_fats'        => trim($request->input('meal_fats')),
            'meal_fiber'   	   => trim($request->input('meal_fiber')),
            'meal_minerals'    => trim($request->input('meal_minerals')),
            'meal_calcium'    => trim($request->input('meal_calcium')),

            'meal_vitmins'     => trim($request->input('meal_vitmins')),
            'meal_water'       => trim($request->input('meal_water')),
            'other_meal_detail' => trim($request->input('other_meal_detail')),
            'meal_quantity'     => trim($request->input('meal_quantity')),
            'meal_plan_type'    => trim($request->input('meal_plan_type')),
            'meal_description'  => trim($request->input('meal_description')),
            
            'meal_status'       => $request->input('meal_status'),
           
        ];

        $Meal->update($data);
        return redirect()->route('meals.index')
                        ->with('success','Meal updated successfully');
    }

    public function DetailView($id)
    {
      $meal = Meal::findOrFail($id);
      $meal_creation_id = $meal->created_by;
      $created_by_name = array();
      $created_by_name = DB::table('users')
      					->where('id', $meal_creation_id)
       				->get();
       				
		return View('pages.admin.coach.meals.detail', compact('meal','created_by_name'));
    }

    public function delete($id)
    {
        $data = Meal::find($id);
        $image_path = public_path().'/media/meal/'.$data->meal_image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();
        
    	// Meal::where('id',$id)->delete();
        return response()->json(['success' => true,'message'=> 'Meal has been deleted']);
    }

    public function SuggestedMealList()
    {

        $meals = suggestedCustomerMeals::orderBy('id','DESC')->get();

       $user_details =  DB::table('users')->get();
       
       return View('pages.admin.coach.suggestMealsToCustomer.list',compact('meals', 'user_details'));
    }
    public function SuggestedMealCreate()
    {

        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();
        $breakfastId = 1;
        $lunchId     = 2;
        $dinnerhId   = 3;

        $breakFastMeals  =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$breakfastId)
                            ->get();

        $lunchMeals     =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$lunchId)
                            ->get();

        $dinnerMeals     =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$dinnerhId)
                            ->get();
       return View('pages.admin.coach.suggestMealsToCustomer.create',compact('customers', 'coaches','breakFastMeals' ,'lunchMeals', 'dinnerMeals'));

    }
    public function GetMealList($id)
    {
        $meal_list = Meal::orderBy('id','DESC')
                            ->where('meal_plan_type',$id)
                            ->get();
            
         return response()->json(['status' => true,'success'=> $meal_list]);
    }
    public function SuggestedMealSave(Request $request)
    {
       $rules = [
        'suggester_coach_id'    =>'required',
            'suggested_customer_id' =>'required|unique:suggested_customer_meals,suggested_customer_id',
            'assign_break_fast_meal'=>'required',
            // 'assign_lunch_meal'     =>'required',
            // 'assign_dinner_meal'    =>'required',
    ];
       
       $customMessages = [
        'suggested_customer_id.unique' => 'Food has already been suggested to this user You can only update.',
    ];
       $this->validate($request, $rules, $customMessages);
  

        $break_fast_string = "";
        $lunch_string      = "";
        $dinner_string     = "";
        

        if($request->input('assign_break_fast_meal')!== null)
        {
            $break_fast_string = implode(", ", $request->input('assign_break_fast_meal'));
        }
        if($request->input('assign_lunch_meal')!== null)
        {
            $lunch_string = implode(", ", $request->input('assign_lunch_meal'));
        }
        if($request->input('assign_dinner_meal')!== null)
        {
            $dinner_string = implode(", ", $request->input('assign_dinner_meal'));
        }
        $currentDate         =  date('Y-m-d');
        $data = [
            'suggester_coach_id'         => $request->input('suggester_coach_id'),
            'suggested_customer_id'      => $request->input('suggested_customer_id'),
            'assign_break_fast_meal'     => $break_fast_string,
            
            'break_fast_days'            => trim($request->input('break_fast_days')),

            'assign_lunch_meal'        => $lunch_string,
            
            'lunch_days'               => trim($request->input('lunch_days')),
            'assign_dinner_meal'       => $dinner_string,
            
            'dinner_days'              => trim($request->input('dinner_days')),
            'other_suggest_meal_detail' => trim($request->input('other_suggest_meal_detail')),

            'suggesting_date'    => $currentDate,
            'suggest_meal_status' => 1,
        ];

        $suggestedMeal = suggestedCustomerMeals::create(
         $data);
        return redirect()->route('suggest_meals.index')
                        ->with('success','Suggested added successfully');
        

    }
    public function SuggestedMealEdit($id)
    {
        $suggested_meal = suggestedCustomerMeals::findOrFail($id);

        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();

        $breakfastId  = 1;
        $lunchId      = 2;
        $dinnerhId    = 3;

        $breakFastMeals  =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$breakfastId)
                            ->get();

        $lunchMeals     =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$lunchId)
                            ->get();

        $dinnerMeals     =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$dinnerhId)
                            ->get();
       
    return View('pages.admin.coach.suggestMealsToCustomer.edit',compact('customers', 'coaches','breakFastMeals' ,'lunchMeals', 'dinnerMeals', 'suggested_meal'));

    }
    public function SuggestedMealUpdate(Request $request, $id)
    {
        $suggested_meal = suggestedCustomerMeals::find($id);
        $this->validate($request, [
            'suggester_coach_id'    =>'required',
            'suggested_customer_id' =>'required',
            'assign_break_fast_meal'=>'required',
            // 'assign_lunch_meal'     =>'required',
            // 'assign_dinner_meal'    =>'required',
            
        ]);

        $break_fast_string = "";
        $lunch_string      = "";
        $dinner_string     = "";
        

        if($request->input('assign_break_fast_meal')!== null)
        {
            $break_fast_string = implode(", ", $request->input('assign_break_fast_meal'));
        }
        if($request->input('assign_lunch_meal')!== null)
        {
            $lunch_string = implode(", ", $request->input('assign_lunch_meal'));
        }
        if($request->input('assign_dinner_meal')!== null)
        {
            $dinner_string = implode(", ", $request->input('assign_dinner_meal'));
        }
        $currentDate         =  date('Y-m-d');
        $data = [
            'suggester_coach_id'         => $request->input('suggester_coach_id'),
            'suggested_customer_id'      => $request->input('suggested_customer_id'),
            'assign_break_fast_meal'     => $break_fast_string,
            
            'break_fast_days'            => trim($request->input('break_fast_days')),

            'assign_lunch_meal'        => $lunch_string,
            
            'lunch_days'               => trim($request->input('lunch_days')),
            'assign_dinner_meal'       => $dinner_string,
            
            'dinner_days'              => trim($request->input('dinner_days')),
            'other_suggest_meal_detail' => trim($request->input('other_suggest_meal_detail')),

            'suggesting_date'    => $currentDate,
            
        ];

        $suggested_meal->update($data);
        return redirect()->route('suggest_meals.index')
                        ->with('success','Suggested Meal updated successfully');


    }
    public function SuggestedMealDetailView($id)
    {
        $suggested_meal = suggestedCustomerMeals::find($id);

        $coach_detail = DB::table('users')
                            ->where('id', $suggested_meal->suggester_coach_id)
                            ->get();

        $customer_detail = DB::table('users')
                            ->where('id', $suggested_meal->suggested_customer_id)
                            ->get();

        $breakfastId  = 1;
        $lunchId      = 2;
        $dinnerhId    = 3;

        $breakFastMeals  =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$breakfastId)
                            ->get();

        $lunchMeals     =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$lunchId)
                            ->get();

        $dinnerMeals     =   Meal::orderBy('id', 'DESC')
                            ->where('meal_plan_type',$dinnerhId)
                            ->get();

        return View('pages.admin.coach.suggestMealsToCustomer.detail',compact('suggested_meal','coach_detail', 'customer_detail', 'breakFastMeals', 'lunchMeals', 'dinnerMeals'));
    }
    public function SuggestedMealDelete($id)
    {
        $suggestedMeal = suggestedCustomerMeals::where('id'
         ,$id)
        ->delete();
        return response()->json(['success' => true,'message'=> 'Suggested Meal has been deleted']);

    }
}
