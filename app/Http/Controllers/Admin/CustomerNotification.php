<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\User;
use App\Models\Customer; 
use App\Models\CustomerNotiication as Notification; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerNotification extends Controller
{
    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('cus_notification')
                ->join('customers','customers.customer_id','cus_notification.customer_id')
                ->select('cus_notification.id','cus_notification.title','cus_notification.description','customers.customer_name','customers.customer_id')->get();
        
        return View('pages.admin.customer.notification.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::get(['customer_id','customer_name']);
        return View('pages.admin.customer.notification.create',compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $request->validate([
            'customer_id' =>'required',
            'title' => 'required',
           // 'description' => 'required',
        ]);

        $res = new Notification([
            'customer_id'   => $request->customer_id,
            'title'         => $request->title,
            'description'  => $request->description,
        ]);

        $res->save();
            $this->pushNotification($request->customer_id,$request->title,$request->description);
        return redirect()->route('cus_notify.index')->with('success',"Customer's notification added successfully");

    }

    public function pushNotification($customer_id,$title,$message,$id = null){

        $fcm_token = User::where('id',$customer_id)->pluck('device_token');
        if($fcm_token[0]){

            $url = "https://fcm.googleapis.com/fcm/send";            
            $header = array("authorization: key=AAAALpaZyWg:APA91bHywGvT2b6mQjoKGTZkUisi05l-qVWpF1vfaudkKmKbLYPkYHSJ9eDcb6QmylcWI0zDBnIjbKQSPA-_0yI7OoI1_bJBG0BlLXZf1zx2G9kIgVVBgbNMMcdAepfCiRga4e07TWaH",
                "content-type: application/json"
            );    

            $postdata = '{
                "to" : "' . $fcm_token . '",
                    "notification" : {
                        "title":"' . $title . '",
                        "text" : "' . $message . '"
                    },
                "data" : {
                    "id" : "'.$id.'",
                    "title":"' . $title . '",
                    "description" : "' . $message . '",
                    "text" : "' . $message . '",
                    "is_read": 0
                  }
            }';

            $ch = curl_init();
            $timeout = 120;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

            // Get URL content
            $result = curl_exec($ch);    
            // close handle to release resources
            curl_close($ch);

            print_r($result);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $row = DB::table('cus_notification')
                ->join('customers','customers.customer_id','cus_notification.customer_id')
                ->where('cus_notification.id',$id)
                ->select('cus_notification.id','cus_notification.title','cus_notification.description','customers.customer_name','customers.customer_id')->first();
        if($row){
            return View('pages.admin.customer.notification.show',compact('row'));
        }
        return abort(404);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        $row = Notification::findOrFail($id);
        $customers = Customer::get(['customer_id','customer_name']);
        return View('pages.admin.customer.notification.edit',compact('row','customers'));
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   /* public function update(Request $request, $id)
    {   
        $notify = Notification::find($id);
        $request->validate([
            'customer_id' =>'required',
            'title' => 'required',
        ]);

        
            $notify->customer_id   = $request->customer_id;
            $notify->title         = $request->title;
            $notify->description  = $request->description;
        

        $notify->save();

        return redirect()->route('cus_notify.index')->with('success',"Customer's notification update successfully");
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Notification::find($id)->delete();
        return response()->json(['success' => true,'message'=> "Customer's notification delete successfully"]);
    }
}
