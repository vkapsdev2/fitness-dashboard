<?php

namespace App\Http\Controllers\Admin;

use App\Models\DailyRoutine;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DailyRoutineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daily_routines = DailyRoutine::with('customer')->orderBy('id','DESC')->get();
        //print_r($daily_routines->toArray()); dd();
        return View('pages.admin.daily_routine.list',  compact('daily_routines'));
    }
}
