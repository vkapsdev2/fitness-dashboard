<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\Contact;
use App\Models\Social;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Mail;
use Mail;

class OthersController extends Controller{

    public function ContactList(){
        $contacts = Contact::orderBy('id','DESC')->get();
  		return View('pages.admin.other.contact-us.list',  compact('contacts'));
    }

    public function ContactCreate(){
    	return View('pages.admin.other.contact-us.create');
    }

    public function ContactSave(Request $request){
    	$this->validate($request, [
            'name' => 'required',
            'email'=> 'required|email',
            'query' => 'required'
        ]);
        $data = [
        	'name'          => trim($request->input('name')),
        	'email'         => trim($request->input('email')),
        	'query'         => trim($request->input('query')),
        	'date'          => date('Y-m-d'),
        	'replied_by_us' => '0',
        	'contact_status'=>'1',
        	
        ];
        $contacts = Contact::create($data);
        return redirect()->route('contact.index')->with('success','Inquiry created successfully');
    }

    public function ContactReply($id){
    	$contact = Contact::find($id);
        return View('pages.admin.other.contact-us.reply',  compact('contact'));
    }

    public function ContactSendReply(Request $request){
    	$this->validate($request, [
           'query_reply' => 'required',
           'to_email'    => 'required|email',
        ]);

    	$to_email = $request->input('to_email');
    	$message  = $request->input('query_reply');
    	$id       = $request->input('id');

		$data = array( 
            'email'        => $request->input('to_email'), 
            'name' 	       => $request->input('name'),
            'message'      => $request->input('query_reply'),
            'from_name'    => 'Swadha Purohit',
            'from_address' => 'Vkaps IT Solution',
        );

		$customerMail = Mail::send('pages.admin.emails.contact-us.customer-mail',['data'=>$data], function($message) use ($data){
	        $message->to($data['email']);
	        $message->subject('Inquiry Reply');
	    });

	    Mail::send('pages.admin.emails.contact-us.admin-mail', ['data'=>$data], function($message) use ($data){
	        $message->to('swadha@vkaps.com');
	      	$message->subject('Customer Request');
	    });

		if (Mail::failures()) {
			$request->session()->flash('error','Not send mail');
		}else{
			$request->session()->flash('success','Reply has successfully sent');
			$contact = Contact::find($id);
			$contact->update(['replied_by_us'=> '1', 'replied_date' =>date('Y-m-d')]);
		}
		return redirect()->route('contact.reply', [$id]);
	}

	public function ConactsDelete($id){
		Contact::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Inquiry has been deleted']);
    }

    public function SocialList(){
        $socials = Social::orderBy('id','DESC')->get();
        return View('pages.admin.social.list',  compact('socials'));
    }
   
    public function SocialEdit($id){
        $social = Social::findOrFail($id);
        return View('pages.admin.social.edit',  compact('social'));
    }

    public function SocialUpdate(Request $request, $id){
        $social = Social::find($id);

        $this->validate($request, [
            'facebook_url' => 'required|url',
            'google_url'   => 'required|url',
            'twitter_url'  => 'required|url',
            'youtube_url'  => 'required|url',
            'instagram_url'=> 'required|url',
        ]);

        $data = [
            'facebook_url' => trim($request->input('facebook_url')),
            'google_url'   => trim($request->input('google_url')),
            'twitter_url'  => trim($request->input('twitter_url')),
            'youtube_url'  => trim($request->input('youtube_url')),
            'instagram_url'=> trim($request->input('instagram_url')),
        ];
            
        $social->update($data);
        return redirect()->route('social.index')->with('success','Links updated successfully');
    }

    public function SocialDelete($id){
        Social::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Social Links has been deleted']);
    }

}