<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use DB;
use Hash;
use Crypt;
use App\User;
use App\Models\Customer; 
use App\Models\Plan; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\CustomWorkout;
use App\Models\CustomerBodyMeasurementLog;
use App\Models\CustomWorkoutDetail;
use App\Models\MindFlexPlanner\Movement\GymWorkout;
use App\Models\MindFlexPlanner\Movement\SubGymWorkout;

class CustomersController extends Controller{
    public function __construct(){
      $this->middleware('auth');
    }

    public function index(){ 
      $customer_role_id = 3;
      $customers = DB::table('users')
        ->join('customers', 'customers.customer_id','users.id')
        ->join('model_has_roles',function($join) {$join
        ->on('model_has_roles.model_id' , '=' , 'users.id')
        ->where('model_has_roles.role_id','=', 3);})
        ->select(['users.*', 'customers.plan as plan','customers.id as cus_id'])
        ->orderBy('id','DESC')
        ->get();
        
        foreach ($customers as $key => $value) {
            $plan_id = $value->plan;
            $plan_title = DB::table('plans')->select('plan_title')->where("id", $plan_id)->first();
            $customers[$key]->plan_title = $plan_title->plan_title;
        }
    	return View('pages.admin.customer.list',  compact('customers'));
    }

    public function allCustomerNum(){
      $customer_role_id = 3;
      $customers = DB::table('users')
        ->join('customers', 'customers.customer_id','users.id')
        ->join('model_has_roles',function($join) {$join
        ->on('model_has_roles.model_id' , '=' , 'users.id')
        ->where('model_has_roles.role_id','=', 3);})
        ->select(['users.*', 'customers.plan as plan','customers.id as cus_id'])
        ->orderBy('id','DESC')
        ->get();
        if($customers->toArray()){
            $res = ['status' => true,'success' => $customers];
        }else{
            $res = ['status' => false,'success' => $customers];
        }
        echo json_encode($res);
    }

    public function getAllCustomer(Request $request){
        $page = $request->input('num');
        $limit = 5;

        if ($page != 1) {
            $start_from = ($page-1) * $limit;  
        }else{
            $start_from = 1;
        }

      $customer_role_id = 3;
      $customers = DB::table('users')
        ->join('customers', 'customers.customer_id','users.id')
        ->join('model_has_roles',function($join) {$join
        ->on('model_has_roles.model_id' , '=' , 'users.id')
        ->where('model_has_roles.role_id','=', 3);})
        ->select(['users.*', 'customers.plan as plan','customers.id as cus_id'])
        ->orderBy('id','DESC')
        ->skip($start_from)
        ->take($limit)
        ->get();
        if($customers->toArray()){
            $res = ['status' => true,'success' => $customers];
        }else{
            $res = ['status' => false,'success' => $customers];
        }
        echo json_encode($res);
    }

    public function create(){
        $plans = Plan::get();
    	return view('pages.admin.customer.create',compact('plans'));
    }

    public function save(Request $request){
        $this->validate(
            $request,
            [
                'email' => "required|email|unique:users,email",
            ]
        );

        $customer_role = 3;

        if ($files = $request->file('image')){
            $destinationPath = public_path().'/media/customer-image';
            $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }

        $password = bcrypt($request->input('password'));
        $customer = User::create([
            'name'           => $request->input('name'),
            'email'          => $request->input('email'),
            'phone'          => $request->input('phone'),
            'address'        => $request->input('address'),
            'gender'         => $request->input('gender'),
            'city'           => $request->input('city'),
            'state'          => $request->input('state'),
            'zip'            => $request->input('zip'),
            'facebook_url'   => $request->input('facebook_url'),
            'google_url'     => $request->input('google_url'),
            'twitter_url'    => $request->input('twitter_url'),
            'youtube_url'    => $request->input('youtube_url'),
            'instagram_url'  => $request->input('instagram_url'),
            'image'          => $fileName,
            'password'       => $password,
            'country'        => '',
            'status'         => 1
        ]); 

        /* Add Personal details in Customer table*/
        if(isset($customer['id'])) {
            if($request->input('family_disease_detail')!== null) {
                $family_disease_string = implode(", ", $request->input('family_disease_detail'));
            } else {
                $family_disease_string = null;
            }
    			
    		$customer_personal = Customer::create([
            'customer_name'   => $customer['name'],
            'customer_id'   => $customer['id'],
            'level'         => $request->input('level'),
            'weight'        => $request->input('weight'),
            'height'        => $request->input('height'),
            'age'        	  => $request->input('age'),

            'neck' 			  => $request->input('neck'),
            'shoulder' 		=> $request->input('shoulder'),
            'right_bicep' => $request->input('right_bicep'),
            'chest' 		  => $request->input('chest'),
            'waist' 		  => $request->input('waist'),
            'hips' 			  => $request->input('hips'),
            'right_thigh' => $request->input('right_thigh'),
            'right_calf' 	=> $request->input('right_calf'),

            'body_fat_percent'   => $request->input('body_fat_percent'),
            'body_fat_mass' 	   => $request->input('body_fat_mass'),
            'skletal_muscle_mass' => $request->input('skletal_muscle_mass'),

            'career' 		  => $request->input('career'),
            'relationship'	=> $request->input('relationship'),
            'health'			  => $request->input('health'),
            'nutrition'		=> $request->input('nutrition'),

            'sleep'			  => $request->input('sleep'),
            'appearance'		=> $request->input('appearance'),
            'purpose'		  => $request->input('purpose'),
            'social'			  => $request->input('social'),
            'spiritual'		=> $request->input('spiritual'),          
            'joy'			         => $request->input('joy'),          
            'ready_for_change'  => $request->input('ready_for_change'),          
            'social_goal'	     => $request->input('social_goal'),
            'consistent_physical_activity' =>$request->input('consistent_physical_activity'),

            'consistent_physical_activity_time' => trim($request->input('consistent_physical_activity_time')),

            'consistent_physical_activity_description' => trim($request->input('consistent_physical_activity_description')),
            'customer_occupation' => trim($request->input('customer_occupation')),
            'houldhold' => trim($request->input('houldhold')),
            'recreational_activity' => trim($request->input('recreational_activity')),
            'hobbies' => trim($request->input('hobbies')),
            'sleep_hours_in_detail' => trim($request->input('sleep_hours_in_detail')),
            'sleep_taking_time' => trim($request->input('sleep_taking_time')),

            'last_physical_examination_time' => trim($request->input('last_physical_examination_time')),

            'ever_been_hospitalized' => trim($request->input('ever_been_hospitalized')),

            'is_pregnant' => trim($request->input('is_pregnant')),
            'somking' => trim($request->input('somking')),
            'alcoholic_quantity_per_week' => trim($request->input('alcoholic_quantity_per_week')),
            'diagnosed_heart_condition' => trim($request->input('diagnosed_heart_condition')),
            'is_feel_chest_pain' => trim($request->input('is_feel_chest_pain')),
            'did_feel_chest_pain_when_not_physical' => trim($request->input('did_feel_chest_pain_when_not_physical')),
            'is_feel_loss_consciousness' => trim($request->input('is_feel_loss_consciousness')),

            'is_joint_issue' => trim($request->input('is_joint_issue')),

            'is_take_medications' => trim($request->input('is_take_medications')),

            'is_diabetic' =>$request->input('is_diabetic'),
            'is_high_blood_pressure' =>$request->input('is_high_blood_pressure'),
            'is_high_cholesterol' =>$request->input('is_high_cholesterol'),
            'is_diagnosed_osteoporosis' =>$request->input('is_diagnosed_osteoporosis'),

            'is_breathless_to_staris' =>$request->input('is_breathless_to_staris'),

            'do_you_have_been_cancer' =>$request->input('do_you_have_been_cancer'),

            'activity_reson_for_not_engage_physical_activity' =>$request->input('activity_reson_for_not_engage_physical_activity'),

            'family_disease_detail' =>$family_disease_string,

            'other_family_disease' => trim($request->input('other_family_disease')),
            'plan'         => $request->input('plan'),
            'plan_detail'   => $request->input('plan_detail'),

            ]);
		}

		$customer->assignRole($customer_role);
		return redirect()->route('customers.index')->with('success','Customer created successfully');
    }

    public function detailView($id){
        $customer = DB::table('users')
            ->join('customers','customers.customer_id','users.id')
            ->join('plans','plans.id','customers.plan')
            ->select(['customers.*' ,'users.*','plans.*'])
            ->where('users.id', $id)
            ->get();
            echo "<echo>";
            print_r($customer);die;
        if(count($customer) > 0){
            return View('pages.admin.customer.detail',  ['customer' => $customer]);
        }
        return abort(404);
    }

    public function edit($id){
      $customer = DB::table('users')
        ->join('customers','customers.customer_id','users.id')
        ->select(['customers.*' ,'users.*'])
        ->where('users.id', $id)
        ->get(); 
    if(count($customer) > 0){
        $family_diseases_arr    = array();
          $family_diseases_types = $customer->toArray();

          if($family_diseases_types){
            $types = $family_diseases_types[0]->family_disease_detail;
            $family_diseases_arr = explode(', ', $types);
          }
          $plans = Plan::get();
              $data = [
              'customer'                => $customer,
              'family_diseases_arr'   => $family_diseases_arr,
              'plans'   => $plans,
          ];
           
          return View('pages.admin.customer.edit', compact('customer', 'family_diseases_arr','plans'));
    }
      return abort(404);
    }

    public function update(Request $request, $id)
    {
    	 $customers = User::find($id);
    	
    	if(($request->input('plan') == 1) && $request->input('plan_detail') == null)
        {
              $this->validate($request, [
                'plan_detail' => 'required',

          ]);
        }
        
    	 if ($request->hasFile('image')){
            $image_path = public_path().'/media/customer-image/'.$customers->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $customerImage = $request->file('image');
            $fileName = $customerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/customer-image';
            $customerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $customers->image;
        }

        $customers->name 	         = $request->input('name');
        $customers->phone 	       = $request->input('phone');
        $customers->address        = $request->input('address');
        $customers->state 	       = $request->input('state');
        $customers->city 	         = $request->input('city');
        $customers->zip 	         = $request->input('zip');
        $customers->gender 	       = $request->input('gender');
        $customers->facebook_url   = $request->input('facebook_url');
        $customers->google_url     = $request->input('google_url');
        $customers->twitter_url    = $request->input('twitter_url');
        $customers->youtube_url    = $request->input('youtube_url');
        $customers->instagram_url  = $request->input('instagram_url');
        $customers->image 	       = $fileName;
        $customers->save();

          $consistent_physical_activity = trim($request->input('consistent_physical_activity'));
        

               $consistent_physical_activity_time = trim($request->input('consistent_physical_activity_time'));

               $consistent_physical_activity_description =trim($request->input('consistent_physical_activity_description'));
               if($consistent_physical_activity==2)
               {
               	 $consistent_physical_activity_time        = '';
               	 $consistent_physical_activity_description = '';
               }

               if($request->input('family_disease_detail')!== null)
                {
                  $family_disease_string = implode(", ", $request->input('family_disease_detail'));
                }
                else
                {
                  $family_disease_string = null;
                }

              $arr = [
                'customer_name' => $request->input('name'),
                'level'         => $request->input('level'),
                'weight'        => $request->input('weight'),
                'height'        => $request->input('height'),
                'age'        	  => $request->input('age'),

                'neck' 			   => $request->input('neck'),
                'shoulder' 		 => $request->input('shoulder'),
                'right_bicep'  => $request->input('right_bicep'),
                'chest' 		   => $request->input('chest'),
                'waist' 		   => $request->input('waist'),
                'hips' 			   => $request->input('hips'),
                'right_thigh'       	=> $request->input('right_thigh'),
                'right_calf' 	        => $request->input('right_calf'),

                'body_fat_percent'    => $request->input('body_fat_percent'),
                'body_fat_mass'     	=> $request->input('body_fat_mass'),
                'skletal_muscle_mass' => $request->input('skletal_muscle_mass'),

                'career' 		=> $request->input('career'),
                'relationship'	=> $request->input('relationship'),
                'health'			=> $request->input('health'),
                'nutrition'		=> $request->input('nutrition'),
                'sleep'			=> $request->input('sleep'),
                'appearance'		=> $request->input('appearance'),
                'purpose'		=> $request->input('purpose'),
                'social'			=> $request->input('social'),
                'spiritual'		=> $request->input('spiritual'),
                'joy'			=> $request->input('joy'),          
                'ready_for_change' => $request->input('ready_for_change'),


                'social_goal'	=> $request->input('social_goal'),

                'consistent_physical_activity' => trim($request->input('consistent_physical_activity')),

                'consistent_physical_activity_time' => $consistent_physical_activity_time,

                'consistent_physical_activity_description' =>$consistent_physical_activity_description,

                'customer_occupation' => trim($request->input('customer_occupation')),
                'houldhold' => trim($request->input('houldhold')),
                'recreational_activity' =>trim($request->input('recreational_activity')),
                'hobbies' => trim($request->input('hobbies')),
                'sleep_hours_in_detail' =>trim($request->input('sleep_hours_in_detail')),
                'sleep_taking_time' => trim($request->input('sleep_taking_time')),

                'ever_been_hospitalized' =>$request->input('ever_been_hospitalized'),

                'is_pregnant' =>$request->input('is_pregnant'),
                'smoking' =>$request->input('somking'),
                'alcoholic_quantity_per_week' =>$request->input('alcoholic_quantity_per_week'),
                'diagnosed_heart_condition' =>$request->input('diagnosed_heart_condition'),
                'is_feel_chest_pain' =>$request->input('is_feel_chest_pain'),
                'did_feel_chest_pain_when_not_physical' =>$request->input('did_feel_chest_pain_when_not_physical'),
                'is_feel_loss_consciousness' =>$request->input('is_feel_loss_consciousness'),

                'is_joint_issue' =>$request->input('is_joint_issue'),

                'is_take_medications' =>$request->input('is_take_medications'),

                'is_diabetic' =>$request->input('is_diabetic'),
                'is_high_blood_pressure' =>$request->input('is_high_blood_pressure'),
                'is_high_cholesterol' =>$request->input('is_high_cholesterol'),
                'is_diagnosed_osteoporosis' =>$request->input('is_diagnosed_osteoporosis'),

                'is_breathless_to_staris' =>$request->input('is_breathless_to_staris'),

                'do_you_have_been_cancer' =>$request->input('do_you_have_been_cancer'),

                'activity_reson_for_not_engage_physical_activity' =>$request->input('activity_reson_for_not_engage_physical_activity'),
                'family_disease_detail' =>$family_disease_string,

                'other_family_disease' =>$request->input('other_family_disease'),
                'plan'         => $request->input('plan'),
                // 'plan_detail'   => $request->input('plan_detail'),

            ];
            
        $affected = DB::table('customers')
              ->where('customer_id', $id)
              ->update($arr);

        return redirect()->route('customers.index')
                        ->with('success','Customer updated successfully');
    }

    public function delete($id){
        $customerData = Customer::where(['customer_id'=>$id])->get();
        $userData     = User::where(['id'=>$id])->get();
        
        $image_path = public_path().'/media/customer-image/'.$userData[0]->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $customerData[0]->delete();
        $userData[0]->delete();

        return response()->json(['success' => true,'message'=> 'Customer has been deleted']); 
    }
    
  public function customWorkout(Request $request){

    $data = DB::table('cus_custom_workout_detail')
      ->join('cus_custom_workout', 'cus_custom_workout.id','cus_custom_workout_detail.customer_workout_id')
      ->join('movement_gymworkouts','movement_gymworkouts.id','cus_custom_workout_detail.gym_category_id')
      ->join('movement_sub_gymworkouts','movement_sub_gymworkouts.id','cus_custom_workout_detail.gym_category_exercise_id')
      ->join('users','users.id','cus_custom_workout.customer_id')
      ->select(['cus_custom_workout_detail.*', 'cus_custom_workout.title as title', 'movement_gymworkouts.name as category_name', 'movement_sub_gymworkouts.sub_name as exercise_name', 'users.name as customer_name'])
     ->orderBy('id','DESC')
        ->get();
    //print_r($data->toArray());
   return View('pages.admin.customer.customWorkout',  compact('data'));
  }

  public function bodyMeasurementLog(Request $request){
    $rows = DB::table('cu_body_measurement_log')
        ->join('users','users.id','cu_body_measurement_log.customer_id')
      ->select(['users.name as user_name' ,'cu_body_measurement_log.*'])
        ->get();
    
    return View('pages.admin.customer.bodyMeasurementLog',  compact('rows'));
  }

    public function searchData(Request $request){
        $searchval = $request->input('searchval');
        // $data = User::where('name', 'LIKE', '%'.$searchval.'%')->get();
        $data = User::where('name', 'LIKE', '%'.$searchval.'%')->orWhere('email', 'LIKE', '%'.$searchval.'%')->get();
        if($data->toArray()){
            $res = ['status' => true,'success' => $data];
        }else{
            $res = ['status' => false,'success' => $data];
        }
        echo json_encode($res);
    }

}

