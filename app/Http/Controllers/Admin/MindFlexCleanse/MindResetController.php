<?php

namespace App\Http\Controllers\Admin\MindFlexCleanse;

use Auth;
use Session;
use DB;
use Hash;
use File; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexCleanse\MindReset;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MindResetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = MindReset::get(); 
        return view('pages.admin.mindFlexCleanse.mindReset.list',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.mindFlexCleanse.mindReset.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'banner' =>'required',
            'banner' => 'mimes:jpeg,jpg,gif,png'
        ]);

        if($banner = $request->file('banner')){
            $destinationPath = public_path().'/media/MindFlexCleanse-mindReset-banner'; // upload path

            $banner_name = date('YmdHis') . "." . $banner->getClientOriginalExtension();
                
            $banner->move($destinationPath, $banner_name);
        }else{
            $banner_name = null;
        }

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/MindFlexCleanse-mindReset-video'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }   
        }

        $row = MindReset::create([
                'name'                    => $request->input('name'),
                'banner'                  => $banner_name,
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'description'             => $request->input('description'),
            ]);           
        $row->save();

        return redirect()->route('cleanse_mind_reset.index')
                        ->with('success','Mind Reset added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = MindReset::findOrFail($id);
        return view('pages.admin.mindFlexCleanse.mindReset.show',compact('row'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = MindReset::findOrFail($id);
        return view('pages.admin.mindFlexCleanse.mindReset.edit',compact('row'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = MindReset::find($id);

        $this->validate($request, [
            'name'=>'required',
            'banner' => 'mimes:jpeg,jpg,gif,png'
        ]);

        if ($request->hasFile('banner')){
            $image_path = public_path().'/media/MindFlexCleanse-mindReset-banner/'.$data->banner;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $banner_name = date('YmdHis') . "." . $bannerImage->getClientOriginalExtension();
           // $banner_name = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/MindFlexCleanse-mindReset-banner';
            $bannerImage->move($destinationPath, $banner_name);
        } else {
            $banner_name = $data->banner;
        } 

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/MindFlexCleanse-mindReset-video'.$data->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $video_file = $request->file('video_file');
            $video_file = date('YmdHis') . "." . $video_file->getClientOriginalExtension();
            //$video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/MindFlexCleanse-mindReset-video';
            $video_file->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        $data->name = $request->input('name');
        $data->description = $request->input('description');
        $data->banner = $banner_name;
        $data->video_file = $video_file;
        $data->video_url = trim($video_url);
        $data->save();
        return redirect()->route('cleanse_mind_reset.index')
                        ->with('success','Exercise updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = MindReset::find($id);
        $banner = public_path().'/media/MindFlexCleanse-mindReset-banner/'.$data->banner;
        if (File::exists($banner)) {
            File::delete($banner);
        }

        $video_file =  public_path().'/media/MindFlexCleanse-mindReset-video'.$data->video_file;
        if (File::exists($video_file)) {
            File::delete($video_file);
        }

        $data->delete();
        return response()->json(['success' => true,'message'=> 'Exercise has been deleted']);
    }
}
