<?php

namespace App\Http\Controllers\Admin\MindFlexCleanse\Nutrition;

use Auth;
use Session;
use DB;
use Hash; 
use App\Models\MindFlexCleanse\Nutrition\FoodLog;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FoodLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foodLogs = FoodLog::orderBy('id','DESC')->paginate(10);
        return View('pages.admin.mindFlexCleanse.nutrition.foodlog.list',  compact('foodLogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexCleanse.nutrition.foodlog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'quantity' => 'required'
        ]);

        $foodLog = FoodLog::create([
            'name'      => $request->input('name'),
            'quantity'  => $request->input('quantity') 
            ]);            
        $foodLog->save();

        return redirect()->route('cleanse_food_log.index')
                        ->with('success','New Food Log added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $foodLog = FoodLog::findOrFail($id);
        $data = [
            'foodLog'            => $foodLog,
        ];
        return View('pages.admin.mindFlexCleanse.nutrition.foodlog.edit',  compact('foodLog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $foodLog = FoodLog::find($id);

        $this->validate($request, [
            'name' => 'required',
            'quantity' => 'required'
        ]);

        $foodLog->name = $request->input('name');
        $foodLog->quantity = $request->input('quantity');
        $foodLog->save();
        
        return redirect()->route('cleanse_food_log.index')
                        ->with('success','Food Log updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        FoodLog::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Food Log has been deleted']);
    }
}
