<?php

namespace App\Http\Controllers\Admin\MindFlexCleanse\Nutrition;


use Auth;
use Session;
use DB;
use Hash; 
use App\Models\MindFlexCleanse\Nutrition\Tip;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tips = Tip::orderBy('id','DESC')->paginate(10);
        return View('pages.admin.mindFlexCleanse.nutrition.tips.list',  compact('tips'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexCleanse.nutrition.tips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'banner' =>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        // Image upload 
        if ($files = $request->file('banner')) {
           $destinationPath = public_path().'/media/cleanse-nutrition-tips-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }else{
            $banner = null;
        }

        $tip = Tip::create([
            'title'                 => $request->input('title'),
            'banner'                => $banner, 
            'description'           => $request->input('description') 
            ]);            
        $tip->save();

        return redirect()->route('cleanse_tip.index')
                        ->with('success','New Tips added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tip = Tip::findOrFail($id);
        return View('pages.admin.mindFlexCleanse.nutrition.tips.show',  compact('tip'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tip = Tip::findOrFail($id);
        $data = [
            'tip'            => $tip,
        ];
        return View('pages.admin.mindFlexCleanse.nutrition.tips.edit',  compact('tip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tip = Tip::find($id);

       $this->validate($request, [
            'title' => 'required',
            'banner' =>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($request->hasFile('banner')){ 
            $image_path = public_path().'/media/cleanse-nutrition-tips-banner/'.$tip->banner;
          
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/cleanse-nutrition-tips-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $tip->banner;
        }

        $tip->title = $request->input('title');
        $tip->banner = $fileName;
        $tip->description = $request->input('description');
        $tip->save();
        
        return redirect()->route('cleanse_tip.index')
                        ->with('success','Tips updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Tip::find($id);
        $image_path = public_path().'/media/cleanse-nutrition-tips-banner/'.$data->banner;
          
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();
        return response()->json(['success' => true,'message'=> 'Tip has been deleted']);
    }
}
