<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\MindFlexPhysio\Store;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class StoreController extends Controller
{
    public function index()
    {
    	$stores = Store::orderBy('id','DESC')->get();
		return View('pages.admin.mindflexPhysio.store.list',  compact('stores'));
	}
	public function create()
    {
    	return View('pages.admin.mindflexPhysio.store.create');
    }

    public function save(Request $request)
    {
    	$this->validate($request, [
        'name'          => 'required',
        // 'actual_price'  => 'required',
        'sales_price'   => 'required',
        'total_quantity'=> 'required',
        'buy_now_url'   => 'required|url',
        'image'         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
        //Upload Store image
        if ($files = $request->file('image')) {
           $destinationPath = public_path().'/media/mindflexPhysio/store'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }

        $data = [
          'name'                => trim($request->input('name')),
          'image'               => $fileName,
          // 'actual_price'     => trim($request->input('actual_price')),
          'sales_price'         => trim($request->input('sales_price')),
          'total_quantity'      => trim($request->input('total_quantity')),
          'remaining_quantity'  => trim($request->input('total_quantity')),
          'buy_now_url'         => trim($request->input('buy_now_url')),
          'description'         => trim($request->input('description')),
          'store_status'        => trim($request->input('store_status')),
        ];

         $stores = Store::create($data);
         return redirect()->route('physio_store.index')
         ->with('success','Item created successfully');
	}

	public function edit($id)
    {
    	$store = Store::findOrFail($id);

    	return View('pages.admin.mindflexPhysio.store.edit', compact('store'));
    }

    public function update(Request $request, $id)
    {
    	$store = Store::findOrFail($id);
    	$this->validate($request, [
           'name'          => 'required',
           //'actual_price'  => 'required',
           'sales_price'   => 'required',
           'total_quantity'=> 'required',
           'buy_now_url'   => 'required|url',
           'image'         => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->hasFile('image')){
		    $image_path = public_path().'/media/mindflexPhysio/store/'.$store->image;
		    if (File::exists($image_path)) {
		        File::delete($image_path);
		    }
		    $bannerImage = $request->file('image');
		    $fileName = $bannerImage->getClientOriginalName();
		    $destinationPath = public_path().'/media/mindflexPhysio/store';
		    $bannerImage->move($destinationPath, $fileName);
		} else {
		    $fileName = $store->image;
		}

		$data = [
            'name'          => trim($request->input('name')),
            'image'         => $fileName,
            //'actual_price'  => trim($request->input('actual_price')),
            'sales_price'   => trim($request->input('sales_price')),
            'total_quantity'=> trim($request->input('total_quantity')),
            'remaining_quantity'=> trim($request->input('total_quantity')),
            'buy_now_url'   => trim($request->input('buy_now_url')),
            'description'   => trim($request->input('description')),
            'store_status'  => $request->input('store_status'),
         ];
         $store->update($data);
         return redirect()->route('physio_store.index')
                        ->with('success','Item updated successfully'); 
    }


    public function detailView($id)
    {
    	$store = Store::findOrFail($id);
    	return view('pages.admin.mindflexPhysio.store.detail', compact('store'));
    }

    public function delete($id)
    {
      $data = Store::find($id);
      $image_path = public_path().'/media/mindflexPhysio/store/'.$data->image;
      if (File::exists($image_path)) {
          File::delete($image_path);
      }
      $data->delete();

    	// Store::findOrFail($id)->delete();
    	return response()->json(['success' => true,'message'=> 'Item has been deleted']);
    }
}
