<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\MindFlexPhysio\PhysioArticle;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class ArticlesPhysioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $articles = PhysioArticle::orderBy('id','DESC')->get();
       
	   return View('pages.admin.mindflexPhysio.article.list',  compact('articles'));
    }

     public function create()
    {
        return View('pages.admin.mindflexPhysio.article.create');
    }
    public function save(Request $request)
    {
    	$this->validate($request, [
            'name'        => 'required',
            'article_img' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author_name'  => 'required',
            'description'  => 'required',
            'physio_article_status'=> 'required',
        ]);

        if ($files = $request->file('article_img')) {
           $destinationPath = public_path().'/media/mindflexPhysio/article'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }else{
            $banner = "";
        }

        $article = PhysioArticle::create(
         [
            'name'          => trim($request->input('name')),
            'article_img'   => $banner,
            'author_name'   => trim($request->input('author_name')),
            'description'   => trim($request->input('description')),
            'physio_article_status'   => $request->input('physio_article_status'),
          ]);
        $article->save();
        return redirect()->route('physio_article.index')
                        ->with('success','Article added successfully');
    }

    public function edit($id){
        $article = PhysioArticle::findOrFail($id);
        
        return View('pages.admin.mindflexPhysio.article.edit',  compact('article'));
    }

    public function update(Request $request, $id)
    {

    	$article = PhysioArticle::find($id);
    	$this->validate($request, [
            'name'        => 'required',
            'article_img' =>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author_name'  => 'required',
            'description'  => 'required',
            'physio_article_status'=> 'required',
        ]);

        if ($request->hasFile('article_img')){
            $image_path = public_path().'/media/mindflexPhysio/article/'.$article->article_img;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('article_img');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindflexPhysio/article';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $article->article_img;
        }

        $data = [
            'name'          => trim($request->input('name')),
            'article_img'   => $fileName,
            'author_name'   => trim($request->input('author_name')),
            'description'   => trim($request->input('description')),
            'physio_article_status'   => $request->input('physio_article_status'),
          ];
          $article->update($data);
          return redirect()->route('physio_article.index')
                        ->with('success','Article updated successfully'); 

    }

    public function detailView(Request $request, $id)
     {
        $article = PhysioArticle::findOrFail($id);
       return View('pages.admin.mindflexPhysio.article.detail',  compact('article'));
    }

    public function delete($id)
    {
    	$data = PhysioArticle::find($id);
        $image_path = public_path().'/media/mindflexPhysio/article/'.$data->article_img;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

        // PhysioArticle::find($id)->delete($id);
        return response()->json(['success' => true,'message'=> 'Article has been deleted']);
    }
}
