<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio;


use Auth;
use Session;
use DB; 
use Hash;
use File; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexPhysio\DailyAffirmation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DailyAffirmationController extends Controller
{
    public function index()
    {
        $rows = DailyAffirmation::orderBy('id','DESC')->get();
        return View('pages.admin.mindflexPhysio.dailyAffirmation.list',  compact('rows'));
    }
    public function create()
    {
        return View('pages.admin.mindflexPhysio.dailyAffirmation.create');
        
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);
 
        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/mindflexPhysio/dailyAffirmation'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
            
        } 
        $row = DailyAffirmation::create([
                'title'                   => $request->input('title'),
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'description'             => $request->input('description'),
                'mark_status'             => $request->input('mark_status'),
            ]);           
        $row->save();

        return redirect()->route('physio_daily_affirmation.index')
                        ->with('success','New Daily Affirmation added successfully');
    }

    public function show($id)
    {
        $row = DailyAffirmation::findOrFail($id);
        return View('pages.admin.mindflexPhysio.dailyAffirmation.detail',  compact('row'));
    }

     public function edit($id)
    {
        $row = DailyAffirmation::findOrFail($id);
        $data = [
            'row'            => $row,
        ];
        return View('pages.admin.mindflexPhysio.dailyAffirmation.edit',  compact('row'));
    }

     public function update(Request $request, $id)
    {
        $data = DailyAffirmation::find($id);
         $this->validate($request, [
            'title' => 'required',
        ]);

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/mindflexPhysio/dailyAffirmation'.$data->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindflexPhysio/dailyAffirmation';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        $data->title = $request->input('title');
        $data->description = $request->input('description');
        $data->mark_status = $request->input('mark_status');
        $data->video_file = $video_file;
        $data->video_url = trim($video_url);
        $data->save();
        return redirect()->route('physio_daily_affirmation.index')
                        ->with('success','Daily Affirmation updated successfully');
    }


    public function delete($id)
    {
        $data = DailyAffirmation::find($id);
        $video_file = public_path().'/media/mindflexPhysio/dailyAffirmation'.$data->video_file;
        if (File::exists($video_file)) {
            File::delete($video_file);
        }
        $data->delete();
        
        // DailyAffirmation::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Daily Affirmation has been deleted']);
    }


}
