<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio;

use DB;
use Auth;
use Session;
use App\Models\MindFlexPhysio\Doc;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docs = Doc::orderBy('id','DESC')->get();
       
        return View('pages.admin.mindflexPhysio.doc.list',  compact('docs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();

        return View('pages.admin.mindflexPhysio.doc.create', compact('coaches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        // dd($request);
        $this->validate($request, [ 'title' => 'required', 'price' => 'required' ]);

        if ($files = $request->file('image')) {
           $destinationPath = public_path().'/media/mindflexPhysio/doc-image'; // upload path

           $image = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $image);
        }else{
            $image = null;
        }

        $row = Doc::create([
                'title' => $request->input('title'),
                'price' => $request->input('price'),
                'therapist_id' => $request->input('therapist_id'),
                'booking_date' => $request->input('booking_date'),
                'time_slot' => $request->input('time_slot'),
                'image' => $image,
                "typeVisitSession" => $request->input('typeVisitSession'),
                'description' => trim($request->input('description')),
            ]);           
        $row->save();

        return redirect()->route('physio_doc.index')
                        ->with('success','See A Doc added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doc = Doc::findOrFail($id);
        $therapist_id = $doc['therapist_id'];

        $therapistData = DB::table('users')->where(['id'=>$therapist_id])->get();
        $therapistName = $therapistData[0]->name;

        return View('pages.admin.mindflexPhysio.doc.detail',  compact('doc', 'therapistName'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();

        $doc = Doc::findOrFail($id);
        return View('pages.admin.mindflexPhysio.doc.edit',  compact('doc', 'coaches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $doc = Doc::find($id);
        $this->validate($request, [ 'title' => 'required', 'price' => 'required' ]);   

        if ($request->hasFile('image')){
            $image_path = public_path().'/media/mindflexPhysio/doc-image/'.$doc->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('image');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindflexPhysio/doc-image';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $doc->image;
        } 

         $data = [
            'title' => $request->input('title'),
            'price' => $request->input('price'),
            'therapist_id' => $request->input('therapist_id'),
            'booking_date' => $request->input('booking_date'),
            'time_slot' => $request->input('time_slot'),
            'image' => $fileName,
            'typeVisitSession' => $request->input('typeVisitSession'),
            'description' => trim($request->input('description')),
          ];
          $doc->update($data);
          return redirect()->route('physio_doc.index')
                        ->with('success','See A Doc updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Doc::find($id);
        $image_path = public_path().'/media/mindflexPhysio/doc-image/'.$data->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

        // Doc::find($id)->delete($id);
        return response()->json(['success' => true,'message'=> 'See A Doc has been deleted']); 
    }
}
