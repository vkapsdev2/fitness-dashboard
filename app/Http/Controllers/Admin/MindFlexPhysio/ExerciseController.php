<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio;

use Auth;
use Session;
use App\Models\MindFlexPhysio\Exercise;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exercises = Exercise::orderBy('id','DESC')->get();
       
        return View('pages.admin.mindflexPhysio.exercise.list',  compact('exercises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindflexPhysio.exercise.create');
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
        ]);

        if ($files = $request->file('banner')) {
           $destinationPath = public_path().'/media/mindflexPhysio/exercise-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }else{
            $banner = null;
        }

        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/mindflexPhysio/exercise-video'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
        } 

        $row = Exercise::create([
                'name'                    => $request->input('name'),
                'banner'                  => $banner,
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'description'             => trim($request->input('description')),
                'mark_status'             => $request->input('mark_status'),
            ]);           
        $row->save();

        return redirect()->route('physio_exercise.index')
                        ->with('success','New Exercise Program added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $exercise = Exercise::findOrFail($id);
       return View('pages.admin.mindflexPhysio.exercise.detail',  compact('exercise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exercise = Exercise::findOrFail($id);
        return View('pages.admin.mindflexPhysio.exercise.edit',  compact('exercise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $exercise = Exercise::find($id);
       $this->validate($request, [
            'name'        => 'required'
        ]);   

        if ($request->hasFile('banner')){
            $image_path = public_path().'/media/mindflexPhysio/exercise-banner/'.$exercise->banner;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindflexPhysio/exercise-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $exercise->banner;
        } 

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/mindflexPhysio/exercise-video'.$exercise->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindflexPhysio/exercise-video';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

         $data = [
            'name'                    => $request->input('name'),
            'banner'                  => $fileName,
            'video_file'              => $video_file,
            'video_url'               => trim($video_url),
            'description'             => trim($request->input('description')),
            'mark_status'             => $request->input('mark_status'),
          ];
          $exercise->update($data);
          return redirect()->route('physio_exercise.index')
                        ->with('success','Exercise Program updated successfully'); 
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Exercise::find($id);

        $image_path = public_path().'/media/mindflexPhysio/exercise-banner/'.$data->banner;
        if (($data->image) && (File::exists($image_path))) {
            File::delete($image_path);
        }

        $video_file =  public_path().'/media/mindflexPhysio/exercise-video'.$data->video_file;
        if (($data->video_file) && (File::exists($video_file))) {
            File::delete($video_file);
        }
        $data->delete();

        // Exercise::find($id)->delete($id);
        return response()->json(['success' => true,'message'=> 'Exercise Program has been deleted']);    
    }
}
