<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio\Nutrition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\Meal;
use App\Models\MindFlexPhysio\Nutrition\Recommendation;
use Illuminate\Support\Facades\Validator;


class RecommendationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
       $suggestedMeals = Recommendation::orderBy('id','DESC')->paginate(10);
        return View('pages.admin.mindflexPhysio.nutrition.recommendMealsToCustomer.list',  compact('suggestedMeals'));

	}

	public function create(){
       return View('pages.admin.mindflexPhysio.nutrition.recommendMealsToCustomer.create');
    }

    public function save(Request $request){
        $this->validate($request, [
            'Food_name'     => 'required',
            'meal_type'     => 'required',
            'calorie'       => 'required',
            'fats'          => 'required',
            'carbohydrate'  => 'required',
            'protein'       => 'required',
        ]);

        $suggestedMeal = Recommendation::create([
            'Food_name'                => $request->input('Food_name'), 
            'meal_type'                => $request->input('meal_type'), 
            'calorie'                  => $request->input('calorie'), 
            'fats'                     => $request->input('fats'), 
            'carbohydrate'             => $request->input('carbohydrate'), 
            'protein'                  => $request->input('protein'), 
            'description'              => $request->input('description') 
            ]);            
        $suggestedMeal->save();

        return redirect()->route('physio_recommend_meal.index')
                        ->with('success','Recommended Meal added successfully');
    }


    public function edit($id){
    $suggestedMeal = Recommendation::findOrFail($id);
        $data = [
            'suggestedMeal' => $suggestedMeal,
        ];
        return View('pages.admin.mindflexPhysio.nutrition.recommendMealsToCustomer.edit',  compact('suggestedMeal'));

    }

    public function update(Request $request, $id){
        $suggestedMeal = Recommendation::find($id);

        $this->validate($request, [
            'Food_name'     => 'required',
            'meal_type'     => 'required',
            'calorie'       => 'required',
            'fats'          => 'required',
            'carbohydrate'  => 'required',
            'protein'       => 'required',
        ]);

        $suggestedMeal->Food_name       = $request->input('Food_name');
        $suggestedMeal->meal_type       = $request->input('meal_type');
        $suggestedMeal->calorie         = $request->input('calorie');
        $suggestedMeal->fats            = $request->input('fats');
        $suggestedMeal->carbohydrate    = $request->input('carbohydrate');
        $suggestedMeal->protein         = $request->input('protein');
        $suggestedMeal->description     = $request->input('description');
        $suggestedMeal->save();
        
        return redirect()->route('physio_recommend_meal.index')
                        ->with('success','Recommended Meal updated successfully');

    }

    // public function detailView($id){
    //     $suggested_meal = Recommendation::find($id);

    //     $coach_detail = DB::table('users')
    //                         ->where('id', $suggested_meal->suggester_coach_id)
    //                         ->get();

    //     $customer_detail = DB::table('users')
    //                         ->where('id', $suggested_meal->suggested_customer_id)
    //                         ->get();

    //     $breakfastId  = 1;
    //     $lunchId      = 2;
    //     $dinnerhId    = 3;

    //     $breakFastMeals  =   Meal::orderBy('id', 'DESC')
    //                         ->where('meal_plan_type',$breakfastId)
    //                         ->get();

    //     $lunchMeals     =   Meal::orderBy('id', 'DESC')
    //                         ->where('meal_plan_type',$lunchId)
    //                         ->get();

    //     $dinnerMeals     =   Meal::orderBy('id', 'DESC')
    //                         ->where('meal_plan_type',$dinnerhId)
    //                         ->get();

    //     return View('pages.admin.mindflexPhysio.nutrition.recommendMealsToCustomer.detail',compact('suggested_meal','coach_detail', 'customer_detail', 'breakFastMeals', 'lunchMeals', 'dinnerMeals'));
    // }

    public function delete($id){
        Recommendation::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Recommended Meal has been deleted']);
    }
}
