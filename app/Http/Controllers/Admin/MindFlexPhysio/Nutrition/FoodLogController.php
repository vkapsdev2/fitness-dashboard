<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio\Nutrition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\MindFlexPhysio\Nutrition\FoodLog;
use Illuminate\Support\Facades\Validator;
class FoodLogController extends Controller
{
    public function index()
    {
        $foodLogs = FoodLog::orderBy('id','DESC')->paginate(10);
        return View('pages.admin.mindflexPhysio.nutrition.foodlog.list',  compact('foodLogs'));
    }
    public function create()
    {
         return View('pages.admin.mindflexPhysio.nutrition.foodlog.create');
    }

     public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'quantity' => 'required'
        ]);

        $foodLog = FoodLog::create([
            'name'                    => $request->input('name'),
            'quantity'                => $request->input('quantity') 
            ]);            
        $foodLog->save();

        return redirect()->route('physio_food_log.index')
                        ->with('success','New Food Log added successfully');
    }

    public function edit($id)
    {
        $foodLog = FoodLog::findOrFail($id);
        $data = [
            'foodLog'            => $foodLog,
        ];
        return View('pages.admin.mindflexPhysio.nutrition.foodlog.edit',  compact('foodLog'));
    }

    public function update(Request $request, $id)
    {
        $foodLog = FoodLog::find($id);

        $this->validate($request, [
            'name' => 'required',
            'quantity' => 'required'
        ]);

        $foodLog->name = $request->input('name');
        $foodLog->quantity = $request->input('quantity');
        $foodLog->save();
        
        return redirect()->route('physio_food_log.index')
                        ->with('success','Food Log updated successfully');
    }

    public function delete($id)
    {
        FoodLog::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Food Log has been deleted']);
    }
}
