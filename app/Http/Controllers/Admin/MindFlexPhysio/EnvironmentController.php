<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio;

use Auth;
use Session;
use App\Models\MindFlexPhysio\Environment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EnvironmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $environments = Environment::orderBy('id','DESC')->get();
       
        return View('pages.admin.mindflexPhysio.environment.list',  compact('environments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindflexPhysio.environment.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'  => 'required',
        ]);

        if ($files = $request->file('image')) {
           $destinationPath = public_path().'/media/mindflexPhysio/environment'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }else{
            $banner = null;
        }
        $environment = Environment::create(
         [
            'name'          => trim($request->input('name')),
            'image'         => $banner,
            'description'   => trim($request->input('description')),
          ]);
        $environment->save();
        return redirect()->route('physio_environment.index')
                        ->with('success','Environment Factor added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $environment = Environment::findOrFail($id);
       return View('pages.admin.mindflexPhysio.environment.detail',  compact('environment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $environment = Environment::findOrFail($id);
        
        return View('pages.admin.mindflexPhysio.environment.edit',  compact('environment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $environment = Environment::find($id);

       $this->validate($request, [
            'name'        => 'required',
            //'image' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'  => 'required',
        ]);

       if ($request->hasFile('image')){
            $image_path = public_path().'/media/mindflexPhysio/environment/'.$environment->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('image');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindflexPhysio/environment';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $environment->image;
        }

        $data = [
            'name'          => trim($request->input('name')),
            'image'         => $fileName,
            'description'   => trim($request->input('description')),
          ];
          $environment->update($data);
          return redirect()->route('physio_environment.index')
                        ->with('success','Environment Factor  updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Environment::find($id);
        $image_path = public_path().'/media/mindflexPhysio/environment/'.$data->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

        // Environment::find($id)->delete($id);
        return response()->json(['success' => true,'message'=> 'Environment Factor has been deleted']);
    }
}
