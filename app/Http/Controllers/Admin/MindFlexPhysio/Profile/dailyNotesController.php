<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\User;
use App\Models\Customer;
use App\Models\MindFlexPhysio\Profile\DailyNote;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class dailyNotesController extends Controller{

  public function index(){
    $daily_notes = DB::table('mindiflex_pysio_profile_daily_notes')
      ->join('customers','customers.customer_id','mindiflex_pysio_profile_daily_notes.customer_id')
      ->select(['customers.customer_name', 'mindiflex_pysio_profile_daily_notes.*'])
      ->orderBy('mindiflex_pysio_profile_daily_notes.id','DESC')
      ->get();
    	return View('pages.admin.mindflexPhysio.profile.dailyNotes.list',  compact('daily_notes'));
	}

	public function create(){
    $customers = DB::table('users')
      ->join('model_has_roles',function($join) { $join->on('model_has_roles.model_id' , '=' , 'users.id')
      ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
      ->get();
		return View('pages.admin.mindflexPhysio.profile.dailyNotes.create', compact('customers'));
	}

	public function save(Request $request){
		$this->validate($request, [
      'daily_note_detail' => 'required',
      'customer_id'       => 'required',
    ]);

    $data = [
      'daily_note_detail'     => trim($request->input('daily_note_detail')),
      'customer_id'           => $request->input('customer_id'),
      'daily_note_date'     	=> date('Y-m-d'),
      'daily_note_status'     => $request->input('daily_note_status'),
    ];
    $daily_notes = DailyNote::create($data);
    return redirect()->route('daily_notes.index')->with('success','Daily Notes created successfully');	
	}

	public function  edit($id){
	 $daily_note = DailyNote::findOrFail($id);
    $customers = DB::table('users')
      ->join('model_has_roles',function($join) { $join->on('model_has_roles.model_id' , '=' , 'users.id')
      ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
      ->get();
		return View('pages.admin.mindflexPhysio.profile.dailyNotes.edit', compact('daily_note', 'customers'));
  }

	public function  update(Request $request, $id){
		$daily_note = DailyNote::find($id);
		$this->validate($request, [
      'daily_note_detail' => 'required',
      'customer_id'       => 'required',
    ]);

    $data = [
      'daily_note_detail'     => trim($request->input('daily_note_detail')),
      'customer_id'           => $request->input('customer_id'),
      'daily_note_status'     => $request->input('daily_note_status'),
    ];
    $daily_note->update($data);
    return redirect()->route('daily_notes.index')->with('success','Daily Notes updated successfully');
	}

	public function delete($id){
    DailyNote::find($id)->delete($id);
    return response()->json(['success' => true,'message'=> 'Daily Note has been deleted']);
	}
  
}
