<?php

namespace App\Http\Controllers\Admin\MindFlexPhysio\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash;
use App\Models\MindFlexPhysio\Profile\MedicalHistory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class medicalHistoryController extends Controller{
  public function __construct(){
      $this->middleware('auth');
  }

  public function index(){
    // $medical_histories = DB::table('mindiflex_pysio_profile_medical_history')
    // ->join('customers','customers.customer_id','mindiflex_pysio_profile_medical_history.customer_id')
    // ->select(['customers.customer_name', 'mindiflex_pysio_profile_medical_history.*'])
    // ->orderBy('mindiflex_pysio_profile_medical_history.id','DESC')
    // ->get();

    $medical_histories = DB::table('mindiflex_pysio_profile_medical_history')
    ->join('users','users.id','mindiflex_pysio_profile_medical_history.customer_id')
    ->select(['users.name', 'mindiflex_pysio_profile_medical_history.*'])
    ->orderBy('mindiflex_pysio_profile_medical_history.id','DESC')
    ->get();

    return View('pages.admin.mindflexPhysio.profile.medicalHistory.list',  compact('medical_histories'));
	}

	public function create(){
    $customers = DB::table('users')
      ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
      ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
      ->get(); 
		return View('pages.admin.mindflexPhysio.profile.medicalHistory.create', compact('customers'));
	}

	public function save(Request $request){
		$this->validate($request, [
        'medical_hisory_detail' => 'required',
        'customer_id'           => 'required',
    ]);

    $data = [
      'medical_hisory_detail'  => trim($request->input('medical_hisory_detail')),
      'customer_id'            => $request->input('customer_id'),
      'date'     					     => date('Y-m-d'),
      'medical_history_status' => $request->input('medical_history_status'),
    ];
    $medicalHistory = MedicalHistory::create($data);
    return redirect()->route('medical_history.index')->with('success','Medical history created successfully');
	}

	public function  edit($id)
	{
    $medical_history = MedicalHistory::findOrFail($id);
    $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

		
		return View('pages.admin.mindflexPhysio.profile.medicalHistory.edit', compact('medical_history', 'customers'));
	}

	public function  update(Request $request, $id)
	{
		$medical_history = MedicalHistory::find($id);

		$this->validate($request, [
            'medical_hisory_detail' => 'required',
            'customer_id'           => 'required',
        ]);

         $data = [
            'medical_hisory_detail'     => trim($request->input('medical_hisory_detail')),
            'customer_id'               => $request->input('customer_id'),
            
           'medical_history_status'      => $request->input('medical_history_status'),
            
        ];
          $medical_history->update($data);
          return redirect()->route('medical_history.index')
                        ->with('success','Medical history updated successfully');

	}

	public function delete($id)
	{
		MedicalHistory::find($id)->delete($id);
    	return response()->json(['success' => true,'message'=> 'Medical history has been deleted']);
	}

}
