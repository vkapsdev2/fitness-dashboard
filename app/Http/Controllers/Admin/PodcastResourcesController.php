<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use DB;
use Hash;
use File;
use FFMpeg;
use App\Models\Podcast;
use App\Models\SubPodcast;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class PodcastResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $podcasts = Podcast::orderBy('id','DESC')->get();
       //($podcasts);
	   return View('pages.admin.podcast.list',  compact('podcasts'));
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.podcast.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'banner' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($files = $request->file('banner')) {
           $destinationPath = public_path().'/media/podcast-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }
  
        $podcast = Podcast::create([
                'name'                    => $request->input('name'),
                'podcast_img'             => $banner,

                
            ]);            
        $podcast->save();

        return redirect()->route('podcast.index')
                        ->with('success','Podcast added successfully');
    }

    public function edit($id){
        $podcast = Podcast::findOrFail($id);
        $data = [
            'podcast'            => $podcast,
        ];
        return View('pages.admin.podcast.edit',  compact('podcast'));
    }

    public function update(Request $request, $id){
          
        $podcast = Podcast::find($id);
         $this->validate($request, [
            'name' => 'required',
        ]);
        
        if ($request->hasFile('podcast_img')){
		    $image_path = public_path().'/media/podcast-banner/'.$podcast->podcast_img;
		    if (File::exists($image_path)) {
		        File::delete($image_path);
		    }
		    $bannerImage = $request->file('podcast_img');
		    $fileName = $bannerImage->getClientOriginalName();
		    $destinationPath = public_path().'/media/podcast-banner';
		    $bannerImage->move($destinationPath, $fileName);
		} else {
		    $fileName = $podcast->podcast_img;
		}
         //print_r($fileName);
         // dd($request->input('name'));
        $podcast->name = $request->input('name');
        $podcast->podcast_img = $fileName;
        $podcast->save();
        return redirect()->route('podcast.index')
                        ->with('success','Podcast updated successfully'); 
        
    }

    public function delete($id){
        $data = Podcast::findOrFail($id);
        $image_path = public_path().'/media/podcast-banner/'.$data->podcast_img;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

        // Podcast::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Podcast has been deleted']);
    }

    //Sub Podcast module start

    public function subPodcastList(Request $request, $podcast_id){
    
    	$subPodcasts = SubPodcast::where('podcast_id',$podcast_id)->orderBy('id','DESC')->get();
        $podcasts = Podcast::findOrFail($podcast_id);
	   return View('pages.admin.podcast.subpodcast_list',  compact('subPodcasts','podcasts'));
    }

    public function createSubPodcast(Request $request, $podcast_id)
    {
    	$podcast = Podcast::findOrFail($podcast_id);
        $data = [
            'podcast'            => $podcast, 
        ];
    	return View('pages.admin.podcast.subpodcast_create', compact('podcast'));
    }

    public function saveSubPodcast(Request $request,$podcast_id)
    { 

       // $id	=  $request->input('id');
    	$podcast = Podcast::findOrFail($podcast_id);
       
            $this->validate($request, [
                'name' =>'required',
            ]);

        if ($files = $request->file('audio_file')) {
        	$this->validate($request, [
            	'audio_file' =>'required',
        	]);
           $destinationPath = public_path().'/media/podcast-episodes'; // upload path

           $audio_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $audio_file);
           $audio_url = null;
        }else{
            
            if($request->input('audio_url')){
                $this->validate($request, [
                    'audio_url' =>'required|url',
                ]);
               $audio_url  = $request->input('audio_url'); 
                $audio_file = null; 
            }else{
                $this->validate($request, [
                    'audio_file' =>'required',
                 ]);
            }
        	
        }
       //dd($request->input('start_date_time'));
        $SubPodcast = SubPodcast::create([
                'sub_name'         => $request->input('name'),
                'podcast_id'       => $podcast_id,
                'audio_file'      => $audio_file,
                'audio_url'      => $audio_url,
                
            ]);            
        $SubPodcast->save();
        return redirect('podcasts/'.$podcast_id.'/SubPodcasts/')->with('success',"Podcast Added successfully"); 
    }

    public function editSubPodcast($podcast_id,$id){

        $SubPodcast = SubPodcast::where('podcast_id',$podcast_id)->findOrFail($id);
        $data = [
            'SubPodcast'            => $SubPodcast,
        ];
        return View('pages.admin.podcast.subpodcast_edit',  compact('SubPodcast'));
    }

    public function updateSubPodcast(Request $request,$podcast_id,$id){
          
        $SubPodcast = SubPodcast::where('podcast_id',$podcast_id)->findOrFail($id);
         $this->validate($request, [
            'name' => 'required',
        ]);
        
        if ($request->hasFile('audio_file')){
		    $image_path =  public_path().'/media/podcast-episodes'.$SubPodcast->audio_file;
		    if (File::exists($image_path)) {
		        File::delete($image_path);
		    }
		    $bannerImage = $request->file('audio_file');
		    $audio_file = $bannerImage->getClientOriginalName();
		    $destinationPath = public_path().'/media/podcast-episodes';
		    $bannerImage->move($destinationPath, $audio_file);
		    $audio_url = null;
		} else {
			if($request->input('audio_url')){
				$audio_url  = $request->input('audio_url'); 
        		$audio_file = null;
			}else{
				$audio_file = $SubPodcast->audio_file;
				$audio_url = null;
			}
		   
		}

        $SubPodcast->sub_name = $request->input('name');
        $SubPodcast->podcast_id = $podcast_id;
        $SubPodcast->audio_file = $audio_file;
        $SubPodcast->audio_url = $audio_url;
        $SubPodcast->save();
       // return redirect()->route('podcast.index')->with('success','Podcast updated successfully');
        return redirect('podcasts/'.$podcast_id.'/SubPodcasts/')->with('success',"Podcast updated successfully"); 
        
    }

     public function deleteSubPodcast($podcast_id,$id){
        SubPodcast::where('podcast_id',$podcast_id)->find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Podcast has been deleted']);
    }

}
