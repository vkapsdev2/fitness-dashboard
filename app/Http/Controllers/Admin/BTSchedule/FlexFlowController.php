<?php

namespace App\Http\Controllers\Admin\BTSchedule;

use Auth;
use Session;
use DB;
use Hash;
use File;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\BTSchedule\FlexFlow;
use Illuminate\Support\Facades\Storage;
class FlexFlowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FlexFlow::with('coach')->get();
        return view('pages.admin.BTSchedule.flexFlow.list',  compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();
       return View('pages.admin.BTSchedule.flexFlow.create', compact('coaches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
       $this->validate($request, [
            'day' =>'required',
            'coach_id' =>'required',
            'start_time' =>'required',
            'end_time' =>'required',
            'banner' =>'required',
        ]);
        if ($files = $request->file('banner')) {
                
           $destinationPath = public_path().'/media/BTSchedule/flexFlow/'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $banner);
        }else{
            $banner= null;
        }

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/BTSchedule/flexFlow-video/'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }   
        }
        $exercise = FlexFlow::create([
            'day'           => $request->input('day'),
            'coach_id'      => $request->input('coach_id'),
            'start_time'    => $request->input('start_time'),
            'end_time'      => $request->input('end_time'),
            'banner'        => $banner,
            'video_file'    => $video_file,
            'video_url'     => trim($video_url),
            'description'   => $request->input('description'),
                    
        ]);            
            $exercise->save();
            return redirect()->route('btSchedule_flex_flow.index')->with('success',"New Exercise Added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exercise = FlexFlow::findOrFail($id);
        return View('pages.admin.BTSchedule.flexFlow.show',  compact('exercise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exercise = FlexFlow::findOrFail($id);
        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();
        return View('pages.admin.BTSchedule.flexFlow.edit',  compact('exercise','coaches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exercise = FlexFlow::findOrFail($id); 
            
         if ($request->hasFile('banner')){
            $image_path = public_path().'/media/BTSchedule/flexFlow/'.$exercise->banner;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName = date('YmdHis') . "." . $bannerImage->getClientOriginalExtension();
            $destinationPath = public_path().'/media/BTSchedule/flexFlow/';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $exercise->banner;
        }

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/BTSchedule/flexFlow-video/'.$exercise->video_file;
            if (File::exists($image_path)) {

                File::delete($image_path);
            }
            $video_file = $request->file('video_file');
            //$video_file = date('YmdHis') . "." . $video_file->getClientOriginalExtension();
            $video_name = $video_file->getClientOriginalName();

            $destinationPath = public_path().'/media/BTSchedule/flexFlow-video/';
            $video_file->move($destinationPath, $video_name);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_name = null;
            }else{
                $video_name = null;
                $video_url = null;
            }
        }


        $exercise->day            = $request->input('day');
        $exercise->coach_id       = $request->input('coach_id');
        $exercise->start_time     = $request->input('start_time');
        $exercise->end_time       = $request->input('end_time');
        $exercise->banner         = $fileName;
        $exercise->video_file     = $video_name;
        $exercise->video_url      = trim($video_url);
        $exercise->description    = $request->input('description');

        $exercise->save();
        return redirect()->route('btSchedule_flex_flow.index')->with('success',"Exercise updated successfully"); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = FlexFlow::find($id);
        $image_path = public_path().'/media/BTSchedule/flexFlow/'.$data->banner;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();
        return response()->json(['success' => true,'message'=> 'Exercise has been deleted']); //
    }

    public function ajaxValidation(Request $request){
        $data = FlexFlow::where('day',$request->day)->where('coach_id',$request->coach_id)->where('start_time',$request->start_time)->first();
           
        return response()->json(['status' => true,'success'=>$data]);

    }
}
