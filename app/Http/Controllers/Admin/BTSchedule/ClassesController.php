<?php

namespace App\Http\Controllers\Admin\BTSchedule;

use Auth;
use Session;
use DB;
use Hash;
use File;
use App\Models\BTSchedule\Classes;
use App\Models\BTSchedule\Exercise;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        $classes = Classes::orderBy('id','DESC')->get();
       //print_r($classes); die;
       return View('pages.admin.BTSchedule.classes.list',  compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return View('pages.admin.BTSchedule.classes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'class_name' => 'required',
        ]);

          $class = Classes::create(
         [
            'class_name'          => $request->input('class_name'),
          ]);
        $class->save();
        return redirect()->route('schedule_classes.index')
                        ->with('success','Schedule Class added successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = Classes::findOrFail($id);
        $data =[
          'class'  => $class,
        ];
        return View('pages.admin.BTSchedule.classes.edit',  compact('class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $class = Classes::find($id);
         $this->validate($request, [
            'class_name'        => 'required',
        ]);

         //print_r($fileName);
         // dd($request->input('name'));
        $class->class_name = $request->input('class_name');
        $class->save();
        return redirect()->route('schedule_classes.index')
                        ->with('success','Schedule Class updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Classes::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Schedule Class has been deleted']);
    }
    
    // Sub Categories module

    public function exerciseList(Request $request, $class_id){
         $exercises = Exercise::where('class_id',$class_id)->orderBy('id','DESC')->get();

            $class = Classes::find($class_id);

           return View('pages.admin.BTSchedule.classes.exercise.list',  compact('exercises','class'));
    }

    public function exerciseCreate(Request $request, $class_id){

        $class = Classes::findOrFail($class_id);
            $data = [
                'class'            => $class, 
            ];
            return View('pages.admin.BTSchedule.classes.exercise.create', compact('class'));
    }

    public function exerciseSave(Request $request, $class_id){
        $class = Classes::findOrFail($class_id);
       
        $this->validate($request, [
            'exercise_name' =>'required',
        ]);
        if ($files = $request->file('video_file')) {
                
           $destinationPath = public_path().'/media/class-exercise-video'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
                 $video_file = null;
                 $video_url = null;
            }
            
        }

        $exercise = Exercise::create([
                    'exercise_name'  => $request->input('exercise_name'),
                    'class_id'       => $class_id,
                    'video_file'     => $video_file,
                    'video_url'      => $video_url,
                    'description'    => $request->input('description')
                    
                ]);            
            $exercise->save();
            return redirect('schedule_classes/'.$class_id.'/class_exercises/')->with('success',"New Exercise Added successfully");

    }

    public function exerciseEdit(Request $request, $class_id,$id){
        $exercise = Exercise::where('class_id',$class_id)->findOrFail($id);
        $data = [
            'exercise'            => $exercise,
        ];
        return View('pages.admin.BTSchedule.classes.exercise.edit',  compact('exercise'));
    }

    public function exerciseUpdate(Request $request, $class_id,$id){

        $exercise = Exercise::where('class_id',$class_id)->findOrFail($id);

        $this->validate($request, [
        'exercise_name' => 'required',
        ]); 
            
        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/class-exercise-video'.$exercise->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/class-exercise-video';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
           
        }

        $exercise->exercise_name = $request->input('exercise_name');
        $exercise->class_id = $class_id;
        $exercise->video_file = $video_file;
        $exercise->video_url = $video_url;
        $exercise->description =  $request->input('description');
        $exercise->save();
        return redirect('schedule_classes/'.$class_id.'/class_exercises/')->with('success',"Exercise updated successfully"); 
    } 

    public function exerciseDelete($class_id,$id){
        Exercise::where('class_id',$class_id)->find($id)->delete();
            return response()->json(['success' => true,'message'=> 'Exercise has been deleted']);
    }

    public function detailView($class_id,$id){
        $exercise = Exercise::where('class_id',$class_id)->find($id);
        return View('pages.admin.BTSchedule.classes.exercise.show',  compact('exercise'));
    }
}
