<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner;

use Auth;
use Session;
use DB;
use Hash; 
use App\User;
use App\Models\MindFlexPlanner\MindFlexRx; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MindFlexRxController extends Controller
{
    public function index()
    {
        $rx = MindFlexRx::with('user')->orderBy('id','DESC')->get();
        return View('pages.admin.mindFlexPlanner.mindflexRx.list',  compact('rx'));
    }

     public function create()
    {   
        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        return View('pages.admin.mindFlexPlanner.mindflexRx.create',compact('customers'));
    }

    public function save(Request $request)
    { 
        $this->validate($request, [
            'customer_id' => 'required',
            'movement' => 'required',
            'mindset' => 'required',
            'nutrition' => 'required',
            'supplimentation' => 'required',

        ]);
         $rx = MindFlexRx::create([
                'customer_id'                => $request->input('customer_id'),
                'movement'                   => $request->input('movement'),
                'mindset'                    => $request->input('mindset'),
                'nutrition'                  => $request->input('nutrition'),
                'supplimentation'            => $request->input('supplimentation'),
                'other'                      => $request->input('other'),
            ]);            
        $rx->save();

        return redirect()->route('mindflex_rx.index')
                        ->with('success','New MindFlex Rx added successfully');

    }

     public function edit($id)
    {
        $rx = MindFlexRx::findOrFail($id);

        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $data = [
            'rx'            => $rx,
            'customers'     > $customers,
        ];
        return View('pages.admin.mindFlexPlanner.mindflexRx.edit',  compact('rx','customers'));
    }

    public function update(Request $request, $id)
    {
         $rx = MindFlexRx::find($id);
        $this->validate($request, [
            'customer_id' => 'required',
            'movement' => 'required',
            'mindset' => 'required',
            'nutrition' => 'required',
            'supplimentation' => 'required',

        ]);

        $rx->customer_id = $request->input('customer_id');
        $rx->movement = $request->input('movement');
        $rx->mindset = $request->input('mindset');
        $rx->nutrition = $request->input('nutrition');
        $rx->supplimentation = $request->input('supplimentation');
        $rx->other = $request->input('other');
        $rx->save();
        return redirect()->route('mindflex_rx.index')
                        ->with('success','MindFlex Rx updated successfully');
    }

    public function delete($id)
    {
        MindFlexRx::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'MindFlex Rx has been deleted']);
    }

    public function show($id){
    	$rx = MindFlexRx::find($id);
       return View('pages.admin.mindFlexPlanner.mindflexRx.detail',  compact('rx'));
    }

    public function ajaxCustomer(Request $request){

        $data = MindFlexRx::where('customer_id',$request->customer_id)->first();
           
        return response()->json(['status' => true,'success'=>$data]);
    }
}
