<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner;

use Auth;
use Session;
use DB;
use Hash; 
use App\Models\MindFlexPlanner\ProgressPhoto; 
use App\User; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgressPhotoController extends Controller
{
    public function index(){

    	//$photos = ProgressPhoto::orderBy('id','DESC')->paginate(5);

      $photos = ProgressPhoto::with('user')->orderBy('id','DESC')->paginate(5); 

      return View('pages.admin.mindFlexPlanner.progress_photos.list',  compact('photos'));
    }

    public function create(){
       $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

    	return View('pages.admin.mindFlexPlanner.progress_photos.create',compact('customers'));
    }

    public function save(Request $request){
    	$this->validate($request, [
                'images' => 'required',
                'images.*' => 'mimes:jpeg,jpg,gif,png'
        ]);
    	 
  		if ($files = $request->file('images')) {
        $i=0;
             $destinationPath = public_path().'/media/progress-photos'; // upload path
              foreach ($files as $file) {
             	  $image = date('YmdHis') .$i. "." . $file->getClientOriginalExtension();
                
            	  $file->move($destinationPath, $image);

            	   	$photos = ProgressPhoto::create(
  		        [
                'image'            => $image,
                'customer_id'      => $request->input('customer_id'),
              ]);
  		        $photos->save();
              $i++;
              }
              
                return redirect()->route('progress_photo.index')->with('success', 'Successfully Save Your Image file');    
      }else{
            $image = null;
             return back()->with('error', 'Photos not uploaded');
      }
    }
    public function delete($id){
      $photos = ProgressPhoto::find($id);
      $image_path = public_path().'/media/progress-photos/'.$photos->image;
      if (File::exists($image_path)) {
          File::delete($image_path);
      }
      ProgressPhoto::find($id)->delete();
      return response()->json(['success' => true,'message'=> 'Photo has been deleted']);
    }
}
