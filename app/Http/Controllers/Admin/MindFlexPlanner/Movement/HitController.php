<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Movement;

use Auth;
use Session;
use DB;
use Hash;
use File;
use App\Models\MindFlexPlanner\Movement\Hit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class HitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hits = Hit::orderBy('id','DESC')->get();
       return View('pages.admin.mindFlexPlanner.movement.hit.list',  compact('hits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexPlanner.movement.hit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'banner' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        // Image upload 
        if ($files = $request->file('banner')) {
           $destinationPath = public_path().'/media/movement-hit-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/movement-hit-video'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
            
        }
  
        $hit = Hit::create([
                'name'                    => $request->input('name'),
                'banner'                  => $banner,
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'description'             => $request->input('description')

                
            ]);            
        $hit->save();

        return redirect()->route('movement_hit.index')
                        ->with('success','New HIT added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailView($id)
    {
       $hit = Hit::findOrFail($id);
       return View('pages.admin.mindFlexPlanner.movement.hit.detail',  compact('hit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $hit = Hit::findOrFail($id);
        $data = [
            'hit'            => $hit,
        ];
        return View('pages.admin.mindFlexPlanner.movement.hit.edit',  compact('hit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hit = Hit::find($id);
         $this->validate($request, [
            'name' => 'required',
        ]);


        if ($request->hasFile('banner')){
            $image_path = public_path().'/media/movement-hit-banner/'.$hit->banner;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/movement-hit-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $hit->banner;
        }

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/movement-hit-video'.$hit->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/movement-hit-video';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        $hit->name = $request->input('name');
        $hit->description = $request->input('description');
        $hit->banner = $fileName;
        $hit->video_file = $video_file;
        $hit->video_url = trim($video_url);
        $hit->save();
        return redirect()->route('movement_hit.index')
                        ->with('success','HIT updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Hit::find($id);

        $image_path = public_path().'/media/movement-hit-banner/'.$data->banner;
        if (($data->image) && (File::exists($image_path))) {
            File::delete($image_path);
        }

        $video_file =  public_path().'/media/movement-hit-video'.$data->video_file;
        if (($data->video_file) && (File::exists($video_file))) {
            File::delete($video_file);
        }
        $data->delete();

        // Hit::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'HIT has been deleted']);
    }
}
