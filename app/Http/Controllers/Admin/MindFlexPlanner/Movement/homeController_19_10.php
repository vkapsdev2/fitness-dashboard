<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Movement;

use Auth;
use Session;
use DB;
use Hash;
use App\Models\MindFlexPlanner\Movement\HomeWorkout;
use App\Models\MindFlexPlanner\Movement\SubHomeWorkout;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use FFMpeg;


class HomeWorkoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        /*$homeworkouts = HomeWorkout::orderBy('id','DESC')->get();
        return View('pages.admin.mindFlexPlanner.movement.homeWorkout.list',compact('homeworkouts'));*/
       /* if(request()->ajax()){

            return datatables()->of(HomeWorkout::latest()->get())
                    ->addColumn('action', function($data){
                        $button ="<a href='' data-toggle='tooltip' data-original-title='Add Exercise' class='btn btn-primary btn-circle'> <i class='fa fa-plus'></i> </a>";
                        $button .= '&nbsp;&nbsp;';
                         $button .= "<a href='' data-toggle='tooltip' data-original-title='Edit' class='btn btn-secondary btn-circle'> <i class='fa fa-pencil-alt'></i> </a>";
                         return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);



                }*/

        return View('pages.admin.mindFlexPlanner.movement.homeWorkout.list');
    } 

    public function ajaxList(Request $request){
        $postData = $request->all();
        $page = (!empty($postData['pagination']['page'])) ? $postData['pagination']['page'] : 1;
        $perpage = (!empty($postData['pagination']['perpage'])) ? $postData['pagination']['perpage'] : 10;
        $sort = (!empty($postData['sort']['sort'])) ? $postData['sort']['sort'] : 'asc';
        $sortField = (!empty($postData['sory']['field'])) ? $postData['sort']['field'] : 'id';

        $data = HomeWorkout::orderBy($sortField ,$sort)->paginate($perpage);

        $meta = [
            'page' => $data->currentPage(),
            'perpage' => $data->perPage(),
            'pages' => $data->lastPage(),
            'page' => $data->currentPage(),
            'total' => $data->total(),
            'sort' => $sort,
            'field' => $sortField

        ];
        
         $lists = [];


        foreach ($data->items() as $home) {

           // <label class="badge badge-success">{{ $v }}</label>
       
            $lists[] = [
                'id' => $home->id,
                'name' => $home->name,
                'banner' => $home->banner,
                'actions' => null
            ];
        }


         return response()->json(['meta' =>$meta,'data'=>$lists]);
    }
    public function create()
    {
        return View('pages.admin.mindFlexPlanner.movement.homeWorkout.create');
    }
    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'banner' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($files = $request->file('banner')) {
           $destinationPath = public_path().'/media/movement-homeworkout-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }
  
        $homeWorkout = HomeWorkout::create([
                'name'                    => $request->input('name'),
                'banner'                  => $banner,

                
            ]);            
        $homeWorkout->save();

        return redirect()->route('movement_home_workout.index')
                        ->with('success','New Home Workout added successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $homeWorkout = HomeWorkout::findOrFail($id);
        $data = [
            'homeWorkout'            => $homeWorkout,
        ];
        return View('pages.admin.mindFlexPlanner.movement.homeWorkout.edit',  compact('homeWorkout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         
        $homeWorkout = HomeWorkout::find($id);
         $this->validate($request, [
            'name' => 'required',
        ]);
        
        if ($request->hasFile('banner')){
            $image_path = public_path().'/media/movement-homeworkout-banner/'.$homeWorkout->banner;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/movement-homeworkout-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $homeWorkout->banner;
        }
         //print_r($fileName);
         // dd($request->input('name'));
        $homeWorkout->name = $request->input('name');
        $homeWorkout->banner = $fileName;
        $homeWorkout->save();
        return redirect()->route('movement_home_workout.index')
                        ->with('success','Home Workout updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        HomeWorkout::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Home Workout has been deleted']);
    }

    /*===== Sub Category Module Start ===================*/

        public function subHomeWorkoutList(Request $request, $homeWorkout_id){
            
            $subHomeWorkouts = SubHomeWorkout::where('homeWorkout_id',$homeWorkout_id)->orderBy('id','DESC')->get();

            $homeWorkout = HomeWorkout::find($homeWorkout_id);

           return View('pages.admin.mindFlexPlanner.movement.homeWorkout.subHomeWorkout_list',  compact('subHomeWorkouts','homeWorkout'));
        }

        public function createSubHomeWorkout(Request $request, $homeWorkout_id)
        {
            $homeWorkout = HomeWorkout::findOrFail($homeWorkout_id);
            $data = [
                'homeWorkout'            => $homeWorkout, 
            ];
            return View('pages.admin.mindFlexPlanner.movement.homeWorkout.subHomeWorkout_create', compact('homeWorkout'));
        }

        public function saveSubHomeWorkout(Request $request,$homeWorkout_id)
        { 

           // $id   =  $request->input('id');
            $homeWorkout = HomeWorkout::findOrFail($homeWorkout_id);
           
                $this->validate($request, [
                    'name' =>'required',
                ]);

            if ($files = $request->file('video_file')) {
                $this->validate($request, [
                    'video_file' =>'required|mimes:mp4,mov,ogg,qt|max:20000',
                ]);
               $destinationPath = public_path().'/media/movement-homeworkout-video'; // upload path

               $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

               $files->move($destinationPath, $video_file);
               $video_url = null;
            }else{
                
                if($request->input('video_url')){
                    $this->validate($request, [
                        'video_url' =>'required|url',
                    ]);
                   $video_url  = $request->input('video_url'); 
                    $video_file = null; 
                }else{
                    $this->validate($request, [
                        'video_file' =>'required|mimes:mp4,mov,ogg,qt|max:20000',
                     ]);
                }
                
            }

            if ($files = $request->file('banner')) {
               $destinationPath = public_path().'/media/movement-homeworkout-banner'; // upload path

               $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
               $files->move($destinationPath, $banner);
            }

           //dd($request->input('start_date_time'));
            $subHomeWorkout = SubHomeWorkout::create([
                    'sub_name'       => $request->input('name'),
                    'homeWorkout_id' => $homeWorkout_id,
                    'video_file'     => $video_file,
                    'video_url'      => trim($video_url),
                    'sub_banner'      => $banner,
                    'workout_timing' => $request->input('workout_timing'),
                    'description'    => $request->input('description')
                    
                ]);            
            $subHomeWorkout->save();
            return redirect('mindflex_planner/movement/home_workout/'.$homeWorkout_id.'/sub_home_workout/')->with('success',"New Workout Added successfully"); 
        }

        public function editSubHomeWorkout($homeWorkout_id,$id){

            $subHomeWorkout = SubHomeWorkout::where('homeWorkout_id',$homeWorkout_id)->findOrFail($id);
            $data = [
                'subHomeWorkout'            => $subHomeWorkout,
            ];
            return View('pages.admin.mindFlexPlanner.movement.homeWorkout.subHomeWorkout_edit',  compact('subHomeWorkout'));
        }

        public function updateSubHomeWorkout(Request $request,$homeWorkout_id,$id){
              
            $subHomeWorkout = SubHomeWorkout::where('homeWorkout_id',$homeWorkout_id)->findOrFail($id);
             $this->validate($request, [
                'name' => 'required',
            ]);
            
            if ($request->hasFile('video_file')){
                $image_path =  public_path().'/media/movement-homeworkout-video'.$subHomeWorkout->video_file;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
                $bannerImage = $request->file('video_file');
                $video_file = $bannerImage->getClientOriginalName();
                $destinationPath = public_path().'/media/movement-homeworkout-video';
                $bannerImage->move($destinationPath, $video_file);
                $video_url = null;
            } else {
                if($request->input('video_url')){
                    $video_url  = $request->input('video_url'); 
                    $video_file = null;
                }else{
                    $video_file = $subHomeWorkout->video_file;
                    $video_url = null;
                }
               
            }

            if ($request->hasFile('banner')){
                $image_path = public_path().'/media/movement-homeworkout-banner/'.$subHomeWorkout->banner;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
                $bannerImage = $request->file('banner');
                $fileName = $bannerImage->getClientOriginalName();
                $destinationPath = public_path().'/media/movement-homeworkout-banner';
                $bannerImage->move($destinationPath, $fileName);
            } else {
                $fileName = $subHomeWorkout->banner;
            }

            $subHomeWorkout->sub_name       = $request->input('name');
            $subHomeWorkout->homeWorkout_id = $homeWorkout_id;
            $subHomeWorkout->video_file     = $video_file;
            $subHomeWorkout->video_url      = trim($video_url);
            $subHomeWorkout->sub_banner     = $fileName;
            $subHomeWorkout->workout_timing = $request->input('workout_timing');
            $subHomeWorkout->description    = $request->input('description');
            $subHomeWorkout->save();
           // return redirect()->route('podcast.index')->with('success','Podcast updated successfully');
            return redirect('mindflex_planner/movement/home_workout/'.$homeWorkout_id.'/sub_home_workout/')->with('success',"Home Workout updated successfully"); 
            
        }

        public function deleteSubHomeWorkout($homeWorkout_id,$id){
            SubHomeWorkout::where('homeWorkout_id',$homeWorkout_id)->find($id)->delete();
            return response()->json(['success' => true,'message'=> 'Workout has been deleted']);
        }

        public function detailView($homeWorkout_id,$id){
            $subHomeWorkout = SubHomeWorkout::where('homeWorkout_id',$homeWorkout_id)->find($id);
           return View('pages.admin.mindFlexPlanner.movement.homeWorkout.subHomeWorkout_show',  compact('subHomeWorkout'));
        }
    /*===== Sub Category Module end ===================*/
}
