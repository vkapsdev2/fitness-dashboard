<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Movement;

use Auth;
use Session;
use DB;
use Hash;
use File;
use App\Models\MindFlexPlanner\Movement\GymWorkout;
use App\Models\MindFlexPlanner\Movement\SubGymWorkout;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GymWorkoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gymworkouts = GymWorkout::orderBy('id','DESC')->get();
       return View('pages.admin.mindFlexPlanner.movement.gymWorkout.list',  compact('gymworkouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexPlanner.movement.gymWorkout.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'banner' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($files = $request->file('banner')) {
           $destinationPath = public_path().'/media/movement-gymworkout-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }
  
        $gymWorkout = GymWorkout::create([
                'name'                    => $request->input('name'),
                'banner'                  => $banner,

                
            ]);            
        $gymWorkout->save();

        return redirect()->route('movement_gym_workout.index')
                        ->with('success','New Gym Workout added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gymWorkout = GymWorkout::findOrFail($id);
        $data = [
            'gymWorkout'            => $gymWorkout,
        ];
        return View('pages.admin.mindFlexPlanner.movement.gymWorkout.edit',  compact('gymWorkout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gymWorkout = GymWorkout::find($id);
         $this->validate($request, [
            'name' => 'required',
        ]);
        
        if ($request->hasFile('banner')){
            $image_path = public_path().'/media/movement-gymworkout-banner/'.$gymWorkout->banner;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/movement-gymworkout-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $gymWorkout->banner;
        }
         //print_r($fileName);
         // dd($request->input('name'));
        $gymWorkout->name = $request->input('name');
        $gymWorkout->banner = $fileName;
        $gymWorkout->save();
        return redirect()->route('movement_gym_workout.index')
                        ->with('success','Gym Workout updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = GymWorkout::find($id);
        $image_path = public_path().'/media/movement-gymworkout-banner/'.$data->banner;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

        // GymWorkout::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Gym Workout has been deleted']);
    }

     /*===== Sub Category Module Start ===================*/

        public function subGymWorkoutList(Request $request, $gymWorkout_id){
            
            $subGymWorkouts = SubGymWorkout::where('gymWorkout_id',$gymWorkout_id)->orderBy('id','DESC')->get();

            $gymWorkout = GymWorkout::findOrFail($gymWorkout_id);

           return View('pages.admin.mindFlexPlanner.movement.gymWorkout.subGymWorkout_list',  compact('subGymWorkouts','gymWorkout'));
        }

        public function createSubGymWorkout(Request $request, $gymWorkout_id)
        {
            $gymWorkout = GymWorkout::findOrFail($gymWorkout_id);
            $data = [
                'gymWorkout'            => $gymWorkout, 
            ];
            return View('pages.admin.mindFlexPlanner.movement.gymWorkout.subGymWorkout_create', compact('gymWorkout'));
        }

        public function saveSubGymWorkout(Request $request,$gymWorkout_id)
        { 
            $gymWorkout = GymWorkout::findOrFail($gymWorkout_id);
           
                $this->validate($request, [
                    'name' =>'required',
                    'banner' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);

            if ($files = $request->file('banner')) {
               $destinationPath = public_path().'/media/movement-gymworkout-banner'; // upload path

               $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
               $files->move($destinationPath, $banner);
            }

            if ($files = $request->file('video_file')) {
                
               $destinationPath = public_path().'/media/movement-gymworkout-video'; // upload path

               $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

               $files->move($destinationPath, $video_file);
               $video_url = null;
            }else{
                
                if($request->input('video_url')){
                    $this->validate($request, [
                        'video_url' =>'required|url',
                    ]);
                   $video_url  = $request->input('video_url'); 
                    $video_file = null; 
                }else{
                     $video_file = null;
                     $video_url = null;
                }
                
            }

            $subGymWorkout = SubGymWorkout::create([
                    'sub_name'        => $request->input('name'),
                    'workout_timing'  => $request->input('workout_timing'),
                    'gymWorkout_id'   => $gymWorkout_id,
                    'video_file'      => $video_file,
                    'video_url'       => trim($video_url),
                    'sub_banner'      => $banner,
                    'description'     => $request->input('description')
                    
                ]);            
            $subGymWorkout->save();
            return redirect('mindflex_planner/movement/gym_workout/'.$gymWorkout_id.'/sub_gym_workout/')->with('success',"New Workout Added successfully"); 
        }

        public function editSubGymWorkout($gymWorkout_id,$id){

            $subGymWorkout = SubGymWorkout::where('gymWorkout_id',$gymWorkout_id)->findOrFail($id);
            $data = [
                'subGymWorkout'            => $subGymWorkout,
            ];
            return View('pages.admin.mindFlexPlanner.movement.gymWorkout.subGymWorkout_edit',  compact('subGymWorkout'));
        }

        public function updateSubGymWorkout(Request $request,$gymWorkout_id,$id){
              
            $subGymWorkout = SubGymWorkout::where('gymWorkout_id',$gymWorkout_id)->findOrFail($id);
             $this->validate($request, [
                'name' => 'required',
            ]);
            
            if ($request->hasFile('video_file')){
                $image_path =  public_path().'/media/movement-gymworkout-video'.$subGymWorkout->video_file;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
                $bannerImage = $request->file('video_file');
                $video_file = $bannerImage->getClientOriginalName();
                $destinationPath = public_path().'/media/movement-gymworkout-video';
                $bannerImage->move($destinationPath, $video_file);
                $video_url = null;
            } else {
                if($request->input('video_url')){
                    $video_url  = $request->input('video_url'); 
                    $video_file = null;
                }else{
                    $video_file = null;
                    $video_url = null;
                }
               
            }

            if ($request->hasFile('banner')){
                $image_path = public_path().'/media/movement-gymworkout-banner/'.$subGymWorkout->banner;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
                $bannerImage = $request->file('banner');
                $banner = $bannerImage->getClientOriginalName();
                $destinationPath = public_path().'/media/movement-gymworkout-banner';
                $bannerImage->move($destinationPath, $banner);
            } else {
                $banner = $subGymWorkout->banner;
            }

            $subGymWorkout->sub_name = $request->input('name');
            $subGymWorkout->workout_timing = $request->input('workout_timing');
            $subGymWorkout->gymWorkout_id = $gymWorkout_id;
            $subGymWorkout->video_file = $video_file;
            $subGymWorkout->video_url = trim($video_url);
            $subGymWorkout->sub_banner = $banner;
            $subGymWorkout->description =  $request->input('description');
            $subGymWorkout->save();
           // return redirect()->route('podcast.index')->with('success','Podcast updated successfully');
            return redirect('mindflex_planner/movement/gym_workout/'.$gymWorkout_id.'/sub_gym_workout/')->with('success',"Gym Workout updated successfully"); 
            
        }

        public function deleteSubGymWorkout($gymWorkout_id,$id){
            $data = SubGymWorkout::where('gymWorkout_id',$gymWorkout_id)->findOrFail($id);

            $image_path = public_path().'/media/movement-gymworkout-banner/'.$data->banner;
            if (($data->image) && (File::exists($image_path))) {
                File::delete($image_path);
            }

            $video_file =  public_path().'/media/movement-gymworkout-video'.$data->video_file;
            if (($data->video_file) && (File::exists($video_file))) {
                File::delete($video_file);
            }
            $data->delete();

            // SubGymWorkout::where('gymWorkout_id',$gymWorkout_id)->find($id)->delete();
            return response()->json(['success' => true,'message'=> 'Workout has been deleted']);
        }

        public function detailView($gymWorkout_id,$id){
            $subGymWorkout = SubGymWorkout::where('gymWorkout_id',$gymWorkout_id)->findOrFail($id);
           return View('pages.admin.mindFlexPlanner.movement.gymWorkout.subGymWorkout_show',  compact('subGymWorkout'));
        }
    /*===== Sub Category Module end ===================*/
}
