<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Movement;

use Auth;
use Session;
use DB;
use Hash;
use File;
use App\Models\MindFlexPlanner\Movement\FreeWeight;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FreeWeightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $freeweights = FreeWeight::orderBy('id','DESC')->get();
       return View('pages.admin.mindFlexPlanner.movement.freeWeight.list',  compact('freeweights'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexPlanner.movement.freeWeight.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'banner' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        // Image upload 
        if ($files = $request->file('banner')) {
           $destinationPath = public_path().'/media/movement-freeweight-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/movement-freeweight-video'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
            
        }
  
        $freeweight = FreeWeight::create([
                'name'                    => $request->input('name'),
                'banner'                  => $banner,
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'description'             => $request->input('description')

                
            ]);            
        $freeweight->save();

        return redirect()->route('movement_free_weight.index')
                        ->with('success','New Free Weight Workout added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailView($id)
    {
       $freeweight = FreeWeight::findOrFail($id);
       return View('pages.admin.mindFlexPlanner.movement.freeWeight.detail',  compact('freeweight'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $freeweight = FreeWeight::findOrFail($id);
        $data = [
            'freeweight'            => $freeweight,
        ];
        return View('pages.admin.mindFlexPlanner.movement.freeWeight.edit',  compact('freeweight'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $freeweight = FreeWeight::find($id);
         $this->validate($request, [
            'name' => 'required',
        ]);


        if ($request->hasFile('banner')){
            $image_path = public_path().'/media/movement-freeweight-banner/'.$freeweight->banner;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/movement-freeweight-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $freeweight->banner;
        }

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/movement-freeweight-video'.$freeweight->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/movement-freeweight-video';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        $freeweight->name = $request->input('name');
        $freeweight->description = $request->input('description');
        $freeweight->banner = $fileName;
        $freeweight->video_file = $video_file;
        $freeweight->video_url = trim($video_url);
        $freeweight->save();
        return redirect()->route('movement_free_weight.index')
                        ->with('success','Free Weight Workout updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = FreeWeight::find($id);

        $image_path = public_path().'/media/movement-freeweight-banner/'.$data->banner;
        if (($data->image) && (File::exists($image_path))) {
            File::delete($image_path);
        }

        $video_file =  public_path().'/media/movement-freeweight-video'.$data->video_file;
        if (($data->video_file) && (File::exists($video_file))) {
            File::delete($video_file);
        }
        $data->delete();
        
        // FreeWeight::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Free Weight Workout has been deleted']);
    }
}
