<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\MindSet;

use Auth;
use Session;
use DB;
use Hash;
use File;
use App\Models\MindFlexPlanner\MindSet\DailyAffirmation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
 
class DailyAffirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $affirmations = DailyAffirmation::orderBy('id','DESC')->get();
        return View('pages.admin.mindFlexPlanner.mindset.affirmation.list',  compact('affirmations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexPlanner.mindset.affirmation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/mindset-affirmation-video'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
            
        }
  
        $affirmation = DailyAffirmation::create([
                'title'                    => $request->input('title'),
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'description'             => $request->input('description')

                
            ]);            
        $affirmation->save();

        return redirect()->route('daily_affirmation.index')
                        ->with('success','New Daily Affirmation added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailView($id)
    {
        $affirmation = DailyAffirmation::findOrFail($id);
       return View('pages.admin.mindFlexPlanner.mindset.affirmation.detail',  compact('affirmation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $affirmation = DailyAffirmation::findOrFail($id);
        $data = [
            'affirmation'            => $affirmation,
        ];
        return View('pages.admin.mindFlexPlanner.mindset.affirmation.edit',  compact('affirmation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $affirmation = DailyAffirmation::find($id);
         $this->validate($request, [
            'title' => 'required',
        ]);

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/mindset-affirmation-video'.$affirmation->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindset-affirmation-video';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        $affirmation->title = $request->input('title');
        $affirmation->description = $request->input('description');
        $affirmation->video_file = $video_file;
        $affirmation->video_url = trim($video_url);
        $affirmation->save();
        return redirect()->route('daily_affirmation.index')
                        ->with('success','Daily Affirmation updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = DailyAffirmation::find($id);
        $video_file = public_path().'/media/mindset-affirmation-video'.$data->video_file;
        if (File::exists($video_file)) {
            File::delete($video_file);
        }
        $data->delete();
        
        // DailyAffirmation::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Daily Affirmation has been deleted']);
    }
}
