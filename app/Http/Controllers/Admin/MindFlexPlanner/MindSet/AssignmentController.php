<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\MindSet;

use Auth;
use Session;
use DB;
use Hash;
use File;
use App\User;
use App\Models\MindFlexPlanner\MindSet\Assignment;
use App\Models\MindFlexPlanner\Movement\HomeWorkout;
use App\Models\MindFlexPlanner\Movement\SubHomeWorkout;
use App\Models\MindFlexPlanner\Movement\GymWorkout;
use App\Models\MindFlexPlanner\Movement\SubGymWorkout;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignments = Assignment::orderBy('id','DESC')->get();
        $ass_arr = $assignments->toArray();
        $apps = Assignment::with('user')->get(); 
        /* $data_arr = array();
  
        foreach ($ass_arr as  $value) {
            $workout_category = $value['workout_category'];
            $customer_id = $value['customer_id'];
            $exercise_id = $value['exercises_id'];
            $id = $value['id'];

            $users = User::where('id',$customer_id)->get();

            if($workout_category == '1'){ 
               $exercise = SubHomeWorkout::whereIn('id',$exercise_id)->get();

                $data = Assignment::where('id',$id)->with('homeWorkout')->get();
 

                $res = array_merge($data->toArray(), [$users->toArray(),$exercise->toArray()]);
                
            }
            else{
                $exercise = SubGymWorkout::whereIn('id',$exercise_id)->get();

               $data = Assignment::where('id',$id)->with('gymWorkout')->get();

               $res = array_merge($data->toArray(), [$users->toArray(),$exercise->toArray()]);
            }           
             array_push($data_arr, $res);
        }*/
        //print_r($apps->toArray());dd('df');
        return View('pages.admin.mindFlexPlanner.mindset.assignment.list',  compact('apps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() 
    {
        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        return View('pages.admin.mindFlexPlanner.mindset.assignment.create',compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required',
            'workout_category' => 'required',

        ]);


        $monday_reps = $request->input('monday_reps');
        $monday_weight = $request->input('monday_weight');
        $monday_sets = array();
        $monday_sets_arr = array();

        for ($i=0; $i < count($request->input('monday_weight')) ; $i++) { 
            $monday_sets['monday_weight'] = $monday_weight[$i];
            $monday_sets['monday_reps'] = $monday_reps[$i];

            array_push($monday_sets_arr, $monday_sets);
            
        }
       
        $tuesday_reps = $request->input('tuesday_reps');
        $tuesday_weight = $request->input('tuesday_weight');
        $tuesday_sets = array();
        $tuesday_sets_arr = array();

        for ($i=0; $i < count($request->input('tuesday_weight')) ; $i++) { 
            $tuesday_sets['tuesday_weight'] = $tuesday_weight[$i];
            $tuesday_sets['tuesday_reps'] = $tuesday_reps[$i];

            array_push($tuesday_sets_arr, $tuesday_sets);
            
        }

        $wednesday_reps = $request->input('wednesday_reps');
        $wednesday_weight = $request->input('wednesday_weight');
        $wednesday_sets = array();
        $wednesday_sets_arr = array();

        for ($i=0; $i < count($request->input('wednesday_weight')) ; $i++) { 
            $wednesday_sets['wednesday_weight'] = $wednesday_weight[$i];
            $wednesday_sets['wednesday_reps'] = $wednesday_reps[$i];

            array_push($wednesday_sets_arr, $wednesday_sets);
            
        }

        $thursday_reps = $request->input('thursday_reps');
        $thursday_weight = $request->input('thursday_weight');
        $thursday_sets = array();
        $thursday_sets_arr = array();

        for ($i=0; $i < count($request->input('thursday_weight')) ; $i++) { 
            $thursday_sets['thursday_weight'] = $thursday_weight[$i];
            $thursday_sets['thursday_reps'] = $thursday_reps[$i];

            array_push($thursday_sets_arr, $thursday_sets);
            
        }

        $friday_reps = $request->input('friday_reps');
        $friday_weight = $request->input('friday_weight');
        $friday_sets = array();
        $friday_sets_arr = array();

        for ($i=0; $i < count($request->input('friday_weight')) ; $i++) { 
            $friday_sets['friday_weight'] = $friday_weight[$i];
            $friday_sets['friday_reps'] = $friday_reps[$i];

            array_push($friday_sets_arr, $friday_sets);
            
        }

        $saturday_reps = $request->input('saturday_reps');
        $saturday_weight = $request->input('saturday_weight');
        $saturday_sets = array();
        $saturday_sets_arr = array();

        for ($i=0; $i < count($request->input('saturday_weight')) ; $i++) { 
            $saturday_sets['saturday_weight'] = $saturday_weight[$i];
            $saturday_sets['saturday_reps'] = $saturday_reps[$i];

            array_push($saturday_sets_arr, $saturday_sets);
            
        }

        $sunday_reps = $request->input('sunday_reps');
        $sunday_weight = $request->input('sunday_weight');
        $sunday_sets = array();
        $sunday_sets_arr = array();

        for ($i=0; $i < count($request->input('sunday_weight')) ; $i++) { 
            $sunday_sets['sunday_weight'] = $sunday_weight[$i];
            $sunday_sets['sunday_reps'] = $sunday_reps[$i];

            array_push($sunday_sets_arr, $sunday_sets);
            
        }

        $final_data['monday_sets']      = $monday_sets_arr;
        $final_data['monday_exercises'] = $request->input('monday_exercises'); 

        $final_data['tuesday_sets']      = $tuesday_sets_arr;
        $final_data['tuesday_exercises'] = $request->input('tuesday_exercises');

        $final_data['wednesday_sets']      = $wednesday_sets_arr;
        $final_data['wednesday_exercises'] = $request->input('wednesday_exercises');

        $final_data['thursday_sets']      = $thursday_sets_arr;
        $final_data['thursday_exercises'] = $request->input('thursday_exercises');

        $final_data['friday_sets']      = $friday_sets_arr;
        $final_data['friday_exercises'] = $request->input('friday_exercises');

        $final_data['saturday_sets']      = $saturday_sets_arr;
        $final_data['saturday_exercises'] = $request->input('saturday_exercises');

        $final_data['sunday_sets']      = $sunday_sets_arr;
        $final_data['sunday_exercises'] = $request->input('sunday_exercises');
        //print_r(json_encode($final_data)); die;
        $assignment = Assignment::create([
            'customer_id'            => $request->input('customer_id'),
            'workout_category'       => $request->input('workout_category'),
            'workout_id'             => $request->input('workout_list'),
            'exercises_id'           => $request->input('workout_exercise'),
            'week'                   => $request->input('week'),
            'rest_timing'            => $request->input('rest_timing'),
            'data'                   => json_encode($final_data),
            ]);            
        $assignment->save();

        return redirect()->route('assignment.index')
                        ->with('success','New Assignment added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assignment = Assignment::findOrFail($id);
       return View('pages.admin.mindFlexPlanner.mindset.assignment.detail',  compact('assignment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        echo "Under Working";
        die;

        $assignment    = Assignment::findOrFail($id);
        
        $home_workouts = HomeWorkout::orderBy('id','DESC')->get();
        $gym_workouts  = GymWorkout::orderBy('id','DESC')->get();

        $home_exercises_list  = SubHomeWorkout::orderBy('id','DESC')->get();
        $gym_exercises_list  = SubGymWorkout::orderBy('id','DESC')->get();

        $customers     = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

       /* if ($assignment->toArray()) {
           $ass_arr = $assignment->toArray();
           $workout_category = $ass_arr['workout_category'];
           $workout_id = $ass_arr['workout_id'];
        }*/

        //Find Excercise list from sub category
        /*if($workout_category == '1'){
            $home_exercises_list = SubHomeWorkout::where('homeWorkout_id',$workout_id)->get();
            $gym_exercises_list = '';
        }elseif ($workout_category == '2') {
             $gym_exercises_list = SubGymWorkout::where('gymWorkout_id',$workout_id)->get();
            $home_exercises_list = '';
        } */
        $data = [
            'assignment'            => $assignment,
            'home_workouts'         => $home_workouts,
            'gym_workouts'          => $gym_workouts,
            'customers'             => $customers,
            'gym_exercises_list'    => $gym_exercises_list,
            'home_exercises_list'   => $home_exercises_list,

        ];

        return View('pages.admin.mindFlexPlanner.mindset.assignment.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // echo "Under Working";
        // die;
        $assignment = Assignment::find($id);

        $monday_reps = $request->input('monday_reps');
        $monday_weight = $request->input('monday_weight');
        $monday_sets = array();
        $monday_sets_arr = array();

        for ($i=0; $i < count($request->input('monday_weight')); $i++) { 
            $monday_sets['monday_weight'] = $monday_weight[$i];
            $monday_sets['monday_reps'] = $monday_reps[$i];

            array_push($monday_sets_arr, $monday_sets);
            
        }
       
        $tuesday_reps = $request->input('tuesday_reps');
        $tuesday_weight = $request->input('tuesday_weight');
        $tuesday_sets = array();
        $tuesday_sets_arr = array();

        for ($i=0; $i < count($request->input('tuesday_weight')); $i++) { 
            $tuesday_sets['tuesday_weight'] = $tuesday_weight[$i];
            $tuesday_sets['tuesday_reps'] = $tuesday_reps[$i];

            array_push($tuesday_sets_arr, $tuesday_sets);
            
        }

        $final_data['monday_sets']      = $monday_sets_arr;
        $final_data['monday_exercises'] = $request->input('monday_exercises'); 

        $final_data['tuesday_sets']      = $tuesday_sets_arr;
        $final_data['tuesday_exercises'] = $request->input('tuesday_exercises');

        // print_r(json_encode($final_data)); die;

        $workout_category = $request->input('workout_category');
        // $days = implode(", ", $request->input('days'));

        $assignment->customer_id          = $request->input('customer_id');
        $assignment->workout_category     = $request->input('workout_category');
        $assignment->workout_id           = $request->input('workout_list');
        $assignment->exercises_id         = $request->input('workout_exercise');
        $assignment->week                 = $request->input('week');
        // $assignment->days                 = $days;
        // $assignment->sets                 = $request->input('sets');
        // $assignment->reps                 = $request->input('reps');
        // $assignment->weight               = $request->input('weight');
        $assignment->rest_timing          = $request->input('rest_timing');
        $assignment->data                 = json_encode($final_data);
        $assignment->save();

        return redirect()->route('assignment.index')
                        ->with('success','Assignment updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Assignment::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Assignment has been deleted']);
    }


    //Ajax Mehtods strat
    public function homeWorkoutList(Request $request){

        $home_workout_list = HomeWorkout::orderBy('id','DESC')->get();
         return response()->json(['status' => true,'success'=>$home_workout_list]);

    }

    public function gymWorkoutList(Request $request){

        $gym_workout_list = GymWorkout::orderBy('id','DESC')->get();
         return response()->json(['status' => true,'success'=>$gym_workout_list]);

    }

    public function homeWorkoutExercise(Request $request){
       
            $exercises_list = SubHomeWorkout::whereIn('homeWorkout_id',$request->input('ids'))->get();
           // return response()->json(['status' => true,'success'=>$exercises_list]);
            return response()->json(['status' => true,'success'=>$exercises_list]);
        
    }
    public function gymWorkoutExercise(Request $request){
        
            $exercises_list = SubGymWorkout::whereIn('gymWorkout_id',$request->input('ids'))->get();
            return response()->json(['status' => true,'success'=>$exercises_list]);
       
    }


}
