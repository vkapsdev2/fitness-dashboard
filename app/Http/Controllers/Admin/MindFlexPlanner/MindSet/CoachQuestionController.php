<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\MindSet;


use Auth;
use Session;
use DB;
use Hash;
use File;
use App\Models\MindFlexPlanner\MindSet\CoachQuestion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class CoachQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = CoachQuestion::with('coach')->orderBy('id','DESC')->get();
         //print_r($questions->toArray());dd('g');
        return View('pages.admin.mindFlexPlanner.mindset.coachesquestion.list',  compact('questions'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();

        return View('pages.admin.mindFlexPlanner.mindset.coachesquestion.create',compact('coaches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',
            'coach_id' => 'required',

            ]);
         $question = CoachQuestion::create([
                'question'                    => $request->input('question'),
                'coach_id'                    => $request->input('coach_id'),
            ]);            
        $question->save();

        return redirect()->route('question_coach.index')
                        ->with('success','New Question added successfully');


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();

        $question = CoachQuestion::findOrFail($id);
        $data = [
            'question'            => $question,
            'coaches'            => $coaches,
        ];
        return View('pages.admin.mindFlexPlanner.mindset.coachesquestion.edit',  compact('question','coaches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $question = CoachQuestion::find($id);
         $this->validate($request, [
            'question' => 'required',
            'coach_id' => 'required',
        ]);

        $question->question = $request->input('question');
        $question->coach_id = $request->input('coach_id');
        $question->save();
        return redirect()->route('question_coach.index')
                        ->with('success','Question updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        CoachQuestion::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Question has been deleted']);
    }
}
