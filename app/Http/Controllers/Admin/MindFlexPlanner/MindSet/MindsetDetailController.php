<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\MindSet;


use Auth;
use Session;
use DB;
use Hash;
use File;
use App\User;
use App\Models\MindFlexPlanner\MindSet\MindsetDetail;
use App\Models\MindFlexPlanner\MindSet\MindsetResource;
use App\Models\MindFlexPlanner\MindSet\MindsetLesson;
use App\Models\MindFlexPlanner\MindSet\MindsetLessonFile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 

class MindsetDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.admin.mindFlexPlanner.mindset.mindsetDetail.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function fileList(Request $request, $id){
        $rows = MindsetResource::where('mindset_detail_id',$id)->get();
        return view('pages.admin.mindFlexPlanner.mindset.mindsetDetail.file_list',compact('rows','id'));
    }
    public function create()
    {
        return view('pages.admin.mindFlexPlanner.mindset.mindsetDetail.create');
    }

    public function addFile(Request $request, $id){

        $row = MindsetDetail::find($id);
        return view('pages.admin.mindFlexPlanner.mindset.mindsetDetail.file_create',  compact('row'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
                'banner' => 'required',
                'banner.*' => 'mimes:jpeg,jpg,gif,png'
        ]);
         
        if ($files = $request->file('banner')) {
       ;
             $destinationPath = public_path().'/media/mindset-detail-banner'; // upload path
              
                  $banner = date('YmdHis'). "." . $files->getClientOriginalExtension();
                
                  $files->move($destinationPath, $banner);

                    $data = MindsetDetail::create(
                [
                'banner'            => $banner,
                'title'             => $request->input('title'),
                'description'       => $request->input('description'),
              ]);
                $data->save();
              
              
                return redirect()->route('mindset_detail.index');    
      }else{
            $banner = null;
             return back()->with('error', 'Photos not uploaded');
      }
    }

   
   public function saveFile(Request $request,$id)
   {
        //dd($id);
         // upload vidoe file or add url
        if ($files = $request->file('audio_file')) {

           $destinationPath = public_path().'/media/mindset-detail-video'; // upload path

           $audio_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $audio_file);
           $audio_url = null;
        }else{
            if($request->input('audio_url')){
                $this->validate($request, [
                    'audio_url' =>'required|url',
                ]);
               $audio_url  = $request->input('audio_url'); 
                $audio_file = null; 
            }else{
               $audio_file = null;
               $audio_url = null;
            }
            
        }

        if ($files = $request->file('pdf_file')) {
            $destinationPath = public_path().'/media/mindset-detail-pdf'; // upload path

           $pdf_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $pdf_file);
        }else{
            $pdf_file = null;
        }

  
        $data = MindsetResource::create([
                'mindset_detail_id'       => $id,
                'audio_file'              => $audio_file,
                'audio_url'               => $audio_url,
                'pdf_file'                => $pdf_file,
            ]);            
        $data->save();

        return redirect()->route('mindset_detail_file.list',[$id]);
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MindsetDetail::findOrFail($id);
       
       return View('pages.admin.mindFlexPlanner.mindset.mindsetDetail.detail',  compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MindsetDetail::findOrFail($id);

        return View('pages.admin.mindFlexPlanner.mindset.mindsetDetail.edit',  compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = MindsetDetail::find($id);


        if ($request->hasFile('banner')){ 
            $image_path = public_path().'/media/mindset-detail-banner/'.$data->banner;
          
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName =  date('YmdHis'). "." . $bannerImage->getClientOriginalExtension();
            $destinationPath = public_path().'/media/mindset-detail-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $data->banner;
        }

        $data->banner = $fileName;
        $data->description = $request->input('description');
        $data->title = $request->input('title');
        $data->save();
        
        return redirect()->route('mindset_detail.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteFile($id)
    {
        MindsetResource::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'File Delete successfully!']);
    }


    // Lesson Module Start
    public function lessonList(){
        $rows = MindsetLesson::get();

        return View('pages.admin.mindFlexPlanner.mindset.mindsetDetail.lesson.list',  compact('rows'));
    }

    public function lessonCreate(){
        return View('pages.admin.mindFlexPlanner.mindset.mindsetDetail.lesson.create');
    }

    public function lessonSave(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'banner' => 'required',
            'banner.*' => 'mimes:jpeg,jpg,gif,png'
        ]);

        if ($files = $request->file('banner')) {
            $destinationPath = public_path().'/media/mindset-lesson'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $banner);
        }else{
            $banner = null;
        }

  
        $data = MindsetLesson::create([
                'name'                => $request->input('name'),
                'banner'              => $banner,
                'description'         => $request->input('description')
            ]);            
        $data->save();

        return redirect()->route('mindset_detail_lesson.list')->with('success','New lesson added successfully');
    }

    public function leassonEdit($id){

        $data = MindsetLesson::findOrFail($id);
        return View('pages.admin.mindFlexPlanner.mindset.mindsetDetail.lesson.edit',  compact('data'));
    }

    public function lessonUpdate(Request $request, $id){

         $data = MindsetLesson::find($id);
         $this->validate($request, [
            'name' => 'required',
            'banner.*' => 'mimes:jpeg,jpg,gif,png'
           
        ]);
        if ($request->hasFile('banner')){ 
            $image_path = public_path().'/media/mindset-lesson/'.$data->banner;
          
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName =  date('YmdHis'). "." . $bannerImage->getClientOriginalExtension();
            $destinationPath = public_path().'/media/mindset-lesson';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $data->banner;
        }

         
        $data->name = $request->input('name');
        $data->description = $request->input('description');
        $data->banner = $fileName;
        $data->save();
        return redirect()->route('mindset_detail_lesson.list')->with('success','lesson updated successfully');
    }

    public function lessonDelete($id)
    {
        MindsetLesson::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Lesson Delete successfully!']);
    }


    public function lessonFileList($id){
        $rows = MindsetLessonFile::where('lesson_id',$id)->get();

        return View('pages.admin.mindFlexPlanner.mindset.mindsetDetail.lesson.file_list',  compact('rows','id'));
    }

    public function lessonFileCreate($id){

        return View('pages.admin.mindFlexPlanner.mindset.mindsetDetail.lesson.file_create',compact('id'));
    }

    public function lessonFileSave(Request $request,$id){

        if ($files = $request->file('audio_file')) {

           $destinationPath = public_path().'/media/mindset-lesson-audio'; // upload path

           $audio_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $audio_file);
           $audio_url = null;
        }else{
            if($request->input('audio_url')){
                $this->validate($request, [
                    'audio_url' =>'required|url',
                ]);
               $audio_url  = $request->input('audio_url'); 
                $audio_file = null; 
            }else{
               $audio_file = null;
               $audio_url = null;
            }
            
        }

        if ($files = $request->file('pdf_file')) {
            $destinationPath = public_path().'/media/mindset-lesson-pdf'; // upload path

           $pdf_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $pdf_file);
        }else{
            $pdf_file = null;
        }

  
          $data = MindsetLessonFile::create([
                'lesson_id'               => $id,
                'audio_file'              => $audio_file,
                'audio_url'               => $audio_url,
                'pdf_file'                => $pdf_file,
            ]);            
        $data->save();

        return redirect()->route('mindset_detail_lesson_file.list',[$id])->with('success','lesson file added successfully');
    }

    public function lessonDeletefile($id)
    {
        MindsetLessonFile::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'File Delete successfully!']);
    }

}
