<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Nutrition;

use Auth;
use Session;
use DB;
use Hash; 
use App\Models\MindFlexPlanner\Nutrition\SuggestedMeal;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SuggestedMealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suggestedMeals = SuggestedMeal::orderBy('id','DESC')->paginate(10);
        return View('pages.admin.mindFlexPlanner.nutrition.suggestedMeal.list',  compact('suggestedMeals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexPlanner.nutrition.suggestedMeal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            //'title'         => 'required',
            'Food_name'     => 'required',
            'meal_type'     => 'required',
            'calorie'       => 'required',
            'fats'          => 'required',
            'carbohydrate'  => 'required',
            'protein'       => 'required',
        ]);

        $suggestedMeal = SuggestedMeal::create([
            //'title'                    => $request->input('title'),
            'Food_name'                => $request->input('Food_name'), 
            'meal_type'                => $request->input('meal_type'), 
            'calorie'                  => $request->input('calorie'), 
            'fats'                     => $request->input('fats'), 
            'carbohydrate'             => $request->input('carbohydrate'), 
            'protein'                  => $request->input('protein'), 
            'description'              => $request->input('description') 
            ]);            
        $suggestedMeal->save();

        return redirect()->route('suggested_meal.index')
                        ->with('success','New Suggest Meal added successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suggestedMeal = SuggestedMeal::findOrFail($id);
        $data = [
            'suggestedMeal'            => $suggestedMeal,
        ];
        return View('pages.admin.mindFlexPlanner.nutrition.suggestedMeal.edit',  compact('suggestedMeal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $suggestedMeal = SuggestedMeal::find($id);

        $this->validate($request, [
            'Food_name'     => 'required',
            'meal_type'     => 'required',
            'calorie'       => 'required',
            'fats'          => 'required',
            'carbohydrate'  => 'required',
            'protein'       => 'required',
        ]);

        $suggestedMeal->Food_name       = $request->input('Food_name');
        $suggestedMeal->meal_type       = $request->input('meal_type');
        $suggestedMeal->calorie         = $request->input('calorie');
        $suggestedMeal->fats            = $request->input('fats');
        $suggestedMeal->carbohydrate    = $request->input('carbohydrate');
        $suggestedMeal->protein         = $request->input('protein');
        $suggestedMeal->description     = $request->input('description');
        $suggestedMeal->save();
        
        return redirect()->route('suggested_meal.index')
                        ->with('success','Suggested Meal updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        SuggestedMeal::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Suggested Meal has been deleted']);
    }
}
