<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Nutrition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;
use Hash; 
use App\Models\MindFlexPlanner\Nutrition\Faq;
use Illuminate\Support\Facades\Validator;


class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::orderBy('id','DESC')->where('faq_status', 1)->get();
        return View('pages.admin.mindFlexPlanner.nutrition.faq.detail',  compact('faqs'));
    }
    public function ListFaq()
    {
    	$faqs = Faq::orderBy('id','DESC')->get();
        return View('pages.admin.mindFlexPlanner.nutrition.faq.list',  compact('faqs'));

    }

    public function create()
    {
        return View('pages.admin.mindFlexPlanner.nutrition.faq.create');
    }
    public function save(Request $request)
    {
    	 $this->validate($request, [
            'faq_title'       => 'required',
            'faq_description' => 'required',
            'faq_status'      => 'required',
            
        ]);
    	 $data = [
            'faq_title'    		=> trim($request->input('faq_title')),
           'faq_description'	=> trim($request->input('faq_description')),
			'faq_status'	    => $request->input('faq_status'),
          ];

    	  $faq = Faq::create($data);
    	  return redirect()->route('faq.index')
                        ->with('success','FAQ added successfully');
         
    }
    public function edit($id)
    {

    	$faq = Faq::findOrFail($id);
    	return View('pages.admin.mindFlexPlanner.nutrition.faq.edit', compact('faq'));
    }
    public function update(Request $request, $id)
    {
    	$Faq = Faq::find($id);
          $this->validate($request, [
            'faq_title'       => 'required',
            'faq_description' => 'required',
            'faq_status'      => 'required',
            
        ]);

          $data = [
            'faq_title'    		=> trim($request->input('faq_title')),
           'faq_description'	=> trim($request->input('faq_description')),
			'faq_status'	    => $request->input('faq_status'),
          ];
          $Faq->update($data);
          return redirect()->route('faq.index')
                        ->with('success','Faq updated successfully'); 
    }
    public function delete($id){

        Faq::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Faq has been deleted']);
    }
}
