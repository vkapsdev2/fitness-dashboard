<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Supplimentation;

use Auth;
use Session;
use DB;
use Hash;
use App\Models\MindFlexPlanner\Supplimentation\ReoccurReminder;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 

class ReoccurReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    { 
        $reminders = ReoccurReminder::with('coach')->with('customer')->orderBy('id','DESC')->get();

        //print_r($reminders->toArray()); dd();
        return view('pages.admin.mindFlexPlanner.supplimentation.reoccurring_reminder.list',  compact('reminders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();

         return view('pages.admin.mindFlexPlanner.supplimentation.reoccurring_reminder.create',  compact('customers','coaches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {      
        $this->validate($request, [
                'coach_id'      => 'required',
                'customer_id'   => 'required',
                'title'         => 'required',
                'days'          => 'required',
                'time'          => 'required',
        ]);

         $days = implode(", ", $request->input('days'));

        $reminder = ReoccurReminder::create([
                'coach_id'      => $request->input('coach_id'),
                'customer_id'   => $request->input('customer_id'),
                'title'         =>$request->input('title'),
                'days'          => $days,
                'time'          => $request->input('time'),
                'repeat_status' => $request->input('repeat_status'),

                
            ]);            
        $reminder->save();

        return redirect()->route('reoccur_reminder.index')
                        ->with('success','New Reminder added successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $reminder = ReoccurReminder::findOrFail($id);
        $customers = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 3); })->orderBy('id','DESC')
        ->get();

        $coaches = DB::table('users')
        ->join('model_has_roles',function($join) {
        $join->on('model_has_roles.model_id' , '=' , 'users.id')
             ->where('model_has_roles.role_id','=', 2); })->orderBy('id','DESC')
        ->get();

        if ($reminder->toArray()) {
           $ass_arr = $reminder->toArray();
            $days_arr = $ass_arr['days'];
            $days = explode(', ', $days_arr);
            
        }

         $data = [
            'reminder'             => $reminder,
            'customers'            => $customers,
            'coaches'              => $coaches,
            'days'                 => $days,
        ];
         return view('pages.admin.mindFlexPlanner.supplimentation.reoccurring_reminder.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reminder = ReoccurReminder::find($id);

        $this->validate($request, [
            'coach_id'      => 'required',
            'customer_id'   => 'required',
            'title'         => 'required',
            'days'          => 'required',
            'time'          => 'required',
        ]);
        $days = implode(", ", $request->input('days'));

        $reminder->coach_id       = $request->input('coach_id');
        $reminder->customer_id    = $request->input('customer_id');
        $reminder->title          = $request->input('title');
        $reminder->time           = $request->input('time');
        $reminder->repeat_status  = $request->input('repeat_status');
        $reminder->days           = $days;
        $reminder->save();

        return redirect()->route('reoccur_reminder.index')
                        ->with('success','Reminder updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
         ReoccurReminder::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Reminder has been deleted']);
    }
}
