<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Supplimentation;

use Auth;
use Session;
use DB;
use Hash;
use App\Models\MindFlexPlanner\Supplimentation\Suppliment;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SupplimentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $suppliments = Suppliment::with('customer')->orderBy('id','DESC')->get();
        return View('pages.admin.mindFlexPlanner.supplimentation.suppliment.list',  compact('suppliments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexPlanner.supplimentation.suppliment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name'       => 'required',
        ]);

        $suppliment = Suppliment::create(
         [
            'name'          => $request->input('name'),
          ]);
        $suppliment->save();
        return redirect()->route('suppliment.index')
                        ->with('success','Supplement added successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $suppliment = Suppliment::findOrFail($id);
        $data = [
            'suppliment'            => $suppliment,
        ];
        return View('pages.admin.mindFlexPlanner.supplimentation.suppliment.edit',  compact('suppliment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $suppliment = Suppliment::find($id);
         $this->validate($request, [
            'name'        => 'required',
        ]);
        
        
         //print_r($fileName);
         // dd($request->input('name'));
        $suppliment->name = $request->input('name');

        $suppliment->save();
        return redirect()->route('suppliment.index')
                        ->with('success','Supplement updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Suppliment::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Supplement has been deleted']);
    }
}
