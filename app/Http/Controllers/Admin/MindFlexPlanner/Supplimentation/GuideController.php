<?php

namespace App\Http\Controllers\Admin\MindFlexPlanner\Supplimentation;

use Auth;
use Session;
use DB;
use Hash;
use App\Models\MindFlexPlanner\Supplimentation\Guide;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GuideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $guides = Guide::orderBy('id','DESC')->get();
        return View('pages.admin.mindFlexPlanner.supplimentation.guide.list',  compact('guides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.mindFlexPlanner.supplimentation.guide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name'       => 'required',
            'book_img'   =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author_name'=> 'required',
        ]);

        if ($files = $request->file('book_img')) {
           $destinationPath = public_path().'/media/supplimentation-guide-banner'; // upload path

           $banner = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $banner);
        }
        $guide = Guide::create(
         [
            'name'          => $request->input('name'),
            'book_img'      => $banner,
            'author_name'   => $request->input('author_name'),
            'description'   => $request->input('description'),
          ]);
        $guide->save();
        return redirect()->route('guide.index')
                        ->with('success','Guide Book added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $guide = Guide::findOrFail($id);
       return View('pages.admin.mindFlexPlanner.supplimentation.guide.detail',  compact('guide'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $guide = Guide::findOrFail($id);
        $data = [
            'guide'            => $guide,
        ];
        return View('pages.admin.mindFlexPlanner.supplimentation.guide.edit',  compact('guide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guide = Guide::find($id);
         $this->validate($request, [
            'name'        => 'required',
            'author_name' => 'required',
        ]);
        
        if ($request->hasFile('book_img')){
            $image_path = public_path().'/media/supplimentation-guide-banner/'.$guide->book_img;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('book_img');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/supplimentation-guide-banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $guide->book_img;
        }
         //print_r($fileName);
         // dd($request->input('name'));
        $guide->name = $request->input('name');
        $guide->author_name = $request->input('author_name');
        $guide->description = $request->input('description');
        $guide->book_img = $fileName;

        $guide->save();
        return redirect()->route('guide.index')
                        ->with('success','Guide Book updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) 
    {
        $data = Guide::find($id);
        $image_path = public_path().'/media/supplimentation-guide-banner/'.$data->book_img;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

        // Guide::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Guide Book has been deleted']);
    }
}
