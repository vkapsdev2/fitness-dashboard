<?php

namespace App\Http\Controllers\Admin\MindFlexKinetics;

use Auth;
use Session;
use DB; 
use Hash;
use File; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexKinetics\DailyAffirmation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DailyAffirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = DailyAffirmation::orderBy('id','DESC')->get();
        return View('pages.admin.MindFlexKinetics.dailyAffirmation.list',  compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.MindFlexKinetics.dailyAffirmation.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
 
        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/mindFlexKinetics/dailyAffirmation'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
            
        }

        //Upload Breathing image
        if ($files = $request->file('image')) {
           $destinationPath = public_path().'/media/mindFlexKinetics/dailyAffirmation/banner'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }

        $row = DailyAffirmation::create([
                'title'                   => $request->input('title'),
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'image'                   => $fileName,
                'description'             => $request->input('description'),
                'mark_status'             => $request->input('mark_status'),
            ]);           
        $row->save();

        return redirect()->route('kinetics_daily_affirmation.index')
                        ->with('success','New Daily Affirmation added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = DailyAffirmation::findOrFail($id);
        return View('pages.admin.MindFlexKinetics.dailyAffirmation.detail',  compact('row'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = DailyAffirmation::findOrFail($id);
        $data = [
            'row'            => $row,
        ];
        return View('pages.admin.MindFlexKinetics.dailyAffirmation.edit',  compact('row'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DailyAffirmation::find($id);
         $this->validate($request, [
            'title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/mindFlexKinetics/dailyAffirmation'.$data->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/dailyAffirmation';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        if ($request->hasFile('image')){
            $image_path = public_path().'/media/mindFlexKinetics/dailyAffirmation/banner/'.$data->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('image');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/dailyAffirmation/banner/';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $data->image;
        }

        $data->title = $request->input('title');
        $data->description = $request->input('description');
        $data->mark_status = $request->input('mark_status');
        $data->video_file  = $video_file;
        $data->image       = $fileName;
        $data->video_url   = trim($video_url);
        $data->save();
        return redirect()->route('kinetics_daily_affirmation.index')
                        ->with('success','Daily Affirmation updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = DailyAffirmation::find($id);

        $image_path = public_path().'/media/mindFlexKinetics/dailyAffirmation/banner/'.$data->image;
        if (($data->image) && (File::exists($image_path))) {
            File::delete($image_path);
        }

        $video_file =  public_path().'/media/mindFlexKinetics/dailyAffirmation'.$data->video_file;
        if (($data->video_file) && (File::exists($video_file))) {
            File::delete($video_file);
        }
        $data->delete();

        return response()->json(['success' => true,'message'=> 'Daily Affirmation has been deleted']);
    }
}
