<?php

namespace App\Http\Controllers\Admin\MindFlexKinetics\WellnessGuide\Intermediate;

use Auth;
use Session; 
use DB;
use Hash;
use File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexKinetics\WellnessGuide\Intermediate\Mindset;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MindsetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Mindset::orderBy('id','DESC')->get();
        return View('pages.admin.MindFlexKinetics.wellnessGuide.intermediateLevel.mindset.list',  compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.MindFlexKinetics.wellnessGuide.intermediateLevel.mindset.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);

        //Upload  image
        if ($files = $request->file('banner')) {
           $destinationPath = public_path().'/media/mindFlexKinetics/wellnessGuide/intermediate/mindset/banner'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/mindFlexKinetics/wellnessGuide/intermediate/mindset'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
            
        } 
        $row = Mindset::create([
                'title'                   => $request->input('title'),
                'description'             => $request->input('description'),
                'banner'                  => $fileName,
                'video_file'              => $video_file,
                'video_url'               => trim($video_url)
            ]);           
        $row->save();

        return redirect()->route('intermediate_mindset.index')
                        ->with('success','New Intermediate Mindset added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Mindset::findOrFail($id);
        return View('pages.admin.MindFlexKinetics.wellnessGuide.intermediateLevel.mindset.detail',  compact('row'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Mindset::findOrFail($id);
        $data = [
            'row'            => $row,
        ];
        return View('pages.admin.MindFlexKinetics.wellnessGuide.intermediateLevel.mindset.edit',  compact('row'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Mindset::find($id);
         $this->validate($request, [
            'title' => 'required',
        ]);

        if ($request->hasFile('banner')){
            $image_path = public_path().'/media/mindFlexKinetics/wellnessGuide/intermediate/mindset/banner/'.$data->banner;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('banner');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/wellnessGuide/intermediate/mindset/banner';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $data->banner;
        }

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/mindFlexKinetics/wellnessGuide/intermediate/mindset'.$data->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/wellnessGuide/intermediate/mindset';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        $data->title = $request->input('title');
        $data->description = $request->input('description');
        $data->banner = $fileName;
        $data->video_file = $video_file;
        $data->video_url = trim($video_url);
        $data->save();
        return redirect()->route('intermediate_mindset.index')
                        ->with('success','Intermediate Mindset updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Mindset::find($id);

        $image_path = public_path().'/media/mindFlexKinetics/wellnessGuide/intermediate/mindset/banner/'.$data->banner;
        if (($data->image) && (File::exists($image_path))) {
            File::delete($image_path);
        }

        $video_file =  public_path().'/media/mindFlexKinetics/wellnessGuide/intermediate/mindset'.$data->video_file;
        if (($data->video_file) && (File::exists($video_file))) {
            File::delete($video_file);
        }
        $data->delete();
        
        // Mindset::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Intermediate Mindset has been deleted']);
    }
}
