<?php

namespace App\Http\Controllers\Admin\MindFlexKinetics\WellnessGuide\Advance;

use Auth;
use Session; 
use DB;
use Hash;
use File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexKinetics\WellnessGuide\Advance\Nutrition;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NutritionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Nutrition::orderBy('id','DESC')->get();
        return View('pages.admin.MindFlexKinetics.wellnessGuide.advanceLevel.nutrition.list', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.MindFlexKinetics.wellnessGuide.advanceLevel.nutrition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {
            $destinationPath = public_path().'/media/mindFlexKinetics/wellnessGuide/advance/nutrition'; // upload path
            $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $video_file);
            $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
                $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
                $video_file = null;
                $video_url = null;
            }
            
        } 
        $row = Nutrition::create([
            'title'       => $request->input('title'),
            'video_file'  => $video_file,
            'video_url'   => trim($video_url),
            'description' => $request->input('description'),
        ]);           
        $row->save();
        return redirect()->route('advance_nutrition.index')->with('success','New advance Nutrition added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Nutrition::findOrFail($id);
        return View('pages.admin.MindFlexKinetics.wellnessGuide.advanceLevel.nutrition.detail',  compact('row'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Nutrition::findOrFail($id);
        $data = [
            'row' => $row,
        ];
        return View('pages.admin.MindFlexKinetics.wellnessGuide.advanceLevel.nutrition.edit',  compact('row'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Nutrition::find($id);
         $this->validate($request, [
            'title' => 'required',
        ]);

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/mindFlexKinetics/wellnessGuide/advance/nutrition'.$data->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/wellnessGuide/advance/nutrition';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        $data->title = $request->input('title');
        $data->video_file = $video_file;
        $data->video_url = trim($video_url);
        $data->save();
        return redirect()->route('advance_nutrition.index')
                         ->with('success','advance Nutrition updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Movement::find($id);
        $video_file =  public_path().'/media/mindFlexKinetics/wellnessGuide/advance/nutrition'.$data->video_file;
        if (($data->video_file) && (File::exists($video_file))) {
            File::delete($video_file);
        }
        $data->delete();
        Nutrition::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'advance Nutrition has been deleted']);
    }
}
