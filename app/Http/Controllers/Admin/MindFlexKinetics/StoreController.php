<?php

namespace App\Http\Controllers\Admin\MindFlexKinetics;

use Auth;
use Session;
use DB;
use Hash;
use File; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexKinetics\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::orderBy('id','DESC')->get();

        return View('pages.admin.MindFlexKinetics.store.list',  compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.MindFlexKinetics.store.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
           'name'          => 'required',
          // 'actual_price'  => 'required',
           'sales_price'   => 'required',
          // 'total_quantity'=> 'required',
           'buy_now_url'   => 'required|url',
           'image'         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        //Upload Store image
        if ($files = $request->file('image')) {
           $destinationPath = public_path().'/media/mindFlexKinetics/store'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }

        $data = [
            'name'          => trim($request->input('name')),
            'image'         => $fileName,
           // 'actual_price'  => trim($request->input('actual_price')),
            'sales_price'   => trim($request->input('sales_price')),
           // 'total_quantity'=> trim($request->input('total_quantity')),
            'buy_now_url'   => trim($request->input('buy_now_url')),
            'description'   => trim($request->input('description')),
            'store_status'   => trim($request->input('store_status')),
         ];

         $stores = Store::create($data);
         return redirect()->route('kinetics_store.index')
         ->with('success','Item created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = Store::findOrFail($id);
        return view('pages.admin.MindFlexKinetics.store.detail', compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::findOrFail($id);

        return View('pages.admin.MindFlexKinetics.store.edit', compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Store::findOrFail($id);
        $this->validate($request, [
           'name'          => 'required',
           //'actual_price'  => 'required',
           'sales_price'   => 'required',
           //'total_quantity'=> 'required',
           'buy_now_url'   => 'required|url',
           'image'         => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->hasFile('image')){
            $image_path = public_path().'/media/mindFlexKinetics/store/'.$store->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('image');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/store';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $store->image;
        }

        $data = [
            'name'          => trim($request->input('name')),
            'image'         => $fileName,
            //'actual_price'  => trim($request->input('actual_price')),
            'sales_price'   => trim($request->input('sales_price')),
            //'total_quantity'=> trim($request->input('total_quantity')),
            'buy_now_url'   => trim($request->input('buy_now_url')),
            'description'   => trim($request->input('description')),
            'store_status'  => $request->input('store_status'),
         ];
         $store->update($data);
         return redirect()->route('kinetics_store.index')
                        ->with('success','Item updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {  
        $data = Store::find($id);
        $image_path = public_path().'/media/mindFlexKinetics/store/'.$data->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $data->delete();

        // Store::findOrFail($id)->delete();
        return response()->json(['success' => true,'message'=> 'Item has been deleted']);
    }
}
