<?php

namespace App\Http\Controllers\Admin\MindFlexKinetics;

use Auth;
use Session;
use DB;
use Hash;
use File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexKinetics\DailyStretch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DailyStretchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
         $stretches = DailyStretch::orderBy('id','DESC')->get();
        return View('pages.admin.MindFlexKinetics.dailyStretch.list',  compact('stretches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.MindFlexKinetics.dailyStretch.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/mindFlexKinetics/dailyStretch'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
            
        } 

        //Upload Daily Stretch image
        if ($files = $request->file('image')) {
           $destinationPath = public_path().'/media/mindFlexKinetics/dailyStretch/banner'; // upload path

           $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $fileName);
        }else{
            $fileName = "";
        }

        $stretch = DailyStretch::create([
                'title'                   => $request->input('title'),
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'image'                   => $fileName,
                'description'             => $request->input('description')

                
            ]);           
        $stretch->save();

        return redirect()->route('kinetics_daily_stretch.index')
                        ->with('success','New Daily stretch added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stretch = DailyStretch::findOrFail($id);
        return View('pages.admin.MindFlexKinetics.dailyStretch.detail',  compact('stretch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stretch = DailyStretch::findOrFail($id);
        $data = [
            'stretch'            => $stretch,
        ];
        return View('pages.admin.MindFlexKinetics.dailyStretch.edit',  compact('stretch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stretch = DailyStretch::find($id);
         $this->validate($request, [
            'title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/mindFlexKinetics/dailyStretch'.$stretch->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/dailyStretch';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        if ($request->hasFile('image')){
            $image_path = public_path().'/media/mindFlexKinetics/dailyStretch/banner/'.$stretch->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('image');
            $fileName = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/dailyStretch/banner/';
            $bannerImage->move($destinationPath, $fileName);
        } else {
            $fileName = $stretch->image;
        }

        $stretch->title = $request->input('title');
        $stretch->description = $request->input('description');
        $stretch->video_file  = $video_file;
        $stretch->video_url   = trim($video_url);
        $stretch->image       = $fileName;
        $stretch->save();
        return redirect()->route('kinetics_daily_stretch.index')
                        ->with('success','Daily Stretch updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = DailyStretch::find($id);

        $image_path = public_path().'/media/mindFlexKinetics/dailyStretch/banner/'.$data->image;
        if (($data->image) && (File::exists($image_path))) {
            File::delete($image_path);
        }

        $video_file =  public_path().'/media/mindFlexKinetics/dailyStretch'.$data->video_file;
        if (($data->video_file) && (File::exists($video_file))) {
            File::delete($video_file);
        }
        $data->delete();

        // DailyStretch::find($id)->delete();
        return response()->json(['success' => true,'message'=> 'Daily Stretch has been deleted']);
    }
}
