<?php

namespace App\Http\Controllers\Admin\MindFlexKinetics;

use Auth;
use Session;
use DB;
use Hash;
use File; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\MindFlexKinetics\Breathing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BreathingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Breathing::orderBy('id','DESC')->get();
        return View('pages.admin.MindFlexKinetics.breathing.list',  compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages.admin.MindFlexKinetics.breathing.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);
 
        // upload vidoe file or add url
        if ($files = $request->file('video_file')) {

           $destinationPath = public_path().'/media/mindFlexKinetics/breathing'; // upload path

           $video_file = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $video_file);
           $video_url = null;
        }else{
            if($request->input('video_url')){
                $this->validate($request, [
                    'video_url' =>'required|url',
                ]);
               $video_url  = $request->input('video_url'); 
                $video_file = null; 
            }else{
               $video_file = null;
               $video_url = null;
            }
            
        } 
        $row = Breathing::create([
                'title'                   => $request->input('title'),
                'video_file'              => $video_file,
                'video_url'               => trim($video_url),
                'description'             => $request->input('description')
            ]);           
        $row->save();

        return redirect()->route('kinetics_breathing.index')
                        ->with('success','New Breathing added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Breathing::findOrFail($id);
        return View('pages.admin.MindFlexKinetics.breathing.detail',  compact('row'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Breathing::findOrFail($id);
        $data = [
            'row'            => $row,
        ];
        return View('pages.admin.MindFlexKinetics.breathing.edit',  compact('row'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Breathing::find($id);
         $this->validate($request, [
            'title' => 'required',
        ]);

        if ($request->hasFile('video_file')){
            $image_path =  public_path().'/media/mindFlexKinetics/breathing'.$data->video_file;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $bannerImage = $request->file('video_file');
            $video_file = $bannerImage->getClientOriginalName();
            $destinationPath = public_path().'/media/mindFlexKinetics/breathing';
            $bannerImage->move($destinationPath, $video_file);
            $video_url = null;
        } else {
            if($request->input('video_url')){
                $video_url  = $request->input('video_url'); 
                $video_file = null;
            }else{
                $video_file = null;
                $video_url = null;
            }
        }

        $data->title = $request->input('title');
        $data->description = $request->input('description');
        $data->video_file = $video_file;
        $data->video_url = trim($video_url);
        $data->save();
        return redirect()->route('kinetics_breathing.index')
                        ->with('success','Breathing updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Breathing::find($id);
        $video_file =  public_path().'/media/mindFlexKinetics/breathing'.$data->video_file;
        if (File::exists($video_file)) {
            File::delete($video_file);
        }
        $data->delete();
        return response()->json(['success' => true,'message'=> 'Breathing has been deleted']);
    }
}
