<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function editProfile($id)
    { 
        //$id = decrypt($id);
        $admin_user = User::findOrFail($id);
        
        $data = [
            'admin_user' => $admin_user,
        ];
        return view('pages.admin.profile.edit', compact('admin_user'));
        
    } 
    public function updateProfile(Request $req, $id)
    {
       // print_r($req->input());
        //print_r($_FILES);die;
       $user       = User::find($id);
        $this->validate($req, [
            'name'      => 'required',
            'email'     => 'required|email',
            'phone'     => 'required|min:10|numeric',
            'address'   => 'required',
            'state'     => 'required',
            'city'      => 'required',
            'zip'       => 'required|numeric',
            'gender'    => 'required',
         ]);

        if ($req->hasFile('image')){
            $image_path = public_path().'/media/admin-profile/'.$user->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $profileImage = $req->file('image');
            $fileName = $profileImage->getClientOriginalName();
            $destinationPath = public_path().'/media/admin-profile';
            $profileImage->move($destinationPath, $fileName);
        } else {
            $fileName = $user->image;
        }

         $user->name            = $req->input('name');
         $user->email           = $req->input('email');
         $user->gender          = $req->input('gender');
         $user->phone           = $req->input('phone');
         $user->address         = $req->input('address');
         $user->state           = $req->input('state');
         $user->city            = $req->input('city');
         $user->zip             = $req->input('zip');
         $user->facebook_url    = $req->input('facebook_url');
         $user->google_url      = $req->input('google_url');
         $user->twitter_url     = $req->input('twitter_url');
         $user->youtube_url     = $req->input('youtube_url');
         $user->instagram_url   = $req->input('instagram_url');
         $user->image           = $fileName;
         $user->save();
         $admin_user = User::findOrFail($id);
         $data = [
            'admin_user' => $admin_user,
        ];


        

         $req->session()->flash('success','Profile updated successfully');
          // return view('pages.admin.profile.edit',compact('admin_user'));
         return redirect()->route('admin.profile', [Auth::user()->id]);
                         
        
    }

    public function changePassword($id)
    {
        return view('pages.admin.profile.changePaasword');
    }
    public function updatePassword(Request $req)
    {
         $this->validate($req, [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'same:new_password',
            
        ]);

          $user = Auth::user();

         if (!(Hash::check($req->get('old_password'), Auth::user()->password))) {
          // echo "if"; die;
                $req->session()->flash('error','Current password does not match');

            return redirect()->route('admin.changePassword', [Auth::user()->id]);
        }
        else if(strcmp($req->get('old_password'), $req->get('new_password')) == 0)
        {
            //echo "else if"; die;
            $req->session()->flash('error','New Password cannot be same as your current password');

            return redirect()->route('admin.changePassword', [Auth::user()->id]);
        }
        else
       {
            //echo "else"; die;
            $user->password = Hash::make($req->get('new_password'));
            $user->save();
             $req->session()->flash('success','Password updated successfully');

             return redirect()->route('admin.changePassword', [Auth::user()->id]);
            //return view('pages.admin.profile.changePaasword');
       }
       

         
    }
}
