<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Spatie\Permission\Models\Role;
use DB;

class AjaxAuthenticationController extends Controller
{

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

   /* protected function create(array $data)
    {
        return User::create([
            'name' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }*/

   
  public function login(Request $request)
	{
	    $validator = Validator::make($request->all(), [
	       'email' => 'required|email',
	        'password' => 'required',
	    ]);

	    if ($validator->passes()) {
	        if (auth()->attempt(array('email' => $request->input('email'),
	          'password' => $request->input('password')),true))
	        {
	            return response()->json(['success' => true,'error' => []]);
	        }
	        return response()->json([
	        	'success' =>false,
			    'error' => ["These credentials do not match our records.!"]
			]);
	    }

	    return response()->json(['success' => false,'error'=>$validator->errors()->all()]);
	}

	public function register(Request $request)  {   
        $validation = $this->validator($request->all());
        if ($validation->fails())  {  
            return response()->json(['success' => false, 'error' => $validation->errors()->toArray()]);
        }
        else{
        	$data =   $request->all();
        	// print_r($data);
            $user = User::create([
	             'name' => $data['name'],
	            'email' => $data['email'],
	            'password' => Hash::make($data['password']),
	            ]);
            $user->assignRole('user');

            Auth::login($user);
            if (Auth::user()){
                return response()->json(['success' => true,'error' => '']);
            }
        }
    }
}
