{{-- Mixed Widget 1 --}}

<div class="card card-custom bg-gray-100 {{ @$class }}">
    {{-- Header --}}
    
    {{-- Body --}}
    <div class="card-body p-0 position-relative overflow-hidden">
        {{-- Chart --}}
        <div  class="card-rounded-bottom bg-danger" style="height: 200px"></div>

        {{-- Stats --}}
        <div class="card-spacer mt-n25">
            {{-- Row --}}
            <div class="row m-0">

               

                <div class="col bg-light-warning px-6 py-8 rounded-xl mr-7 mb-7">
                    {{ Metronic::getSVG("media/svg/icons/Communication/Group.svg", "svg-icon-3x svg-icon-warning d-block my-2") }}
                    <a href="#" class="text-warning font-weight-bold font-size-h6">

                        Coaches {{$coach}}
                    </a>
                </div>
                 <div class="col bg-light-primary px-6 py-8 rounded-xl mb-7">
                    {{ Metronic::getSVG("media/svg/icons/Communication/Add-user.svg", "svg-icon-3x svg-icon-primary d-block my-2") }}
                    <a href="#" class="text-primary font-weight-bold font-size-h6 mt-2">
                        Customers  {{$customer}}
                    </a>
                </div>
            </div>

            


            {{-- Row --}}
             <div class="row m-0">

               <div class="col bg-light-danger px-6 py-8 rounded-xl mr-7 mb-7">
                   {{ Metronic::getSVG("media/svg/icons/Shopping/Cart3.svg", "svg-icon-3x svg-icon-success d-block my-2") }}
                    <a href="#" class="text-danger font-weight-bold font-size-h6 mt-2">

                      
                       Orders 75
                    </a>
                </div>
                 <div class="col bg-light-success px-6 py-8 rounded-xl mb-7">
                     
                     {{ Metronic::getSVG("media/svg/icons/Shopping/Dollar.svg", "svg-icon-3x svg-icon-danger d-block my-2") }}
                    <a href="#" class="text-success font-weight-bold font-size-h6 mt-2">
                        Revenue $1000
                    </a>
                </div>
            </div>
           
        </div>
    </div>
</div>
