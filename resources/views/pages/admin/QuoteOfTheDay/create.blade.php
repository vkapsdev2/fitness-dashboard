{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Quote Of The Day')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row">  
	<div class="col-lg-12">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Quote Of The Day <i class="mr-2"></i> 
					</h3>
				</div>
				<div class="card-toolbar">
					<a href="{{route ('quote_of_day.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
						<i class="ki ki-long-arrow-back icon-sm"></i>
						Back
					</a>
				</div> 
			</div>
			<div class="card-body">
				<!--begin::Form-->
				<form class="form" id="quote-of-day-add" method="POST" action="{{ route('quote_of_day.save') }}" accept-charset="utf-8">
					@csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Quote Of The Day:</h3>
								<div class="form-group row">
									<label class="col-3">Title <span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="title" value="{{ old('title') }}" placeholder="Enter Title"/>
										@error('title')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Quote of Author <span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="author" value="{{ old('author') }}" placeholder="Enter Title"/>
										@error('author')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Quote Date <span style="color:red">*</span></label>
									<div class="col-9">										<input type="text" class="form-control  kt_datetimepicker_1" id="quote_date" name="quote_date" value="{{ old('quote_date') }}" readonly  placeholder="Quote Date"/>
										<div class="date_ajax_message" style="color:red"></div>
										@error('quote_date')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Description</label>
									<div class="col-9">
										<textarea name="description" class="form-control" data-provide="description" id="kt-ckeditor-3">{{ old('description') }}</textarea>
									</div>
								</div>
							</div>
							<!-- </div> -->
							<div class="col-xl-2"></div>
							<div class="form-group text-center m-t-20">
								<div class="col-xs-12 text-center">
									<button class="btn btn-primary" type="submit" id="kt_blockui_page_default">  {{ __('Create') }}</button>
									<a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
    <script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	$('.kt_datetimepicker_1').datetimepicker({
    		format: "yyyy-mm-dd",
    		startDate: new Date(),
    		todayHighlight: true,
    		autoclose: true,
    		startView: 2,
    		minView: 2,
    		forceParse: 0,
    		pickerPosition: 'bottom-right'
    	});

    	var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		    	ClassicEditor
		    	.create( document.querySelector( '#kt-ckeditor-3' ) )
		    	.then( editor => {
		    		console.log( editor );
		    	} )
		    	.catch( error => {
		    		console.error( error );
		    	} );
		    }
		    return {
		        // public functions
		        init: function() {
		        	demos();
		        }
		    };
		}();
    </script>
    <script type="text/javascript">
    	FormValidation.formValidation(
    		document.getElementById('quote-of-day-add'),
    		{
    			fields: {
    				title: {
    					validators: {
    						notEmpty: {
    							message: 'Title is required'
    						}
    					}
    				},
    				author : {
    					validators: {
    						notEmpty: {
    							message: 'Author Name is required'
    						}
    					}
    				},
    				quote_date: {
    					validators: {
    						notEmpty: {
    							message: 'Quote date is required'
    						},
    						date: {
    							format: 'YYYY-MM-DD',
    							message: 'The value is not a valid date'
    						}
    					}
    				},
    			},

    			plugins: {
    				trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			        },
			    }
		);

    	$("#quote_date").change(function(event){
    		event.preventDefault();
    		var date = $('#quote_date').val();
    		quoteDateValidation(date);
    	});
    	function quoteDateValidation(date){
    		var formData = {
    			date: date,
    		};

    		$.ajax({
    			type: "GET",
    			url: 'https://vkapsprojects.com/pedro/skeleton/quote_of_day/date_ajax',
    			data: formData,
    			dataType: 'json',
    			success: function (data) {
    				console.log(data.success);
    				if(data.success){
    					$("div.date_ajax_message").html('<div>This date already exist, use other date</div>');
    					$("#quote_date").addClass('is-invalid');
    				}else{
    					$("div.date_ajax_message").html('<div></div>');
    					$("#quote_date").removeClass('is-invalid').addClass('is-valid');
    				}

    			},
    			error: function (data) {
    				console.log(data);
    			}
    		});
    	}
    </script>
@endsection
