{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Quote Of The Day')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

    <div class="row"> 
    	<div class="col-lg-12">
    		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
    			<div class="card-header">
    				<div class="card-title">
    					<h3 class="card-label">
    						Quote Of The Day <i class="mr-2"></i>
    					</h3>
    				</div>
    				<div class="card-toolbar">
    					<a href="{{route ('quote_of_day.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
    						<i class="ki ki-long-arrow-back icon-sm"></i>
    						Back
    					</a>
    				</div>
    			</div>
    			<div class="card-body">
    				<!--begin::Form-->
    				<form class="form" id="quote-of-day-edit" method="POST" action="{{ route('quote_of_day.update',[$quote->id]) }}" accept-charset="utf-8" enctype="multipart/form-data">
    					@csrf
    					<div class="row">
    						<div class="col-xl-2"></div>
    						<div class="col-xl-8">
    							<div class="my-5">
    								<h3 class=" text-dark font-weight-bold mb-10">Quote Of The Day:</h3>
    								<div class="form-group row">
    									<label class="col-3">Title<span style="color:red">*</span></label>
    									<div class="col-9">
    										<input class="form-control" type="text" name="title" value="{{$quote->title}}" placeholder="Enter Title" />
    										@error('title')
    										<div class="alert alert-danger">{{ $message }}</div>
    										@enderror
    									</div>
    								</div>
    								<div class="form-group row">
    									<label class="col-3">Quote of Author <span style="color:red">*</span></label>
    									<div class="col-9">
    										<input class="form-control" type="text" name="author" value="{{$quote->author}}" placeholder="Enter Title"/>
    										@error('author')
    										<div class="alert alert-danger">{{ $message }}</div>
    										@enderror
    									</div>
    								</div>
    								<div class="form-group row">
    									<label class="col-3">Quote Date</label>
    									<div class="col-9">
    										<div class="input-group date">
    											<input type="text" class="form-control kt_datetimepicker_1" name="quote_date" value="{{$quote->quote_date}}" />
    											@error('quote_date')
    											<div class="alert alert-danger">{{ $message }}</div>
    											@enderror
    										</div>
    									</div>
    								</div>
    								<div class="form-group row">
    									<label class="col-3">Description</label>
    									<div class="col-9">
    										<textarea name="description"  class="form-control" data-provide="description" id="kt-ckeditor-3">{{$quote->description}}</textarea>
    									</div>
    								</div>

    							</div>
    							<div class="col-xl-2"></div>
    							<div class="form-group text-center m-t-20">
    								<div class="col-xs-12">
    									<button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
    									<a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
    								</div>
    							</div>
    						</div>
    					</div>
    				</form>
    				<!--end::Form-->
    			</div>
    		</div>
    	</div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
    <script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	$('.kt_datetimepicker_1').datetimepicker({
    		format: "yyyy-mm-dd",
    		startDate: new Date(),
    		todayHighlight: true,
    		autoclose: true,
    		startView: 2,
    		minView: 2,
    		forceParse: 0,
    		pickerPosition: 'bottom-right'
    	});
    	var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		    	ClassicEditor
		    	.create( document.querySelector( '#kt-ckeditor-3' ) )
		    	.then( editor => {
		    		console.log( editor );
		    	} )
		    	.catch( error => {
		    		console.error( error );
		    	} );
		    }
		    return {
		        // public functions
		        init: function() {
		        	demos();
		        }
		    };
		}();
    </script>
    <script type="text/javascript">
    	FormValidation.formValidation(
    		document.getElementById('quote-of-day-edit'),
    		{
    			fields: {
    				title: {
    					validators: {
    						notEmpty: {
    							message: 'Title is required'
    						}
    					}
    				},
    				author : {
    					validators: {
    						notEmpty: {
    							message: 'Author Name is required'
    						}
    					}
    				},
    				quote_date: {
    					validators: {
    						notEmpty: {
    							message: 'Event Start date is required'
    						},
    						date: {
    							format: 'YYYY-MM-DD',
    							message: 'The value is not a valid date'
    						}
    					}
    				},
    			},

    			plugins: {
    				trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),

			        }
			    }
		);
    </script>
@endsection
