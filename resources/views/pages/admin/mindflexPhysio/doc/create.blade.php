 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add See A Doc')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							See A Doc <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('physio_doc.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('physio_doc.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="doc-add">
					  @csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">See A Doc Info:</h3>

								<div class="form-group row">
									<label class="col-3">Title<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="title" value="{{ old('title') }}" placeholder="Enter title" onkeyup="removeErrors(this)" />
										@error('title')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Price<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="price" value="{{ old('price') }}" placeholder="Enter price" onkeyup="removeErrors(this)" />
										@error('price')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Therapist<span style="color:red">*</span></label>
									<div class="col-9">
										<select class="form-control form-control-solid" name="therapist_id">
										 	<option value="">Select Therapist</option>
											@foreach ($coaches as $coache)
										 	<option value="{{$coache->id}}">{{$coache->name}}</option>
										 	@endforeach
										 </select>
										@error('therapist_id')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Image<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="file" name="image" value="{{ old('image') }}"/>
										@error('image')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>


								<div class="form-group row">
									<label class="col-3">Booking Date</label>
									<div class="col-9">
											<input type="text" class="form-control kt_datetimepicker_1" id="booking_date"  name="booking_date" value="{{ old('booking_date') }}" readonly/>	
											@error('booking_date')
												<div class="alert alert-danger">{{ $message }}</div>
									  		 @enderror
									</div>
								</div>
								<div class="form-group row ">
									<label class="col-3">Time Slot</label>
									<div class="col-9">
											<input type="text" class="form-control kt_datetimepicker_2"  name="time_slot" value="{{ old('time_slot') }}" readonly/>
											@error('time_slot')
												<div class="alert alert-danger">{{ $message }}</div>
									   		@enderror
									</div>
								</div>

								<div class="form-group row">
								    <label class="col-3">Type</label>
								    <div class="col-9">
										<input type="radio" name="typeVisitSession" value="VISIT" checked="checked"> VISIT
										<br/>
										<input type="radio" name="typeVisitSession" value="SESSION"> SESSION
								    </div>
								</div>

								<div class="form-group row">
								    <label class="col-3">Description</label>
								    <div class="col-9">
								        <textarea name="description" id="kt-ckeditor-3">
								        	{{ old('description') }}
								        </textarea>
								        @error('description')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
								    </div>
								</div>		
							</div>
						</div>
						<div class="col-xl-2"></div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
    </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/video_option.js?sh') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
	<script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
	<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/StartEndDate.min.js?sdg') }}" type="text/javascript"></script>
    <script type="text/javascript">
			$('.kt_datetimepicker_1').datetimepicker({
			format: "yyyy-mm-dd",
			startDate: new Date(),
			todayHighlight: true,
			autoclose: true,
			startView: 2,
			minView: 2,
			forceParse: 0,
			pickerPosition: 'bottom-right'
		});
		$('.kt_datetimepicker_2').datetimepicker({
			format: "hh:ii",
			showMeridian: true,
			todayHighlight: true,
			autoclose: true,
			startView: 1,
			minView: 0,
			maxView: 1,
			forceParse: 0,
			pickerPosition: 'bottom-right'
		});
    </script>

    <script type="text/javascript">
		var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		        ClassicEditor
		            .create( document.querySelector( '#kt-ckeditor-3' ) )
		            .then( editor => {
		                console.log( editor );
		            } )
		            .catch( error => {
		                console.error( error );
		            } );
		    }
		    return {
		        // public functions
		        init: function() {
		            demos();
		        }
		    };
		}();
	</script>
	<script type="text/javascript">
		FormValidation.formValidation(
			document.getElementById('doc-add'),
			{
			  fields: {
				   title: {
				    validators: {
				     notEmpty: {
				      message: 'See A Doc title is required'
				     },
				    }
				   },
				   price: {
				    validators: {
				     notEmpty: {
				      message: 'See A Doc price is required'
				     },
				    }
				   },
				   image: {
				    validators: {
				     notEmpty: {
				      message: 'See A Doc image is required'
				     },
				     file: {
	                    extension: 'jpeg,png,jpg,svg',
	                    message: 'The selected file is not valid'
                        }
				    }
				   },
				},
				   
				plugins: {
				   trigger: new FormValidation.plugins.Trigger(),
				   // Bootstrap Framework Integration
				   bootstrap: new FormValidation.plugins.Bootstrap(),
				   // Validate fields when clicking the Submit button
				   submitButton: new FormValidation.plugins.SubmitButton(),
				            // Submit the form when all fields are valid
				   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
		 	}
		);
	</script>
@endsection
