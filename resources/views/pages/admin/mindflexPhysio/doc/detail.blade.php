 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							See A Doc <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('physio_doc.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="my-5 detail_view">
							<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
							<div class = "detail_img text-center">
								@if ($doc->image)
								<img style= "height:200px;" src="{{ url('/'). '/public/media/mindflexPhysio/doc-image/'.$doc->image}}" />
								@else
								<img style= "height:200px;" src="{{ url('/'). '/public/media/book-banner/Product-Dummy.jpg'}}"/>
								@endif
							</div>
							<div class ="detail_name text-center description_txt_box">
								<div class="row">
									<div class="col plan_title"><p>Title: </p></div>
									<div class="col plan_title"><p><b> {{ $doc->title }} </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Price: </p></div>
									<div class="col plan_title"><p><b> {{ $doc->price }} </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Therapist: </p></div>
									<div class="col plan_title"><p><b> {{ $therapistName }} </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Booking Date: </p></div>
									<div class="col plan_title"><p><b> {{ $doc->booking_date }} </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Time Slot: </p></div>
									<div class="col plan_title"><p><b> {{ $doc->time_slot }} </b></p></div>
								</div>
							</div>
							<div class="detail_description text-center>">
								{{ strip_tags($doc->description) }}
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
.detail_name.text-center.description_txt_box {
    display: grid;
    text-align: center;
    text-align: left !important;
    padding: 10px 15% 10px 30%;
    font-size: 14px;
}
ul.shake_hd_list {
    list-style: none;
    font-weight: 600;
    padding-inline-start: 0px !important;
}
@media only screen and (max-width: 767px){
.detail_name.text-center.description_txt_box {
    padding: 10px 0% 10px 0%;
}
}

</style>