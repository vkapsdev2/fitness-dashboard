 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Article')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Edit Article <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('physio_article.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('physio_article.update',[$article->id]) }}" accept-charset="utf-8" enctype="multipart/form-data" id="edit-article" name="edit-article">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Article Info:</h3>
								<div class="form-group row">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="name" value="{{$article->name}}" placeholder="Enter name"/>
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="file" name="article_img" value="{{public_path().'/media/mindflexPhysio/article/'.$article->article_img}}"/>

										<img style="height: 50px;" src="{{ url('/'). '/public/media/mindflexPhysio/article/'.$article->article_img}}" />
								</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Author Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="author_name" value="{{$article->author_name}}" placeholder="Enter author name"/>
										@error('author_name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
							    <label class="col-3">Description</label>
							    
							    <div class="col-9">
							        <textarea name="description" id="kt-ckeditor-3">
							        	{{ $article->description }}
							        </textarea>

							        @error('description')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
							    </div>
							    </div>
							    <div class="form-group row">
							      <label class="col-3 col-form-label">Status<span style="color:red">*</span></label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="physio_article_status" checked="checked"  value="1"  {{ $article->physio_article_status == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                  <input type="radio" name="physio_article_status" value="0" {{ $article->physio_article_status == "0" ? 'checked' : '' }} />
							                  <span></span>
							                  Deactivate
							              </label>
							          </div>
							           @error('physio_article_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>

							</div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">   {{ __('Update') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
		<!--end::Page Vendors-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	// Class definition

var KTCkeditor = function () {
    // Private functions
    var demos = function () {
        ClassicEditor
            .create( document.querySelector( '#kt-ckeditor-3' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

// Initialization
</script>
<script type="text/javascript">
	FormValidation.formValidation(
 document.getElementById('edit-article'),
 {
  fields: {
   name: {
    validators: {
     notEmpty: {
      message: 'Article name is required'
     },
    }
   },

   author_name: {
    validators: {
     notEmpty: {
      message: 'Author name is required'
     },
    }
   },
   
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);
</script>
@endsection
