 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Article <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('physio_article.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							
								<div class = "mb-4 detail_img text-center">
									@if ($article->article_img)
									<img style= "height:200px;" src="{{ url('/'). '/public/media/mindflexPhysio/article/'.$article->article_img}}" />
									@else
									<img style= "height:200px;" src="{{ url('/'). '/public/media/article-banner/Product-Dummy.jpg'}}"/>
									@endif
								</div>
								<div class="mindflex_detail_box">
								<div class ="row mb-2 plan_title">
									<div class="col plan_title"><span>Title:</span></div>
									<div class="col plan_txt"><span><b>{{$article->name}}</b></span></div>
								</div>
								<div class ="row mb-2 plan_validity_days">
									<div class="col plan_title"><span>Author:</span></div>
									<div class="col plan_txt"><span><b>{{$article->author_name}}</b></span></div>
								</div>
								</div>
								<div class="mindflex_detail_description detail_description">
									<h3 class="badges_sub_hd_txt">Description</h3>
									{!! $article->description !!}
								</div>
							</div>
 						</div>
					</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}
.detail_description {
    text-align: justify;
}
.mindflex_detail_box {
    display: grid;
    padding: 10px 20% 10px 30%;
}
.mindflex_detail_description h3 {
	font-weight: 400;
    opacity: 0.56;
    font-size: 18px !important;
    margin-bottom: 10px;
}
.detail_img{
	overflow: hidden;
}
@media only screen and (max-width: 767px) {
  .mindflex_detail_box {
    padding: 10px 0% 10px 0%;
	}
}
</style>