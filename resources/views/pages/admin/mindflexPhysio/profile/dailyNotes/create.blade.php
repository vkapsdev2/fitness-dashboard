 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Daily Notes')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Daily Notes <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('daily_notes.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form"  method="POST" action="{{ route('daily_notes.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-daily-notes" name="add-daily-notes">
					  @csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Daily Notes Info:</h3>

							<div class="form-group row">
			                    <label class="col-3">Customer Name<span style="color:red">*</span></label>
			                    <div class="col-9">
			                      <select class="form-control" name="customer_id">
			                        <option value= "">Select Customer</option>
			                          @foreach ($customers as $value)
			                            <option value="{{$value->id}}" {{ old('customer_id') == $value->id ? 'selected' : '' }}> {{$value->name}} </option>
			                          @endforeach
			                      </select>
			                      @error('customer_id')
			                        <div class="alert alert-danger">{{ $message }}</div>
			                      @enderror
			                    </div>
			                  </div>

							</div>
								<div class="form-group row">
							    <label class="col-3" >Add your medical history after the therapy<span style="color:red">*</span></label>
							    <div class="col-9">
							        <textarea name="daily_note_detail" class="form-control"  data-provide="daily_note_detail" rows="10">{{old('daily_note_detail')}}</textarea>
									@error('daily_note_detail')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
								</div>
							 </div>
							  <div class="form-group row">
							      <label class="col-3 col-form-label">Status</label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="daily_note_status" checked="checked"  value="1"  {{ old('daily_note_status') == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                  <input type="radio" name="daily_note_status" value="0" {{ old('daily_note_status') == "0" ? 'checked' : '' }} />
							                  <span></span>
							                  Deactivate
							              </label>
							          </div>
							           @error('daily_note_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>

							</div>
							</div>
						</div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit" id="add-book-submit">  {{ __('Create') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</form>
						<!--end::Form-->
				</div>
			</div>
        </div>
	</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom.js?fgf') }}" type="text/javascript"></script>
 
 <script type="text/javascript">
	
FormValidation.formValidation(
 document.getElementById('add-daily-notes'),
 {
  fields: {
  	customer_id: {
    validators: {
     notEmpty: {
      message: 'Customer is required'
     },
    }
   },
   daily_note_detail: {
    validators: {
     notEmpty: {
      message: 'Daily Notes detail is required'
     },
    }
   },
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>
@endsection
