 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Exercise Program')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Exercise Program <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('physio_exercise.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('physio_exercise.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="exercise-add">
					  @csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Exercise Program Info:</h3>

								<div class="form-group row">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Enter name" onkeyup="removeErrors(this)" />
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="file" name="banner" value="{{ old('banner') }}"/>
										@error('banner')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
							        <label class="col-3 col-form-label">Video File Or URL</label>
							        <div class="col-9 col-form-label">
							            <div class="radio-inline">
							                <label class="radio">
							                    <input type="radio" name="video" checked="checked" id="file" value="file" />
							                    <span></span>
							                    Video File
							                </label>
							                <label class="radio">
							                    <input type="radio" name="video" id="url" value="url" />
							                    <span></span>
							                    Video URL
							                </label>
							            </div>
							            <span class="form-text text-muted"></span>
							        </div>
							    </div>

								<div class="form-group row" id="video_file">
									<label class="col-3">Video File</label>
									<div class="col-9">
										<input class="form-control" type="file" name="video_file" value="{{ old('video_file') }}"/>
										@error('video_file')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror 
									</div>
								</div>

								<div class="form-group row" id="video_url" style="display: none;">
									<label class="col-3">Video URL</label>
									<div class="col-9">
										<input class="form-control" type="text" name="video_url" value="{{ old('video_url') }}" placeholder="Enter Video URL" />
										<span class="form-text text-muted">Please enter your Video URL</span>
										@error('video_url')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
								    <label class="col-3">Description</label>
								    <div class="col-9">
								        <textarea name="description" id="kt-ckeditor-3">
								        	{{ old('description') }}
								        </textarea>
								        @error('description')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
								    </div>
								</div>

								<div class="form-group row">
			                      <label class="col-3">Mark as Complete</label>
			                      <div class="col-9">
			                       <input data-switch="true" type="checkbox" name="mark_status" id="test" data-on-color="success"/>
			                       <span class="form-text text-muted"></span>
			                      </div>
			                    </div>

							</div>
						</div>
						<div class="col-xl-2"></div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
    </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/video_option.js?sh') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
	<script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
		var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		        ClassicEditor
		            .create( document.querySelector( '#kt-ckeditor-3' ) )
		            .then( editor => {
		                console.log( editor );
		            } )
		            .catch( error => {
		                console.error( error );
		            } );
		    }
		    return {
		        // public functions
		        init: function() {
		            demos();
		        }
		    };
		}();
	</script>
	<script type="text/javascript">
		FormValidation.formValidation(
			document.getElementById('exercise-add'),
			{
			  fields: {
				   name: {
				    validators: {
				     notEmpty: {
				      message: 'Exercise Program name is required'
				     },
				    }
				   },
				   banner: {
				    validators: {
				     notEmpty: {
				      message: 'Exercise Program image is required'
				     },
				     file: {
	                    extension: 'jpeg,png,jpg,svg',
	                    message: 'The selected file is not valid'
                        }
				    }
				   },
				   video_url: {
				    validators: {
				     uri: {
				      message: 'The Video address is not valid'
				     }
				    }
					},
					video_file: { 
					    validators: {
					        file: {
		                        extension: 'mp4,mov,ogg,qt',
		                        maxSize: 2000*1024,
		                        message: 'The selected file is not valid'
	                        }
					    }
					},
				   },
				   
				plugins: {
				   trigger: new FormValidation.plugins.Trigger(),
				   // Bootstrap Framework Integration
				   bootstrap: new FormValidation.plugins.Bootstrap(),
				   // Validate fields when clicking the Submit button
				   submitButton: new FormValidation.plugins.SubmitButton(),
				            // Submit the form when all fields are valid
				   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
		 	}
		);
	</script>
	<script type="text/javascript">
	    // Bootstrap Switch
	    $('[data-switch=true]').bootstrapSwitch();
	    $('[data-switch=true]').on('switchChange.bootstrapSwitch', function() {
	     // Revalidate field
	     validator.revalidateField('switch');
	    });
	</script>
@endsection
