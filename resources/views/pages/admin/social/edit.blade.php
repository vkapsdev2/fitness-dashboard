 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Social Links')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Edit Social Links <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('social.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('social.update',[$social->id]) }}" accept-charset="utf-8" enctype="multipart/form-data" id="edit-social" name="edit-social">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Social Links Info:</h3>
								<div class="form-group row">
          <label class="col-3">Facebook<span style="color:red">*</span></label>
          <div class="col-lg-9">
            <div class="input-group">
              <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="flaticon2-website"></i>
                  </span>
              </div>
              <input type="text" class="form-control form-control-solid" name="facebook_url" value="{{ $social->facebook_url }}" />
            </div>
            <span class="form-text text-muted">Please enter your Facebook website URL.</span>
            @error('facebook_url')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>

        <div class="form-group row">
          <label class="col-3">Google<span style="color:red">*</span></label>
          <div class="col-lg-9">
            <div class="input-group">
              <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="flaticon2-website"></i>
                  </span>
              </div>
              <input type="text" class="form-control form-control-solid" name="google_url" value="{{ $social->google_url }}" />
            </div>
            <span class="form-text text-muted">Please enter your Google website URL.</span>
            @error('google_url')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>

        <div class="form-group row">
          <label class="col-3">Twitter<span style="color:red">*</span></label>
          <div class="col-lg-9">
            <div class="input-group">
              <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="flaticon2-website"></i>
                  </span>
              </div>
              <input type="text" class="form-control form-control-solid" name="twitter_url" value="{{ $social->twitter_url }}" />
            </div>
            <span class="form-text text-muted">Please enter your Twitter website URL.</span>
            @error('twitter_url')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>

        <div class="form-group row">
          <label class="col-3">YouTube<span style="color:red">*</span></label>
          <div class="col-lg-9">
            <div class="input-group">
              <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="flaticon2-website"></i>
                  </span>
              </div>
              <input type="text" class="form-control form-control-solid" name="youtube_url" value="{{ $social->youtube_url }}" />
            </div>
            <span class="form-text text-muted">Please enter your YouTube website URL.</span>
            @error('youtube_url')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>

        <div class="form-group row">
          <label class="col-3">Instagram<span style="color:red">*</span></label>
          <div class="col-lg-9">
            <div class="input-group">
              <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="flaticon2-website"></i>
                  </span>
              </div>
              <input type="text" class="form-control form-control-solid" name="instagram_url" value="{{ $social->instagram_url }}" />
            </div>
            <span class="form-text text-muted">Please enter your Instagram website URL.</span>
            @error('instagram_url')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
							</div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">   {{ __('Update') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
			</div>
        
 </div>

@endsection
{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script type="text/javascript">
      
    FormValidation.formValidation(
 document.getElementById('edit-social'),
 {
  fields: {
  facebook_url: {
    validators: {
      notEmpty: {
      message: 'Facebook URL is required'
     },
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
   google_url: {
    validators: {
      notEmpty: {
      message: 'Google URL is required'
     },
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
   twitter_url: {
    validators: {
      notEmpty: {
      message: 'Twitter URL is required'
     },
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
   youtube_url: {
    validators: {
      notEmpty: {
      message: 'YouTube URL is required'
     },
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
   Instagram: {
    validators: {
      notEmpty: {
      message: 'Facebook URL is required'
     },
      uri: {
      message: 'The website address is not valid'
     }
    }
  },
 },

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);
</script>
@endsection
