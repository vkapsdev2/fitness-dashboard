 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Social Links')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
           	<div class="card card-custom gutter-b">
				<div class="card-header flex-wrap border-0 pt-6 pb-0">
					<div class="card-title">
						<h3 class="card-label">Social Link
						<span class="d-block text-muted pt-2 font-size-sm">List of Social Link</span></h3>
					</div>	
					 <div class="card-toolbar">
					 
					 </div>
				</div>
				@if ($message = Session::get('success'))
			        <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
				<div class="card-body">
					<!--begin: Datatable -->
					<table class="table table-separate table-head-custom table-checkable" id="socialList" style="text-align: center;">
						<thead>
							<tr>
								<th>#</th>
								<th>FaceBook</th>
								<th>Google</th>
                <!-- <th>Twitter</th>
                <th>YouTube</th> -->
                <th>Instagram</th>
                <th >Action</th>
							</tr>
						</thead>
						<tbody>
							 @foreach ($socials as $social)
							 	<tr>
							 		<td>{{ $loop->iteration }}</td>
									<td>{{ $social->facebook_url }}</td>
                  <td>{{ $social->google_url }}</td>
                  <!-- <td>{{ $social->twitter_url }}</td>
                  <td>{{ $social->youtube_url }}</td> -->
                  <td>{{ $social->instagram_url }}</td>
									
                    
									<td class="text-nowrap">
									 <a href="{{ route('social.edit',[$social->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>

                    <!-- <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelete({{$social->id}})">  <i class="fa fa-times"></i> </a> -->
                  </td>
								</tr>
							 @endforeach
						</tbody>
					</table>
					<!--end: Datatable-->
				</div>
			</div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">


  function comfrimDelete(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this Social Link !", 
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax('https://vkapsprojects.com/pedro/skeleton/public/others/social/delete/'+id, {
          method: 'DELETE',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        }).done(function (r) {
          console.log('result',r); 
          
         if(r.success){
            window.location.reload();
          }else{                
            swal.fire({
            text: 'Social Link does not exist..',
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
              confirmButton: "btn font-weight-bold btn-light-primary"
            }
          }).then(function () {
            KTUtil.scrollTop();
          });            
          }    
        }).fail(function (jqXHR, textStatus, errorThrown) {             
            swal.fire({
            text: errors,
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
              confirmButton: "btn font-weight-bold btn-light-primary"
            }
          }).then(function () {
            KTUtil.scrollTop();
          });    
        });
      }
    });
  }

    </script>
    <script type="text/javascript">
     $('#socialList').dataTable({
        // "scrollX": true,
        // "scrollY": true, 
    });
  </script>
@endsection
