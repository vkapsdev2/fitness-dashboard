{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Event')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row"> 
	<div class="col-lg-12">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Events <i class="mr-2"></i>
					</h3>
				</div>
				<div class="card-toolbar">
					<a href="{{route ('event.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
						<i class="ki ki-long-arrow-back icon-sm"></i>
						Back
					</a>
				</div>
			</div>
			<div class="card-body">
				<!--begin::Form-->
				<form class="form" id="event-edit" method="POST" action="{{ route('event.update',[$event->id]) }}" accept-charset="utf-8" enctype="multipart/form-data">
					@csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Event Info:</h3>
								<div class="form-group row">
									<label class="col-3">Title<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="title" value="{{$event->title}}" placeholder="Enter Title" />
										@error('title')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner</label>
									<div class="col-9">
										<img style="height: 50px;" src="{{ url('/'). '/public/media/event-banner/'.$event->banner_img}}" />
										<input class="form-control" type="file" name="banner_img" value="{{public_path().'/media/event-banner/'.$event->banner_img}}"/>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Event Url<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="event_url" value="{{$event->event_url}}" />
										@error('event_url')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror

									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Start Date<span style="color:red">*</span></label>
									<div class="col-9">
										<div class="input-group date">
											<input type="text" class="form-control kt_datetimepicker_1" name="start_date" value="{{$event->start_date}}" />
											@error('start_date')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Start Time<span style="color:red">*</span></label>
									<div class="col-9">
										<div class="input-group date">
											<input type="text" class="form-control kt_datetimepicker_2" name="start_time" value="{{$event->start_time}}"/>
											@error('start_time')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror

										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">End Date<span style="color:red">*</span></label>
									<div class="col-9">
										<div class="input-group date">
											<input type="text" class="form-control  kt_datetimepicker_1"  name="end_date" value="{{$event->end_date}}" />
											@error('end_date')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>
								</div>
								<div class="form-group row ">
									<label class="col-3">End Time<span style="color:red">*</span></label>
									<div class="col-9">
										<div class="input-group date">
											<input type="text" class="form-control kt_datetimepicker_2"  name="end_time" value="{{$event->end_time}}"  />
											@error('end_time')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Location</label>
									<div class="col-9">
										<input class="form-control" type="text" name="location" value="{{$event->location}}"/>
									</div>
								</div>
							</div>
							<div class="col-xl-2"></div>
							<div class="form-group text-center m-t-20">
								<div class="col-xs-12">
									<button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
									<a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/StartEndDate.min.js?sddxg') }}" type="text/javascript"></script>
    <script type="text/javascript">
			$('.kt_datetimepicker_1').datetimepicker({
			format: "yyyy-mm-dd",
			startDate: new Date(),
			minDate:0,
			todayHighlight: true,
			autoclose: true,
			startView: 2,
			minView: 2,
			forceParse: 0,
			pickerPosition: 'bottom-right'
		});
		$('.kt_datetimepicker_2').datetimepicker({
			format: "hh:ii",
			showMeridian: true,
			todayHighlight: true,
			autoclose: true,
			startView: 1,
			minView: 0,
			maxView: 1,
			forceParse: 0,
			pickerPosition: 'bottom-right'
		});
    </script>
    <script type="text/javascript">
    	FormValidation.formValidation(
			 document.getElementById('event-edit'),
			 {
			  fields: {
			   title: {
			    validators: {
			     notEmpty: {
			      message: 'Event Name is required'
			     }
			    }
			   },
			    banner_img: {
			    validators: {
				    file: {
	                    extension: 'jpeg,png,jpg,gif,svg',
	                    message: 'The selected file is not valid'
                        }
			    }
			   },
			   start_date: {
			    validators: {
				    notEmpty: {
				      message: 'Event Start date is required'
				    },
				    date: {
                        format: 'YYYY-MM-DD',
                        message: 'The value is not a valid date'
                    },
			    }
			   },
			   start_time: {
			    validators: {
				    notEmpty: {
				      message: 'Event Start time is required' 
				    }
			    }
			   },
			   end_date: {
			    validators: {
				    notEmpty: {
				      message: 'Event End date is required'
				    },
				    date: {
                        format: 'YYYY-MM-DD',
                        message: 'The value is not a valid date'
                    },
			    }
			   },
			   end_time: {
			    validators: {
				    notEmpty: {
				      message: 'Event End time is required'
			     	}
			    }
			   },
			   event_url: {
				    validators: {
				    	notEmpty: {
					      message: 'Event URL is required'
					    },
				        uri: {
				      		message: 'The Event address is not valid'
				     	}
				    }
				},
			  },

			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			   startEndDate: new FormValidation.plugins.StartEndDate({
                    format: 'YYYY-MM-DD',
                    startDate: {
                        field: 'start_date',
                        message: 'The start date must be a valid date and ealier than the end date'
                    },
                    endDate: {
                        field: 'end_date',
                        message: 'The end date must be a valid date and later than the start date'
                    },
                }),
			  }
			 }
		);
    </script>
@endsection
