 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Ranking')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}
 
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
          <div class="card-title">
            <h3 class="card-label">Ranking
            <span class="d-block text-muted pt-2 font-size-sm">List of Ranking</span></h3>
          </div> 
        </div>
        <div class="card-body">
          <!--begin: Datatable -->
          <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_cleanse_ranking" style="text-align: center;">
              
            <thead>
              <tr>
                <th>Customer Name</th>
                <th>Image</th>
                <th>Rank</th>
              </tr>
            </thead>
            <tbody>
               @foreach ($data as $row)
                <tr>
                  <td>{{$row->name}}</td>
                  <td>
                    @if($row->image)
                    <img style="height: 40px; width: 80px;" src="{{ url('/'). '/public/media/customer-image/'.$row->image}}" />
                    @else
                    {{'No Image'}}
                    @endif
                  </td>
                     <td>RANK #{{$loop->iteration}}</td>
                </tr>
               @endforeach
            </tbody>
          </table>
          <!--end: Datatable-->
        </div>
      </div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
  <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/pages/custom.js?cvb') }}" type="text/javascript"></script>
@endsection
