 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Suggested Meal')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}
 
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
          <div class="card-title">
            <h3 class="card-label">Nutrition Suggested Meals
            <span class="d-block text-muted pt-2 font-size-sm">List of Suggested Meals</span></h3>
          </div>  
           <div class="card-toolbar">
            <a href="{{ route('cleanse_suggested_meal.create') }}" class="btn btn-primary font-weight-bolder">
              <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <rect x="0" y="0" width="24" height="24"/>
                  <circle fill="#000000" cx="9" cy="15" r="6"/>
                  <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
              </g>
          </svg><!--end::Svg Icon--></span>New Suggest Meal
          </a>
           </div>
        </div>
        @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  {{ $message }}
              </div>
          @endif
          <div class="delete_message"></div>
        <div class="card-body">
          <!--begin: Datatable -->
          <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_suggested_meal" style="text-align: center;">
              
            <thead>
              <tr>
                <th>#</th>
                <th>Meal Type</th>
                <th>Food Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
               @foreach ($suggestedMeals as $suggestedMeal)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$suggestedMeal->meal_type}}</td>
                  <td>{{$suggestedMeal->Food_name}}</td>
                  <td class="text-nowrap"> 

                    <a href="{{ route('cleanse_suggested_meal.edit',[$suggestedMeal->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>

                    <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="cleanseNutritionMealDelete({{$suggestedMeal->id}})">  <i class="fa fa-times"></i> </a>
                  </td>
                </tr>
               @endforeach
            </tbody>
          </table>
          {{ $suggestedMeals->links() }}
          <!--end: Datatable-->
        </div>
      </div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
  <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/pages/custom.js?fghjggf') }}" type="text/javascript"></script>
@endsection
