 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Suggest Meal')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Suggest Meal <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('cleanse_suggested_meal.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" id="suggested-meal-add" method="POST" action="{{ route('cleanse_suggested_meal.save') }}" accept-charset="utf-8" enctype="multipart/form-data">
					  @csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8"> 
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Nutrition Suggest Meal Info:</h3>
								<div class="form-group row">
									<label class="col-3">Meal Type<span style="color:red">*</span></label>
									<div class="col-9">
										<select class="form-control" name="meal_type">
											<option disabled="disabled">Select Type</option>
					                        <option value="Breakfast Meal">Breakfast Meal</option>
					                        <option value="Lunch Meal">Lunch Meal</option>
					                        <option value="Dinner Meal">Dinner Meal</option>
					                      </select>
					                      @error('meal_type')
					                        <div class="alert alert-danger">{{ $message }}</div>
					                      @enderror
									</div> 
								</div>

								<!-- <div class="form-group row">
									<label class="col-3">Title<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="title" value="{{ old('title') }}" placeholder="Enter title" />
										@error('title')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div> -->
								<div class="form-group row">
									<label class="col-3">Food Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="Food_name" value="{{ old('Food_name') }}" placeholder="Enter Food Name" />
										@error('Food_name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Calorie<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="calorie" value="{{ old('calorie') }}" placeholder="Enter Calorie" />
										@error('calorie')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Fats<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="fats" value="{{ old('fats') }}" placeholder="Enter Fats" />
										@error('fats')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Carbohydrate<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="carbohydrate" value="{{ old('carbohydrate') }}" placeholder="Enter Carbohydrate" />
										@error('carbohydrate')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Protein<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="protein" value="{{ old('protein') }}" placeholder="Enter Protein" />
										@error('protein')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-3">Description</label>
								<div class="col-9">
									<textarea name="description" class="form-control" data-provide="description" id="kt-ckeditor-1">{{ old('description') }}</textarea>
								</div>
							</div>
						</div>
						<div class="col-xl-2"></div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
    <script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	FormValidation.formValidation(
			 document.getElementById('suggested-meal-add'),
			 {
			  fields: {
			   /*title: {
			    validators: {
			     notEmpty: {
			      message: 'Title is required'
			     }
			    }
			   },*/
			   Food_name: {
			    validators: {
			     notEmpty: {
			      message: 'Food name is required'
			     }
			    }
			   },
			   meal_type: {
			    validators: {
			     notEmpty: {
			      message: 'Meal Type is required'
			     }
			    }
			   },
			   calorie: {
			    validators: {
			     notEmpty: {
			      message: 'Calorie is required'
			     }
			    }
			   },
			   fats: {
			    validators: {
			     notEmpty: {
			      message: 'Fat is required'
			     }
			    }
			   },
			   carbohydrate: {
			    validators: {
			     notEmpty: {
			      message: 'Carbohydrate is required'
			     }
			    }
			   },
			   protein: {
			    validators: {
			     notEmpty: {
			      message: 'protein is required'
			     }
			    }
			   },
			  },

			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			  }
			 }
		);
    </script>
     <script type="text/javascript">
    	// Class definition
		var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		        ClassicEditor
		            .create( document.querySelector( '#kt-ckeditor-1' ) )
		            .then( editor => {
		                console.log( editor );
		            } )
		            .catch( error => {
		                console.error( error );
		            } );
		    }

		    return {
		        // public functions
		        init: function() {
		            demos();
		        }
		    };
		}();

		
    </script>
@endsection
