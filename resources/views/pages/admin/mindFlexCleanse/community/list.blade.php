{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Community')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}
 
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
          <div class="card-title">
            <h3 class="card-label">Community
            <span class="d-block text-muted pt-2 font-size-sm">List of Community</span></h3>
          </div> 
        </div>
        <div class="card-body">
          <!--begin: Datatable -->
          <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_cleanse_ranking" style="text-align: center;">
              
            <thead>
              <tr>
                <th>title</th>
                <th>Customer Name</th>
                <th>Banner</th>
                <th>Like</th>
                
              </tr>
            </thead> 
            <tbody>
               @foreach ($data as $row)
                <tr>
                  <td>{{$row->title}}</td>
                  <td>{{$row->name}}</td>
                  <td>
                   
                     @if ($row->community_banner)
                        <img style="height: 50px;width: 50px;border-radius: 25px;" src="{{ url('/'). '/public//media/community-image/'.$row->community_banner}}" />
                        @else
                        <img style="height: 50px;width: 50px;border-radius: 25px;" src="{{ url('/'). '/public/media/customer-image/dummy450x450.jpg'}}" />
                        <!-- {{'No Image'}} -->
                      @endif

                  </td>
                     <td>{{$row->like}}</td>
                </tr>
               @endforeach
            </tbody>
          </table>
          <!--end: Datatable-->
        </div>
      </div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
  <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/pages/custom.js?cvb') }}" type="text/javascript"></script>
@endsection
