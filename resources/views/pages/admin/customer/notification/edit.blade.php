 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Customer Reminder')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

  <div class="row">  
    <div class="col-lg-12">
      <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
          <div class="card-title">
            <h3 class="card-label">
              Workout Reminder <i class="mr-2"></i>
            </h3>
          </div>
          <div class="card-toolbar">
            <a href="{{route ('cus_notify.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
              <i class="ki ki-long-arrow-back icon-sm"></i>
              Back
            </a>
          </div> 
        </div>
        <div class="card-body">
          <!--begin::Form-->
          <form class="form" id="edit-notify" name="add-cust-reminder" action="{{ route('cus_notify.update',[$row->id])}}" method="post" accept-charset="utf-8" >
            @csrf
            <div class="row"> 
              <div class="col-xl-12">
                <div class="my-5">

                  <div class="form-group row">
                    <label class="col-3">Customer Name<span style="color:red">*</span></label>
                    <div class="col-9">
                      <select class="form-control" name="customer_id">
                        <option value= "" >Select Customer</option>
                          @foreach ($customers as $value)
                            <option value="{{$value->customer_id}}" {{$row->customer_id == $value->customer_id  ? 'selected' : ''}}>{{$value->customer_name}}</option>
                          @endforeach
                      </select>
                      @error('customer_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-3">Title<span style="color:red">*</span></label>
                    <div class="col-9">
                      <div class="input-group">
                       <input type="text" class="form-control" name = "title" placeholder="Title of Notification" value="{{ $row->title }}"/>
                      </div>
                        @error('title')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-3">Description<span style="color:red">*</span></label>
                    <div class="col-9">
                      <textarea name="description" class="form-control" data-provide="description" placeholder="Write Prescription here.." id="kt-ckeditor-1">{{$row->description}}</textarea>
                    </div>
                  </div>

                  <!-- END PLANN DETAIL SECTION -->
                  <div class="separator separator-dashed my-10"></div>
                  <div class="form-group text-center m-t-20">
                      <div class="col-xs-12">
                          <button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
                           <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <!--end::Form-->
        </div>
      </div>
    </div>  
  </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
 <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
  <script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script> 
 <script type="text/javascript">
    FormValidation.formValidation(
       document.getElementById('edit-notify'),
       {
        fields: {
          customer_id: {
            validators: {
             notEmpty: {
              message: 'Customer Name is required'
             }
            }
          }, 
         title: {
          validators: {
           notEmpty: {
            message: 'Title is required'
           }
          }
         },

        },

        plugins: {
         trigger: new FormValidation.plugins.Trigger(),
         // Bootstrap Framework Integration
         bootstrap: new FormValidation.plugins.Bootstrap(),
         // Validate fields when clicking the Submit button
         submitButton: new FormValidation.plugins.SubmitButton(),
                  // Submit the form when all fields are valid
         defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        },
      }
    );
  </script>
  <script type="text/javascript">
      // Class definition

    var KTCkeditor = function () {
        // Private functions
        var demos = function () {
            ClassicEditor
                .create( document.querySelector( '#kt-ckeditor-1') )
                .then( editor => {
                    console.log( editor );
                } )
                .catch( error => {
                    console.error( error );
                } );
        }

        return {
            // public functions
            init: function() {
                demos();
            }
        };
    }();
  </script>
 @endsection
