{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Customers')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row">
    <div class="col-lg-12">
        <div class="card card-custom gutter-b">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Customers List
                        <span class="d-block text-muted pt-2 font-size-sm"></span>
                    </h3>
                </div>	
                <div class="card-toolbar">
                    <a href="{{ route('customers.create') }}" class="btn btn-primary font-weight-bolder">
                        <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <circle fill="#000000" cx="9" cy="15" r="6"/>
                                <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg><!--end::Svg Icon--></span>Add Customer
                    </a>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    {{ $message }}
                </div>
            @endif
            <div class="delete_message"></div>
            <!-- <input type="text" name="searchdata" id="searchdata"> -->
            <!-- <div class="card-body">
                <table class="table table-separate table-head-custom table-checkable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Image</th>
                            <th>Plan</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody id="searchDataTable">

                    </tbody>
            </div>
            <div id="paginate">

            </div> -->
            <div class="card-body">
                <!--begin: Datatable -->
                <table class="table table-separate table-head-custom table-checkable" id="customerList">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Image</th>
                            <th>Plan</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customers as $value)
                        <tr>
                            <td>{{$value->name}}</td>
                            <td>{{$value->email}}</td>
                            <td>
                                @if ($value->image)
                                <img style="height: 50px;width: 50px;border-radius: 25px;" src="{{ url('/'). '/public/media/customer-image/'.$value->image}}" />
                                @else
                                <img style="height: 50px;width: 50px;border-radius: 25px;" src="{{ url('/'). '/public/media/customer-image/dummy450x450.jpg'}}" />
                                <!-- {{'No Image'}} -->
                                @endif

                            </td>
                            <!-- <td>{{$value->plan}}
                                @if ($value->plan == 1)
                                <span class="label label-lg font-weight-bold label-light-primary label-inline">Paid</span>
                                @else
                                <span class="label label-lg font-weight-bold label-light-danger label-inline">Free</span>
                                @endif
                            </td> -->
                            <td>
                                <span class="label label-lg font-weight-bold label-light-primary label-inline">{{$value->plan_title}}</span>
                            </td>
                            <td class="text-nowrap">
                                <a href="{{ route('customers.edit',[$value->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                                <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelete({{$value->id}})">  <i class="fa fa-times"></i></a>
                                <a href="{{ route('customers.detailView',[$value->id]) }}" data-toggle="tooltip" data-original-title="View All Detail" class="btn btn-primary btn-circle"> <i class="fa fa-eye"></i> </a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
    </div> 
</div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        /* FETCH ALL DATA */
        /*$(document).ready(function() {
            $.ajax({
                type: "GET",
                url: 'https://vkapsprojects.com/pedro/skeleton/customers/allNum',
                // data: formData,
                dataType: 'json',
                success: function (data) {
                    if(data.status == true){
                        var resData = data.success;
                        var totalCount = resData.length;
                        var pages = Math.round(totalCount/5);
                        for(var i=1; i<=pages; i++){
                            var html = '<a href="javascript:void(0);" onclick="test('+i+')">'+ i +'</a>';

                            // var html = '<a href="javascript:void(0);" class="page-link" data-id="'+i+'">'+ i +'</a>';
                            $('#paginate').append(html);
                        }
                    }else{
                        console.log('else');
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });

        });*/
        /*function test(num){
        // $(".page-link").click(function(event){
            // event.preventDefault();
            // alert();
            var formData = {num:num};
            // var num = $(this).attr("data-id");
            // var formData = {num:num};
            $.ajax({
                type: "GET",
                url: 'https://vkapsprojects.com/pedro/skeleton/customers/allCustomers',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    if(data.status == true){
                        $('#searchDataTable').html('');
                        var resData = data.success;
                        var imageUrl = 'https://vkapsprojects.com/pedro/skeleton/public/media/';
                        var baseUrl = 'https://vkapsprojects.com/pedro/skeleton/';
                        var html = '';
                        $.each( resData, function( key, value ) {
                            html = '<tr><td>'+value.name+'</td><td>'+value.email+'</td><td>'+(value.image ? '<img style="height: 50px;width: 50px;border-radius: 25px;" src="'+imageUrl+'customer-image/'+value.image+'">': '<img style="height: 50px;width: 50px;border-radius: 25px;" src="'+imageUrl+'customer-image/dummy450x450.jpg">')+'</td><td>'+(value.email == 1 ? '<span class="label label-lg font-weight-bold label-light-primary label-inline">Paid</span>': '<span class="label label-lg font-weight-bold label-light-danger label-inline">Free</span>')+'</td><td><a href="'+baseUrl+'customers/edit/'+value.id+'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"><i class="fa fa-pencil-alt"></i></a><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelete('+value.id+')"><i class="fa fa-times"></i></a><a href="'+baseUrl+'customers/detailView/'+value.id+'" data-toggle="tooltip" data-original-title="View All Detail" class="btn btn-primary btn-circle"><i class="fa fa-eye"></i> </a></td></tr>';
                            $('#searchDataTable').append(html);
                        });
                    }else{
                        console.log('else');
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        // });
        }*/


        /* SEARCHING */
        /*$("#searchdata").keyup(function(event){
            event.preventDefault();
            var searchval = $('#searchdata').val();
            var count = searchval.length;
            if(count == 3){
                customSearch(searchval);
            }
        });*/
        /* SEARCHING */
        /*function customSearch(searchval){
            var formData = {searchval:searchval};
            $.ajax({
                type: "GET",
                url: 'https://vkapsprojects.com/pedro/skeleton/customers/search',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    if(data.status == true){
                        $('#searchDataTable').html('');
                        var resData = data.success;
                        var html = '';
                        $.each( resData, function( key, value ) {
                            html = '<tr><td>'+value.name+'</td><td>'+value.email+'</td><td>'+(value.image ? '<img style="height: 50px;width: 50px;border-radius: 25px;" src="https://vkapsprojects.com/pedro/skeleton/public/media/customer-image/'+value.image+'">': '<img style="height: 50px;width: 50px;border-radius: 25px;" src="https://vkapsprojects.com/pedro/skeleton/public/media/customer-image/dummy450x450.jpg">')+'</td><td>'+(value.email == 1 ? '<span class="label label-lg font-weight-bold label-light-primary label-inline">Paid</span>': '<span class="label label-lg font-weight-bold label-light-danger label-inline">Free</span>')+'</td><td><a href="https://vkapsprojects.com/pedro/skeleton/customers/edit/'+value.id+'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"><i class="fa fa-pencil-alt"></i></a><a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelete('+value.id+')"><i class="fa fa-times"></i></a><a href="https://vkapsprojects.com/pedro/skeleton/customers/detailView/'+value.id+'" data-toggle="tooltip" data-original-title="View All Detail" class="btn btn-primary btn-circle"><i class="fa fa-eye"></i> </a></td></tr>';
                            $('#searchDataTable').append(html);
                        });
                    }else{
                        console.log('else');
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }*/
        /* DELETE */
        function comfrimDelete(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!", 
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax('https://vkapsprojects.com/pedro/skeleton/public/customers/delete/'+id, {
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    }).done(function (r) {
                        console.log('result',r); 

                        if(r.success){
                            $("div.delete_message").html('<div class="alert alert-success">' + r.message + '</div>');
                            window.setTimeout(function(){location.reload()},2000);
                        }else{                
                            swal.fire({
                                text: 'Customer does not exist..',
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn font-weight-bold btn-light-primary"
                                }
                            }).then(function () {
                                KTUtil.scrollTop();
                            });            
                        }    
                    }).fail(function (jqXHR, textStatus, errorThrown) {             
                        swal.fire({
                            text: errors,
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        }).then(function () {
                            KTUtil.scrollTop();
                        });    
                    });
                }
            });
        }
    </script>
    <script type="text/javascript">
        $('#customerList').dataTable({
            // "scrollX": true,
            // "scrollY": true, 
        });
    </script>
@endsection
