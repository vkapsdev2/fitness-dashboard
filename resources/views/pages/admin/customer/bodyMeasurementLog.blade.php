{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Body Measurement Log')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
           	<div class="card card-custom gutter-b">
				<div class="card-header flex-wrap border-0 pt-6 pb-0">
					<div class="card-title">
						<h3 class="card-label">Body Measurement Log List
						<span class="d-block text-muted pt-2 font-size-sm"></span></h3>
					</div>
				</div>
				@if ($message = Session::get('success'))
			        <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
				<div class="card-body">
					<!--begin: Datatable -->
					<table class="table table-separate table-head-custom table-checkable" id="bodyMeasurementLog">
						<thead>
							<tr>
								<th>Name</th>
                <th>Shoulder</th>
                <th>Right Bicep</th>
                <th>Chest</th>
                <th>Waist</th>
                <th>Belly</th>
                <th>Hips</th>
                <th>Right Thigh</th>
                <th>Right Calf</th>
                <th>Log Date</th>
							</tr>
						</thead>
						<tbody>
							 @foreach ($rows as $value)
							 	<tr>
									<td>{{$value->user_name}}</td>
                  <td>{{$value->shoulder}}</td>
                  <td>{{$value->right_bicep}}</td>
                  <td>{{$value->chest}}</td>
                  <td>{{$value->waist}}</td>
                  <td>{{$value->belly}}</td>
                  <td>{{$value->hips}}</td>
                  <td>{{$value->right_thigh}}</td>
                  <td>{{$value->right_calf}}</td>
                  <td>{{$value->log_date}}</td>
								</tr>
							 @endforeach
						</tbody>
					</table>
					<!--end: Datatable-->
				</div>
			</div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script type="text/javascript">
     $('#bodyMeasurementLog').dataTable({
    });
  </script>
@endsection
