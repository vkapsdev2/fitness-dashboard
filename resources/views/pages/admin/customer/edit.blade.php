{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Customer')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row">
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    Edit Customer <i class="mr-2"></i>

                </h3>
            </div>
            <div class="card-toolbar">
                <a href="{{route ('customers.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
                    <i class="ki ki-long-arrow-back icon-sm"></i>
                    Back
                </a>

            </div>
        </div>
        <div class="card-body">
            <!--begin::Form-->

            <form class="form" id="edit-customer" name="edit-customer" action="{{ route('customers.update',[$customer[0]->id])}}" method="post" accept-charset="utf-8" enctype="multipart/form-data" >
                @csrf
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <div class="my-5">
                            <h3 class=" text-dark font-weight-bold mb-10">Customer Info:</h3>
                            <div class="form-group row">
                                <label class="col-3">Full Name<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" name= "name" value="{{$customer[0]->name}}" />
                                    @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Email Address<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <div class="input-group input-group-solid">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                        <input type="text" class="form-control form-control-solid" name = "email" placeholder="Email" value="{{$customer[0]->email}}" disabled="disabled" />
                                    </div>
                                    <span class="form-text text-muted">We'll never share your email with anyone else. </span>

                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        <!--  <div class="form-group row">
                        <label class="col-3">Password</label>
                        <div class="col-9">
                        <input class="form-control form-control-solid" type="text" name ="password" value=""/>
                        @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        </div> -->

                        <div class="form-group row">
                            <label class="col-3">Contact Number<span style="color:red">*</span></label>
                            <div class="col-9">
                                <div class="input-group input-group-solid">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                    <input type="text" class="form-control form-control-solid" name="phone" value="{{$customer[0]->phone}}" placeholder="Phone" maxlength="10" minlength="10" />
                                </div>
                                @error('phone')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3">Image</label>
                            <div class="col-9">
                                <input class="form-control form-control-solid" type="file" name="image" value="{{public_path().'/media/customer-image/'.$customer[0]->image}}"/>
                                @if ($customer[0]->image)
                                <img style= "height:50px;" src="{{ url('/'). '/public/media/customer-image/'.$customer[0]->image}}" />
                                @endif
                            </div>
                        </div>
                        </div>
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5">
                            <h3 class=" text-dark font-weight-bold mb-10">Address Details:</h3>
                            <div class="form-group row">
                                <label class="col-3">Address<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->address }}" name="address"/>
                                    @error('address')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">City<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->city }}" name="city"/>
                                    @error('city')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">State<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->state }}" name="state"/>
                                    @error('state')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Zip / Postal Code<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->zip }}" name="zip" maxlength="6" />
                                    @error('zip')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <!-- START SOCIAL SECTION -->
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5">
                            <h3 class=" text-dark font-weight-bold mb-10">Social Details:</h3>
                            <div class="form-group row">
                                <label class="col-3">Facebook</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="facebook_url" value="{{ $customer[0]->facebook_url }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your Facebook website URL.</span>
                                    @error('facebook_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Google</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="google_url" value="{{ $customer[0]->google_url }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your Google website URL.</span>
                                    @error('google_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Twitter</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="twitter_url" value="{{ $customer[0]->twitter_url }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your Twitter website URL.</span>
                                    @error('twitter_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">YouTube</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="youtube_url" value="{{ $customer[0]->youtube_url }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your YouTube website URL.</span>
                                    @error('youtube_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Instagram</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="instagram_url" value="{{ $customer[0]->instagram_url }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your Instagram website URL.</span>
                                    @error('instagram_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- END   SOCIAL SECTION -->

                        <!-- START PERSONAL INFORMATION -->
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-52">
                            <h3 class=" text-dark font-weight-bold mb-10">Personal Info:</h3>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Gender<span style="color:red">*</span></label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="gender" id="male" value="1"  {{ ($customer[0]->gender=="1")? "checked" : "" }}/>
                                            <span></span>
                                            Male
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="gender" id="female" value="2"  {{ ($customer[0]->gender=="2")? "checked" : "" }}/>
                                            <span></span>
                                            Female
                                        </label>
                                    </div>
                                    @error('gender')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Level</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="level" id="biginner" value="1" {{ ($customer[0]->level=="1")? "checked" : "" }} />
                                            <span></span>
                                            Biginner
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="level" id="intermediate" value="2" {{ ($customer[0]->level=="2")? "checked" : "" }}/>
                                            <span></span>
                                            Intermediate
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="level" id="advance" value="3" {{ ($customer[0]->level=="3")? "checked" : "" }}/>
                                            <span></span>
                                            Advance
                                        </label>
                                    </div>
                                    @error('level')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Weight<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->weight }}" name="weight" maxlength="3" />
                                    @error('weight')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3"> Height<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <select class="form-control form-control-solid" name="height">
                                        <option value= "" >Select Height</option>
                                        <option value ="4 Ft 8 Inch" {{ ($customer[0]->height=="4 Ft 8 Inch")? "selected" : "" }}>4 Ft 8 Inch</option>
                                        <option value ="4 Ft 9 Inch" {{ ($customer[0]->height=="4 Ft 9 Inch")? "selected" : "" }}>4 Ft 9 Inch</option>
                                        <option value ="4 Ft 10 Inch" {{ ($customer[0]->height=="4 Ft 10 Inch")? "selected" : "" }}>4 Ft 10 Inch</option>
                                        <option value ="4 Ft 11 Inch" {{ ($customer[0]->height=="4 Ft 11 Inch")? "selected" : "" }}>4 Ft 11 Inch</option>
                                        <option value ="5 Ft 0 Inch" {{ ($customer[0]->height=="5 Ft 0 Inch")? "selected" : "" }}>5 Ft 0 Inch</option>
                                        <option value ="5 Ft 1 Inch" {{ ($customer[0]->height=="5 Ft 1 Inch")? "selected" : "" }}>5 Ft 1 Inch</option>
                                        <option value ="5 Ft 2 Inch" {{ ($customer[0]->height=="5 Ft 2 Inch")? "selected" : "" }}>5 Ft 2 Inch</option>
                                        <option value ="5 Ft 3 Inch" {{ ($customer[0]->height=="5 Ft 3 Inch")? "selected" : "" }}>5 Ft 3 Inch</option>
                                        <option value ="5 Ft 4 Inch" {{ ($customer[0]->height=="5 Ft 4 Inch")? "selected" : "" }}>5 Ft 4 Inch</option>
                                        <option value ="5 Ft 5 Inch" {{ ($customer[0]->height=="5 Ft 5 Inch")? "selected" : "" }}>5 Ft 5 Inch</option>
                                        <option value ="5 Ft 6 Inch" {{ ($customer[0]->height=="5 Ft 6 Inch")? "selected" : "" }}>5 Ft 6 Inch</option>
                                        <option value ="5 Ft 7 Inch" {{ ($customer[0]->height=="5 Ft 7 Inch")? "selected" : "" }}>5 Ft 7 Inch</option>
                                        <option value ="5 Ft 8 Inch" {{ ($customer[0]->height=="5 Ft 8 Inch")? "selected" : "" }}>5 Ft 8 Inch</option>
                                        <option value ="5 Ft 9 Inch" {{ ($customer[0]->height=="5 Ft 9 Inch")? "selected" : "" }}>5 Ft 9 Inch</option>
                                        <option value ="5 Ft 10 Inch" {{ ($customer[0]->height=="5 Ft 10 Inch")? "selected" : "" }}>5 Ft 10 Inch</option>
                                        <option value ="5 Ft 11 Inch" {{ ($customer[0]->height=="5 Ft 11 Inch")? "selected" : "" }}>5 Ft 11 Inch</option>
                                        <option value ="6 Ft 0 Inch" {{ ($customer[0]->height=="6 Ft 0 Inch")? "selected" : "" }}>6 Ft 0 Inch</option>
                                        <option value ="6 Ft 1 Inch" {{ ($customer[0]->height=="6 Ft 1 Inch")? "selected" : "" }}>6 Ft 1 Inch</option>
                                        <option value ="6 Ft 2 Inch" {{ ($customer[0]->height=="6 Ft 2 Inch")? "selected" : "" }}>6 Ft 2 Inch</option>
                                        <option value ="6 Ft 3 Inch" {{ ($customer[0]->height=="6 Ft 3 Inch")? "selected" : "" }}>6 Ft 3 Inch</option>
                                        <option value ="6 Ft 4 Inch" {{ ($customer[0]->height=="6 Ft 4 Inch")? "selected" : "" }}>6 Ft 4 Inch</option>
                                        <option value ="6 Ft 5 Inch" {{ ($customer[0]->height=="6 Ft 5 Inch")? "selected" : "" }}>6 Ft 5 Inch</option>
                                        <option value ="6 Ft 6 Inch" {{ ($customer[0]->height=="6 Ft 6 Inch")? "selected" : "" }}>6 Ft 6 Inch</option>
                                        <option value ="6 Ft 7 Inch" {{ ($customer[0]->height=="6 Ft 7 Inch")? "selected" : "" }}>6 Ft 7 Inch</option>
                                        <option value ="6 Ft 8 Inch" {{ ($customer[0]->height=="6 Ft 8 Inch")? "selected" : "" }}>6 Ft 8 Inch</option>
                                        <option value ="6 Ft 9 Inch" {{ ($customer[0]->height=="6 Ft 9 Inch")? "selected" : "" }}>6 Ft 9 Inch</option>
                                        <option value ="6 Ft 10 Inch" {{ ($customer[0]->height=="6 Ft 10 Inch")? "selected" : "" }}>6 Ft 10 Inch</option>
                                        <option value ="6 Ft 11 Inch" {{ ($customer[0]->height=="6 Ft 11 Inch")? "selected" : "" }}>6 Ft 11 Inch</option>
                                    </select>
                                    @error('height')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Age<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->age }}" name="age" maxlength="2" />
                                    @error('age')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Body Fat %<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->body_fat_percent }}" name="body_fat_percent" maxlength="4" />
                                    @error('body_fat_percent')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Body Fat Mass<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->body_fat_mass }}" name="body_fat_mass" maxlength="4"/>
                                    @error('body_fat_mass')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Skeletal Muscle Mass<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->skletal_muscle_mass }}" name="skletal_muscle_mass" maxlength="4"/>
                                    @error('skletal_muscle_mass')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="separator separator-dashed my-10"></div>
                            <div class="my-5">
                                <h3 class=" text-dark font-weight-bold mb-10">Body Measurements:</h3>
                                <div class="form-group row">
                                    <label class="col-3">Neck<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->neck }}" name="neck" maxlength="2" />
                                        @error('neck')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3">Shoulder<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->shoulder }}" name="shoulder" maxlength="2"/>
                                        @error('shoulder')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Right Bicep<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->right_bicep }}" name="right_bicep" maxlength="2"/>
                                        @error('right_bicep')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Chest<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->chest }}" name="chest" maxlength="2"/>
                                        @error('chest')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3">Waist<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->waist }}" name="waist" maxlength="2"/>
                                        @error('waist')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3">Hips<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->hips }}" name="hips" maxlength="2"/>
                                        @error('hips')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Right Thigh<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->right_thigh  }}" name="right_thigh" maxlength="2"/>
                                        @error('right_thigh')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Right Calf<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->right_calf }}" name="right_calf" maxlength="2"/>
                                        @error('right_calf')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                            </div>

                            <!-- Satisfaction Section -->
                            <div class="separator separator-dashed my-10"></div>
                            <div class="my-5">
                                <h3 class=" text-dark font-weight-bold mb-10">Satisfaction:</h3>
                                <div class="form-group row">
                                    <label class="col-3">Career</label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->career }}" name="career" maxlength="2" />
                                        @error('career')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3">Relationship</label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->relationship }}" name="relationship" maxlength="2"/>
                                        @error('relationship')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3">Joy<span style="color:red">*</span></label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->joy }}" name="joy" maxlength="2"/>
                                        @error('joy')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3">Health</label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->health }}" name="health" maxlength="2"/>
                                        @error('health')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Nutrition</label>
                                    <div class="col-9">
                                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->nutrition }}" name="nutrition" maxlength="2"/>
                                        @error('nutrition')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                        <!--  <div class="form-group row">
                        <label class="col-3">Waist</label>
                        <div class="col-9">
                        <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->waist_satisfaction }}" name="waist_satisfaction" maxlength="2"/>
                        @error('waist')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        </div> -->

                        <div class="form-group row">
                            <label class="col-3">Sleep</label>
                            <div class="col-9">
                                <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->sleep }}" name="sleep" maxlength="2"/>
                                @error('sleep')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3">Appearance</label>
                            <div class="col-9">
                                <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->appearance }}" name="appearance" maxlength="2"/>
                                @error('appearance')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3">Purpose</label>
                            <div class="col-9">
                                <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->purpose }}" name="purpose" maxlength="2"/>
                                @error('right_thigh')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3">Social</label>
                            <div class="col-9">
                                <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->social }}" name="social" maxlength="2"/>
                                @error('social')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3">Emotional/Spiritual</label>
                            <div class="col-9">
                                <input class="form-control form-control-solid" type="text" value="{{ $customer[0]->spiritual }}" name="spiritual" maxlength="2"/>
                                @error('spiritual')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3">Ready for Change?<span style="color:red">*</span></label>
                            <div class="col-9">

                                <select class="form-control form-control-solid" name="ready_for_change">
                                    <option value="">Choose your answer</option>
                                    <option value= "1"  {{ ($customer[0]->ready_for_change=="1")? "selected" : "" }}>Yes</option>
                                    <option value= "2" {{ ($customer[0]->ready_for_change=="2")? "selected" : "" }}>No</option>
                                </select>
                                @error('ready_for_change')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3"> Social Goal<span style="color:red">*</span></label>
                            <div class="col-9">
                                <select class="form-control form-control-solid" name="social_goal">
                                    <option value="">Select Social Goal</option>
                                    <option value= "1"  {{ ($customer[0]->social_goal=="1")? "selected" : "" }}>Fat Loss</option>
                                    <option value= "2" {{ ($customer[0]->social_goal=="2")? "selected" : "" }}>Weight Gain</option>
                                    <option value= "3" {{ ($customer[0]->social_goal=="3")? "selected" : "" }}>Muscle Gain</option>
                                    <option value= "4" {{ ($customer[0]->social_goal=="4")? "selected" : "" }}>Other</option>
                                </select>
                            </div>
                        </div>
                        </div>

                        <!-- PERSONAL PROFILE QUESTIONNAIRE Section -->
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5">
                            <h3 class=" text-dark font-weight-bold mb-10">PERSONAL PROFILE QUESTIONNAIRE:</h3>
                            <div class="form-group row">
                                <label class="col-3"> Social Goal</label>
                                <div class="col-9">
                                    <select class="form-control form-control-solid" name="social_goal">
                                        <option value="">Select Social Goal</option>
                                        <option value= "1"  {{ ($customer[0]->social_goal=="1")? "selected" : "" }}>Fat Loss</option>
                                        <option value= "2" {{ ($customer[0]->social_goal=="2")? "selected" : "" }}>Waight Gain</option>
                                        <option value= "3" {{ ($customer[0]->social_goal=="3")? "selected" : "" }}>Muscle Gain</option>
                                        <option value= "4" {{ ($customer[0]->social_goal=="4")? "selected" : "" }}>Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Have you participated in any form of consistent physical activity? (Y/N) </label>
                                <div class="col-9">

                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="consistent_physical_activity"  id="consistent_physical_activity_1" value="1" class="consistent_physical_activity" {{ ($customer[0]->consistent_physical_activity=="1")? "checked" : "" }}  />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="consistent_physical_activity" id="consistent_physical_activity_2" value="2" class="consistent_physical_activity" {{ ($customer[0]->consistent_physical_activity=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>

                                    @error('consistent_physical_activity')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <!--  physical activity hidden fields -->
                            <div class="physical_activity_section" style="display: none;">
                                <div class="form-group row green-border-focus shadow-textarea">
                                    <label class="col-3">If so, how long?</label>
                                    <div class="col-9">
                                        <textarea class="form-control form-control-solid" id="exampleTextarea" name="consistent_physical_activity_time" placeholder="Write your answer here.." style="resize: none;">{{ $customer[0]->consistent_physical_activity_time }}
                                        </textarea>
                                        @error('consistent_physical_activity_time')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row green-border-focus shadow-textarea">
                                    <label class="col-3">What did your consistent physical activity entail?</label>
                                    <div class="col-9">
                                        <textarea class="form-control form-control-solid" id="exampleTextarea" name="consistent_physical_activity_description" placeholder="Write your answer here.." style="resize: none;" >{{ $customer[0]->consistent_physical_activity_description }}</textarea>
                                        @error('consistent_physical_activity_description')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!--  End physical activity hidden fields -->
                            <div class="form-group row">
                                <label class="col-3">What is your occupation? Describe your daily routine at work? </label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid" id="exampleTextarea" name="customer_occupation" placeholder="Write your answer here.." style="resize: none;" >{{ $customer[0]->customer_occupation }}</textarea>
                                    @error('customer_occupation')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Who lives in household? </label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid" id="exampleTextarea" name="houldhold"  placeholder="Write your answer here.." style="resize: none;">{{ $customer[0]->houldhold }}
                                    </textarea>
                                    @error('houldhold')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Do you partake in any recreational activities? (Golf, Tennis, Skiing, etc.) </label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid" id="exampleTextarea" name="recreational_activity" placeholder="Golf, Tennis, Skiing, etc." style="resize: none;">{{ $customer[0]->recreational_activity }}
                                    </textarea>
                                    @error('recreational_activity')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">Do you have any additional hobbies? (Reading, Video Games, etc.) </label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid" id="exampleTextarea" name="hobbies" placeholder="Reading, Video Games, etc." style="resize: none;">{{ $customer[0]->hobbies }}
                                    </textarea>
                                    @error('hobbies')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">How many hours of sleep do you get a night? Do you wake up rested in the morning?</label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid" id="exampleTextarea" name="sleep_hours_in_detail" placeholder="Write your answer here.." style="resize: none;">{{ $customer[0]->sleep_hours_in_detail }}
                                    </textarea>
                                    @error('sleep_hours_in_detail')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3">How long does it take you to fall asleep ?</label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid" id="exampleTextarea" name="sleep_taking_time" placeholder="Write your answer here.." style="resize: none;">{{ $customer[0]->sleep_taking_time }}
                                    </textarea>
                                    @error('sleep_taking_time')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <!-- MEDICAL PROFILE SECTION -->
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5">
                            <h3 class="text-dark font-weight-bold mb-10">MEDICAL PROFILE:</h3>
                            <div class="form-group row">
                                <label class="col-3">When was the last time you had a physical examination?</label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid" id="" name="last_physical_examination_time" placeholder="Write your answer here..." style="resize: none;">{{ $customer[0]->last_physical_examination_time }}</textarea>
                                    @error('last_physical_examination_time')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3 col-form-label">Have you ever been hospitalized?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="ever_been_hospitalized" checked="checked" value="1"  {{ ($customer[0]->ever_been_hospitalized=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="ever_been_hospitalized"  value="2"  {{ ($customer[0]->ever_been_hospitalized=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('ever_been_hospitalized')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Are you Pregnant?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_pregnant" checked="checked" value="1" {{ ($customer[0]->is_pregnant=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_pregnant"  value="2" {{ ($customer[0]->is_pregnant=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_pregnant')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do you smoke?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="smoking" checked="checked" value="1" {{ ($customer[0]->smoking=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="smoking"  value="2" {{ ($customer[0]->smoking=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('smoking')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">How many alcoholic beverages do you have per week?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="alcoholic_quantity_per_week" checked="checked" value="1" {{ ($customer[0]->alcoholic_quantity_per_week=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="alcoholic_quantity_per_week"  value="2" {{ ($customer[0]->alcoholic_quantity_per_week=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('alcoholic_quantity_per_week')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Have you been diagnosed with a heart condition?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="diagnosed_heart_condition" checked="checked" value="1" {{ ($customer[0]->diagnosed_heart_condition=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="diagnosed_heart_condition"  value="2" {{ ($customer[0]->diagnosed_heart_condition=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('diagnosed_heart_condition')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do you feel ever feel chest pain when doing physical activity?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_feel_chest_pain" checked="checked" value="1" {{ ($customer[0]->is_feel_chest_pain=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_feel_chest_pain"  value="2" {{ ($customer[0]->is_feel_chest_pain=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_feel_chest_pain')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">In the past month, have you had chest pain when you were not doing physical activity?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="did_feel_chest_pain_when_not_physical" checked="checked" value="1" {{ ($customer[0]->did_feel_chest_pain_when_not_physical=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="did_feel_chest_pain_when_not_physical" value="2" {{ ($customer[0]->did_feel_chest_pain_when_not_physical=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('did_feel_chest_pain_when_not_physical')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do experience dizziness or loss of consciousness?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_feel_loss_consciousness" checked="checked" value="1"  {{ ($customer[0]->is_feel_loss_consciousness=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_feel_loss_consciousness"  value="2" {{ ($customer[0]->is_feel_loss_consciousness=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_feel_loss_consciousness')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do you have any skeletal or joint issue?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_joint_issue" checked="checked" value="1" {{ ($customer[0]->is_joint_issue=="1")? "checked" : "" }}  />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_joint_issue"  value="2" {{ ($customer[0]->is_joint_issue=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_joint_issue')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do you currently take any medications?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_take_medications" checked="checked" value="1"  {{ ($customer[0]->is_take_medications=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_take_medications"  value="2" {{ ($customer[0]->is_take_medications=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_take_medications')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Are you diabetic or suffer from the low blood sugar?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_diabetic" checked="checked" value="1"  {{ ($customer[0]->is_diabetic=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_diabetic"  value="2" {{ ($customer[0]->is_diabetic=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_diabetic')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do you have high blood pressure?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_high_blood_pressure" checked="checked" value="1" {{ ($customer[0]->is_high_blood_pressure=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_high_blood_pressure"  value="2"  {{ ($customer[0]->is_high_blood_pressure=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_high_blood_pressure')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do you have high cholesterol?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_high_cholesterol" checked="checked" value="1"  {{ ($customer[0]->is_high_cholesterol=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_high_cholesterol"  value="2" {{ ($customer[0]->is_high_cholesterol=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_high_cholesterol')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Have you been diagnosed with osteoporosis?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_diagnosed_osteoporosis" checked="checked" value="1"  {{ ($customer[0]->is_diagnosed_osteoporosis=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_diagnosed_osteoporosis"  value="2" {{ ($customer[0]->is_diagnosed_osteoporosis=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_diagnosed_osteoporosis')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do you become breathless going up a flight of stairs?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="is_breathless_to_staris" checked="checked" value="1"   {{ ($customer[0]->is_breathless_to_staris=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="is_breathless_to_staris"  value="2" {{ ($customer[0]->is_breathless_to_staris=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('is_breathless_to_staris')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Have you ever been diagnosed with cancer?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="do_you_have_been_cancer" checked="checked" value="1" {{ ($customer[0]->do_you_have_been_cancer=="1")? "checked" : "" }} />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="do_you_have_been_cancer"  value="2" {{ ($customer[0]->do_you_have_been_cancer=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('do_you_have_been_cancer')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Do you know of any other reason why you should not engage in any physical activity?</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="activity_reson_for_not_engage_physical_activity" checked="checked" value="1" {{ ($customer[0]->activity_reson_for_not_engage_physical_activity=="1")? "checked" : "" }}  />
                                            <span></span>
                                            Yes
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="activity_reson_for_not_engage_physical_activity"  value="2"  {{ ($customer[0]->activity_reson_for_not_engage_physical_activity=="2")? "checked" : "" }} />
                                            <span></span>
                                            No
                                        </label>
                                    </div>
                                    @error('activity_reson_for_not_engage_physical_activity')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                        </div>
                        <!-- END MEDICAL PROFILE SECTION -->

                        <!-- FAMILY DETAIL SECTION -->
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5">
                            <h3 class="text-dark font-weight-bold mb-10">FAMILY DETAIL:</h3>
                            <div class="form-group row">
                                <label class="col-3"></label>
                                <div class="col-9">
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Kidney Disease" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Kidney Disease') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>
                                        />
                                        <span></span>
                                        Kidney Disease
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Osteopenia" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Osteopenia') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>
                                        />
                                        <span></span>
                                        Osteopenia
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Depression" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Depression') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>
                                        />
                                        <span></span>
                                        Depression
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Heart Disease or Stroke" 
                                        <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Heart Disease or Stroke') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>
                                        />
                                        <span></span>
                                        Heart Disease or Stroke
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Osteoporosis" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Osteoporosis') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Osteoporosis
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Compulsive Overeating" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Compulsive Overeating') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Compulsive Overeating
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="High Blood Pressure" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'High Blood Pressure') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        High Blood Pressure
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Diabetes Mellitus" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Diabetes Mellitus') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  
                                        />
                                        <span></span>
                                        Diabetes Mellitus
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Anorexia/Bulimia" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Anorexia/Bulimia') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  
                                        />
                                        <span></span>
                                        Anorexia/Bulimia
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Prostate Disease" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Prostate Disease') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  
                                        />
                                        <span></span>
                                        Prostate Disease
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Anemia" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Anemia') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  
                                        />
                                        <span></span>
                                        Anemia
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Ulcer" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Ulcer') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  
                                        />
                                        <span></span>
                                        Ulcer
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Gallbladder Disease" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Gallbladder Disease') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  
                                        />
                                        <span></span>
                                        Gallbladder Disease
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Obesity" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Obesity') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  
                                        />
                                        <span></span>
                                        Obesity
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Gastrointestinal Disease" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Gastrointestinal Disease') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  
                                        />
                                        <span></span>
                                        Gastrointestinal Disease
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="High Triglycerides" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'High Triglycerides') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>  />
                                        <span></span>
                                        High Triglycerides
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Arthritis" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Arthritis') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Arthritis
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Food Allergies" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Food Allergies') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Food Allergies
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Cancer" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Cancer') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Cancer
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Low-back pain" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Low-back pain') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Low-back pain
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Sleep Disorder" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Sleep Disorder') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Sleep Disorder
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Lung/Pulmonary/Disease" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Lung/Pulmonary/Disease') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Lung/Pulmonary/Disease
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Neuromuscular Disease" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Neuromuscular Disease') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?>
                                        />
                                        <span></span>
                                        Neuromuscular Disease
                                    </label>

                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Gout" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Gout') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Gout
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Arteriosclerosis" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Arteriosclerosis') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Arteriosclerosis
                                    </label>
                                    <label class="checkbox checkbox-success">
                                        <input type="checkbox" class="form-control form-control-solid" name="family_disease_detail[]" value="Psychological Problems" <?php
                                        foreach ($family_diseases_arr as $type) {
                                            if ($type == 'Psychological Problems') { echo 'checked';}
                                            else { echo '';}
                                        }
                                        ?> 
                                        />
                                        <span></span>
                                        Psychological Problems
                                    </label>


                                    @error('family_disease_detail')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Other</label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid" id="" name="other_family_disease" placeholder="Write your answer here..." style="resize: none;">{{ $customer[0]->other_family_disease }}</textarea>
                                    @error('other_family_disease')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- END FAMILY DETAIL SECTION  -->
                        <!-- START PLAN DETAIL SECTION -->
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5 ">
                            <h3 class="text-dark font-weight-bold mb-10">PLAN DETAIL:</h3>

                            <div class="form-group row">
                                <label class="col-3">Plan Details</label>
                                <div class="col-9 ">
                                    <select class="form-control form-control-solid" name ="plan" value="">Please Select Plan</option>
                                        @foreach($plans as $plan)
                                        <option value="{{$plan->id}}" {{ ($customer[0]->plan) == $plan->id  ? 'selected' : '' }}> {{$plan->plan_title}}</option>
                                        @endforeach
                                    </select>  
                                    @error('plan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- END PLANN DETAIL SECTION -->
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
                                <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
                            </div>
                        </div>
                        </div>
                        </div>
                    <div class="col-xl-2"></div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/AutoFocus.js?gg') }}" type="text/javascript"></script>
    <script type="text/javascript">
        FormValidation.formValidation(
            document.getElementById('edit-customer'),
            {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Customer name is required'
                            },
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email is required'
                            },
                            emailAddress: {
                                message: 'The value is not a valid email address'
                            }
                        }
                    },
                    // paasword: {
                    //  validators: {
                    //   notEmpty: {
                    //    message: 'Paasword is required'
                    //   },
                    //  }
                    // },
                    phone: {
                        validators: {
                            notEmpty: {
                                message: 'Phone number is required'
                            },

                        }

                    },

                    address: {
                        validators: {
                            notEmpty: {
                                message: 'Address is required'
                            },
                        }
                    },
                    state: {
                        validators: {
                            notEmpty: {
                                message: 'State is required'
                            },
                        }
                    },
                    city: {
                        validators: {

                            notEmpty: {
                                message: 'City is required'
                            },
                        }
                    },
                    zip: {
                        validators: {
                            notEmpty: {
                                message: 'Zip is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    weight: {
                        validators: {
                            notEmpty: {
                                message: 'Weight is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    height: {
                        validators: {
                            notEmpty: {
                                message: 'Height is required'
                            }
                        }
                    },
                    age: {
                        validators: {
                            notEmpty: {
                                message: 'Age is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    body_fat_percent:{
                        validators: {
                            notEmpty: {
                                message: 'Body Fat percentage is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    body_fat_mass:{
                        validators: {
                            notEmpty: {
                                message: 'Body Fat Mass is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    skletal_muscle_mass:{
                        validators: {
                            notEmpty: {
                                message: 'Skeletal muscle Mass is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    neck:{
                        validators: {
                            notEmpty: {
                                message: 'Neck is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    shoulder:{
                        validators: {
                            notEmpty: {
                                message: 'Shoulder is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    right_bicep:{
                        validators: {
                            notEmpty: {
                                message: 'Right Bicep is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    chest:{
                        validators: {
                            notEmpty: {
                                message: 'Chest is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    waist:{
                        validators: {
                            notEmpty: {
                                message: 'Waist is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    hips:{
                        validators: {
                            notEmpty: {
                                message: 'Hips is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    right_thigh:{
                        validators: {
                            notEmpty: {
                                message: 'Right Thigh is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    right_calf:{
                        validators: {
                            notEmpty: {
                                message: 'Right Calf is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    career:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    relationship:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    health:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    nutrition:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    waist_satisfaction:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    sleep:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    appearance:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    purpose:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    social:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    spiritual:{
                        validators: {
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    social_goal:{
                        validators: {
                            notEmpty: {
                                message: 'Social goal is required'
                            }
                        }
                    },
                    joy:{
                        validators: {
                            notEmpty: {
                                message: 'Joy is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    ready_for_change:{
                        validators: {
                            notEmpty: {
                                message: 'Ready for change is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    facebook_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    google_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    twitter_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    youtube_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    instagram_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    plan: {
                        validators: {
                            notEmpty: {
                                message: 'Plan is required'
                            },
                        }
                    },
                    },

                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        // Bootstrap Framework Integration
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        autoFocus: new FormValidation.plugins.AutoFocus(),
                        // Validate fields when clicking the Submit button
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        // Submit the form when all fields are valid
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                    }
                } 
        );
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            var physicalActivityValue = $("input[name='consistent_physical_activity']:checked").val();
            if(physicalActivityValue == 1)
            {
                $(".physical_activity_section").show();
            }
            else
            {
                $(".physical_activity_section").hide();
            }

            $(".consistent_physical_activity").click(function(){
                var value = $(this).val();

                if(value == 1)
                {
                    $(".physical_activity_section").slideDown("slow");
                }
                else
                {
                    $(".physical_activity_section").slideUp("slow");
                    $("input[name='consistent_physical_activity_time']").val('');
                    $("input[name='consistent_physical_activity_description']").val('');

                }
            });
        });
    </script>
    <style type="text/css">
        div#kt_page_sticky_card {
            width: 100%;
        }
    </style>
 @endsection
