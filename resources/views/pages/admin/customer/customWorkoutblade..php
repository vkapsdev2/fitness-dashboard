{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Custom Workout')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
           	<div class="card card-custom gutter-b">
				
				@if ($message = Session::get('success'))
			        <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
          
				<div class="card-body">
					<!--begin: Datatable -->
					<table class="table table-separate table-head-custom table-checkable" id="customerList">
						<thead>
							<tr>
								<th>Name</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							 @foreach ($data as $value)
							 	<tr>
									<td>{{$value->customer_workout_id}}</td>
									<td class="text-nowrap">
                    <a href="" data-toggle="tooltip" data-original-title="View All Detail" class="btn btn-primary btn-circle"> <i class="fa fa-eye"></i> </a>

                  </td>
								</tr>
							 @endforeach
						</tbody>
					</table>
					<!--end: Datatable-->
				</div>
			</div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
 <script type="text/javascript">
     $('#customerList').dataTable();
  </script>
@endsection
