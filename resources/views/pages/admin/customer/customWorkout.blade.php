{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Custom Workout')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
           	<div class="card card-custom gutter-b">
				
				<div class="card-header flex-wrap border-0 pt-6 pb-0">
					<div class="card-title">
						<h3 class="card-label">Custom Workout List
						<span class="d-block text-muted pt-2 font-size-sm"></span></h3>
					</div>
				</div>
          
				<div class="card-body">
					<!--begin: Datatable -->
					<table class="table table-separate table-head-custom table-checkable" id="custom_workout">
						<thead>
							<tr>
								<th>Customer</th>
				                <th>Custom Workout</th>
				                <th>Category</th>
				                <th>Exercise</th>
							</tr>
						</thead>
						<tbody>
							 @foreach ($data as $value)
							 	<tr>
									<td>{{$value->customer_name}}</td>
				                  	<td>{{$value->title}}</td>
				                  	<td>{{$value->category_name}}</td>
					                <td>{{$value->exercise_name}}</td>
								</tr>
							 @endforeach
						</tbody>
					</table>
					<!--end: Datatable-->
				</div>
			</div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
 <script type="text/javascript">
     $('#custom_workout').dataTable();
  </script>
@endsection
