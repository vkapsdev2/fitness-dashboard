 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Profile')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
    	<div class="col-lg-4">
    		<div class="card admin_image_sec">
    			<div class="view_profile_details card-body">
					<img style="height: 50px;" src="{{ url('/'). '/public/media/admin-profile/'. $admin_user->image}}" />
					<h3><b>{{ $admin_user->name }} </b></h3>
					<h6>{{ $admin_user->email }} </h6>
					<a class="btn btn-success" type="button" href="{{route ('admin.changePassword', [$admin_user->id])}}">   {{ __('Change Password') }}</a>
				</div>

				<div class="admin_basic_detail">
					 	<div class="show_email">
					 		<span>Email address</span>
					 		<p>{{ $admin_user->email }}</p>
					 	</div>
					 	<div class="show_phone">
					 		<span>Phone Number</span>
					 		<p>{{ $admin_user->phone }}</p>
					 	</div>
						<div class="show_address">
					 		<span>Address</span>
					 		<p>{{ $admin_user->address }} , {{ $admin_user->city }} , {{ $admin_user->state }}  {{ $admin_user->zip }}</p>
					 	</div>
					 	<div>
					 		 <small class="text-muted pt-4 d-block">Social Profile</small>
                                <br />
                                <a class="btn btn-circle btn-secondary" href="{{$admin_user->facebook_url}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                <a class="btn btn-circle btn-secondary" href="{{$admin_user->google_url}}" target="_blank"><i class="fab fa-google"></i></a>
                                <a class="btn btn-circle btn-secondary" href="{{$admin_user->twitter_url}}" target="_blank"><i class="fab fa-twitter"></i></a>
                                <a class="btn btn-circle btn-secondary" href="{{$admin_user->youtube_url}}" target="_blank"><i class="fab fa-youtube"></i></a>
                                 <a class="btn btn-circle btn-secondary" href="{{$admin_user->instagram_url}}" target="_blank"><i class="fab fa-instagram" ></i></a>

					 	</div>
				 </div>

    		</div>
    	</div> 
        <div class="col-lg-8">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Edit profile <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="">
				@if(Session::has('error'))
					<div class="alert alert-danger alert-block">
    					<button type="button" class="close" data-dismiss="alert">×</button>    
    					<strong>{{ session('error') }}</strong>
				 </div> 
			@endif

				@if(Session::has('success'))
					<div class="alert alert-success alert-block">
    					<button type="button" class="close" data-dismiss="alert">×</button>    
    					<strong>{{ session('success') }}</strong>
					</div>   
				@endif
				
				<!--begin::Form-->
				<form class="form card-body" method="POST" action="{{route('admin.update', [$admin_user->id])}}" accept-charset="utf-8" enctype="multipart/form-data" id="edit-profile" name="edit-profile" >
					  @csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10"></h3>
								<div class="form-group row">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="name" value="{{$admin_user->name}}" placeholder="Enter name"/>
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Profile Picture</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="file" name="image" value="{{ old('image') }}"/>
										@error('image')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
										
									</div>
									
								</div>
								<div class="form-group row">
									<label class="col-3">Email<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="email" value="{{$admin_user->email}}" placeholder="Enter email"/>
										@error('email')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								 <div class="form-group row">
									<label class="col-3 ">Gender</label>
									<div class="col-9">
									    <div class="radio-inline">
									        <label class="radio">
									            <input type="radio" name="gender" id="male" value="1"  {{ ($admin_user->gender=="1")? "checked" : "" }}/>
									            <span></span>
									           Male
									        </label>
									        <label class="radio">
									            <input type="radio" name="gender" id="female" value="2"  {{ ($admin_user->gender=="2")? "checked" : "" }}/>
									            <span></span>
									            Female
									        </label>
									    </div>
									     @error('gender')
										<div class="alert alert-danger">{{ $message }}</div>
									@enderror
									    <span class="form-text text-muted"></span>
									</div>
									</div>
								<div class="form-group row">
									<label class="col-3">Phone<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="phone" value="{{$admin_user->phone}}" placeholder="Enter phone" maxlength="10" />
										@error('phone')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Address<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="address" value="{{$admin_user->address}}" placeholder="Enter address"/>
										@error('address')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">State<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="state" value="{{$admin_user->state}}" placeholder="Enter state"/>
										@error('state')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">City<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="city" value="{{$admin_user->city}}" placeholder="Enter city"/>
										@error('city')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Zip<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="zip" value="{{$admin_user->zip}}" placeholder="Enter zip" maxlength="6" />
										@error('zip')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Facebook</label>
									<div class="col-lg-9">
									<div class="input-group">
									<div class="input-group-prepend">
									<span class="input-group-text">
									  <i class="flaticon2-website"></i>
									 </span>
									</div>
									<input type="text" class="form-control form-control-solid" name="facebook_url" value="{{$admin_user->facebook_url}}" />
									</div>
									<span class="form-text text-muted">Please enter your Facebook website URL.</span>
									@error('facebook_url')
									<div class="alert alert-danger">{{ $message }}</div>
									@enderror
									</div>
									</div>
								<div class="form-group row">
								  <label class="col-3">Google</label>
								  <div class="col-lg-9">
								    <div class="input-group">
								      <div class="input-group-prepend">
								        <span class="input-group-text">
								          <i class="flaticon2-website"></i>
								         </span>
								      </div>
								      <input type="text" class="form-control form-control-solid" name="google_url" value="{{$admin_user->google_url}}"  />
								    </div>
								    <span class="form-text text-muted">Please enter your Google website URL.</span>
								   @error('google_url')
								      <div class="alert alert-danger">{{ $message }}</div>
								    @enderror
								  </div>
								  </div>
								<div class="form-group row">
								  <label class="col-3">Twitter</label>
								  <div class="col-lg-9">
								    <div class="input-group">
								      <div class="input-group-prepend">
								        <span class="input-group-text">
								          <i class="flaticon2-website"></i>
								         </span>
								      </div>
								      <input type="text" class="form-control form-control-solid" name="twitter_url" value="{{$admin_user->twitter_url}}" />
								    </div>
								    <span class="form-text text-muted">Please enter your Twitter website URL.</span>
								   @error('twitter_url')
								      <div class="alert alert-danger">{{ $message }}</div>
								    @enderror
								  </div>
								  </div>
								  
								
								<div class="form-group row">
								  <label class="col-3">YouTube</label>
								  <div class="col-lg-9">
								   <div class="input-group">
								    <div class="input-group-prepend">
								     <span class="input-group-text">
								      <i class="flaticon2-website"></i>
								     </span>
								    </div>
								    <input type="text" class="form-control form-control-solid" name="youtube_url" value="{{$admin_user->youtube_url}}" />
								   </div>
								   <span class="form-text text-muted">Please enter your Youtube website URL.</span>
								   @error('youtube_url')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
								  </div>
								 </div>
								 <div class="form-group row">
								  <label class="col-3">Instagram</label>
								  <div class="col-lg-9">
								   <div class="input-group">
								    <div class="input-group-prepend">
								     <span class="input-group-text">
								      <i class="flaticon2-website"></i>
								     </span>
								    </div>
								    <input type="text" class="form-control form-control-solid" name="instagram_url" value="{{$admin_user->instagram_url}}" />
								   </div>
								   <span class="form-text text-muted">Please enter your Instagram website URL.</span>
								   @error('instagram_url')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
								  </div>
								 </div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20 change-paasword">
		                    <div class="col-lg-12">
		                        <button class="btn btn-primary" type="submit">   {{ __('Update Profile') }}</button>

		                       <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
							</div>
                		</div>
                		
						</div>
					</form>
					
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	FormValidation.formValidation(
 document.getElementById('edit-profile'),
 {
  fields: {
  	name: {
    validators: {
     notEmpty: {
      message: 'Name is required'
     },
    }
   },
   address: {
    validators: {
     notEmpty: {
      message: 'Address is required'
     },
    }
   },
   state: {
    validators: {
     notEmpty: {
      message: 'State is required'
     },
    }
   },
   city: {
    validators: {
     notEmpty: {
      message: 'City is required'
     },
    }
   },
   email: {
    validators: {
     notEmpty: {
      message: 'Email is required'
     },
     emailAddress: {
      message: 'The value is not a valid email address'
     }
    }
   },
   phone: {
    validators: {
     notEmpty: {
      message: 'Phone number is required'
     },
      digits: {
      message: 'The velue is not a valid digit'
     }
    }
   },
   gender: {
    validators: {
     notEmpty: {
      message: 'Gender is required'
     },
    }
   },
   zip: {
    validators: {
    	notEmpty: {
      	message: 'Zip is required'
     },
     digits: {
      message: 'The velue is not a valid digit'
     }
    }
   },
   facebook_url: {
    validators: {
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
   google_url: {
    validators: {
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
   twitter_url: {
    validators: {
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
   youtube_url: {
    validators: {
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
   instagram_url: {
    validators: {
      uri: {
      message: 'The website address is not valid'
     }
    }
   },
  },

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);
    </script>
    <style type="text/css">
    	.view_profile_details.card-body {
    text-align: center;
}

.view_profile_details img {max-width: 90px;height: auto !important;border-radius: 50%;}

.view_profile_details h3 {
    font-size: 16px;
    margin: 15px 0px 10px;
}

.view_profile_details h6 {
    color: #c5baba;
}
.change-paasword {
    width: 100%;
    
}
form#edit-profile{
	min-height: 400px;
}
.view_profile_details.card-body 
{
	min-height: 480px;
}

.card.admin_image_sec .view_profile_details.card-body {
    min-height: auto;
}
.admin_basic_detail {
    padding: 15px;
    border-top: 2px solid #e4e4e4;
}

.admin_basic_detail .show_email {}

.admin_basic_detail span {
    color: #c5baba;
}

    </style>
@endsection
