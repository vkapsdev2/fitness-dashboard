 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Change Password')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Change Password<i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('admin.profile', [Auth::user()->id]) }}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">
				@if(Session::has('error'))
					<div class="alert alert-danger alert-block">
    					<button type="button" class="close" data-dismiss="alert">×</button>    
    					<strong>{{ session('error') }}</strong>
				 </div> 
			@endif

				@if(Session::has('success'))
					<div class="alert alert-success alert-block">
    					<button type="button" class="close" data-dismiss="alert">×</button>    
    					<strong>{{ session('success') }}</strong>
					</div>   
				@endif
				 
				<!--begin::Form-->
				<form class="form"  method="POST" action="{{ route('admin.updatePassword') }}" accept-charset="utf-8" enctype="multipart/form-data" id="update-password" name="update-password">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Change Password:</h3>
								<div class="form-group row">
									<label class="col-3">Old Password</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="old_password" value="{{ old('old_password') }}" placeholder="Enter old Password"/>
										@error('old_password')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
                    				</div>
								</div>

								<div class="form-group row">
									<label class="col-3">New Password</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="new_password" value="{{ old('new_password') }}" placeholder="Enter new password" />
										@error('new_password')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
										
									</div>
									
								</div>
								<div class="form-group row">
									<label class="col-3">Confirm Password</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="confirm_password" value="{{ old('confirm_password') }}" placeholder="Enter confirm password"/>
										@error('confirm_password')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
								</div>
								</div>
							</div>
						</div>
					</div>
					
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit" id="update-password-btn">  {{ __('Update Password') }}</button>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?fgf') }}" type="text/javascript"></script>
      <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
		<!--end::Page Vendors-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
		
    <script type="text/javascript">
    	// Class definition

var KTCkeditor = function () {
    // Private functions
    var demos = function () {
        ClassicEditor
            .create( document.querySelector( '#kt-ckeditor-3' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

// Initialization
</script>
<script type="text/javascript">
	
	FormValidation.formValidation(
 document.getElementById('update-password'),
 {
  fields: {
   old_password: {
    validators: {
     notEmpty: {
      message: 'Old password is required'
     },
    }
   },
   new_password: {
    validators: {
     notEmpty: {
      message: 'New password is required'
     },
    }
   },
   
   confirm_password:{
   	validators: {
     notEmpty: {
      message: 'Confirm password is required.'
     },
    },
    identical: {
              compare: function compare() {
                return form.querySelector('[name="new_password"]').value;
              },
              message: 'The password and its confirm are not the same'
            }
   },
   
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>
@endsection
