 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edot Exercise')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Exercise <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('btSchedule_flex_flow.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">
				@if($exercise->video_file)
					@php
					$ext = $exercise->video_file; 
					$ext = explode(".",$ext);
					@endphp
				@endif
				<!--begin::Form-->
				<form class="form" id="flex-flow-edit" method="POST" action="{{ route('btSchedule_flex_flow.update',[$exercise->id]) }}"accept-charset="utf-8" enctype="multipart/form-data">
					  @csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Exercise Info:</h3>
								<div class="form-group row" >
									<label class="col-3">Day<span style="color:red">*</span></label>
									<div class="col-9">
										<select class="form-control" name="day" id="day">
					                        <option value= "" >Select Day</option>
					                        <option value="Monday" {{ ($exercise->day == 'Monday' ? "selected":"") }}>Monday</option>
					                        <option value="Tuesday" {{ ($exercise->day == 'Tuesday' ? "selected":"") }}>Tuesday</option>
					                        <option value="Wednesday" {{ ($exercise->day == 'Wednesday' ? "selected":"") }}>Wednesday</option>
					                        <option value="Thursday" {{ ($exercise->day=='Thursday' ? "selected":"") }}>Thursday</option>
					                        <option value="Friday" {{ ($exercise->day == 'Friday' ? "selected":"") }}>Friday</option>
					                        <option value="Saturday" {{ ($exercise->day =='Saturday' ? "selected":"") }}>Saturday</option>
					                        <option value="Sunday" ($exercise->day =='Sunday' ? "selected":"") >Sunday</option>
					                      </select>
					                      <div class="ajax_message" style="color:red"></div>
					                      @error('day')
					                        <div class="alert alert-danger">{{ $message }}</div>
					                      @enderror
									</div> 
								</div>
								<div class="form-group row">
									<label class="col-3">Coach<span style="color:red">*</span></label>
									<div class="col-9">
										<select class="form-control" name="coach_id" id="coach_id">
					                        <option value= "" >Select Coach</option>
					                        @foreach ($coaches as $value)
						                        <option value="{{$value->id}}"  @if($value->id == $exercise->coach_id) {{'selected'}} @endif >{{$value->name}}</option>
						                    @endforeach
					                      </select>
					                      <div class="ajax_message" style="color:red"></div>
					                      @error('coach_id')
					                        <div class="alert alert-danger">{{ $message }}</div>
					                      @enderror
									</div> 
								</div>
								<div class="form-group row">
				                    <label class="col-3">Start Time <span style="color:red">*</span></label>
				                    <div class="col-9">
					                    <div class="input-group date">
					                        <input class="form-control" id="start_time" placeholder="Select time" name="start_time" type="text" value="{{ $exercise->start_time }}"/>
					                     </div>
					                     <div class="ajax_message" style="color:red"></div>
					                     <div class="time-diff-start" style="color:red"></div>
					                    @error('start_time')
					                        <div class="alert alert-danger">{{ $message }}</div>
					                    @enderror
				                    </div>
				                </div>
				                <div class="form-group row">
				                    <label class="col-3">End Time <span style="color:red">*</span></label>
				                    <div class="col-9">
					                    <div class="input-group date">
					                        <input class="form-control" id="end_time" placeholder="Select time" name="end_time" type="text" value="{{ $exercise->end_time }}"/>
					                     </div>
					                     <div class="time-diff-end" style="color:red"></div>
					                    @error('end_time')
					                        <div class="alert alert-danger">{{ $message }}</div>
					                    @enderror
				                    </div>
				                </div>
				                <div class="form-group row">
									<label class="col-3">Banner<span style="color:red">*</span></label>
									<div class="col-9">
										<img style="height: 50px;" src="{{ url('/'). '/public/media/BTSchedule/flexFlow/'.$exercise->banner}}" />
										<input class="form-control" type="file" name="banner" value="{{public_path().'/media/BTSchedule/flexFlow/'.$exercise->banner}}"/>
									</div>
								</div>
								<div class="form-group row">
							        <label class="col-3 col-form-label">Video File Or URL</label>
							        <div class="col-9 col-form-label">
							            <div class="radio-inline">
							                <label class="radio">
							                    <input type="radio" name="video" checked="checked" id="file" value="file" @if($exercise->video_file) {{'checked'}} @else {{ ' ' }} @endif />
							                    <span></span>
							                    Video File
							                </label>
							                <label class="radio">
							                    <input type="radio" name="video" id="url" value="url" @if($exercise->video_url) {{'checked'}} @else {{ ' ' }} @endif />
							                    <span></span>
							                    Video URL
							                </label>
							            </div>
							            <span class="form-text text-muted"></span>
							        </div>
							    </div>

								<div class="form-group row" id="video_file" <?php if($exercise->video_url){ ?> style="display: none;" <?php }  ?> >
										<label class="col-3">Video File</label>
										<div class="col-9">
											@if($exercise->video_file)

											<video width="320" height="240" controls>
									  			<source src="{{ url('/'). '/public/media/BTSchedule/flexFlow-video/'.$exercise->video_file}}" type='video/{{$ext[1]}}'>
									  			Your browser does not support the video tag.
											</video>

											@endif
											<input class="form-control" type="file" name="video_file" value=" @if($exercise->video_file) {{public_path().'/media/BTSchedule/flexFlow-video/'.$exercise->video_file}} @endif " />
										</div>
									</div>
									
									<div class="form-group row" id="video_url" <?php if($exercise->video_file){ ?> style="display: none;" <?php }  ?> >
										<label class="col-3">Video URL</label>
										<div class="col-9">
											@if($exercise->video_url)
												
												<?php $video_url = $exercise->video_url;
												if(strpos($video_url, "vimeo.com") !== false){

												   $video_url = str_replace('vimeo.com/', 'player.vimeo.com/video/', $video_url);
												} else{
												    if(strpos($video_url, "vimeo.com") !== false){
													  $video_url = str_replace('vimeo.com/', 'player.vimeo.com/video/', $video_url);
													} else{
														$video_url = str_replace('watch?v=', 'embed/', $video_url);
													}
												}
												 ?>
												<iframe width="420" height="345" src="{{$video_url}}"> </iframe>
											@endif
											<input class="form-control " type="text" name="video_url" value="@if($exercise->video_url){{$exercise->video_url}}@endif"/>
											<span class="form-text text-muted">Please enter your Video URL</span>
												@error('video_url')
													<div class="alert alert-danger">{{ $message }}</div>
												@enderror
										</div>
									</div>
				                <div class="form-group row">
									<label class="col-3">Description</label>
									<div class="col-9">
										<textarea name="description" class="form-control" data-provide="description" id="kt-ckeditor-1">{{ $exercise->description }}</textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-2"></div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
    <script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	$('#start_time, #end_time').timepicker({
    		format: "hh:ii",
		    minuteStep: 1,
		    showSeconds: true,
		    showMeridian: true
	    });
    </script>
    <script type="text/javascript">
    	FormValidation.formValidation(
			 document.getElementById('flex-flow-edit'),
			 {
			  fields: {
			   coach_id: {
			    validators: {
			     notEmpty: {
			      message: 'Coach is required'
			     }
			    }
			   },
			   day: {
			    validators: {
			     notEmpty: {
			      message: 'Day is required'
			     }
			    }
			   },

			   banner: {
			    validators: {
				    file: {
	                    extension: 'jpeg,png,jpg,gif,svg',
	                    message: 'The selected file is not valid'
                        }
			    }
			   },
			   start_time: {
			    validators: {
				    notEmpty: {
				      message: 'Exercise Start time is required'
				    },
				    
			    }
			   },
			   end_time: {
			    validators: {
				    notEmpty: {
				      message: 'Exercise End time is required'
			     	},
			    }
			   },
			   video_url: {
				    validators: {
				     uri: {
				      message: 'The Video address is not valid'
				     }
				    }
				},
				video_file: {
				    validators: {
				        file: {
	                        extension: 'mp4,mov,ogg,qt',
	                        maxSize: 2000*1024,
	                        message: 'The selected file is not valid'
                        }
				    }
				},
			  },

			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			  },
			}
		);
    </script>
    <script type="text/javascript">
    	$(document).ready(function() {
		    $("input[name$='video']").click(function() {
		        var video = $(this).val();
		        console.log('audio', video);
		        if(video == 'file'){
		        	$("#video_url").hide();
		        	$("#video_file").show();
		        }if(video == 'url'){
		        	$("#video_file").hide();
		        	$("#video_url").show();
		        }
		        
		    });
		});
		
    	// Class definition
		var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		        ClassicEditor
		            .create( document.querySelector( '#kt-ckeditor-1' ) )
		            .then( editor => {
		                console.log( editor );
		            } )
		            .catch( error => {
		                console.error( error );
		            } );
		    }

		    return {
		        // public functions
		        init: function() {
		            demos();
		        }
		    };
		}();	
    </script>
    <script type="text/javascript">

    	$('#start_time').on("change", function(event){
            event.preventDefault();
            var start_time = $("#start_time").val();
            var end_time = $("#end_time").val();
            var coach_id = $('#coach_id option:selected').val();
            var day = $('#day option:selected').val();

            customFormValidation(day,coach_id,start_time);

             
            timeDiff(start_time,end_time);


        });

        $('#end_time').on("change", function(event){
            event.preventDefault();

            var start_time = $("#start_time").val();
            var end_time = $("#end_time").val();
            timeDiff(start_time,end_time);
        });


        $('#day').on('change', function(event){
        	event.preventDefault();
        	 var day = $('#day option:selected').val();
        	 var coach_id = $('#coach_id option:selected').val();
        	 var start_time = $("#start_time").val();

        	 customFormValidation(day,coach_id,start_time);
        
        });

        $('#coach_id').on('change', function(event){
        	event.preventDefault();
        	 var day = $('#day option:selected').val();
        	 var coach_id = $('#coach_id option:selected').val();
        	 var start_time = $("#start_time").val();

        	 customFormValidation(day,coach_id,start_time);
        
        });

        function customFormValidation(day,coach_id,start_time){
        	if(day && coach_id && start_time){
        	 	console.log('all data', day, coach_id,start_time);
        	 	var formData = {
		            day: day,
			        coach_id: coach_id,
			        start_time: start_time
		        };

				$.ajax({
                	type: "GET",
		            url: 'https://vkapsprojects.com/pedro/skeleton/public/schedule_classes/flex_flow/ajax_validation/',
		            data: formData,
		            dataType: 'json',
		            success: function (data) {
		            	console.log(data.success);
		            	if(data.success){
		            		$("div.ajax_message").html('<div>This slot already taken</div>');
		            		$("#day").addClass('is-invalid');
		            		$("#coach_id").addClass('is-invalid');
		            		$("#start_time").addClass('is-invalid');
		            	}else{
		            		$("div.ajax_message").html('<div></div>');
		            		$("#day").removeClass('is-invalid').addClass('is-valid');
		            		$("#coach_id").removeClass('is-invalid').addClass('is-valid');
		            		$("#start_time").removeClass('is-invalid').addClass('is-valid');
		            	}
	                  
		            },
		            error: function (data) {
		                console.log(data);
		            }
                });
        	}
        }

        function timeDiff(start_time,end_time){
        	var diff = ( new Date("1970-1-1 " + end_time) - new Date("1970-1-1 " + start_time) ) / 1000 / 60 / 60; 
        	var time_diff = diff.toFixed(0);
        	if(time_diff <= 0){
        		$("div.time-diff-start").html('<div>Start Time less than end time</div>');
        		$("div.time-diff-end").html('<div>End Time greater than end time</div>');
        		$("#end_time").addClass('is-invalid');
        		$("#start_time").addClass('is-invalid');
        	}else{
        		$("div.time-diff-start").html('<div></div>');
        		$("div.time-diff-end").html('<div></div>');
        		$("#end_time").removeClass('is-invalid').addClass('is-valid');
        		$("#start_time").removeClass('is-invalid').addClass('is-valid');
        	}
        	
        }
    </script>
@endsection
