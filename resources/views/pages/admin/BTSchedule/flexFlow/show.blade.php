 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Show')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Express Class <i class="mr-2"></i>
						</h3>
					</div> 
					<div class="card-toolbar">
						<a href="{{route('btSchedule_flex_flow.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="my-5 detail_view">
							<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
							<div class = "detail_img text-center">
								<img style= "height:200px;" src="{{ url('/'). '/public/media/BTSchedule/flexFlow/'.$exercise->banner}}"/>
							</div>
							<div class="detail_description">
								{!!$exercise->description!!}
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
</style>