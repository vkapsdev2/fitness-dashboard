 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Show')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							{{ $exercise->exercise_name }} <i class="mr-2"></i>
						</h3>
					</div> 
					<div class="card-toolbar">
						<a href="{{route('schedule_exercise.index',[$exercise->class_id]) }}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							
								<div class = "detail_img text-center">
									@if ($exercise->video_url)

									<?php $video_url = $exercise->video_url;
									$video_url = str_replace('watch', 'embed', $video_url); ?>
									<iframe width="420" height="345" src="{{$video_url}}"> </iframe>
									@elseif($exercise->video_file)
										<video width="320" height="240" controls>
										  <source src="{{ url('/'). '/public/media/class-exercise-video/'.$exercise->video_file}}" >
										  Your browser does not support the video tag.
										</video>
									@else
									<img style= "height:200px;" src="{{ url('/'). '/public/media/book-banner/Product-Dummy.jpg'}}"/>
									@endif
								</div>
								<div class ="detail_name text-center">
								<h3><b>{{ $exercise->exercise_name }} </b></h3>
								</div>
									<div class="detail_description">
									{!!$exercise->description!!}
								</div>
							</div>
 						</div>
				
					
						
					
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
</style>