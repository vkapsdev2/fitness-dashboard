 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Daily Affirmation')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							{{$row->title}} File<i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('mindset_detail_file.list',[$row->id])}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" id="files-add" method="POST" action="{{ route('mindset_detail.save_file',[$row->id]) }}" accept-charset="utf-8" enctype="multipart/form-data">
					  @csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<div class="form-group row">
							        <label class="col-3 col-form-label">audio File Or URL</label>
							        <div class="col-9 col-form-label">
							            <div class="radio-inline">
							                <label class="radio">
							                    <input type="radio" name="audio" checked="checked" id="file" value="file" />
							                    <span></span>
							                    audio File
							                </label>
							                <label class="radio">
							                    <input type="radio" name="audio" id="url" value="url" />
							                    <span></span>
							                    audio URL
							                </label>
							            </div>
							            <span class="form-text text-muted"></span>
							        </div>
							    </div>

								<div class="form-group row" id="audio_file">
									<label class="col-3">audio File</label>
									<div class="col-9">
										<input class="form-control" type="file" name="audio_file" value="{{ old('audio_file') }}"/>
										@error('audio_file')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror 
									</div>
								</div>

								<div class="form-group row" id="audio_url" style="display: none;">
									<label class="col-3">audio URL</label>
									<div class="col-9">
										<input class="form-control" type="text" name="audio_url" value="{{ old('audio_url') }}" placeholder="Enter audio URL" />
										<span class="form-text text-muted">Please enter your audio URL</span>
										@error('audio_url')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">PDF File</label>
									<div class="col-9">
										<input class="form-control" type="file" name="pdf_file" value="{{ old('pdf_file') }}"/>
										@error('pdf_file')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror 
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-2"></div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	FormValidation.formValidation(
			 document.getElementById('files-add'),
			 {
			  fields: {
			   audio_url: {
				    validators: {
				     uri: {
				      message: 'The audio address is not valid'
				     }
				    }
				},
				audio_file: { 
				    validators: {
				        file: {
	                        extension: 'wav,mp3,au',
	                        maxSize: 2000*1024,
	                        message: 'The selected file is not valid'
                        }
				    }
				},
			   	
			   	pdf_file:{
			   		validators: {
				        file: {
	                        extension: 'pdf',
	                        maxSize: 2000*1024,
	                        message: 'The selected file is not valid'
                        }
				    }
			   	}
			  },

			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			  }
			 }
		);
    </script>
    <script type="text/javascript">
    	$(document).ready(function() {
		    $("input[name$='audio']").click(function() {
		        var audio = $(this).val();
		        console.log('audio', audio);
		        if(audio == 'file'){
		        	$("#audio_url").hide();
		        	$("#audio_file").show();
		        }if(audio == 'url'){
		        	$("#audio_file").hide();
		        	$("#audio_url").show();
		        }
		        
		    });
		});
    </script>
@endsection
