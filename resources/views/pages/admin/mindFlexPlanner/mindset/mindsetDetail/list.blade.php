 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Daily Affirmations')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}
 
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
          <div class="card-title">
            <h3 class="card-label">Mindset
            <span class="d-block text-muted pt-2 font-size-sm">MindSet Details</span></h3>
          </div>  
        </div>
      </div>
        </div> 
    </div>
    <div class="row">
      <div class="col-lg-3">
        <div class="card card-custom gutter-b">
         <div class="card-body">
          <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
            <?php
              $start_here = 1;
             ?> 
            <a href="{{ route('mindset_detail.show',[$start_here]) }}" class="h4 text-dark text-hover-primary mb-5">
                Start Here
            </a>
          </div>
         </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card card-custom gutter-b">
         <div class="card-body">
          <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
            <?php
              $resource = 2;
             ?> 
            <a href="{{ route('mindset_detail.show',[$resource]) }}" class="h4 text-dark text-hover-primary mb-5">
                Resources
            </a>
          </div>
         </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card card-custom gutter-b">
         <div class="card-body">
          <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
            <a href="{{ route('mindset_detail_lesson.list') }}" class="h4 text-dark text-hover-primary mb-5">
                Life Without Borders
            </a>
          </div>
         </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card card-custom gutter-b">
         <div class="card-body">
          <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
            <?php
              $success = 3;
             ?> 
            <a href="{{ route('mindset_detail.show',[$success]) }}" class="h4 text-dark text-hover-primary mb-5">
                Success
            </a>
          </div>
         </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3">
        <div class="card card-custom gutter-b">
         <div class="card-body">
          <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
            <?php
              $beyond_sight = 4;
            ?>
            <a href="{{ route('mindset_detail.show',[$beyond_sight]) }}" class="h4 text-dark text-hover-primary mb-5">
                Beyond Your Sight
            </a>
          </div>
         </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card card-custom gutter-b">
         <div class="card-body">
          <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
            <?php
              $world_around = 5;
            ?>
            <a href="{{ route('mindset_detail.show',[$world_around]) }}" class="h4 text-dark text-hover-primary mb-5">
                The World Around You
            </a>
          </div>
         </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card card-custom gutter-b">
         <div class="card-body">
          <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
            <?php
              $ideal = 6;
            ?>
            <a href="{{ route('mindset_detail.show',[$ideal])}}" class="h4 text-dark text-hover-primary mb-5">
                Creating The Ideal You
            </a>
          </div>
         </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card card-custom gutter-b">
         <div class="card-body">
          <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
            <?php
              $legancy = 7;
            ?>
            <a href="{{ route('mindset_detail.show',[$legancy])}}" class="h4 text-dark text-hover-primary mb-5">
                Building a Legancy
            </a>
          </div>
         </div>
        </div>
      </div>
    </div>
@endsection

{{-- Scripts Section --}}

@section('scripts')
  <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/pages/custom.js?fhcv') }}" type="text/javascript"></script>
@endsection
