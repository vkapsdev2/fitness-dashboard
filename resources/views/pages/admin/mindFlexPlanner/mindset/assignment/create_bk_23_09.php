 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Assignment')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}
 
    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Assignment <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('assignment.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i> 
							Back
						</a>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Form-->
					<form class="form" id="assignment-add" method="POST" action="{{ route('assignment.save') }}" accept-charset="utf-8" enctype="multipart/form-data">
						  @csrf
						<div class="row">
							<div class="col-xl-2"></div>
							<div class="col-xl-8">
								<div class="my-5">  
									<h3 class=" text-dark font-weight-bold mb-10">Assignment Info:</h3>
									<div class="form-group row">
										<label class="col-3">Customer Name <span style="color:red">*</span></label>
										<div class="col-9">
											<select class="form-control" name="customer_id" id="customers">
												<option disabled="disabled">Select Customer</option>
						                          @foreach ($customers as $value)
						                            <option value="{{$value->id}}">{{$value->name}}</option>
						                          @endforeach
						                      </select>
						                      @error('customer_id')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>
									<div class="form-group row">
										<label class="col-3">Workout Type<span style="color:red">*</span></label>
										<div class="col-9">
											<select class="form-control" name="workout_category">
						                        <option value= "" >Select Workout</option>
						                        <option value="1">In Home Workout</option>
						                        <option value="2">Gym Workout</option>
						                      </select>
						                      @error('workout_category')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>
									<div class="form-group row" id="workout_list_div">
										<label class="col-3">Category List <span style="color:red">*</span></label>
										<div class="col-9">
											<select class="form-control" name="workout_list" id="workout_list_option">
						                        <option value= "" >Select Workout Type</option>
						                       
						                      </select>
						                      @error('workout_list')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>

									<div class="form-group row" id="workout_exercise_div">
										<label class="col-3">Exercise <span style="color:red">*</span></label>
										<div class="col-9" id="exercises_options">
											
						                      @error('workout_exercise')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>
 
									<div class="form-group row ">
										<label class="col-3">Total Week <span style="color:red">*</span></label>
										<div class="col-9">
											<input class="form-control " type="number" name="week" value="" placeholder="Enter Number of Week" id="total_week" />
											@error('week')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>

									<div class="form-group row">
										<label class="col-3">Days <span style="color:red">*</span></label>
										<div class="col-9">
						                      <table class="table table-bordered" id="dynamic_days">  
			                                    <tr>  
			                                        <td>
			                                         	<select class="form-control days" name="days[]" multiple>
									                        <option value= "" >Select Days</option>
									                        <option value= "1" >Monday</option>
									                        <option value= "2" >Tuesday</option>
									                        <option value= "3" >Wednesday</option>
									                        <option value= "4" >Thursday</option>
									                        <option value= "5" >Friday</option>
									                        <option value= "6" >Saturday</option>
									                        <option value= "7" >Sunday</option>
									                      </select>
									                      @error('days')
									                        <div class="alert alert-danger">{{ $message }}</div>
									                      @enderror
			                                        </td>
			                                        <td>
														<select class="form-control" name="days_excercise[]" id="days_excercise">
			                                         		<option value="">Select</option>
			                                         	</select>
			                                        </td>
			                                        <td>
			                                         	<button type="button" name="add_days" id="add_days" class="btn btn-success">Add More</button>
			                                        </td>  
			                                    </tr>  
			                               </table>
										</div> 
									</div>

									<div class="form-group row">
									    <label class="col-3">Rest Time <span style="color:red">*</span></label>
									    <div class="col-9">
									      <input class="form-control" id="rest_timing" placeholder="Select time" name="rest_timing" type="text"/>
									      @error('rest_timing')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
									    </div>
									</div> 

									<div class="form-group row">  
										<label class="col-3">Sets </label>
										<div class="col-9">
			                               <table class="table table-bordered" id="dynamic_sets">  
			                                    <tr>  
			                                        <td>
			                                         	<input type="text" name="weight[]" placeholder="Enter Weight" class="form-control name_list" id="weight" />
			                                        </td>
			                                        <td>
			                                         	<input type="text" name="reps[]" placeholder="Enter Reps" class="form-control name_list" />
			                                        </td>

			                                        <td>
														<select class="form-control" name="set_excercise[]" id="set_excercise">
			                                         		<option value="">Select</option>
			                                         	</select>
			                                        </td>
			                                        <td>
			                                         	<button type="button" name="add" id="add" class="btn btn-success">Add More</button>
			                                        </td>  
			                                    </tr>  
			                               </table>  
		                             	</div>
		                            </div>  

								</div>
							</div>
							<div class="col-xl-2"></div>
							<div class="form-group text-center m-t-20">
			                    <div class="col-xs-12">
			                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
			                    </div>
	                		</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
            </div>
        </div>   
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script>  
	    $(document).ready(function(){  
	    	$('.days').selectpicker(); //multiple select
	    	var select_exercise;
	    	$("#total_week"). click(function(){
				var exercise_days = "<option disabled='disabled'>Select  Exercise</option>";
					//exercise_val. push($(this). val());
					var count = 0;
				$('input[name="workout_exercise"]:checked').each(function() {
					var exercise_val =this.value;
					
					var exercise_text = $(this).attr('text');
					

					 exercise_days += "<option value ='"+exercise_val+"'>"+exercise_text+"</option>";
					 count++;
				});
				$('#days_excercise').html(exercise_days);
				$('#set_excercise').html(exercise_days);

				select_exercise = exercise_days;
				
				
			});
	        var i=1;  
	        $('#add_days').click(function(){  
	           i++;  
	           console.log('select_exercise',select_exercise);
	           $('#dynamic_days').append('<tr id="row'+i+'"><td><select class="form-control days" name="days[]" multiple><option value= "" >Select Days</option><option value= "1" >Monday</option><option value= "2" >Tuesday</option><option value= "3" >Wednesday</option><option value= "4" >Thursday</option><option value= "5" >Friday</option><option value= "6" >Saturday</option><option value= "7" >Sunday</option></select></td><td><select class="form-control" name="days_excercise[]" id="days_excercise">'+select_exercise+'</select></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove_days">X</button></td></tr>');  
	           $('.days').selectpicker();
	        }); 
		    $(document).on('click', '.btn_remove_days', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });
	       
			var j=1;
	        $('#add').click(function(){  
	           j++;  
	           $('#dynamic_sets').append('<tr id="row'+j+'"><td><input type="text" name="weight[]" placeholder="Enter Weight" class="form-control name_list" /></td><td><input type="text" name="reps[]" placeholder="Enter Reps" class="form-control name_list" /></td> <td><select class="form-control" name="set_excercise[]" id="set_excercise">'+select_exercise+'</select></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
	        });  
	        $(document).on('click', '.btn_remove', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        }); 

	          
	         
	   });  
	</script>

    <script type="text/javascript">
    	$('#rest_timing').timepicker({
		   minuteStep: 1,
		   defaultTime: '',
		   showSeconds: true,
		   showMeridian: false,
		   snapToStep: true
		 });

    	FormValidation.formValidation(
			 document.getElementById('assignment-add'),
			 {
			  fields: {
			   'customer_id[]': {
				    validators: {
				        notEmpty: {
				        	message: 'Customer name is required'
				        }
			        }
			   },

			   rest_timing: {
			    validators: {
			     notEmpty: {
			      message: 'Rest Timing is required'
			     }
			    }
			   },
			   
			    workout_category: {
				    validators: {
				     	notEmpty: {
				      		message: 'Workout Type is required'
				     	},
				    }
			   },
			    workout_list: {
				    validators: {
				     	notEmpty: {
				      		message: 'Workout is required'
				     	},
				    }
			    },
			    'workout_exercise[]': {
				    validators: {
				     	notEmpty: {
				      		message: 'Exercise is required'
				     	},
				    }
			    },
			   week: {
				    validators: {
				     	notEmpty: {
				      		message: 'Week is required'
				     	},
				    }
			   },
			   'days[]': {
				    validators: {
					    choice: {
					      min:2,
					      max:5,
					      message: 'Please check at least 2 and maximum 5 options'
					    }
					}
			   },
			  },
			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			  }
			 }
		);
    </script>
    <script type="text/javascript">
    	//On change Workout
    	$('select[name="workout_category"]').change(function(){
		    if ($(this).val() == "1"){
		    	$.ajax('https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/home_workout_list', {
		            method: 'GET',
		            headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		        }).done(function (r) {
		        	if(r.status){
		        		console.log('r.status',r.status);
		        		var workout_list = r.success;
		        		var workout_option = "<option value=''>Select Categories</option>";
		        		$(workout_list).each(function(index,item){
		                    var name = item.name;
		                    var id = item.id;

		                    workout_option += "<option value ='"+id+"'>"+name+"</option>";
	                  });
	                  $('#workout_list_option').html(workout_option);
	                  $("#workout_exercise_div").addClass("home_exercises");
	                  $("#workout_exercise_div").removeClass("gym_exercises");
			        }
		        });


		    } else if($(this).val() == "2"){
		        $.ajax('https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/gym_workout_list', {
		            method: 'GET',
		            headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		        }).done(function (r) {
		        	if(r.status){
		        		console.log('r.status',r.status);
		        		var workout_list = r.success;
		        		var workout_option = "<option value=''>Select Categories</option>";
		        		$(workout_list).each(function(index,item){
		                    var name = item.name;
		                    var id = item.id;

		                    workout_option += "<option value ='"+id+"'>"+name+"</option>";
	                  });
	                  $('#workout_list_option').html(workout_option);
	                  $("#workout_exercise_div").addClass("gym_exercises");
	                  $("#workout_exercise_div").removeClass("home_exercises");
			        }
		        });
		    }   
		})
		
		//On change Home Workout
		$('select[name="workout_list"]').change(function(){
			;
			var $select = $('#exercises_options');
			
			if ($('#workout_exercise_div').hasClass('home_exercises')){
			 	console.log('home_exercises');
			 	var home_workout_id = $(this).val(); 

			    if(home_workout_id){

	                $.ajax('https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/home_workout_exercise/'+home_workout_id, {
			            method: 'GET',
			            headers: {
			              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			            },
			        }).done(function (r) {
			        	if(r.status){ 
			        		
			        		//$select.html();

			        		var exercise_list = r.success;
			        		var exercise_option = $select.html();
			        		//var exercise_option = "<option disabled='disabled'>Select Workout Exercise</option>";

			        		$(exercise_list).each(function(index,item){
			                    var name = item.sub_name;
			                    var id = item.id;

			                    exercise_option += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='workout_exercise' value = '"+id+"' text='"+name+"'/><span></span>"+name+"</label>";
			                   // exercise_option += "<option value ='"+id+"'>"+name+"</option>";
		                  });
			        		
		                  $('#exercises_options').html(exercise_option)
		                 // $('#exercises_options').html(exercise_option).attr('multiple','multiple').selectpicker('refresh');
				        }
			        });
			    }
			} 
			else if($('#workout_exercise_div').hasClass('gym_exercises')){
				var gym_workout_id = $(this).val();  
			    if(gym_workout_id){

	                $.ajax('https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/gym_workout_exercise/'+gym_workout_id, {
			            method: 'GET',
			            headers: {
			              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			            },
			        }).done(function (r) {
			        	if(r.status){
			        		//$select.html('');
			        		var exercise_list = r.success;
			        		var exercise_option = $select.html();
			        		$(exercise_list).each(function(index,item){
			                    var name = item.sub_name;
			                    var id = item.id;

			                      exercise_option += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='workout_exercise' value = '"+id+"' text='"+name+"'/><span></span>"+name+"</label>";
		                  });
		                  $('#exercises_options').html(exercise_option);
		                //  $('#exercises_options').html(exercise_option).html(exercise_option).attr('multiple','multiple').selectpicker('refresh');
				        }
			         });
			    }
			}    
		})	
    </script>

@endsection
