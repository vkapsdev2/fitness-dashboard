{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Assignment')

{{-- Content --}}
@section('content')  

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							edit Assignment<i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('assignment.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Form-->
					<form class="form" id="assignment-edit" method="POST" action="{{ route('assignment.update',[$assignment->id]) }}" accept-charset="utf-8" enctype="multipart/form-data">
						  @csrf
						  <div class="row">
							<div class="col-xl-2"></div>
							<div class="col-xl-8">
								<div class="my-5">
									<h3 class=" text-dark font-weight-bold mb-10">Assignment Info:</h3>
									<div class="form-group row">
										<label class="col-3">Customer Name</label>
										<div class="col-9">
						                      <select class="form-control" name="customer_id" id="customers">
												<option disabled="disabled">Select Customer</option>
						                          @foreach ($customers as $value)
						                     <option value="{{$value->id}}" @if($value->id == $assignment->customer_id) {{'selected'}} @endif >{{$value->name}}</option>
						                          @endforeach
						                      </select>
						                      @error('customer_id')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>
									<div class="form-group row">
										<label class="col-3">Workout Type</label>
										<div class="col-9">
											<select class="form-control" name="workout_category">
						                        <option value= "" >Select Workout</option>
						                        <option value="1" {{$assignment->workout_category == '1'  ? 'selected' : ''}}>Home Workout</option>
						                        <option value="2" {{$assignment->workout_category == '2' ? 'selected' : ''}}>Gym Workout</option>
						                      </select>
						                      @error('workout_category')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>

									<div class="form-group row" id="workout_list_div">
										<label class="col-3">Category List <span style="color:red">*</span></label>
										<div class="col-9">
											<select class="form-control" name="workout_list[]" id="workout_list_option" multiple>
						                       	@if($assignment->workout_category == '1')
						                       		<option value= "" disabled="">Home Categories List</option>
						                       		@foreach ($home_workouts as $value)
						                            <option value="{{$value->id}}"  <?php
									                    foreach ($assignment->workout_id as $workout_id) {
									                    	if ($workout_id == $value->id) { echo 'selected';}
									                    	else { echo '';}
									                    }
									                ?> >{{$value->name}}</option>
						                          @endforeach
						                       	@else
						                       		<option value= "" disabled="">Gym Categories List</option>
						                       		@foreach ($gym_workouts as $value)
						                            <option value="{{$value->id}}"  <?php
									                    foreach ($assignment->workout_id as $workout_id) {
									                    	if ($workout_id == $value->id) { echo 'selected';}
									                    	else { echo '';}
									                    }
									                ?> >{{$value->name}}</option>
						                          @endforeach
						                       	@endif
						                      </select>
						                      @error('workout_list')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>

									<div class="form-group row" id="workout_exercise_div">
										<label class="col-3">Exercise <span style="color:red">*</span></label>
										<div class="col-9" id="exercises_options">
											@if($assignment->workout_category == '1')
												@foreach ($home_exercises_list as $value)
													
									                @foreach($assignment->exercises_id as $exercises_id) 
									                    @if ($exercises_id == $value->id) 
									                    	<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' checked ='checked' name='workout_exercise[]' value = {{$value->id}} text={{$value->id}}    /><span></span>{{$value->sub_name}}</label>
									                    @endif
									                @endforeach
												@endforeach
											@else
												@foreach ($gym_exercises_list as $value)
													
									                @foreach($assignment->exercises_id as $exercises_id)
									                    @if ($exercises_id == $value->id) 
									                    	<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' checked ='checked' name='workout_exercise[]' value = {{$value->id}} text={{$value->id}} /><span></span>{{$value->sub_name}}</label>
									                    @endif
									                @endforeach
												@endforeach
											@endif
											@error('workout_exercise')
							                    <div class="alert alert-danger">{{ $message }}</div>
							                @enderror
										</div> 
									</div>

									<div class="form-group row ">
										<label class="col-3">Total Week <span style="color:red">*</span></label>
										<div class="col-9">
											<input class="form-control " type="number" name="week" value="{{$assignment->week}}" placeholder="Enter Number of Week" id="total_week" />
											@error('week')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>

									<div class="form-group row">
									    <label class="col-3">Rest Time <span style="color:red">*</span></label>
									    <div class="col-9">
									      <input class="form-control" id="rest_timing" placeholder="Select time" name="rest_timing" type="text" value="{{$assignment->rest_timing}}" />
									      @error('rest_timing')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
									    </div>
									</div> 

									<div class="all_days" >
										<!-- MONDAY -->
										<?php 
											$result = json_decode($assignment->data, true);
											$monday_sets_arr = $result['monday_sets'];
											$monday_exercises_arr = $result['monday_exercises'];
										?>
										<div class="form-group row">
											<label class="col-3">Monday</label>
											@if($monday_exercises_arr != '')
											<div class="col-9">
												<div class="col-11" id="monday_exercises">

													@foreach($monday_exercises_arr as $key => $monday_value)
														@if($assignment->workout_category == '1')
														   	@foreach ($home_exercises_list as $value)
											                    @if ($monday_value == $value->id)
																	<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='monday_exercises[]' value = '{{$value->id}}' text='{{$value->sub_name}}' checked/><span></span>{{$value->sub_name}}</label>
																@endif
															@endforeach
														@else
															@foreach ($gym_exercises_list as $value)
											                    @if ($monday_value == $value->id)
																	<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='monday_exercises[]' value = '{{$value->id}}' text='{{$value->sub_name}}' checked/><span></span>{{$value->sub_name}}</label>
																
																@endif
															@endforeach
														@endif
													@endforeach
													
												</div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="monday_sets_table">  
														@foreach($monday_sets_arr as $value)
															
															<tr id="row{{$loop->iteration}}">  
						                                        <td>
						                                         	<input type="text" name="monday_weight[]" placeholder="Enter Weight" class="form-control name_list" value="{{$value['monday_weight']}}" />
						                                        </td>
						                                        <td>
						                                         	<input type="text" name="monday_reps[]" placeholder="Enter Reps" class="form-control name_list" value="{{$value['monday_reps']}}" />
						                                        </td>
						                                       
						                                        @if($loop->iteration > 1)
						                                        
						                                        <td>
						                                        	<button type="button" name="remove" id="{{$loop->iteration }}" class="btn btn-danger btn_remove_monday">X</button>
						                                        </td>
						                                        @else
						                                        	<td>
						                                         	<button type="button" name="add" id="monday_sets_add_btn" class="btn btn-success">Add More</button>
						                                        </td>
						                                        @endif
						                                    </tr>
						                                    @php
																 $monday_iteration = $loop->iteration 
															@endphp
						                                    
														@endforeach
					                                      <input type="hidden" name="" id= "monday_iteration" value="{{$monday_iteration}}">
					                               </table> 
												</div>
											</div>
											@else
											<div class="col-9">
												<div class="col-11" id="monday_exercises"></div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="monday_sets_table">  
					                                    <tr>  
					                                        <td>
					                                         	<input type="text" name="monday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
					                                        </td>
					                                        <td>
					                                         	<input type="text" name="monday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
					                                        </td>
					                                        <td>
					                                         	<button type="button" name="add" id="monday_sets_add_btn" class="btn btn-success">Add More</button>
					                                        </td>  
					                                    </tr>  
					                               </table> 
												</div>
												<input type="hidden" name="monday" value="yes">
											</div>
											@endif
										</div>
										<br/>
										<!-- TUESDAY -->
										<?php
											$tuesday_sets_arr = $result['tuesday_sets'];
											$tuesday_exercises_arr = $result['tuesday_exercises'];
										?>
										<div class="form-group row">
											<label class="col-3">Tuesday</label>
											@if($tuesday_exercises_arr != '')
											<div class="col-9">
												<div class="col-11" id="tuesday_exercises">
													@foreach($tuesday_exercises_arr as $key => $tuesday_value)
														@if($assignment->workout_category == '1')
														   	@foreach ($home_exercises_list as $value)
											                    @if ($tuesday_value == $value->id)
																	<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='tuesday_exercises[]' value = '{{$value->id}}' text='{{$value->sub_name}}' checked/><span></span>{{$value->sub_name}}</label>
																@endif
															@endforeach
														@else
															@foreach ($gym_exercises_list as $value)
											                    @if ($tuesday_value == $value->id)
																	<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='tuesday_exercises[]' value = '{{$value->id}}' text='{{$value->sub_name}}' checked/><span></span>{{$value->sub_name}}</label>
																
																@endif
															@endforeach
														@endif
													@endforeach
													
												</div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="tuesday_sets_table">  
														@foreach($tuesday_sets_arr as $value)
															
															<tr id="row{{$loop->iteration}}">  
						                                        <td>
						                                         	<input type="text" name="tuesday_weight[]" placeholder="Enter Weight" class="form-control name_list" value="{{$value['tuesday_weight'] != '' ? $value['tuesday_weight'] : ''}}" />
						                                        </td>
						                                        <td>
						                                         	<input type="text" name="tuesday_reps[]" placeholder="Enter Reps" class="form-control name_list" value="{{$value['tuesday_reps'] != '' ? $value['tuesday_reps'] : ''}}" />
						                                        </td>
						                                       
						                                        @if($loop->iteration > 1)
						                                        
						                                        <td>
						                                        	<button type="button" name="remove" id="{{$loop->iteration }}" class="btn btn-danger btn_remove_tuesday">X</button>
						                                        </td>
						                                        @else
						                                        	<td>
						                                         	<button type="button" name="add" id="tuesday_sets_add_btn" class="btn btn-success">Add More</button>
						                                        </td>
						                                        @endif
						                                    </tr>
						                                    @php
																 $tuesday_iteration = $loop->iteration 
															@endphp
						                                    
														@endforeach
					                                      <input type="hidden" name="" id= "tuesday_iteration" value="{{$tuesday_iteration}}">
					                               </table> 
												</div>
											</div>
											@else
											<div class="form-group row">
												<div class="col-9">
													<div class="col-11" id="tuesday_exercises"></div>
													<div></div>
													<div class="col-11">
														<label class="col-3">Sets </label>
														<table class="table table-bordered" id="tuesday_sets_table">  
						                                    <tr>  
						                                        <td>
						                                         	<input type="text" name="tuesday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
						                                        </td>
						                                        <td>
						                                         	<input type="text" name="tuesday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
						                                        </td>
						                                        <td>
						                                         	<button type="button" name="add" id="tuesday_sets_add_btn" class="btn btn-success">Add More</button>
						                                        </td>  
						                                    </tr>  
						                               </table> 
													</div>
													<input type="hidden" name="tuesday" value="yes">
												</div>
											</div>
											@endif
										</div>
									</div>
										 
							</div>
							<div class="col-xl-2"></div>
							<div class="form-group text-center m-t-20">
			                    <div class="col-xs-12">
			                        <button class="btn btn-primary" type="submit">  {{ __('update') }}</button>
			                    </div>
	                		</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
            </div>
        </div> 
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	$(document).ready(function(){ 
    		var mon_i=$('#monday_iteration').value;  
	        $('#monday_sets_add_btn').click(function(){  
	           mon_i++; 
	           $('#monday_sets_table').append('<tr id="row'+mon_i+'"><td><input type="text" name="monday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="monday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+mon_i+'" class="btn btn-danger btn_remove_monday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_monday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });

	        var mon_i=$('#tuesday_iteration').value;  
	        $('#tuesday_sets_add_btn').click(function(){  
	           mon_i++; 
	           $('#tuesday_sets_table').append('<tr id="row'+mon_i+'"><td><input type="text" name="tuesday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="tuesday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+mon_i+'" class="btn btn-danger btn_remove_tuesday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_tuesday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });
    	});
    </script>
    <script type="text/javascript">
    	$('#workout_list_option').selectpicker();
    	$('#rest_timing').timepicker({
		   minuteStep: 1,
		   defaultTime: '',
		   showSeconds: true,
		   showMeridian: false,
		   snapToStep: true
		 });

    	FormValidation.formValidation(
			 document.getElementById('assignment-edit'),
			 {
			  fields: {
			   customer_id: {
				    validators: {
				        notEmpty: {
				        	message: 'Customer name is required'
				        }
			        }
			   },
			   rest_timing: {
			    validators: {
			     notEmpty: {
			      message: 'Rest Timing is required'
			     }
			    }
			   },
			   
			    workout_category: {
				    validators: {
				     	notEmpty: {
				      		message: 'Workout Type is required'
				     	},
				    }
			   },
			    'workout_list[]': {
				    validators: {
				     	notEmpty: {
				      		message: 'Workout is required'
				     	},
				    }
			    },
			    'workout_exercise[]': {
				    validators: {
				     	notEmpty: {
				      		message: 'Exercise is required'
				     	},
				    }
			    },
			   week: {
				    validators: {
				     	notEmpty: {
				      		message: 'Week is required'
				     	},
				    }
			   },
			  
			  },
			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			  }
			 }
		);
    </script>
    <script type="text/javascript">
    	$(document).ready(function() { 
	        $('#customers').selectpicker();
	        $('#exercises_options').selectpicker();
	    });
    	//On change Workout
    	$('select[name="workout_category"]').change(function(){

		    if ($(this).val() == "1"){
		    	$.ajax('https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/home_workout_list', {
		            method: 'GET',
		            headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		        }).done(function (r) {
		        	if(r.status){
		        		//console.log('r.status',r.status);
		        		var workout_list = r.success;
		        		var workout_option = "<option value='' disabled>Workout Exercise</option>";
		        		$(workout_list).each(function(index,item){
		                    var name = item.name;
		                    var id = item.id;

		                    workout_option += "<option value ='"+id+"'>"+name+"</option>";
	                  });
	                  $('#workout_list_option').html(workout_option).attr('multiple','multiple').selectpicker('refresh');
	                  //$('#workout_list_option').html(workout_option).;
	                  $("#workout_exercise_div").addClass("home_exercises");
	                  $("#workout_exercise_div").removeClass("gym_exercises");
			        }
		        });


		    } else if($(this).val() == "2"){
		        $.ajax('https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/gym_workout_list', {
		            method: 'GET',
		            headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		        }).done(function (r) {
		        	if(r.status){
		        		console.log('r.status',r.status);
		        		var workout_list = r.success;
		        		var workout_option = "<option value=''>Workout Exercise</option>";
		        		$(workout_list).each(function(index,item){
		                    var name = item.name;
		                    var id = item.id;

		                    workout_option += "<option value ='"+id+"'>"+name+"</option>";
	                  });
	                  $('#workout_list_option').html(workout_option).attr('multiple','multiple').selectpicker('refresh');
	                  $("#workout_exercise_div").addClass("gym_exercises");
	                  $("#workout_exercise_div").removeClass("home_exercises");
			        }
		        });
		    }   
		})
		//On change Home Workout
		$('select[name="workout_list[]"]').change(function(){
			
			var workout_type = $('select[name="workout_category"]').val();

			var $select = $('#exercises_options');
			//var home_workout_id = $(this).val();
			console.log('home_workout_id',home_workout_id);
			if (workout_type == '1'){
			 	
			 	var home_workout_id = $(this).val(); 

			    if(home_workout_id){
			    	var formData = {
			            ids: home_workout_id,
			        };
			    	console.log('home_exercises Categories if', home_workout_id);
	                $.ajax({
	                	type: "GET",
			            url: 'https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/home_workout_exercise/',
			            data: formData,
			            dataType: 'json',
			            success: function (data) {
			            	var exercise_list = data.success;
			            	var exercise_option='';
			            	$(exercise_list).each(function(index,item){
			                    var name = item.sub_name;
			                    var id = item.id;

			                      exercise_option += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='workout_exercise[]' value = '"+id+"' text='"+name+"'/><span></span>"+name+"</label>";
		                  });
			            	console.log('exercise_option',exercise_option);
			            	if(exercise_option != ''){
			            		$('.exercises_span').css('display','none');
			            		$('#exercises_options').html(exercise_option);
			            	}
		                  
			            },
			            error: function (data) {
			                console.log(data);
			            }
	                });
			    }
			} 
			else if(workout_type == '2'){
				var gym_workout_id = $(this).val();  
			    if(gym_workout_id){

			        var formData = {
			            ids: gym_workout_id,
			        };
			    	console.log('gym_exercises Categories if', gym_workout_id);
	                $.ajax({
	                	type: "GET",
			            url: 'https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/gym_workout_exercise/',
			            data: formData,
			            dataType: 'json',
			            success: function (data) {
			            	var exercise_list = data.success;
			            	var exercise_option='';
			            	$(exercise_list).each(function(index,item){
			                    var name = item.sub_name;
			                    var id = item.id;

			                      exercise_option += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='workout_exercise[]' value = '"+id+"' text='"+name+"'/><span></span>"+name+"</label>";
		                  });
			            	if(exercise_option != ''){
			            		$('.exercises_span').css('display','none');
			            		$('#exercises_options').html(exercise_option);
			            	}

		                  
			            },
			            error: function (data) {
			                console.log(data);
			            }
	                });
			    }
			}    
		})
    </script>
   
@endsection
