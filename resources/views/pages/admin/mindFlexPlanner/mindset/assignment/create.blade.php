 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Assignment')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}
 
    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Assignment <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('assignment.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i> 
							Back
						</a>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Form-->
					<form class="form" id="assignment-add" method="POST" action="{{ route('assignment.save') }}" accept-charset="utf-8" enctype="multipart/form-data">
						  @csrf
						<div class="row">
							<div class="col-xl-2"></div>
							<div class="col-xl-8">
								<div class="my-5">  
									<h3 class=" text-dark font-weight-bold mb-10">Assignment Info:</h3>
									<div class="form-group row">
										<label class="col-3">Customer Name <span style="color:red">*</span></label>
										<div class="col-9">
											<select class="form-control" name="customer_id" id="customers">
												<option disabled="disabled">Select Customer</option>
						                          @foreach ($customers as $value)
						                            <option value="{{$value->id}}">{{$value->name}}</option>
						                          @endforeach
						                      </select>
						                      @error('customer_id')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>
									<div class="form-group row">
										<label class="col-3">Workout Type<span style="color:red">*</span></label>
										<div class="col-9">
											<select class="form-control" name="workout_category">
						                        <option value= "" >Select Workout</option>
						                        <option value="1">Home Workout</option>
						                        <option value="2">Gym Workout</option>
						                      </select>
						                      @error('workout_category')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>
									<div class="form-group row" id="workout_list_div">
										<label class="col-3">Category List<span style="color:red">*</span></label>
										<div class="col-9">
											<select class="form-control" name="workout_list[]" id="workout_list_option">
						                        <option value= "" >Select Categories</option>
						                       
						                      </select>
						                      @error('workout_list')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>

									<div class="form-group row" id="workout_exercise_div">
										<label class="col-3">Exercise <span style="color:red">*</span></label>
										<div class="col-9" id="exercises_options">
												<span class="exercises_span">No Data Found</span>
						                      @error('workout_exercise')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>

									<div class="form-group row ">
										<label class="col-3">Total Week <span style="color:red">*</span></label>
										<div class="col-9">
											<input class="form-control " type="number" name="week" value="" placeholder="Enter Number of Week" id="total_week"/>
											@error('week')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>

									<div class="form-group row">
									    <label class="col-3">Rest Time <span style="color:red">*</span></label>
									    <div class="col-9">
									      <input class="form-control" id="rest_timing" placeholder="Select time" name="rest_timing" type="text"/>
									      @error('rest_timing')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
									    </div>
									</div> 

									<div class="all_days" style="display: none;">
										<div class="form-group row">
											<label class="col-3">Monday</label>
											<div class="col-9">
												<div class="col-11" id="monday_exercises"></div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="monday_sets_table">  
					                                    <tr>  
					                                        <td>
					                                         	<input type="text" name="monday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
					                                        </td>
					                                        <td>
					                                         	<input type="text" name="monday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
					                                        </td>
					                                        <td>
					                                         	<button type="button" name="add" id="monday_sets_add_btn" class="btn btn-success">Add More</button>
					                                        </td>  
					                                    </tr>  
					                               </table> 
												</div>
												<input type="hidden" name="monday" value="yes">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-3">Tuesday</label>
											<div class="col-9">
												<div class="col-11" id="tuesday_exercises"></div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="tuesday_sets_table">  
					                                    <tr>  
					                                        <td>
					                                         	<input type="text" name="tuesday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
					                                        </td>
					                                        <td>
					                                         	<input type="text" name="tuesday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
					                                        </td>
					                                        <td>
					                                         	<button type="button" name="add" id="tuesday_sets_add_btn" class="btn btn-success">Add More</button>
					                                        </td>  
					                                    </tr>  
					                               </table> 
												</div>
												<input type="hidden" name="tuesday" value="yes">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-3">Wednesday</label>
											<div class="col-9">
												<div class="col-11" id="wednesday_exercises"></div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="wednesday_sets_table">  
					                                    <tr>  
					                                        <td>
					                                         	<input type="text" name="wednesday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
					                                        </td>
					                                        <td>
					                                         	<input type="text" name="wednesday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
					                                        </td>
					                                        <td>
					                                         	<button type="button" name="add" id="wednesday_sets_add_btn" class="btn btn-success">Add More</button>
					                                        </td>  
					                                    </tr>  
					                               </table> 
												</div>
												<input type="hidden" name="wednesday" value="yes">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-3">Thursday</label>
											<div class="col-9">
												<div class="col-11" id="thursday_exercises"></div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="thursday_sets_table">  
					                                    <tr>  
					                                        <td>
					                                         	<input type="text" name="thursday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
					                                        </td>
					                                        <td>
					                                         	<input type="text" name="thursday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
					                                        </td>
					                                        <td>
					                                         	<button type="button" name="add" id="thursday_sets_add_btn" class="btn btn-success">Add More</button>
					                                        </td>  
					                                    </tr>  
					                               </table> 
												</div>
												<input type="hidden" name="thursday" value="yes">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-3">Friday</label>
											<div class="col-9">
												<div class="col-11" id="friday_exercises"></div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="friday_sets_table">  
					                                    <tr>  
					                                        <td>
					                                         	<input type="text" name="friday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
					                                        </td>
					                                        <td>
					                                         	<input type="text" name="friday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
					                                        </td>
					                                        <td>
					                                         	<button type="button" name="add" id="friday_sets_add_btn" class="btn btn-success">Add More</button>
					                                        </td>  
					                                    </tr>  
					                               </table> 
												</div>
												<input type="hidden" name="friday" value="yes">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-3">Saturday</label>
											<div class="col-9">
												<div class="col-11" id="saturday_exercises"></div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="saturday_sets_table">  
					                                    <tr>  
					                                        <td>
					                                         	<input type="text" name="saturday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
					                                        </td>
					                                        <td>
					                                         	<input type="text" name="saturday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
					                                        </td>
					                                        <td>
					                                         	<button type="button" name="add" id="saturday_sets_add_btn" class="btn btn-success">Add More</button>
					                                        </td>  
					                                    </tr>  
					                               </table> 
												</div>
												<input type="hidden" name="saturday" value="yes">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-3">Sunday</label>
											<div class="col-9">
												<div class="col-11" id="sunday_exercises"></div>
												<div></div>
												<div class="col-11">
													<label class="col-3">Sets </label>
													<table class="table table-bordered" id="sunday_sets_table">  
					                                    <tr>  
					                                        <td>
					                                         	<input type="text" name="sunday_weight[]" placeholder="Enter Weight" class="form-control name_list"/>
					                                        </td>
					                                        <td>
					                                         	<input type="text" name="sunday_reps[]" placeholder="Enter Reps" class="form-control name_list" />
					                                        </td>
					                                        <td>
					                                         	<button type="button" name="add" id="sunday_sets_add_btn" class="btn btn-success">Add More</button>
					                                        </td>  
					                                    </tr>  
					                               </table> 
												</div>
												<input type="hidden" name="sunday" value="yes">
											</div>
										</div>
									</div>
							</div>
							<div class="col-xl-2"></div>
							<div class="form-group text-center m-t-20">
			                    <div class="col-xs-12">
			                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
			                    </div>
	                		</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
            </div>
        </div>   
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script>  
	    $(document).ready(function(){

	    	// $("#total_week").attr('disabled','disabled');
	    	// $("#total_week").removeAttr('disabled');
	    	var select_exercise;
	    	$("#total_week"). click(function(){
				var monday_exercises = '';
				var tuesday_exercises = '';
				var wednesday_exercises = '';
				var thursday_exercises = '';
				var friday_exercises = '';
				var saturday_exercises = '';
				var sunday_exercises = '';
				$('input[name="workout_exercise[]"]:checked').each(function() {
					var exercise_val =this.value;
					var exercise_text = $(this).attr('text');

					monday_exercises += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='monday_exercises[]' value = '"+exercise_val+"' text='"+exercise_text+"'/><span></span>"+exercise_text+"</label>";

					tuesday_exercises += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='tuesday_exercises[]' value = '"+exercise_val+"' text='"+exercise_text+"'/><span></span>"+exercise_text+"</label>";

					wednesday_exercises += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='wednesday_exercises[]' value = '"+exercise_val+"' text='"+exercise_text+"'/><span></span>"+exercise_text+"</label>";

					thursday_exercises += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='thursday_exercises[]' value = '"+exercise_val+"' text='"+exercise_text+"'/><span></span>"+exercise_text+"</label>";

					friday_exercises += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='friday_exercises[]' value = '"+exercise_val+"' text='"+exercise_text+"'/><span></span>"+exercise_text+"</label>";

					saturday_exercises += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='saturday_exercises[]' value = '"+exercise_val+"' text='"+exercise_text+"'/><span></span>"+exercise_text+"</label>";

					sunday_exercises += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='sunday_exercises[]' value = '"+exercise_val+"' text='"+exercise_text+"'/><span></span>"+exercise_text+"</label>";
					
				});
				$('#monday_exercises').html(monday_exercises);
				$('#tuesday_exercises').html(tuesday_exercises);
				$('#wednesday_exercises').html(wednesday_exercises);
				$('#thursday_exercises').html(thursday_exercises);
				$('#friday_exercises').html(friday_exercises);
				$('#saturday_exercises').html(saturday_exercises);
				$('#sunday_exercises').html(sunday_exercises);
				$('.all_days').css('display','block');
			});

	        var mon_i=1;  
	        $('#monday_sets_add_btn').click(function(){  
	           mon_i++; 
	           $('#monday_sets_table').append('<tr id="row'+mon_i+'"><td><input type="text" name="monday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="monday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+mon_i+'" class="btn btn-danger btn_remove_monday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_monday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        }); 

	        var tue_i=1;   
	        $('#tuesday_sets_add_btn').click(function(){  
	           tue_i++; 
	           $('#tuesday_sets_table').append('<tr id="row'+tue_i+'"><td><input type="text" name="tuesday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="tuesday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+tue_i+'" class="btn btn-danger btn_remove_tuesday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_tuesday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });

	        var wed_i=1;   
	        $('#wednesday_sets_add_btn').click(function(){  
	           wed_i++; 
	           $('#wednesday_sets_table').append('<tr id="row'+wed_i+'"><td><input type="text" name="wednesday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="wednesday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+wed_i+'" class="btn btn-danger btn_remove_wednesday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_wednesday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });
	        var thu_i=1;   
	        $('#thursday_sets_add_btn').click(function(){  
	           thu_i++; 
	           $('#thursday_sets_table').append('<tr id="row'+thu_i+'"><td><input type="text" name="thursday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="thursday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+thu_i+'" class="btn btn-danger btn_remove_thursday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_thursday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });
	        var fri_i=1;   
	        $('#friday_sets_add_btn').click(function(){  
	           fri_i++; 
	           $('#friday_sets_table').append('<tr id="row'+fri_i+'"><td><input type="text" name="friday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="friday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+fri_i+'" class="btn btn-danger btn_remove_friday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_friday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });
	        var sat_i=1;   
	        $('#saturday_sets_add_btn').click(function(){  
	           sat_i++; 
	           $('#saturday_sets_table').append('<tr id="row'+sat_i+'"><td><input type="text" name="saturday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="saturday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+sat_i+'" class="btn btn-danger btn_remove_saturday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_saturday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });
	        var sun_i=1;   
	        $('#sunday_sets_add_btn').click(function(){  
	           sun_i++; 
	           $('#sunday_sets_table').append('<tr id="row'+sun_i+'"><td><input type="text" name="sunday_weight[]" placeholder="Enter Weight" class="form-control name_list"/></td><td><input type="text" name="sunday_reps[]" placeholder="Enter Reps" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+sun_i+'" class="btn btn-danger btn_remove_sunday">X</button></td></tr>');  
	        }); 
		    $(document).on('click', '.btn_remove_sunday', function(){  
	           var button_id = $(this).attr("id");   
	           $('#row'+button_id+'').remove();  
	        });   
	   });  
	</script>

    <script type="text/javascript">
    	$('#rest_timing').timepicker({
		   minuteStep: 1,
		   defaultTime: '',
		   showSeconds: true,
		   showMeridian: false,
		   snapToStep: true
		 });

    	FormValidation.formValidation(
			 document.getElementById('assignment-add'),
			 {
			  fields: {
			   customer_id: {
				    validators: {
				        notEmpty: {
				        	message: 'Customer name is required'
				        }
			        }
			   },

			   rest_timing: {
			    validators: {
			     notEmpty: {
			      message: 'Rest Timing is required'
			     }
			    }
			   },
			   
			    workout_category: {
				    validators: {
				     	notEmpty: {
				      		message: 'Workout Type is required'
				     	},
				    }
			   },
			    'workout_list[]': {
				    validators: {
				     	notEmpty: {
				      		message: 'Workout is required'
				     	},
				    }
			    },
			    'workout_exercise[]': {
				    validators: {
				     	notEmpty: {
				      		message: 'Exercise is required'
				     	},
				    }
			    },
			   week: {
				    validators: {
				     	notEmpty: {
				      		message: 'Week is required'
				     	},
				    }
			   },

			  },
			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			  }
			 }
		);
    </script>
    <script type="text/javascript">
    	//On change Workout
    	$('select[name="workout_category"]').change(function(){
		    if ($(this).val() == "1"){
		    	$.ajax('https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/home_workout_list', {
		            method: 'GET',
		            headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		        }).done(function (r) {
		        	if(r.status){
		        		console.log('r.status',r.status);
		        		var workout_list = r.success;
		        		var workout_option = "<option value='' disabled>Select Categories</option>";
		        		$(workout_list).each(function(index,item){
		                    var name = item.name;
		                    var id = item.id;

		                    workout_option += "<option value ='"+id+"'>"+name+"</option>";
	                  });
		        		$('#workout_list_option').html(workout_option).attr('multiple','multiple').selectpicker('refresh');
	                  //$('#workout_list_option').html(workout_option).;
	                  $("#workout_exercise_div").addClass("home_exercises");
	                  $("#workout_exercise_div").removeClass("gym_exercises");
			        }
		        });

		    } else if($(this).val() == "2"){
		        $.ajax('https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/gym_workout_list', {
		            method: 'GET',
		            headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		        }).done(function (r) {
		        	if(r.status){
		        		console.log('r.status',r.status);
		        		var workout_list = r.success;
		        		var workout_option = "<option value='' disabled>Select Categories</option>";
		        		$(workout_list).each(function(index,item){
		                    var name = item.name;
		                    var id = item.id;

		                    workout_option += "<option value ='"+id+"'>"+name+"</option>";
	                  });
	                  $('#workout_list_option').html(workout_option).attr('multiple','multiple').selectpicker('refresh');
	                  $("#workout_exercise_div").addClass("gym_exercises");
	                  $("#workout_exercise_div").removeClass("home_exercises");
			        }
		        });
		    }   
		})
		
		//On change Home Workout
		$('select[name="workout_list[]"]').change(function(){
			;
			var $select = $('#exercises_options');
			//var home_workout_id = $(this).val();
			console.log('home_workout_id',home_workout_id);
			if ($('#workout_exercise_div').hasClass('home_exercises')){
			 	
			 	var home_workout_id = $(this).val(); 

			    if(home_workout_id){
			    	var formData = {
			            ids: home_workout_id,
			        };
			    	console.log('home_exercises Categories if', home_workout_id);
	                $.ajax({
	                	type: "GET",
			            url: 'https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/home_workout_exercise/',
			            data: formData,
			            dataType: 'json',
			            success: function (data) {
			            	var exercise_list = data.success;
			            	var exercise_option='';
			            	$(exercise_list).each(function(index,item){
			                    var name = item.sub_name;
			                    var id = item.id;

			                      exercise_option += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='workout_exercise[]' value = '"+id+"' text='"+name+"'/><span></span>"+name+"</label>";
		                 	});
		                 	// $("#total_week").removeAttr('disabled');
			            	console.log('exercise_option',exercise_option);
			            	if(exercise_option != ''){
			            		$('.exercises_span').css('display','none');
			            		$('#exercises_options').html(exercise_option);
			            	}
		                  
			            },
			            error: function (data) {
			                console.log(data);
			            }
	                });
			    }
			} 
			else if($('#workout_exercise_div').hasClass('gym_exercises')){
				var gym_workout_id = $(this).val();  
			    if(gym_workout_id){

			        var formData = {
			            ids: gym_workout_id,
			        };
			    	console.log('gym_exercises Categories if', gym_workout_id);
	                $.ajax({
	                	type: "GET",
			            url: 'https://vkapsprojects.com/pedro/skeleton/public/mindflex_planner/mindset/gym_workout_exercise/',
			            data: formData,
			            dataType: 'json',
			            success: function (data) {
			            	var exercise_list = data.success;
			            	var exercise_option='';
			            	$(exercise_list).each(function(index,item){
			                    var name = item.sub_name;
			                    var id = item.id;

			                      exercise_option += "<label class='checkbox checkbox-success'><input type='checkbox' class='form-control' name='workout_exercise[]' value = '"+id+"' text='"+name+"'/><span></span>"+name+"</label>";
		                  });
			            	if(exercise_option != ''){
			            		$('.exercises_span').css('display','none');
			            		$('#exercises_options').html(exercise_option);
			            	}

		                  
			            },
			            error: function (data) {
			                console.log(data);
			            }
	                });
			    }
			}    
		})	
    </script>

@endsection
