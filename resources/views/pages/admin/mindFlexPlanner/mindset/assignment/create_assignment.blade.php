 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Assignment')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}
 
    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Assignment <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('assignment.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i> 
							Back
						</a>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Form-->
					<form class="form" id="assignment-add" method="POST" action="{{ route('assignment.save_assign') }}" accept-charset="utf-8" enctype="multipart/form-data">
						  @csrf
						<div class="row">
							<div class="col-xl-2"></div>
							<div class="col-xl-8">
								<div class="my-5">  
									<h3 class=" text-dark font-weight-bold mb-10">Assignment Info:</h3>
									<div class="form-group row">
										<label class="col-3">Customer Name <span style="color:red">*</span></label>
										<div class="col-9">
											<select class="form-control" name="customer_id" id="customers">
												<option disabled="disabled">Select Customer</option>
						                          @foreach ($customers as $value)
						                            <option value="{{$value->id}}">{{$value->name}}</option>
						                          @endforeach
						                      </select>
						                      @error('customer_id')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>

									<div class="form-group row" id="week_div">
										<label class="col-3">Total Week <span style="color:red">*</span></label>
										<div class="col-9">
											<input class="form-control" type="number" id="week" name="week" value="" placeholder="Enter Number of Week" />
											@error('week')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>

									<div class="form-group row" id="days">
										<label class="col-3">Days <span style="color:red">*</span></label>
										<div class="col-9">
										<label class="checkbox checkbox-success">
						                    <input type="checkbox" class="form-control" name="days[]" value="1" />
						                    <span></span>
						                    Monday
						                </label>
						                <label class="checkbox checkbox-success">
						                    <input type="checkbox" class="form-control" name="days[]" value="2"/>
						                    <span></span>
						                    Tuesday
						                </label>
						                <label class="checkbox checkbox-success">
						                    <input type="checkbox" class="form-control" name="days[]" value="3"/>
						                    <span></span>
						                    Wednesday
						                 </label>
						                 <label class="checkbox checkbox-success">
						                    <input type="checkbox" class="form-control" name="days[]" value="4"/>
						                    <span></span>
						                    Thursday
						                 </label>
						                 <label class="checkbox checkbox-success">
						                    <input type="checkbox" class="form-control" name="days[]" value="5"/>
						                    <span></span>
						                    Friday
						                 </label>
						                 <label class="checkbox checkbox-success">
						                    <input type="checkbox" class="form-control" name="days[]" value="6"/>
						                    <span></span>
						                    Saturday
						                 </label>
						                 <label class="checkbox checkbox-success">
						                    <input type="checkbox" class="form-control" name="days[]" value="7"/>
						                    <span></span>
						                    Sunday
						                 </label>
											@error('days')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>

								</div>
							</div>
							<div class="col-xl-2"></div>
							<div class="form-group text-center m-t-20">
			                    <div class="col-xs-12">
			                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
			                    </div>
	                		</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
            </div>
        </div>   
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    
    	$("#week").on('change keyup paste', function () {
		    WeekNumber();
		});

		function WeekNumber() {
		    var week_no = $("#week").val();
		     console.log("no_week",week_no);
		    if(week_no)
		     	$('#days').clone().insertAfter( "#week_div" );
		     	
		    
		      
		}

    </script>
    <script type="text/javascript">

    	FormValidation.formValidation(
			 document.getElementById('assignment-add'),
			 {
			  fields: {
			   'customer_id': {
				    validators: {
				        notEmpty: {
				        	message: 'Customer name is required'
				        }
			        }
			   },
			   week: {
				    validators: {
				     	notEmpty: {
				      		message: 'Week is required'
				     	},
				    }
			   },
			  },
			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			  }
			 }
		);
    </script>
@endsection
