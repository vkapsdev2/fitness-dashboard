 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Question to Coaches')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Question to Coaches <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('question_coach.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" id="question-add" method="POST" action="{{ route('question_coach.save') }}" accept-charset="utf-8" enctype="multipart/form-data">
					  @csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Add Question to Coaches:</h3>

								<div class="form-group row">
				                    <label class="col-3">coach Name<span style="color:red">*</span></label>
				                    <div class="col-9">
				                      <select class="form-control" name="coach_id">
				                        <option value= "" >Select coach</option>
				                          @foreach ($coaches as $value)
				                            <option value="{{$value->id}}">{{$value->name}}</option>
				                          @endforeach
				                      </select>
				                      @error('coach_id')
				                        <div class="alert alert-danger">{{ $message }}</div>
				                      @enderror
				                    </div>
				                </div>

								<div class="form-group row">
									<label class="col-3">Question <span style="color:red">*</span></label>
									<div class="col-9">
										<textarea name="question" class="form-control" data-provide="question">{{ old('question') }}</textarea>
									</div>
								</div>
						</div>
						<div class="col-xl-2"></div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	FormValidation.formValidation(
			 document.getElementById('question-add'),
			 {
			  fields: {
			   question: {
			    validators: {
			     notEmpty: {
			      message: 'Question filed is required'
			     }
			    }
			   },
			   coach_id: {
			    validators: {
			     notEmpty: {
			      message: 'Coach name is required'
			     }
			    }
			   },
			   
			  },

			  plugins: {
			   trigger: new FormValidation.plugins.Trigger(),
			   // Bootstrap Framework Integration
			   bootstrap: new FormValidation.plugins.Bootstrap(),
			   // Validate fields when clicking the Submit button
			   submitButton: new FormValidation.plugins.SubmitButton(),
			            // Submit the form when all fields are valid
			   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			  }
			 }
		);
    </script>
@endsection
