{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Gym Workout')

{{-- Content --}}
@section('content') 

{{-- Dashboard 1 --}}

    <div class="row"> 
    	<div class="col-lg-12">
    		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
    			<div class="card-header">
    				<div class="card-title">
    					<h3 class="card-label">
    						Edit Gym Workout<i class="mr-2"></i>
    					</h3>
    				</div>
    				<div class="card-toolbar">
    					<a href="{{route ('movement_gym_workout.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
    						<i class="ki ki-long-arrow-back icon-sm"></i>
    						Back
    					</a>
    				</div>
    			</div>
    			<div class="card-body">
    				<!--begin::Form-->
    				<form class="form" id="gymWorkout-edit" method="POST" action="{{ route('movement_gym_workout.update',[$gymWorkout->id]) }}" accept-charset="utf-8" enctype="multipart/form-data">
    					@csrf
    					<div class="row">
    						<div class="col-xl-2"></div>
    						<div class="col-xl-8">
    							<div class="my-5">
    								<h3 class=" text-dark font-weight-bold mb-10">Gym Workout Info:</h3>
    								<div class="form-group row ">
    									<label class="col-3">Name<span style="color:red">*</span></label>
    									<div class="col-9">
    										<input class="form-control " type="text" name="name" value="{{$gymWorkout->name}}" placeholder="Enter Name" />
    										@error('name')
    										<div class="alert alert-danger">{{ $message }}</div>
    										@enderror
    									</div>
    								</div>
    								<div class="form-group row">
    									<label class="col-3">Banner</label>
    									<div class="col-9">
    										<img style="height: 50px;" src="{{ url('/'). '/public/media/movement-gymworkout-banner/'.$gymWorkout->banner}}" />
    										<input class="form-control" type="file" name="banner" value="{{public_path().'/media/movement-gymworkout-banner/'.$gymWorkout->banner}}"/>
    									</div>
    								</div>
    							</div>
    							<div class="col-xl-2"></div>
    							<div class="form-group text-center m-t-20">
    								<div class="col-xs-12">
    									<button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
    									<a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
    								</div>
    							</div>
    						</div>
    					</div>
    				</form>
    				<!--end::Form-->
    			</div>
    		</div>
    	</div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		FormValidation.formValidation(
   			document.getElementById('gymWorkout-edit'),
   			{
   				fields: {
   					name: {
   						validators: {
   							notEmpty: {
   								message: 'Workout Name is required'
   							}
   						}
   					},
   					banner: {
   						validators: {
   							file: {
   								extension: 'jpeg,png,jpg,gif,svg',
   								message: 'The selected file is not valid'
   							}
   						}
   					},

   				},

   				plugins: {
   					trigger: new FormValidation.plugins.Trigger(),
			   		// Bootstrap Framework Integration
			   		bootstrap: new FormValidation.plugins.Bootstrap(),
			   		// Validate fields when clicking the Submit button
			   		submitButton: new FormValidation.plugins.SubmitButton(),
			        // Submit the form when all fields are valid
			        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			    }
			}
		);
    </script>
@endsection
