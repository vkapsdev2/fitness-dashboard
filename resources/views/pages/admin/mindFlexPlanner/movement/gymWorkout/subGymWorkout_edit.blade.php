{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Gym Workout')

{{-- Content --}}
@section('content') 

{{-- Dashboard 1 --}}

<div class="row"> 
	<div class="col-lg-12">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						edit {{$subGymWorkout->sub_name}}<i class="mr-2"></i>
					</h3>
				</div>
				<div class="card-toolbar">
					<a href="{{route ('movement_sub_gym_workout.index',[$subGymWorkout->gymWorkout_id])}}" class="btn btn-light-primary font-weight-bolder mr-2">
						<i class="ki ki-long-arrow-back icon-sm"></i>
						Back
					</a>
				</div>
			</div>
			<div class="card-body">
				<!--begin::Form-->
				@if($subGymWorkout->video_file)
				@php
				$ext = $subGymWorkout->video_file;
				$ext = explode(".",$ext);
				@endphp
				@endif
				<form class="form" id="Sub-gymWorkout-edit" method="POST" action="{{ route('movement_sub_gym_workout.update',[$subGymWorkout->gymWorkout_id,$subGymWorkout->id]) }}" accept-charset="utf-8" enctype="multipart/form-data">
					@csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Gym Workout Info:</h3>
								<div class="form-group row ">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control " type="text" name="name" value="{{$subGymWorkout->sub_name}}" placeholder="Enter Name"  />
										@error('name')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Video File Or URL</label>
									<div class="col-9 col-form-label">
										<div class="radio-inline">
											<label class="radio">
												<input type="radio" name="video" checked="checked" id="file" value="file" @if($subGymWorkout->video_file) {{'checked'}} @else {{ ' ' }} @endif />
												<span></span>
												Video File
											</label>
											<label class="radio">
												<input type="radio" name="video" id="url" value="url"  @if($subGymWorkout->video_url) {{'checked'}} @else {{ ' ' }} @endif />
												<span></span>
												Video URL
											</label>
										</div>
										<span class="form-text text-muted"></span>
									</div>
								</div>

								<div class="form-group row" id="video_file" <?php if($subGymWorkout->video_url){ ?> style="display: none;" <?php }  ?> >
									<label class="col-3">Video File</label>
									<div class="col-9">
										@if($subGymWorkout->video_file)
										<video width="320" height="240" controls>
											<source src="{{ url('/'). '/public/media/movement-gymworkout-video/'.$subGymWorkout->video_file}}" type='video/{{$ext[1]}}'>
												Your browser does not support the video tag.
											</video>

											@endif
											<input class="form-control" type="file" name="video_file" value=" @if($subGymWorkout->video_file) {{public_path().'/media/movement-gymworkout-video/'.$subGymWorkout->video_file}} @endif " />
										</div>
									</div>
									<div class="form-group row" id="video_url" <?php if($subGymWorkout->video_file){ ?> style="display: none;" <?php }  ?> >
										<label class="col-3">Video URL</label>
										<div class="col-9">
											@if($subGymWorkout->video_url)

											<?php $video_url = $subGymWorkout->video_url;
											$video_url = str_replace('watch', 'embed', $video_url); ?>
											<iframe width="420" height="345" src="{{$video_url}}"> </iframe>
											@endif
											<input class="form-control " type="text" name="video_url" value="@if($subGymWorkout->video_url){{ $subGymWorkout->video_url }}@endif"/>
											<span class="form-text text-muted">Please enter your Video URL</span>
											@error('video_url')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>
									<div class="form-group row">
										<label class="col-3">Timing</label>
										<div class="col-9">
											<input class="form-control" id="workout_timing" placeholder="Select time" name="workout_timing" type="text" value="{{$subGymWorkout->workout_timing}}" />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-3">Banner</label>
										<div class="col-9">
											<img style="height: 50px;" src="{{ url('/'). '/public/media/movement-gymworkout-banner/'.$subGymWorkout->sub_banner}}" />
											<input class="form-control" type="file" name="banner" value="{{public_path().'/media/movement-gymworkout-banner/'.$subGymWorkout->sub_banner}}"/>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-3">Description</label>
										<div class="col-9">
											<textarea name="description" class="form-control" data-provide="description" id="kt-ckeditor-3">{{$subGymWorkout->description}}</textarea>
										</div>
									</div>
								</div>
								<div class="col-xl-2"></div>
								<div class="form-group text-center m-t-20">
									<div class="col-xs-12">
										<button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
										<a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
									</div>
								</div>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
			</div>
		</div>
	</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
    <script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
		    $("input[name$='video']").click(function() {
		        var video = $(this).val();
		        console.log('audio', video);
		        if(video == 'file'){
		        	$("#video_url").hide();
		        	$("#video_file").show();
		        }if(video == 'url'){
		        	$("#video_file").hide();
		        	$("#video_url").show();
		        }
		        
		    });
		});
    </script>	
    <script type="text/javascript">
    	$('#workout_timing').timepicker({
    		minuteStep: 1,
    		defaultTime: '',
    		showSeconds: true,
    		showMeridian: false,
    		snapToStep: true
    	});
    	// Class definition
		var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		        ClassicEditor
		            .create( document.querySelector( '#kt-ckeditor-3' ) )
		            .then( editor => {
		                console.log( editor );
		            } )
		            .catch( error => {
		                console.error( error );
		            } );
		    }

		    return {
		        // public functions
		        init: function() {
		            demos();
		        }
		    };
		}();
	// Initialization
    </script>
    <script type="text/javascript">
    	FormValidation.formValidation(
    		document.getElementById('Sub-homeWorkout-edit'),
    		{
    			fields: {
    				name: {
    					validators: {
    						notEmpty: {
    							message: 'Workout Name is required'
    						}
    					}
    				},
    				workout_timing: {
    					validators: {
    						notEmpty: {
    							message: 'Workout Timing is required'
    						}
    					}
    				},
    				video_url: {
    					validators: {
    						uri: {
    							message: 'The Video address is not valid'
    						}
    					}
    				},
    				video_file: {
    					validators: {
    						file: {
    							extension: 'mp4,mov,ogg,qt',
    							maxSize: 2000*1024,
    							message: 'The selected file is not valid'
    						}
    					}
    				},
    			},

    			plugins: {
    				trigger: new FormValidation.plugins.Trigger(),
		   			// Bootstrap Framework Integration
			   		bootstrap: new FormValidation.plugins.Bootstrap(),
			   		// Validate fields when clicking the Submit button
			   		submitButton: new FormValidation.plugins.SubmitButton(),
					// Submit the form when all fields are valid
			        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
			    }
			}
		);
    </script>
@endsection
