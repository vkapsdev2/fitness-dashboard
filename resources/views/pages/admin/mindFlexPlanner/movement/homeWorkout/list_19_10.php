 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'HomeWorkout')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
            <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
          <div class="card-title">
            <h3 class="card-label">Home Workout
            <span class="d-block text-muted pt-2 font-size-sm">List of Home Workouts</span></h3>
          </div>  
           <div class="card-toolbar">
            <a href="{{ route('movement_home_workout.create') }}" class="btn btn-primary font-weight-bolder">
              <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                  <rect x="0" y="0" width="24" height="24"/>
                  <circle fill="#000000" cx="9" cy="15" r="6"/>
                  <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
              </g>
          </svg><!--end::Svg Icon--></span>   Add Workout
          </a>
           </div>
        </div>
        @if ($message = Session::get('success'))
              <div class="alert alert-success">
                  {{ $message }}
              </div>
          @endif
          <div class="delete_message"></div>
        <div class="card-body">
          <!--begin: Datatable-->
              <div class="datatable datatable-bordered datatable-head-custom" id="kt_home_list_datatable"></div>
              <!--end: Datatable-->
        </div>
      </div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
  <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/pages/custom.js?caf') }}" type="text/javascript"></script>
 
  <script type="text/javascript">
    var KTAppsHomeListDatatable = function () {
      var _demo = function _demo() {
        var datatable = $('#kt_home_list_datatable').KTDatatable({
          // datasource definition HOST_URL
          data: {
            type: 'remote',
            source: {
              read: {
                url:  "{{ route('ajax_home_list.ajaxList') }}",
                params: {
                  _token : $('meta[name="csrf-token"]').attr('content')
                }
              }

            },
            pageSize: 10,
            // display 20 records per page
            serverPaging: true,
            //serverFiltering: true,
            serverSorting: true,
          },
          // layout definition
          layout: {
            scroll: false,
            // enable/disable datatable scroll both horizontal and vertical when needed.
            footer: false // display/hide footer

          },
          // column sorting
          sortable: true,
          pagination: true,
          search: {
            input: $('#kt_subheader_search_form'),
            delay: 400,
            key: 'generalSearch'
          },
          // columns definition
          columns: [
            {
              field: 'id',
              title: '#',
              sortable: 'asc',
              width: 40,
              type: 'number',
              selector: false,
              textAlign: 'left',
              template: function template(data) {
                return '<span class="font-weight-bolder">' + data.id + '</span>';
              }
            }, 
            {
              field: 'name',
              title: 'Name',
              template: function template(row) {
                var output = '';
                output += '<div class="font-weight-bolder font-size-lg mb-0">' + row.name + '</div>';
                return output;
              }
            },
            {
              field:'banner',
              title:'Banner',
              sortable: false,
              template:function template(row){
                return "<img src= '{{ URL::to('/') }}/public/media/movement-homeworkout-banner/"+row.banner+"' style='height: 40px; width: 80px;' class='img-thumbnail ' />"; 
              },
            },
            {
              field: 'actions',
              title: 'Actions',
              sortable: false,
              overflow: 'visible',
              autoHide: false,
              template: function template(row) {
                return '\ <a href="javascript:void(0);" onclick="movementHomeDelete('+row.id+')" class="btn btn-danger btn-circle" data-original-title="Delete" data-toggle="tooltip">\
                      <i class="fa fa-times"></i>\ </a>\ <a href="{{ URL::to("/") }}/mindflex_planner/movement/home_workout/'+row.id+'/sub_home_workout/create" class="btn btn-primary btn-circle" data-original-title="Add Exercise" data-toggle="tooltip">\
                      <i class="fa fa-plus"></i>\ </a> \ <a href="{{ URL::to("/") }}/mindflex_planner/movement/home_workout/edit/'+row.id+'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a> \ <a href="{{ URL::to("/") }}/mindflex_planner/movement/home_workout/'+row.id+'/sub_home_workout" data-toggle="tooltip" data-original-title="View All Exercises" class="btn btn-primary btn-circle"> <i class="fa fa-eye"></i> </a>';
              }
            }
          ]
        });
        $('#kt_datatable_search_status').on('change', function () {
          datatable.search($(this).val().toLowerCase(), 'Status');
        });
        $('#kt_datatable_search_type').on('change', function () {
          datatable.search($(this).val().toLowerCase(), 'Type');
        });
        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();
      };

      return {
        // public functions
        init: function init() {
          _demo();
        }
      };
    }();

    jQuery(document).ready(function () {
      KTAppsHomeListDatatable.init();
    });
  </script>
@endsection
