 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							MindSet RX <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('mindflex_rx.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="my-5 detail_view">
							<h5 class=" text-dark font-weight-bold mb-3">Movement:</h5>	
						    <p  class="detail_value">{!! $rx->movement !!}</p>	

							<h5 class=" text-dark font-weight-bold mb-3">MindSet:</h5>
						    <p  class="detail_value">{!! $rx->mindset !!}</p>	

						    <h5 class=" text-dark font-weight-bold mb-3">Nutrition:</h5>
						    <p  class="detail_value">{!! $rx->nutrition !!}</p>	

						    <h5 class=" text-dark font-weight-bold mb-3">Supplementation:</h5>
						    <p  class="detail_value">{!! $rx->supplimentation !!}</p>	

						    <h5 class=" text-dark font-weight-bold mb-3">Other:</h5>
						    <p  class="detail_value">{!! $rx->other !!}</p>	
						</div>
			        </div>
			   </div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
	    padding: 15px;
	    width: 70%;
	    box-shadow: 0px 0px 7px #ccc;
	    margin: 0px auto;
	}

	.detail_value {
		text-align: justify;
	}
</style>