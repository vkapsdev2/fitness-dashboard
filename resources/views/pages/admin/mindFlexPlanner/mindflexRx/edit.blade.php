{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit MindFlex RX')

{{-- Content --}}
@section('content') 

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							edit MindFlex RX<i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('mindflex_rx.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">
				<!--begin::Form-->
				<form class="form" id="rx-edit" method="POST" action="{{ route('mindflex_rx.update',[$rx->id]) }}">
					  @csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">MindFlex Rx:</h3>
									<div class="form-group row">
										<label class="col-3">Customer Name</label>
										<div class="col-9">
						                      <select class="form-control" name="customer_id" id="customers">
												<option disabled="disabled">Select Customer</option>
						                          @foreach ($customers as $value)
						                     <option value="{{$value->id}}" @if($value->id == $rx->customer_id) {{'selected'}} @endif >{{$value->name}}</option>
						                          @endforeach
						                      </select>
						                      <div class="ajax_message" style="color:red"></div>
						                      @error('customer_id')
						                        <div class="alert alert-danger">{{ $message }}</div>
						                      @enderror
										</div> 
									</div>

									<div class="form-group row">
									<label class="col-3">Movement<span style="color:red">*</span></label>
									<div class="col-9">
										<textarea name="movement" class="form-control" data-provide="movement" placeholder="Write Prescription here.." id="kt-ckeditor-1">{{$rx->movement}}</textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">MINDSET<span style="color:red">*</span></label>
									<div class="col-9">
										<textarea name="mindset" class="form-control" data-provide="mindset" placeholder="Write Prescription here.." id="kt-ckeditor-2">{{ $rx->mindset }}</textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">NUTRITION<span style="color:red">*</span></label>
									<div class="col-9">
										<textarea name="nutrition" class="form-control" data-provide="nutrition" placeholder="Write Prescription here.." id="kt-ckeditor-3">{{$rx->nutrition }}</textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">SUPPLEMENTATION<span style="color:red">*</span></label>
									<div class="col-9">
										<textarea name="supplimentation" class="form-control" data-provide="supplimentation" placeholder="Write Prescription here.." id="kt-ckeditor-4">{{ $rx->supplimentation}}</textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">OTHER</label>
									<div class="col-9">
										<textarea name="other" class="form-control" data-provide="other" placeholder="Write Prescription here.." id="kt-ckeditor-5">{{$rx->other }}</textarea>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xl-2"></div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
    <script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	FormValidation.formValidation(
			document.getElementById('rx-add'),
			 {
				fields: {
					customer_id: {
					    validators: {
						    notEmpty: {
						      message: 'customer filed is required'
						    }
					    }
					},
				   
				},

				plugins: {
				   trigger: new FormValidation.plugins.Trigger(),
				   // Bootstrap Framework Integration
				   bootstrap: new FormValidation.plugins.Bootstrap(),
				   // Validate fields when clicking the Submit button
				   submitButton: new FormValidation.plugins.SubmitButton(),
				            // Submit the form when all fields are valid
				   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
			}
		);
    </script>
    <!-- <script type="text/javascript">
    	$('#customers').on('change', function(event){
        	event.preventDefault();
        	 var customer_id = $('#customers option:selected').val();

        	 customFormValidation(customer_id);
        
        });
        function customFormValidation(customer_id){
        	
        	 	var formData = {
		            customer_id: customer_id,
		        };

				$.ajax({
                	type: "GET",
		            url: 'https://vkapsprojects.com/pedro/skeleton/mindflex_planner/mindflex_rx/ajax_customer',
		            data: formData,
		            dataType: 'json',
		            success: function (data) {
		            	console.log(data.success);
		            	if(data.success){
		            		$("div.ajax_message").html('<div>This Customer already taken</div>');
		            		$("#customers").addClass('is-invalid');
		            	}else{
		            		$("div.ajax_message").html('<div></div>');
		            		$("#customers").removeClass('is-invalid').addClass('is-valid');
		            	}
	                  
		            },
		            error: function (data) {
		                console.log(data);
		            }
                });
        }

    </script> -->
      <script type="text/javascript">
    	// Class definition

		var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		        ClassicEditor
		            .create( document.querySelector( '#kt-ckeditor-1,#kt-ckeditor-2,#kt-ckeditor-3,#kt-ckeditor-4,#kt-ckeditor-5' ) )
		            .then( editor => {
		                console.log( editor );
		            } )
		            .catch( error => {
		                console.error( error );
		            } );
		    }

		    return {
		        // public functions
		        init: function() {
		            demos();
		        }
		    };
		}();

	// Initialization
    </script>
@endsection
