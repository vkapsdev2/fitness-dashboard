 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add FAQ')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							FAQ <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('faq.list')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('faq.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-faq">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">FAQ Info:</h3>
								<div class="form-group row">
									<label class="col-3">Title<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="faq_title" value="{{ old('faq_title') }}"/>
										@error('faq_title')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								
								<div class="form-group row">
				                  <label class="col-3">Description<span style="color:red">*</span></label>
				                  <div class="col-lg-9">
				                    <div class="input-group">
				                      <textarea class="form-control form-control-solid" rows="10" name="faq_description" placeholder="Enter your answer here.." style="resize: none;" >{{old('faq_description')}}</textarea>
				                    </div>
				                      @error('faq_description')
				                      <div class="alert alert-danger">{{ $message }}</div>
				                    @enderror
				                  </div>
				                </div>
							</div>
							<div class="form-group row">
							      <label class="col-3 col-form-label">Status<span style="color:red">*</span></label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="faq_status" checked="checked"  value="1"  {{ old('faq_status') == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                  <input type="radio" name="faq_status" value="0" {{ old('faq_status') == "0" ? 'checked' : '' }} />
							                  <span></span>
							                  Deactive
							              </label>
							          </div>
							           @error('faq_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?xg') }}" type="text/javascript"></script>
  
<script type="text/javascript">
	
	FormValidation.formValidation(
 document.getElementById('add-faq'),
 {
  fields: {
   faq_title: {
    validators: {
     notEmpty: {
      message: ' FAQ Title is required'
     },
    }
   },
   
   faq_description: {
    validators: {
     notEmpty: {
      message: 'FAQ Description is required'
     },
    }
   },
 },

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>
@endsection
