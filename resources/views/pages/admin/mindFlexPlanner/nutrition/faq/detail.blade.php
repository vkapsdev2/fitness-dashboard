 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

   <div class="row">
     <!-- column -->
    <div class="col-12">
        <div class="card">
        	<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							FAQ <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('faq.list')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Go To List
						</a>
						<a href="{{ route('faq.create') }}" class="btn btn-primary font-weight-bolder">
					    <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
					    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					        <rect x="0" y="0" width="24" height="24"/>
					        <circle fill="#000000" cx="9" cy="15" r="6"/>
					        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
					    </g>
					</svg><!--end::Svg Icon--></span>   Add FAQ
					</a>
					</div>
				</div>
            <div class="card-body">
				<div class="container">
					<div class="accordion-container faq-section">
					<div class="faq_header">	
					  <h2>Frequently Asked Questions</h2>
					</div>
					  @foreach($faqs as $faq)
					  	<div class="set faq_hd_content">
						    <a href="#" style="font-size: 17px;">
						    	<i class="fa fa-plus"></i> 
						    	{{ $faq->faq_title }}	
						    </a>
						    <div class="content faq_content">
						      <p>{{ $faq->faq_description }}</p>
						    </div>
						</div>
					  @endforeach
					  
					</div>
				</div>
            </div>
        	<div class="card-body">
				<div class="container">
					<div class="support-text">
						<p>Please take advantage of our FAQ section above. Many issues and questions are addressed within and can be resolved in a more timely manner than using our support form.</p>
					</div>
				</div>
			</div>
           
            <div class="return_page">
                <p>Go To:</p>
                <a href="{{route ('contact.create')}}" target="_blank">Contact us</a>
                
            </div>
        </div>
    </div>
</div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	  $(".set > a").on("click", function() {
	    if ($(this).hasClass("active")) {
	      $(this).removeClass("active");
	      $(this)
	        .siblings(".content")
	        .slideUp(200);
	      $(".set > a i")
	        .removeClass("fa-minus")
	        .addClass("fa-plus");
	    } else {
	      $(".set > a i")
	        .removeClass("fa-minus")
	        .addClass("fa-plus");
	      $(this)
	        .find("i")
	        .removeClass("fa-plus")
	        .addClass("fa-minus");
	      $(".set > a").removeClass("active");
	      $(this).addClass("active");
	      $(".content").slideUp(200);
	      $(this)
	        .siblings(".content")
	        .slideDown(200);
	    }
	  });
	});
</script>
<style type="text/css">
	.form-group.first-filed {
	    width: 49%;
	    float: left;
	    margin-right: 7px;
	}

	.form-group.second-field {
	    width: 49%;
	    float: left;
	    margin-left: 14px;
	}
	form.site-contact {
	    background-color: whitesmoke;
	    padding: 15px;
	}
	.faq_header {
    	display: block;
	}
	.faq_header h2 {
	    font-size: 28px;
	    line-height: 1.1;
	    margin-bottom: 30px;
	}
	.faq_hd_content {
	    border-color: #f0f0f0;
	    background-color: #f8f8f8;
	    padding: 10px 15px;
	    margin-bottom: 2px;
	}
	.faq_hd_content a, .faq_hd_content a:hover{
		color: #a09f9f;
	}
	.faq_hd_content a .fa {
	    padding-right: 10px;
	    color: #a09f9f;
	}
	.faq_content p {
	    padding-left: 15px;
	}
	.faq_content {
	    background: #fff;
	    padding: 15px 15px !important;
	    border-radius: 3px;
	    margin-top: 15px;
	}
	@media only screen and (max-width: 767px){
		.form-group.first-filed, .form-group.second-field{
			width: 100%;
			margin: 0px;
			float: none;
		}
		form.site-contact .form-group {
		    margin-bottom: 15px;
		}
	}
</style>