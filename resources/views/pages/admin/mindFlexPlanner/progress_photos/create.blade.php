 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Photos')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row"> 
	<div class="col-lg-12">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Progress Photos <i class="mr-2"></i>
					</h3>
				</div>
				<div class="card-toolbar">
					<a href="{{route ('progress_photo.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
						<i class="ki ki-long-arrow-back icon-sm"></i>
						Back
					</a>
				</div>
			</div>
			<div class="card-body">
				<!--begin::Form-->
				<form class="form" id="photo-add" method="POST" action="{{ route('progress_photo.save') }}" accept-charset="utf-8" enctype="multipart/form-data">
					@csrf
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12 text-lg-right">Customer Name<span style="color:red">*</span></label>
						<div class="col-9">
							<select class="form-control" name="customer_id">
								<option value= "" >Select Customer</option>
								@foreach ($customers as $value)
								<option value="{{$value->id}}">{{$value->name}}</option>
								@endforeach
							</select>
							@error('customer_id')
							<div class="alert alert-danger">{{ $message }}</div>
							@enderror
						</div>
					</div>
					<div class="card-body">
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12 text-lg-right">Multiple File Upload <span style="color:red">*</span></label>
							<div class="col-9">
								<input required type="file" class="form-control" id="images" name="images[]" multiple>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-4">
								<button class="btn btn-primary" type="submit">  {{ __('Submit') }}</button>
								<!-- <button type="reset" class="btn btn-light-primary mr-2">Cancel</button> -->
								<button type="reset" class="btn btn-primary">Cancel</button>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/crud/file-upload/dropzonejs.js') }}"></script>
    <script type="text/javascript">
    	FormValidation.formValidation(
    		document.getElementById('photo-add'),
    		{
    			fields: {
    				customer_id: {
    					validators: {
    						notEmpty: {
    							message: 'Customer Name is required'
    						}
    					}
    				},
    				'images[]': {
    					validators: {
    						notEmpty: {
    							message: 'Image is required'
    						},
    						file: {
    							extension: 'jpeg,png,jpg,gif,svg',
    							message: 'The selected file is not valid'
    						}
    					}
    				},
    			},

    			plugins: {
    				trigger: new FormValidation.plugins.Trigger(),
			   		// Bootstrap Framework Integration
			  		bootstrap: new FormValidation.plugins.Bootstrap(),
			   		// Validate fields when clicking the Submit button
			   		submitButton: new FormValidation.plugins.SubmitButton(),
					// Submit the form when all fields are valid
			        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
			}
		);
    </script>
@endsection
