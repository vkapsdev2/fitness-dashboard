 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Store <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('store.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							
								<div class = "store_img detail_img text-center">
									@if ($store->image)
									<img style= "height:200px;" src="{{ url('/'). '/public/media/suppliment-store/'.$store->image}}" />
									@else
									<img style= "height:200px;" src="{{ url('/'). '/public/media/book-banner/Product-Dummy.jpg'}}"/>
									@endif
								</div>
								<div class ="row detail_name text-center store_detail_txt">
									<div class ="row mb-3 plan_title">
										<div class="col"><span>Title:</span></div>
										<div class="col"><span><b>{{$store->name}}</b></span></div>
									</div>
									<!-- <div class ="row mb-3 plan_validity_days">
										<div class="col"><span>Actual Price:</span></div>
										<div class="col"><span><b>{{$store->actual_price}}</b></span></div>
									</div> -->
									<div class ="row mb-3 plan_validity_days">
										<div class="col"><span>Sales Price:</span></div>
										<div class="col"><span><b>{{$store->sales_price}}</b></span></div>
									</div>
									<!-- <div class ="row mb-3 plan_validity_days">
										<div class="col"><span>Quantity:</span></div>
										<div class="col"><span><b>{{$store->total_quantity}}</b></span></div>
									</div> -->
									<div class ="row mb-3 plan_validity_days">
										<div class="col"><span>Link:</span></div>
										<div class="col"><span><b>
											<a href="{{$store->buy_now_url}}" class="store_buy_btn btn btn-success font-weight-bold btn-pill" target="_blank">Buy Now</a></b></span></div>
									</div>
									<div class ="row mb-3 plan_validity_days">
										<div class="col"><span>Stock Status:</span></div>
										<div class="col">
										 @if ($store->remaining_quantity > 1)
						                      <span class="label label-lg font-weight-bold label-light-primary label-inline">In Stock</span>
						                      @else
						                      <span class="label label-lg font-weight-bold label-light-danger label-inline">Out of Stock</span>
						                      @endif
						                  </div>
									</div>
								</div>
									<div class="detail_description text-center>">
									 {!! $store->description !!}
								</div>
							</div>
 						</div>
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
.detail_name.text-center.store_detail_txt {
    display: grid;
    text-align: center;
    text-align: left !important;
    padding: 10px 16% 10px 34%;
    font-size: 14px;
}
a.store_buy_btn{
	line-height: 1!important;
}
p.shake_hd {
    float: left;
}
ul.shake_hd_list {
    list-style: none;
    font-weight: 600;
    float: right;
    padding-inline-start: 15px !important;
}
@media only screen and (max-width: 767px){
.detail_name.text-center.store_detail_txt {
    padding: 10px 0% 10px 6%;
}
.store_img.detail_img.text-center {
    overflow: hidden;
}
}

</style>