 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Store')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Store <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('store.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('store.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-store" name="add-store">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Store Info:</h3>
								<div class="form-group row">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="name" value="{{ old('name') }}" placeholder="Enter name" maxlength="255" />
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<!-- <div class="form-group row">
									<label class="col-3">Actual Price<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="actual_price" value="{{ old('actual_price') }}" placeholder="Enter sales price" maxlength="10" />
										@error('actual_price')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div> -->
								<div class="form-group row">
									<label class="col-3">Sale Price<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="sales_price" value="{{ old('sales_price') }}" placeholder="Enter sales price" maxlength="10"/>
										@error('sales_price')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<!-- <div class="form-group row">
									<label class="col-3">Quantity<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="total_quantity" value="{{ old('total_quantity') }}" placeholder="Enter Quantity" maxlength="10" />
										@error('total_quantity')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div> -->
								        <div class="form-group row">
								          <label class="col-3">URL<span style="color:red">*</span></label>
								          <div class="col-lg-9">
								            <div class="input-group">
								              <div class="input-group-prepend">
								                  <span class="input-group-text">
								                    <i class="flaticon2-website"></i>
								                  </span>
								              </div>
								              <input type="text" class="form-control form-control-solid" name="buy_now_url" value="{{ old('buy_now_url') }}" />
								            </div>
								            <span class="form-text text-muted">Please enter your BuyNow URL.</span>
								            @error('buy_now_url')
								              <div class="alert alert-danger">{{ $message }}</div>
								            @enderror
								          </div>
								        </div>
								<div class="form-group row">
									<label class="col-3">Image<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="file" name="image" value="{{ old('image') }}"/>
										@error('image')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
							       <label class="col-3">Description</label>
							       <div class="col-9">
							       <textarea class="form-control form-control-solid" id="kt-ckeditor-3" data-provide="description" name="description" placeholder="Write your description here.." style="resize: none;" >{{ old('description') }} </textarea>
							        @error('description')
							           <div class="alert alert-danger">{{ $message }}</div>
							        @enderror
							       </div>
							      </div>
								
							<div class="form-group row">
							      <label class="col-3 col-form-label">Status<span style="color:red">*</span></label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="store_status" checked="checked"  value="1"  {{ old('store_status') == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                  <input type="radio" name="store_status" value="0" {{ old('store_status') == "0" ? 'checked' : '' }} />
							                  <span></span>
							                  Deactive
							              </label>
							          </div>
							           @error('store_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>
							</div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
			</div>
        </div>
	 </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
		<!--end::Page Vendors-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
		
    <script type="text/javascript">
    	// Class definition

var KTCkeditor = function () {
    // Private functions
    var demos = function () {
        ClassicEditor
            .create( document.querySelector( '#kt-ckeditor-3' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

// Initialization
</script>
<script type="text/javascript"> 
FormValidation.formValidation(
 document.getElementById('add-store'),
 {
 fields: {
   	name: {
    validators: {
     notEmpty: {
      message: 'Name is required'
     },
    }
   },
  actual_price: {
    validators: {
	     notEmpty: {
	      message: 'Actual Price is required'
	    },
	    numeric: {
	    message: 'The value is not a number',
	     // The default separators
	    thousandsSeparator: ',',
	    decimalSeparator: '.'
	    }
    }
   },
    sales_price: {
    validators: {
	     notEmpty: {
	      message: 'Sales Price is required'
	    },
	    numeric: {
	    message: 'The value is not a number',
	     // The default separators
	    thousandsSeparator: ',',
	    decimalSeparator: '.'
	    }
     },
   },
    total_quantity: {
    validators: {
     notEmpty: {
      message: 'Quantity is required'
     },
      digits: {
      message: 'The velue is not a valid digit'
     }
    }
   },
   buy_now_url: {
    validators: {
    	notEmpty: {
      message: 'Website is required'
     },
      uri: {
      message: 'The URL is not valid'
     }
    }
   },
   image: {
    validators: {
      notEmpty: {
      message: 'Image is required'
     },
    }
   },
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>
@endsection
