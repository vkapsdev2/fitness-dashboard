 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Supplement')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
           	<div class="card card-custom gutter-b">
				<div class="card-header flex-wrap border-0 pt-6 pb-0">
					<div class="card-title">
						<h3 class="card-label"> Supplements
						<span class="d-block text-muted pt-2 font-size-sm">List of Supplements</span></h3>
					</div>	
					 
				</div>
				@if ($message = Session::get('success'))
			        <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
          <div class="delete_message"></div>
				<div class="card-body">
					<!--begin: Datatable -->
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable_supplement" style="text-align: center;">
						<thead>
							<tr>
								<th>#</th>
								<th>Customer Name</th>
								<th>Name</th>
							</tr>
						</thead>
						<tbody>
							 @foreach ($suppliments as $suppliment)
							 	<tr>
							 		<td>{{$loop->iteration}}</td>
									<td>{{$suppliment->customer->name}}</td>
									<td>{{$suppliment->name}}</td>
									
								</tr>
							 @endforeach
						</tbody>
					</table>
          
					<!--end: Datatable-->
				</div>
			</div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
  <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/pages/custom.js?gh') }}" type="text/javascript"></script>

@endsection
