 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Badge <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('badges.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							
								<div class = "detail_img text-center">
									@if ($badge->image)
									<img style= "height:200px;" src="{{ url('/'). '/public/media/badge/'.$badge->image}}" />
									@else
									<img style= "height:200px;" src="{{ url('/'). '/public/media/book-banner/Product-Dummy.jpg'}}"/>
									@endif
								</div>
								<div class ="detail_name text-center">
								<h3 class="badges_hd_txt"><b>{{ $badge->title }} </b></h3>
								
								</div>
									<div class="sub_hd_box detail_description text-center>">
										<h3 class="badges_sub_hd_txt">Purpose</h3>
									{{ strip_tags($badge->purpose) }}
								</div>
								<div class="sub_hd_box detail_description text-center>">
										<h3 class="badges_sub_hd_txt">Benifits</h3>
									{{ strip_tags($badge->benifit) }}
								</div>
								<div class="sub_hd_box detail_description text-center>">
										<h3 class="badges_sub_hd_txt">Description</h3>
									{{ strip_tags($badge->description) }}
								</div>
							</div>
 						</div>
				
					
						
					
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}
.detail_description {
    text-align: justify;
}
h3.badges_hd_txt {
    margin: 15px auto;
}
h3.badges_sub_hd_txt {
    font-weight: 400;
    opacity: 0.56;
    font-size: 18px !important;
    margin-bottom: 10px;
}
.sub_hd_box {
    margin: 15px;
}
</style>