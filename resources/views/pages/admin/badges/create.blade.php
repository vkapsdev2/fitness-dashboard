{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Badge')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row"> 
	<div class="col-lg-12">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Badges <i class="mr-2"></i>
					</h3>
				</div>
				<div class="card-toolbar">
					<a href="{{route ('badges.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
						<i class="ki ki-long-arrow-back icon-sm"></i>
						Back
					</a>
				</div>
			</div>
			<div class="card-body">
				<!--begin::Form-->
				<form class="form"  method="POST" action="{{ route('badges.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-badge" name="add-badge">
					@csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Badge Info:</h3>
								<div class="form-group row">
									<label class="col-3">Title<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="title" value="{{ old('title') }}" placeholder="Enter title"/>
										@error('title')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="file" name="image" value="{{ old('image') }}"/>
										@error('image')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3" >Purpose<span style="color:red">*</span></label>
									<div class="col-9">
										<textarea name="purpose" class="form-control"  data-provide="purpose" style="resize: none;" >{{ old('purpose')}}</textarea>
										@error('purpose')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3" >Benifits</label>
									<div class="col-9">
										<textarea name="benifit" class="form-control"  data-provide="benifit" style="resize: none;" >{{ old('benifit')}}</textarea>
										@error('benifit')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3" >Description</label>
									<div class="col-9">
										<textarea name="description" class="form-control"  data-provide="description" style="resize: none;">{{ old('description')}}</textarea>
										@error('description')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Status<span style="color:red">*</span></label>
									<div class="col-9 col-form-label">
										<div class="radio-inline">
											<label class="radio">
												<input type="radio" name="badge_status" checked="checked"  value="1"  {{ old('badge_status') == "1" ? 'checked' : '' }}/>
												<span></span>
												Activate
											</label>
											<label class="radio">
												<input type="radio" name="badge_status" value="0" {{ old('badge_status') == "0" ? 'checked' : '' }} />
												<span></span>
												Deactive
											</label>
										</div>
										@error('badge_status')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
										<span class="form-text text-muted"></span>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-primary" type="submit" id="add-book-submit">  {{ __('Create') }}</button>
							<a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom.js?fgf') }}" type="text/javascript"></script>
	<script type="text/javascript">
		FormValidation.formValidation(
			document.getElementById('add-badge'),
			{
				fields: {
					title: {
						validators: {
							notEmpty: {
								message: 'Badge Title is required'
							},
						}
					},
					image: {
						validators: {
							notEmpty: {
								message: 'Image is required'
							},
						}
					},
					purpose: {
						validators: {
							notEmpty: {
								message: 'Purpose is required'
							},
						}
					},
				},

				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
					// Submit the form when all fields are valid
					defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
			}
		);
	</script>
@endsection
