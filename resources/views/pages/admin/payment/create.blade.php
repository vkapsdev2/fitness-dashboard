{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add GymWorkout')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row"> 
	<div class="col-lg-12">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Payment Key <i class="mr-2"></i>
					</h3>
				</div>
				<div class="card-toolbar">
					<a href="{{route ('stripe.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
						<i class="ki ki-long-arrow-back icon-sm"></i>
						Back
					</a>
				</div>
			</div>
			<div class="card-body">
				<!--begin::Form-->
				<form class="form" id="payment-add" method="POST" action="{{ route('stripe.save') }}" accept-charset="utf-8" enctype="multipart/form-data">
					@csrf
					<div class="row">
						<div class="col-xl-2"></div>
						<div class="col-xl-8">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Payment Key Info:</h3>
								<div class="form-group row">
									<label class="col-3">Publishable Key<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="publishable_key" value="{{ old('publishable_key') }}" placeholder="Enter publishable_key" />
										@error('publishable_key')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Secret Key<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control" type="text" name="secret_key" value="{{ old('secret_key') }}" placeholder="Enter secret_key" />
										@error('secret_key')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Status</label>
									<div class="col-9 col-form-label">
										<div class="radio-inline">
											<label class="radio">
												<input type="radio" name="key_status" checked="checked"  value="1"  {{ old('key_status') == "1" ? 'checked' : '' }}/>
												<span></span>
												Activate
											</label>
											<label class="radio">
												<input type="radio" name="key_status" value="0" {{ old('key_status') == "0" ? 'checked' : '' }} />
												<span></span>
												Deactive
											</label>
										</div>
										@error('key_status')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
										<span class="form-text text-muted"></span>
									</div>
								</div>
							</div>
							<div class="col-xl-2"></div>
							<div class="form-group text-center m-t-20">
								<div class="col-xs-12">
									<button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
									<a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	FormValidation.formValidation(
    		document.getElementById('payment-add'),
    		{
    			fields: {
    				publishable_key: {
    					validators: {
    						notEmpty: {
    							message: 'Publishable Key is required'
    						}
    					}
    				},
    				secret_key: {
    					validators: {
    						notEmpty: {
    							message: 'Secret Key is required'
    						}
    					}
    				},
    			},

    			plugins: {
    				trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
					// Submit the form when all fields are valid
					defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
			}
		);
   	</script>
@endsection
