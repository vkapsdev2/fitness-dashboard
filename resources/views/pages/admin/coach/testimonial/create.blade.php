 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Testimonial')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

<div class="row">
  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    <div class="card-header">
      <div class="card-title"><h3 class="card-label">Testimonial <i class="mr-2"></i></h3>
      </div>

      <div class="card-toolbar">
        <a href="{{route ('coachTestimonial.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
         <i class="ki ki-long-arrow-back icon-sm"></i>
        Back
        </a>
      </div>
    </div>
    <div class="card-body">
      <!--begin::Form-->
      <form class="form" id="add-testimonial" name="add-testimonial" action="{{ route('coachTestimonial.save')}}" method="post" accept-charset="utf-8">
      	@csrf
       <div class="row">
          <div class="col-xl-2"></div>
          <div class="col-xl-8">
            <div class="my-5">
              <h3 class=" text-dark font-weight-bold mb-10">Testimonial Info:</h3>
              <div class="form-group row">
              <label class="col-3">Title<span style="color:red">*</span></label>
              <div class="col-9">
                <input class="form-control form-control-solid" type="text" name= "title" value="{{ old('title') }}"/>
                @error('title')
    			       <div class="alert alert-danger">{{ $message }}</div>
    		        @enderror
              </div>
              </div>
              <div class="form-group row">
                  <label class="col-3">From Cusomer<span style="color:red">*</span></label>
                   <div class="col-9">
                    <select class="form-control form-control-solid" name="from_customer">
                      <option value="">Select Customer</option>
                       @foreach ($customers as $value)
                       <option value="{{ $value->id}}">{{ $value->name }}</option>
                      @endforeach
                    </select>
                    @error('from_customer')
                    <div class="alert alert-danger">{{ $message }}</div>
                   @enderror
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-3">To Coach<span style="color:red">*</span></label>
                   <div class="col-9">
                    <select class="form-control form-control-solid" name="to_coach">
                      <option value="">Select Coach</option>
                       @foreach ($coaches as $value)
                       <option value="{{ $value->id}}">{{ $value->name }}</option>
                      @endforeach
                    </select>
                    @error('to_coach')
                    <div class="alert alert-danger">{{ $message }}</div>
                   @enderror
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-3">Description<span style="color:red">*</span></label>
                   <div class="col-9">
                    <textarea class="form-control form-control-solid"  name="description" placeholder="Enter your feedback here.." style="resize: none;">{{ old('description') }}</textarea>
                    @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                   @enderror
                  </div>
                </div>
            </div>
            <div class="form-group text-center m-t-20">
             <div class="col-xs-12">
               <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
               <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
             </div>
          </div>
          </div>
          
      </div>
      </form>
      <!--end::Form-->
    </div>
    <div class="col-xl-2"></div>
    </div>
</div>



@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
    <style type="text/css">
    	div#kt_page_sticky_card {
    		width: 100%;
		}
      .green-border-focus .form-control:focus {
       border: 1px solid #8bc34a;
        box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, .25);
    }
    .shadow-textarea textarea.form-control::placeholder {
     font-weight: 300;
    }
    .shadow-textarea textarea.form-control {
      padding-left: 0.8rem;
    }
    </style>
    <script type="text/javascript">
     
      
    FormValidation.formValidation(
 document.getElementById('add-testimonial'),
 {
  fields: {
   title: {
    validators: {
     notEmpty: {
      message: 'Title is required.'
     },
    }
   },
   
   from_customer: {
    validators: {
     notEmpty: {
      message: 'Please select Customer.'
     },
    }
   },
  to_coach: {
    validators: {
     notEmpty: {
      message: 'Please select Coach.'
     },
    }
   },
   description:{
    validators: {
     notEmpty: {
      message: 'Description is required.'
     },
    }
   },
  },

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);
   </script>
   @endsection
