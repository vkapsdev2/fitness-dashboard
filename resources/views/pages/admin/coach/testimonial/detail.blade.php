 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Testimonials <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('coachTestimonial.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							<div class ="detail_name">
								<div class="testimonial_details">
									<p class="detail_title">From Customer:</p> 
									<p  class="detail_value"><b>{{ $customer[0]->name }} </b></p>
								</div>

								<div class="testimonial_details">
									<p class="detail_title">To Coach:</p>
									<p  class="detail_value"> <b>{{ $coach[0]->name }} </b></p>
								</div>

								<div class="testimonial_details">
									<p class="detail_title">Title:</p>
									<p  class="detail_value"> <b> {{ $testimonials[0]->title }} </b></p>
								</div>
								</div class="testimonial_details">
									<div class="detail_description text-center>">
									{{ strip_tags($testimonials[0]->description) }}
								</div>
							</div>
 						</div>
				
					
						
					
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
.testimonial_details {
    display: flex;
}

.testimonial_details p.detail_title {
    padding-right: 5px;
    text-align: left;
    width: 116px;
}

.detail_name {
    width: 300px;
    max-width: 100%;
    margin: 0px auto;
}
</style>