{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Testimonial')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
           	<div class="card card-custom gutter-b">
				<div class="card-header flex-wrap border-0 pt-6 pb-0">
					<div class="card-title">
						<h3 class="card-label">Testimonials List
						<span class="d-block text-muted pt-2 font-size-sm"></span></h3>
					</div>	
					 <div class="card-toolbar">
					 	<a href="{{ route('coachTestimonial.create') }}" class="btn btn-primary font-weight-bolder">
					    <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
					    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					        <rect x="0" y="0" width="24" height="24"/>
					        <circle fill="#000000" cx="9" cy="15" r="6"/>
					        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
					    </g>
					</svg><!--end::Svg Icon--></span>Add Testimonial
					</a>
					 </div>
				</div>
				@if ($message = Session::get('success'))
			        <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
				<div class="card-body">
					<!--begin: Datatable -->
          
					<table class="table table-separate table-head-custom table-checkable" id="testimonialList">
						<thead>
							<tr>
								<th>Title</th>
								<th>From Cusomer</th>
								<th>To Coach</th> 
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
              <?php $i = 0; ?>
                @foreach ($testimonials as  $value)

                  <tr>
                    <td>{{$value->title}}</td>
                    <td>{{ $customer[$i]->name }}</td>
                    <td>{{ $coach[$i]->name }}</td>
                    <?php $i++;?>
                    <td class="text-nowrap">
                      <a href="{{ route('coachTestimonial.edit',[$value->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                      <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelete({{$value->id}})">  <i class="fa fa-times"></i></a>
                      <a href="{{ route('coachTestimonial.detailView',[$value->id]) }}" data-toggle="tooltip" data-original-title="View All Detail" class="btn btn-primary btn-circle"> <i class="fa fa-eye"></i> </a>
                    </td>
                  </tr>
                @endforeach
          </tbody>
					</table>
					<!--end: Datatable-->
				</div>
			</div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">


  function comfrimDelete(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!", 
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax('https://vkapsprojects.com/pedro/skeleton/public/coach/testimonial/delete/'+id, {
          method: 'DELETE',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        }).done(function (r) {
          console.log('result',r); 
          
         if(r.success){
            window.location.reload();
          }else{                
            swal.fire({
            text: 'Event does not exist..',
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
              confirmButton: "btn font-weight-bold btn-light-primary"
            }
          }).then(function () {
            KTUtil.scrollTop();
          });            
          }    
        }).fail(function (jqXHR, textStatus, errorThrown) {             
            swal.fire({
            text: errors,
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
              confirmButton: "btn font-weight-bold btn-light-primary"
            }
          }).then(function () {
            KTUtil.scrollTop();
          });    
        });
      }
    });
  }
</script>
<script type="text/javascript">
     $('#testimonialList').dataTable({
        // "scrollX": true,
        // "scrollY": true, 
    });
  </script>
@endsection
