 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Coach Assignment')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Coach assignment to Customer <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('customer_assign.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
					@if ($message = Session::get('error'))
			        <div class="alert alert-danger">
			            {{ $message }}
			        </div>
			        @elseif ($message = Session::get('succcess'))
			         <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
				<div class="card-body">
				<!--begin::Form-->
				<form class="form"  method="POST" action="{{ route('customer_assign.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-assign-coach-to-customer" name="add-assign-coach-to-customer">
					  @csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Assignment Info:</h3>
								<div class="form-group row">
				                    <label class="col-3">Coach Name<span style="color:red">*</span></label>
				                    <div class="col-9">
				                      <select class="form-control" name="coach_assign_id" id="coach_assign_id">
				                        <option value= "">Select Coach</option>
				                          @foreach ($coaches as $value)
				                            <option value="{{$value->id}}" {{ old('coach_assign_id') == $value->id ? 'selected' : '' }}>{{$value->name}}</option>
				                          @endforeach
				                      </select>
				                      @error('coach_assign_id')
				                        <div class="alert alert-danger">{{ $message }}</div>
				                      @enderror
				                    </div>
				                  </div>

			                  <div class="form-group row">
			                    <label class="col-3">Customer Name<span style="color:red">*</span></label>
			                    <div class="col-9">
			                      <select class="form-control multiple_customer_assign" name="coach_assign_customer_id">
			                        <option value= "">Select Customer</option>
			                          @foreach ($customers as $value)
			                            <option value="{{$value->id}}" {{ old('coach_assign_customer_id') == $value->id ? 'selected' : '' }}>{{$value->name}}</option>
			                          @endforeach
			                      </select>
			                      @error('coach_assign_customer_id')
			                        <div class="alert alert-danger">{{ $message }}</div>
			                      @enderror
			                    </div>
			                  </div>

			                  <div class="form-group text-center m-t-20">
		                    		<div class="col-xs-12">
		                        		<button class="btn btn-primary" type="submit">  {{ __('Assign') }}</button>
		                        		<a class="btn btn-primary" type="button" href="">{{ __('Cancel') }}</a>
		                    		</div>
								</div>
							</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?fgf') }}" type="text/javascript"></script>

<script type="text/javascript">
FormValidation.formValidation(
 document.getElementById('add-assign-coach-to-customer'),
 {
  fields: {
   coach_assign_id: {
    validators: {
     notEmpty: {
      message: 'Coach is required'
     },
    }
   },
   coach_assign_customer_id:{
    validators: {
     notEmpty: {
      message: 'Customer is required'
     },
    }
   },
  },

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
   icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
  }
 }
);
</script>

@endsection
