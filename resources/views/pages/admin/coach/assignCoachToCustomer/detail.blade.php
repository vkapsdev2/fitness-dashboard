 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
    	<div class="col-lg-4">
    		<div class="card cutomer_image_sec">
    			<div class="view_profile_details card-body">
    				
					@if ($customer[0]->image)
									<img style= "height:200px;" src="{{ url('/'). '/public/media/customer-image/'.$customer[0]->image}}" />
									@else
									<img style= "height:200px;" src="{{ url('/'). '/public/media/users/blank.png'}}"/>
									@endif
						
					<h6>{{ $customer[0]->name }} </h6>
				</div>
				 
				 <div class="customer_basic_detail">
					 	<div class="show_email">
					 		<span>Email address</span>
					 		<p>{{ $customer[0]->email }}</p>
					 	</div>
					 	<div class="show_phone">
					 		<span>Phone Number</span>
					 		<p>{{ $customer[0]->phone }}</p>
					 	</div>
						<div class="show_address">
					 		<span>Address</span>
					 		<p>{{ $customer[0]->address }} ,  {{ $customer[0]->city }} , {{ $customer[0]->state }} , {{ $customer[0]->zip }}</p>
					 	</div>
					 	
				 </div>
    		</div>
    	</div> 
        <div class="col-lg-8">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Personal Detail: <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route('customer_assign.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="customer_other_details">
					<div class="row personal_sec">
						<div class="col-xl-12">
							<h3 class=" text-dark font-weight-bold mb-10"></h3>
							<div class="form-group ">
								<label class="col-3 col-form-label">Name</label>
								{{ $customer[0]->name }}
							</div>
							<div class="form-group">
								 <label class="col-3 col-form-label">Gender</label>
								   	@if ($customer[0]->gender == "1")
								   		Male
								   	@else
								   		Female
								   	@endif
							</div>
							<div class="form-group ">
								 <label class="col-3 col-form-label">Level</label>
							   @if ($customer[0]->level == "1")
							    	Biginner
							    @elseif ($customer[0]->level == "2")
							    	 Intermediate
							    @else
							    	Advance
							    @endif
							 </div>
							<div class="form-group">
					       		<label class="col-3">Weight</label>
					       		{{ $customer[0]->weight }} <span> kg</span>
					     	</div>
					     	<div class="form-group">
				    			<label class="col-3"> Height</label>
				    			{{ $customer[0]->height }}
			   				</div>
		   					<div class="form-group">
						       <label class="col-3">Age</label>
						       	{{ $customer[0]->age }} years
						    </div>
						    <div class="form-group">
						       <label class="col-3">Body Fat %</label>
						       	{{ $customer[0]->body_fat_percent }} <span>%</span>
						    </div>
						    <div class="form-group">
						       <label class="col-3">Body Fat Mass</label>
						       {{ $customer[0]->body_fat_mass }}
						    </div>
						    <div class="form-group">
						       <label class="col-3">Skeletal Muscle Mass</label>
						        {{ $customer[0]->skletal_muscle_mass }}
						    </div>
						</div>
					</div>
					
				</div>
			</div>
			<!-- Body measurment section -->
			<div class="card card-custom card-sticky" id="kt_page_sticky_card">
				
				<div class="customer_other_details">
					
					<div class="measurement_sec">
      					<div class="card card-custom card-sticky" id="kt_page_sticky_card">
    						<div class="separator separator-dashed my-10"></div>
     						<div class="my-5">
      						<h3 class=" text-dark font-weight-bold mb-10">Body Measurements:</h3>
						  	<div class="form-group">
						   		<label class="col-3">Neck</label>
						  	 {{ $customer[0]->neck }} <span>cm</span>
						  	</div>
     						<div class="form-group">
						       <label class="col-3">Shoulder</label>
						      	{{ $customer[0]->shoulder }} <span>cm</span>
						    </div>
						  	<div class="form-group">
						   		<label class="col-3">Right Bicep</label>
						  		{{ $customer[0]->right_bicep }} <span>cm</span>
						  	</div>
							<div class="form-group">
							   <label class="col-3">Chest</label>
							   {{ $customer[0]->right_bicep }} <span>cm</span>
							</div>
							<div class="form-group">
						       	<label class="col-3">Waist</label>
						       {{ $customer[0]->waist }} <span>cm</span>
						    </div>
							<div class="form-group">
	       						<label class="col-3">Hips</label>
	      	 					{{ $customer[0]->hips }} <span>cm</span>
	      					</div>
							<div class="form-group">
	       						<label class="col-3">Right Thigh</label>
	       						{{ $customer[0]->right_thigh }} <span>cm</span>
	      					</div>
	      					<div class="form-group">
	       						<label class="col-3">Right Calf</label>
	     		 				{{ $customer[0]->right_calf }} <span>cm</span>
	      					</div>
    						</div>
 						</div>
					</div>
				</div>
			</div>
			<!-- Satisfaction section -->
			<div class="card card-custom card-sticky" id="kt_page_sticky_card">
				
				<div class="customer_other_details">
					
					<div class="measurement_sec">
      					<div class="card card-custom card-sticky" id="kt_page_sticky_card">
    						<div class="separator separator-dashed my-10"></div>
     						<div class="my-5">
      						<h3 class=" text-dark font-weight-bold mb-10">Satisfaction:</h3>
						  	<div class="form-group">
						   		<label class="col-3">Career</label>
						  	 {{ $customer[0]->career }} 
						  	</div>

     						<div class="form-group">
						       <label class="col-3">Relationship</label>
						      	{{ $customer[0]->relationship }}
						    </div>

						  	<div class="form-group">
						   		<label class="col-3">Joy</label>
						  		{{ $customer[0]->joy }} 
						  	</div>

							<div class="form-group">
							   <label class="col-3">Health</label>
							   {{ $customer[0]->health }} 
							</div>

							<div class="form-group">
						       	<label class="col-3">Nutrition</label>
						       {{ $customer[0]->nutrition }} 
						    </div>

							<div class="form-group">
	       						<label class="col-3">Hips</label>
	      	 					{{ $customer[0]->hips }} 
	      	 				</div>

							<div class="form-group">
	       						<label class="col-3">Sleep</label>
	       						{{ $customer[0]->sleep }} 
	      					</div>

	      					<div class="form-group">
	       						<label class="col-3">Appearance</label>
	     		 				{{ $customer[0]->appearance }} 
	      					</div>

	      					<div class="form-group">
	       						<label class="col-3">Purpose</label>
	     		 				{{ $customer[0]->purpose }} 
	      					</div>

	      					<div class="form-group">
	       						<label class="col-3">Social</label>
	     		 				{{ $customer[0]->social }} 
	      					</div>

	      					<div class="form-group">
	       						<label class="col-3">Emotional/Spiritual</label>
	     		 				{{ $customer[0]->spiritual }}
	      					</div>

	      					<div class="form-group">
	       						<label class="col-3">Ready for Change</label>
	       						@if ($customer[0]->ready_for_change==1)
	       						Yes
	       						@else
	       						No
	     		 				@endif 
	      					</div>

    						</div>
 						</div>
					</div>
				</div>
			</div>
			<!-- End SATISFACTION SECTION -->

			
			<!-- FAMILY DETAIL SECTION -->
			<div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="customer_other_details">
					<div class="measurement_sec">
      					<div class="card card-custom card-sticky" id="kt_page_sticky_card">
    						<div class="separator separator-dashed my-10"></div>
     						<div class="my-5">
      							<h3 class=" text-dark font-weight-bold mb-10">GOAL</h3>
						  		<div class="row form-group">
						   			<div class="col-3 basic_dtl">
						   				<label>
						   				Goal
						   				</label>
						   			</div>
						   			<div class="col-9 basic_txt">
						   			<span >
						   				 @if ($customer[0]->social_goal ==1)
						   				 Fat Loss
						   				 @elseif ($customer[0]->social_goal ==2)
						   				 Weight Gain
						   				 @elseif ($customer[0]->social_goal ==3)
						   				 Muscle Gain

						   				 @else
						   				Other
						   				@endif
						   				
						   			</span>
						   			</div>
						   		</div>
						  	  	<div class="row form-group">
						  	  		<div class="col-3 basic_dtl">
							   			<label>Body Fat Mass
							   			</label>
							   		</div>
							   		<div class="col-9 basic_txt">
							   			<span>
							   				{{ $customer[0]->body_fat_mass }}
							   			</span>
							   		</div>
						   		</div>
						   		<div class="row form-group">
						  	  		<div class="col-3 basic_dtl">
							   			<label>Skeletal Muscle Mass
							   			</label>
							   		</div>
							   		<div class="col-9 basic_txt">
							   			<span>
							   				{{ $customer[0]->skletal_muscle_mass }}
							   			</span>
							   		</div>
						   		</div>
     						</div>
 						</div>
					</div>
				</div>
			</div>
			<!-- End FAMILY DETAIL SECTION -->
			<!-- PLAN SECTION -->
			<div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="customer_other_details">
					<div class="measurement_sec">
      					<div class="card card-custom card-sticky" id="kt_page_sticky_card">
    						<div class="separator separator-dashed my-10"></div>
     						<div class="my-5">
      							<h3 class=" text-dark font-weight-bold mb-10">PLAN DETAIL:</h3>
						  		<div class="form-group">
						   			<label class="col-3">Title</label>
						  	  		<span>{{$customer[0]->plan_title}}</span>
						  		</div>
						  		<div class="form-group">
						   			<label class="col-3">Actual Price</label>
						  	  		<span>$ {{$customer[0]->actual_price}}</span>
						  		</div>
						  		<div class="form-group">
						   			<label class="col-3">Sales Price</label>
						  	  		<span>$ {{$customer[0]->sales_price}}</span>
						  		</div>
						  		<div class="form-group">
						   			<label class="col-3">Type</label>
						  	  		@if ($customer[0]->plan_type == 1)
				                      <span class="label label-lg font-weight-bold label-light-primary label-inline">Paid</span>
				                      @else
				                      <span class="label label-lg font-weight-bold label-light-danger label-inline">Free</span>
				                      @endif
						  		</div>
						  		<div class="form-group">
						   			<label class="col-3">Type</label>
						  	  		<span> {{ $customer[0]->	validity_days }} Days</span>
						  		</div>
						  		<div class="form-group">
						   			<label class="col-3">Validity</label>
						  	  		<b>From: <span class="label label-lg font-weight-bold label-light-danger label-inline">{{ $customer[0]->plan_start_date }} </span> To: <span class="label label-lg font-weight-bold label-light-danger label-inline">  {{ $customer[0]->plan_end_date }}</span></b>
						  		</div>

						  	 </div>
						  	 </div>
						  	</div>
						</div>
					</div>
					<!-- END PLAN SECTION -->
		</div>
	</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  <style type="text/css">
    	.view_profile_details.card-body {
    text-align: center;

}

.view_profile_details img {max-width: 90px;height: auto !important;border-radius: 50%;}

.view_profile_details h3 {
    font-size: 16px;
    margin: 10px 0px 10px;
}

.view_profile_details h6 {
    color: #c5baba;
}


.view_profile_details.card-body 
{
	min-height: 480px;
}

.form-group h4 {
    padding: 0px 13px;
    font-size: 20px;
}
.customer_other_details {
    padding: 0px 16px;
}


.card.cutomer_image_sec .view_profile_details.card-body {
    min-height: auto;
}

.customer_basic_detail {
    padding: 15px;
    border-top: 2px solid #e4e4e4;
}

.customer_basic_detail .show_email {}

.customer_basic_detail span {
    color: #c5baba;
}
.form-group label {
    font-size: 1rem;
    font-weight: 400;
    color: #464E5F;
    position: relative;
}
.form-group div.basic_dtl{
	padding-left: 20px;
}
span{
    position: relative;
    min-height: auto;
}
    </style>
@endsection
