 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Suggest Meal <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('suggest_meals.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							
								<div class = "detail_img text-center">
								</div>
								<div class ="detail_name text-center description_txt_box">
									<div class="row">
									<div class="col plan_title"><p>By Coach: </p></div>
									<div class="col plan_title"><p><b> {{ $coach_detail[0]->name }} </b> <img style= "height:50px; width: 50px;" src="{{ url('/'). '/public/media/coach/'.$coach_detail[0]->image}}" class="rounded-circle"/></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>To Customer: </p></div>
									<div class="col plan_title"><p><b> {{ $customer_detail[0]->name }} </b><img style= "height:50px;" src="{{ url('/'). '/public/media/customer-image/'.$customer_detail[0]->image}}" class="rounded-circle"/></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Break Fast Meal: </p></div>

									<div class="col plan_title">
										<ul class="shake_hd_list">
											<?php 
											$breakFast_arr = explode(',',$suggested_meal->assign_break_fast_meal);
											for($i=0; $i<count($breakFastMeals); $i++)
											{
												 for($j=0; $j<count($breakFast_arr); $j++)
												{
													 if($breakFastMeals[$i]->id == $breakFast_arr[$j])
													 {
														echo '<li>'.$breakFastMeals[$i]->meal_title.'</li>';
													}
												}
											}
											
											?>
										
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Break Fast Meal Days: </p></div>
									<div class="col plan_title"><p><b> {{ $suggested_meal->break_fast_days }} Days</b></p></div>
								</div>

								<div class="row">
									<div class="col plan_title"><p>Lunch Meal: </p></div>
									<div class="col plan_title">
										<ul class="shake_hd_list">
											<?php 
											$lunch_arr = explode(',',$suggested_meal->assign_lunch_meal);
											for($i=0; $i<count($lunchMeals); $i++)
											{
												 for($j=0; $j<count($lunch_arr); $j++)
												{
													 if($lunchMeals[$i]->id == $lunch_arr[$j])
													 {
														echo '<li>'.$lunchMeals[$i]->meal_title.'</li>';
													}
												}
											}
											
											?>
										
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Lunch Meal Days: </p></div>
									<div class="col plan_title">
										<p><b> {{ $suggested_meal->lunch_days }} Days</b></p>
								</div>
							</div>

								<div class="row">
									<div class="col plan_title"><p>Dinner Meal: </p></div>
									<div class="col plan_title">
										<ul class="shake_hd_list">
											<?php 
											$dinner_arr = explode(',',$suggested_meal->assign_dinner_meal);
											for($i=0; $i<count($dinnerMeals); $i++)
											{
												 for($j=0; $j<count($dinner_arr); $j++)
												{
													 if($dinnerMeals[$i]->id == $dinner_arr[$j])
													 {
														echo '<li>'.$dinnerMeals[$i]->meal_title.'</li>';
													}
												}
											}
											
											?>
										
										</ul>
									</div>
									</div>
								
								<div class="row">
									<div class="col plan_title"><p>Dinner Meal Days: </p></div>
									<div class="col plan_title"><p><b> {{ $suggested_meal->	dinner_days }} Days</b></p></div>
								</div>
								
								<div class="row">
									<div class="col plan_title"><p>Other: </p></div>
									<div class="col plan_title"><p><b> {{ $suggested_meal->other_suggest_meal_detail }}</b></p>
									</div>
								</div>
								</div>

							</div>
 						</div>
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
.detail_name.text-center.description_txt_box {
    display: grid;
    text-align: center;
    text-align: left !important;
    padding: 10px 15% 10px 30%;
    font-size: 14px;
}
ul.shake_hd_list {
    list-style: none;
    font-weight: 600;
    padding-inline-start: 0px !important;
}
@media only screen and (max-width: 767px){
.detail_name.text-center.description_txt_box {
    padding: 10px 0% 10px 0%;
}
}

</style>