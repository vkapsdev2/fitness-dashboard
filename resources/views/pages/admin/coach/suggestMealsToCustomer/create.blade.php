 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Suggest Meal')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Suggest Meals To Customer <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('suggest_meals.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
					@if ($message = Session::get('error'))
			        <div class="alert alert-danger">
			            {{ $message }}
			        </div>
			        @elseif ($message = Session::get('succcess'))
			         <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
				<div class="card-body">
				<!--begin::Form-->
				<form class="form"  method="POST" action="{{ route('suggest_meals.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-suggest-meals" name="add-suggest-meals">
					  @csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Meals Info:</h3>
								<div class="form-group row">
				                    <label class="col-3">Coach Name<span style="color:red">*</span></label>
				                    <div class="col-9">
				                      <select class="form-control" name="suggester_coach_id">
				                        <option value= "" >Select Coach</option>
				                          @foreach ($coaches as $value)
				                            <option value="{{$value->id}}" {{ old('suggester_coach_id') == $value->id ? 'selected' : '' }}>{{$value->name}}</option>
				                          @endforeach
				                      </select>
				                      @error('suggester_coach_id')
				                        <div class="alert alert-danger">{{ $message }}</div>
				                      @enderror
				                    </div>
				                  </div>

			                  <div class="form-group row">
			                    <label class="col-3">Customer Name<span style="color:red">*</span></label>
			                    <div class="col-9">
			                      <select class="form-control" name="suggested_customer_id">
			                        <option value= "" >Select Customer</option>
			                          @foreach ($customers as $value)
			                            <option value="{{$value->id}}" {{ old('suggested_customer_id') == $value->id ? 'selected' : '' }}>{{$value->name}}</option>
			                          @endforeach
			                      </select>
			                      @error('suggested_customer_id')
			                        <div class="alert alert-danger">{{ $message }}</div>
			                      @enderror
			                    </div>
			                  </div>

			                  <div class="form-group row">
			                    <label class="col-3">Breakfast Meal Detail<span style="color:red">*</span></label>
			                    <div class="col-9">
			                      <select class="form-control selectpicker" name="assign_break_fast_meal[]" multiple="multiple">
			                        <option value= "" disabled="">Select Break Fast Meal</option>
			                         @foreach ($breakFastMeals as $breakfast)
			                            <option value="{{$breakfast->id}}" @if(is_array(old('assign_break_fast_meal')) && in_array($breakfast->id, old('assign_break_fast_meal'))) selected @endif>{{$breakfast->meal_title}}</option>
			                         @endforeach
			                      </select>
			                      @error('assign_break_fast_meal')
			                        <div class="alert alert-danger">{{ $message }}</div>
			                      @enderror
			                    </div>
			                  </div>

			                  <div class="form-group row ">
										<label class="col-3">Break Fast Days</label>
										<div class="col-9">
											<input class="form-control " type="text" name="break_fast_days" value="" placeholder="Enter Break Fast Days meal days" />
											@error('break_fast_days')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>

			                  <div class="form-group row">
			                    <label class="col-3">Lunch Meal Detail</label>
			                    <div class="col-9">
			                      <select class="form-control selectpicker" name="assign_lunch_meal[]" multiple="multiple">
			                        <option value= "" disabled="">Select Lunch Meal</option>
			                         @foreach ($lunchMeals as $lunch)
			                            <option value="{{$lunch->id}}" @if(is_array(old('assign_lunch_meal')) && in_array($lunch->id, old('assign_lunch_meal'))) selected @endif>{{$lunch->meal_title}}</option>
			                         @endforeach
			                      </select>
			                      @error('assign_lunch_meal')
			                        <div class="alert alert-danger">{{ $message }}</div>
			                      @enderror
			                    </div>
			                  </div>

			                  <div class="form-group row ">
										<label class="col-3">Lunch Days</label>
										<div class="col-9">
											<input class="form-control " type="text" name="lunch_days" value="" placeholder="Enter Lunch meal days" />
											@error('lunch_days')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>

			                  <div class="form-group row">
			                    <label class="col-3">Dinner Meal Detail</label>
			                    <div class="col-9">
			                      <select class="form-control selectpicker" name="assign_dinner_meal[]" multiple="multiple">
			                        <option value= "" disabled="">Select Dinner Meal</option>
			                         @foreach ($dinnerMeals as $dinner)
			                            <option value="{{$dinner->id}}" @if(is_array(old('assign_dinner_meal')) && in_array($dinner->id, old('assign_dinner_meal'))) selected @endif>{{$dinner->meal_title}}</option>
			                         @endforeach
			                      </select>
			                      @error('assign_dinner_meal')
			                        <div class="alert alert-danger">{{ $message }}</div>
			                      @enderror
			                    </div>
			                  </div>

			                  <div class="form-group row ">
										<label class="col-3">Dinner Days</label>
										<div class="col-9">
											<input class="form-control " type="text" name="dinner_days" value="" placeholder="Enter Dinner meal days" />
											@error('dinner_days')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>

			                  <div class="form-group row green-border-focus shadow-textarea">
						            <label class="col-3">Other Details</label>
						            <div class="col-9">
						              <textarea class="form-control"  name="other_suggest_meal_detail" placeholder="Write your other detail.." style="resize: none;">{{ old('other_suggest_meal_detail') }}</textarea>
						              @error('other_suggest_meal_detail')
						                <div class="alert alert-danger">{{ $message }}</div>
						              @enderror
						            </div>
        						</div>
							</div>



								<div class="form-group text-center m-t-20">
		                    		<div class="col-xs-12">
		                        		<button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                        		<a class="btn btn-primary" type="button" href="">{{ __('Cancel') }}</a>
		                    		</div>
								</div>
							</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?fgf') }}" type="text/javascript"></script>

  <script type="text/javascript">
  	/*$(document).ready(function()
  	{
		$('select[name="assign_meal_for"]').change(function()
		{
			var value = $(this).val();
		 $.ajax("https://vkapsprojects.com/pedro/skeleton/public/coach/suggest_meals/getMeal/"+value, {
		            method: 'GET',
		            headers: {
		              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		            }).done(function (r) {
		        	if(r.status){
		        		console.log('r.status',r.status);
		        		var meal_list = r.success;
		        		var meal_option = "<option value=''>Select Meal</option>";
		        		$(meal_list).each(function(index,item){

		                    var name = item.meal_title;
		                    var id = item.id;

		                    meal_option += "<option value ='"+id+"'>"+name+"</option>";
	                  });
	                  $('#meal_list_option').html(meal_option);
	                 
			        }
			        else
			        {
			        	console.log("no voutput");
			        }
		        });

  		});
  	});*/
  </script>

<script type="text/javascript">
FormValidation.formValidation(
 document.getElementById('add-suggest-meals'),
 {
  fields: {
   suggester_coach_id: {
    validators: {
     notEmpty: {
      message: 'Coach is required'
     },
    }
   },
   suggested_customer_id:{
    validators: {
     notEmpty: {
      message: 'Customer is required'
     },
    }
   },
   'assign_break_fast_meal[]':{
    validators: {
     choice: {
                min: 1,
                message: 'Please choose at least 1 Breakfast Meal'
            }
    }
   },
   break_fast_days:{
    validators: {
     digits: {
      message: 'The value is not a valid digit'
     }
    }
   },
   lunch_days:{
    validators: {
     digits: {
      message: 'The value is not a valid digit'
     }
    }
   },
    dinner_days:{
    validators: {
     digits: {
      message: 'The value is not a valid digit'
     }
    }
   },
   /*'assign_lunch_meal[]':{
    validators: {
     choice: {
                min: 1,
                message: 'Please choose atleast 1 Lunch Meal'
                        }
    }
   },
   'assign_dinner_meal[]':{
    validators: {
     notEmpty: {
      message: 'Dinner Meal is required'
     },
    }
   },*/
  
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
   icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
  }
 }
);
</script>

@endsection
