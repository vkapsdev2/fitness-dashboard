{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Coach')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row">
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
            <div class="card-title"><h3 class="card-label">Coach <i class="mr-2"></i></h3>
            </div>
            <div class="card-toolbar">
                <a href="{{route ('coach.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
                    <i class="ki ki-long-arrow-back icon-sm"></i>
                    Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <!--begin::Form-->
            <form class="form" id="add-coach" name="add-coach" action="{{ route('coach.save')}}" method="post" accept-charset="utf-8" enctype="multipart/form-data" >
                @csrf
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <div class="my-5">
                            <h3 class=" text-dark font-weight-bold mb-10">Coach Info:</h3>
                            <div class="form-group row">
                                <label class="col-3">Full Name<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" name= "name" value="{{ old('name') }}"/>
                                    @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Email Address<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <div class="input-group input-group-solid">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                        <input type="text" class="form-control form-control-solid" id="email" name = "email" placeholder="Email" value="{{ old('email') }}"/>
                                    </div>
                                    <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                                    <div class="email_ajax_message" style="color:red"></div>
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Password<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" name ="password" value="{{ old('password') }}" maxlength="6" />
                                    @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Contact Number<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <div class="input-group input-group-solid">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                        <input type="text" class="form-control form-control-solid" name="phone" value="{{ old('phone') }}" placeholder="Phone" maxlength="10" />
                                    </div>
                                    @error('phone')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">Gender</label>
                                <div class="col-9 col-form-label">
                                    <div class="radio-inline">
                                        <label class="radio">
                                            <input type="radio" name="gender" checked="checked" id="male" value="1"  {{ old('gender') == "2" ? 'checked' : '' }}/>
                                            <span></span>
                                            Male
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="gender" id="female" value="2" {{ old('gender') == "2" ? 'checked' : '' }} />
                                            <span></span>
                                            Female
                                        </label>
                                    </div>
                                    @error('gender')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Image</label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="file" name="image" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5">
                            <h3 class=" text-dark font-weight-bold mb-10">Address Details:</h3>
                            <div class="form-group row">
                                <label class="col-3">Address<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ old('address') }}" name="address"/>
                                    @error('address')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">City<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ old('city') }}" name="city"/>
                                    @error('city')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">State<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ old('state') }}" name="state"/>
                                    @error('state')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Zip / Postal Code<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <input class="form-control form-control-solid" type="text" value="{{ old('zip') }}" name="zip" maxlength="6" />
                                    @error('zip')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- START SOCIAL SECTION -->
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5">
                            <h3 class=" text-dark font-weight-bold mb-10">Social Details:</h3>
                            <div class="form-group row">
                                <label class="col-3">Facebook</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="facebook_url" value="{{ old('facebook_url') }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your Facebook website URL.</span>
                                    @error('facebook_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Google</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="google_url" value="{{ old('google_url') }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your Google website URL.</span>
                                    @error('google_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Twitter</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="twitter_url" value="{{ old('twitter_url') }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your  Twitter website URL.</span>
                                    @error('twitter_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">YouTube</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="youtube_url" value="{{ old('youtube_url') }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your YouTube website URL.</span>
                                    @error('youtube_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Instagram</label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon2-website"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-solid" name="instagram_url" value="{{ old('instagram_url') }}" />
                                    </div>
                                    <span class="form-text text-muted">Please enter your Instagram website URL.</span>
                                    @error('instagram_url')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- END   SOCIAL SECTION -->

                        <!-- START PROFESSIONAL SECTION -->
                        <div class="separator separator-dashed my-10"></div>
                        <div class="my-5">
                            <h3 class=" text-dark font-weight-bold mb-10">Specializations Details:</h3>
                            <div class="form-group row">
                                <label class="col-3">Specializations<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <select class="form-control form-control-solid" name="specializations">
                                        <option value="">Select Specialization</option>
                                        <option value="Strength and conditioning coach" {{ old('specializations') == "Strength and conditioning coach" ? 'selected' : '' }}>Strength and conditioning coach</option>
                                        <option value="Group exercise instructor"  {{ old('specializations') == "Group exercise instructor" ? 'selected' : '' }}>Group exercise instructor</option>
                                        <option value="Fitness manager" {{ old('specializations') == "Fitness manager" ? 'selected' : '' }}>Fitness manager</option>
                                        <option value="Senior fitness specialist" {{ old('specializations') == "Senior fitness specialist" ? 'selected' : '' }}>Senior fitness specialist</option>
                                        <option value="Youth fitness specialist" {{ old('specializations') == "Youth fitness specialist" ? 'selected' : '' }}>Youth fitness specialist</option>
                                        <option value="Weight loss transformation specialist" {{ old('specializations') == "Weight loss transformation specialist" ? 'selected' : '' }}>Weight loss transformation specialist</option>
                                        <option value="Bodybuilding specialist" {{ old('specializations') == "Bodybuilding specialist" ? 'selected' : '' }}>Bodybuilding specialist</option>
                                        <option value="Corrective exercise specialist" {{ old('specializations') == "Corrective exercise specialist" ? 'selected' : '' }}>Corrective exercise specialist</option>
                                        <option value="Health coaching" {{ old('specializations') == "Health coaching" ? 'selected' : '' }}>Health coaching</option>
                                    </select>
                                    @error('specializations')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Certifications<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <textarea class="form-control form-control-solid"  name="certification" placeholder="Enter your answer here.." style="resize: none;">{{ old('certification') }}</textarea>
                                    @error('certification')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Awards<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <div class="input-group">
                                        <textarea class="form-control form-control-solid"  name="awards" placeholder="Enter your answer here.." style="resize: none;">{{ old('awards') }}</textarea>
                                    </div>
                                    @error('awards')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Publish Articles<span style="color:red">*</span></label>
                                <div class="col-9">
                                    <div class="input-group">
                                        <textarea class="form-control form-control-solid"  name="publish_articles" placeholder="Enter your answer here.." style="resize: none;">{{ old('publish_articles') }}</textarea>
                                    </div>
                                    @error('publish_articles')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3">Personal Training & Client Feedback<span style="color:red">*</span></label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <textarea class="form-control form-control-solid"  name="client_feedback" placeholder="Enter your answer here.." style="resize: none;">{{old('client_feedback')}}</textarea>
                                    </div>
                                    @error('client_feedback')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- END PROFESSIONAL SECTION -->
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
                                <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            <!--end::Form-->
        </div>
        <div class="col-xl-2"></div>
    </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <style type="text/css">
        div#kt_page_sticky_card {
            width: 100%;
        }
        .green-border-focus .form-control:focus {
            border: 1px solid #8bc34a;
            box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, .25);
        }
        .shadow-textarea textarea.form-control::placeholder {
            font-weight: 300;
        }
        .shadow-textarea textarea.form-control {
            padding-left: 0.8rem;
        }
    </style>
    <script type="text/javascript">  
        FormValidation.formValidation(
            document.getElementById('add-coach'),
            {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Coach name is required'
                            },
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email is required'
                            },
                            emailAddress: {
                                message: 'The value is not a valid email address'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Password is required'
                            },
                        }
                    },
                    phone: {
                        validators: {
                            notEmpty: {
                                message: 'Phone number is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    address: {
                        validators: {
                            notEmpty: {
                                message: 'Address is required'
                            },
                        }
                    },
                    state: {
                        validators: {
                            notEmpty: {
                                message: 'State is required'
                            },
                        }
                    },
                    city: {
                        validators: {

                            notEmpty: {
                                message: 'City is required'
                            },
                        }
                    },
                    zip: {
                        validators: {
                            notEmpty: {
                                message: 'Zip is required'
                            },
                            digits: {
                                message: 'The value is not a valid digit'
                            }
                        }
                    },
                    facebook_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    google_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    twitter_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    youtube_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    instagram_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },
                    specializations:{
                        validators: {
                            notEmpty: {
                                message: 'Specialization is required'
                            },
                        }
                    },
                    certification:{
                        validators: {
                            notEmpty: {
                                message: 'Certification is required'
                            },
                        }
                    },
                    awards:{
                        validators: {
                            notEmpty: {
                                message: 'Awards is required'
                            },
                        }
                    },
                    publish_articles:{
                        validators: {
                            notEmpty: {
                                message: 'Publish Article is required'
                            },
                        }
                    },
                    client_feedback:{
                        validators: {
                            notEmpty: {
                                message: 'Client Feedback is required'
                            },
                        }
                    },
                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        );
    </script>
    <script type="text/javascript">
        $("#email").keyup(function(event){
            event.preventDefault();
            var email = $('#email').val();
            customEmailValidation(email);
        });
        function customEmailValidation(email){
            var formData = {
                email: email,
            };

            $.ajax({
                type: "GET",
                url: 'https://vkapsprojects.com/pedro/skeleton/coach/email_ajax_customer',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data.success);
                    if(data.success){
                        $("div.email_ajax_message").html('<div>Email already exist, use other email address</div>');
                        $("#email").addClass('is-invalid');
                    }else{
                        $("div.email_ajax_message").html('<div></div>');
                        $("#email").removeClass('is-invalid').addClass('is-valid');
                    }

                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    </script>
 @endsection
