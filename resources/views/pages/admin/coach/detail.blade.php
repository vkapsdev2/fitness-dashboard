{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row">
	<div class="col-lg-4">
		<div class="card cutomer_image_sec">
			<div class="view_profile_details card-body">

				@if ($coach[0]->image)
				<img src="{{ url('/'). '/public/media/coach/'.$coach[0]->image}}" />
				@else
				<img src="{{ url('/'). '/public/media/users/blank.png'}}"/>
				@endif
				<br/><br/>
				<h6>{{ $coach[0]->name }} </h6>
			</div>

			<div class="customer_basic_detail">
				<div class="show_email">
					<span>Email address</span>
					<p>{{ $coach[0]->email }}</p>
				</div>
				<div class="show_phone">
					<span>Phone Number</span>
					<p>{{ $coach[0]->phone }}</p>
				</div>
				<div class="show_address">
					<span>Address</span>
					<p>{{ $coach[0]->address }} ,  {{ $coach[0]->city }} , {{ $coach[0]->state }}, {{ $coach[0]->zip }} </p>
				</div>
				<div>
					<small class="text-muted pt-4 d-block">Social Profile</small>
					<br />
					<a class="btn btn-circle btn-secondary" href="{{$coach[0]->facebook_url}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
					<a class="btn btn-circle btn-secondary" href="{{$coach[0]->google_url}}" target="_blank"><i class="fab fa-google"></i></a>
					<a class="btn btn-circle btn-secondary" href="{{$coach[0]->twitter_url}}" target="_blank"><i class="fab fa-twitter"></i></a>
					<a class="btn btn-circle btn-secondary" href="{{$coach[0]->youtube_url}}" target="_blank"><i class="fab fa-youtube"></i></a>
					<a class="btn btn-circle btn-secondary" href="{{$coach[0]->instagram_url}}" target="_blank"><i class="fab fa-instagram"></i></a>

				</div>
			</div>
		</div>
	</div> 
	<div class="col-lg-8">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Personal Detail: <i class="mr-2"></i>
					</h3>
				</div>
				<div class="card-toolbar">
					<a href="{{ route('coach.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
						<i class="ki ki-long-arrow-back icon-sm"></i>
						Back
					</a>
				</div>
			</div>
			<div class="customer_other_details">
				<div class="row personal_sec">
					<div class="col-xl-12">
						<h3 class=" text-dark font-weight-bold mb-10"></h3>
						<div class="form-group ">
							<label class="col-3 col-form-label">Name</label>
							{{ $coach[0]->name }}
						</div>
						<div class="form-group">
							<label class="col-3 col-form-label">Gender</label>
							@if ($coach[0]->gender == "1")
							Male
							@else
							Female
							@endif
						</div>


					</div>
				</div>

			</div>
		</div>

		<!-- SPECIALIZATION SECTION -->
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="customer_other_details">
				<div class="measurement_sec">
					<div class="card card-custom card-sticky" id="kt_page_sticky_card">
						<div class="separator separator-dashed my-10"></div>
						<div class="my-5">
							<h3 class=" text-dark font-weight-bold mb-10">SPECIALIZATION Details:</h3>

							<div class="form-group">
								<label class="col-3">Specializations
								</label>
								{{ $coach[0]->specializations }}
							</div>

							<div class="form-group">
								<label class="col-3">Certifications
								</label>
								{{ $coach[0]->certification }}
							</div>

							<div class="form-group">
								<label class="col-3">Awards
								</label>
								{{ $coach[0]->awards }}
							</div>
							<div class="form-group">
								<label class="col-3">Publish Articles
								</label>
								{{ $coach[0]->publish_articles }}
							</div>
							<div class="form-group">
								<label class="col-3">Personal Training & Client Feedback
								</label>
								{{ $coach[0]->client_feedback }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End FAMILY DETAIL SECTION -->
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
	<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  	<style type="text/css">
  		.view_profile_details.card-body {
  			text-align: center;

  		}
  		.view_profile_details img {
  			/*max-width: 90px;*/
  			/*height: auto !important;*/
  			/*border-radius: 50%;*/
  			height: 150px;
  			width: 150px;
  			border-radius: 50%;
  		}
  		.view_profile_details h3 {
  			font-size: 16px;
  			margin: 10px 0px 10px;
  		}
  		.view_profile_details h6 {
  			color: #c5baba;
  		}
  		.view_profile_details.card-body 
  		{
  			min-height: 480px;
  		}
  		.form-group h4 {
  			padding: 0px 13px;
  			font-size: 20px;
  		}
  		.customer_other_details {
  			padding: 0px 16px;
  		}
  		.card.cutomer_image_sec .view_profile_details.card-body {
  			min-height: auto;
  		}
  		.customer_basic_detail {
  			padding: 15px;
  			border-top: 2px solid #e4e4e4;
  		}
  		.customer_basic_detail .show_email {}

  		.customer_basic_detail span {
  			color: #c5baba;
  		}
	</style>
@endsection
