 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Meal <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('meals.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							
								<div class = "detail_img text-center">
									@if ($meal->meal_image)
									<img style= "height:200px;" src="{{ url('/'). '/public/media/meal/'.$meal->meal_image}}" />
									@else
									<img style= "height:200px;" src="{{ url('/'). '/public/media/book-banner/Product-Dummy.jpg'}}"/>
									@endif
								</div>
								<div class ="detail_name text-center description_txt_box">
									<div class="row">
									<div class="col plan_title"><p>Created By: </p></div>
									<div class="col plan_title"><p><b> {{ $created_by_name[0]->name }} </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Title: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_title }} </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Calories: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_calories }} Cal </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Protein: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_protein }} gm </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Carbs: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_carbs }} gm </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Fats: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_fats }} gm</b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Fiber: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_fiber }} gm</b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Minerals: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_minerals }} gm</b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Calcium: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_calcium }} gm</b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Vitmins: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_vitmins }}</b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Water: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_water }} gm</b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Other: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->other_meal_detail }}</b></p></div>
								</div>

								<div class="row">
									<div class="col plan_title"><p>Quantity: </p></div>
									<div class="col plan_title"><p><b> {{ $meal->meal_quantity }}</b></p></div>
								</div>

								<div class="row">
									<div class="col plan_title"><p>Meal For: </p></div>
									<div class="col plan_title"><p><b>
									@if($meal->meal_plan_type ==1)
					                    BREAKFAST 
					                    @elseif ($meal->meal_plan_type ==2)
					                    LUNCH
					                    @elseif ($meal->meal_plan_type ==3)
					                    DINNER
					                    @endif 
									</b></p></div>
								</div>
	 
								</div>
									<div class="detail_description text-center>">
									{{ strip_tags($meal->meal_description) }}
								</div>
							</div>
 						</div>
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
.detail_name.text-center.description_txt_box {
    display: grid;
    text-align: center;
    text-align: left !important;
    padding: 10px 15% 10px 30%;
    font-size: 14px;
}
ul.shake_hd_list {
    list-style: none;
    font-weight: 600;
    padding-inline-start: 0px !important;
}
@media only screen and (max-width: 767px){
.detail_name.text-center.description_txt_box {
    padding: 10px 0% 10px 0%;
}
}

</style>