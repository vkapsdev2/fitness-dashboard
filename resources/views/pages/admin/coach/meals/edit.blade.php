 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Meals')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Edit Meals <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('meals.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">
				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('meals.update',[$meal->id]) }}" accept-charset="utf-8" enctype="multipart/form-data" id="edit-shakeBar" name="edit-meals">
					  @csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Meals Info:</h3>
								<div class="form-group row">
									<label class="col-3">Title<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_title" value="{{ $meal->meal_title }}" placeholder="Enter title"/>
										@error('meal_title')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
                    				</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="file" name="meal_image" value="{{ public_path().'/media/meal/'.$meal->meal_image}}"/>

										<img style="height: 50px;" src="{{ url('/'). '/public/media/meal/'.$meal->meal_image}}" />
											@error('meal_image')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
							</div>
								<div class="separator separator-dashed my-10"></div>
								<div class="my-5">
									<h3 class=" text-dark font-weight-bold mb-10">Nutrients Details:</h3>
								<div class="form-group row">
									<label class="col-3">Calories<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_calories" value="{{ $meal->meal_calories }}" placeholder="Enter Calories"  maxlength="6"/>
										@error('meal_calories')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Protein<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_protein" value="{{ $meal->meal_protein }}" placeholder="Enter Protein"  maxlength="6"/>
										@error('meal_protein')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Carbs/Lipids<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_carbs" value="{{ $meal->meal_carbs }}" placeholder="Enter Carbs"  maxlength="6"/>
										@error('meal_carbs')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Fats<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_fats" value="{{ $meal->meal_fats }}" placeholder="Enter Fats"  maxlength="6"/>
										@error('meal_fats')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Fiber<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_fiber" value="{{ $meal->meal_fiber }}" placeholder="Enter Fiber"  maxlength="6"/>
										@error('meal_fiber')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Dietary Minerals<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_minerals" value="{{ $meal->meal_minerals }}" placeholder="Enter Dietary Minerals"  maxlength="6"/>
										@error('meal_minerals')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								

								<div class="form-group row">
									<label class="col-3">Calcium<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_calcium" value="{{ $meal->meal_calcium }}" placeholder="Enter Calcium"  maxlength="6"/>
										@error('meal_calcium')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Vitamins<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_vitmins" value="{{ $meal->meal_vitmins }}" placeholder="Enter vitmins"  maxlength="6"/>
										@error('meal_vitmins')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Water Level</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_water" value="{{ $meal->meal_water }}" placeholder="Enter Water"  maxlength="6"/>
										@error('meal_water')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row green-border-focus shadow-textarea">
						            <label class="col-3">Other Details</label>
						            <div class="col-9">
						              <textarea class="form-control form-control-solid"  name="other_meal_detail" placeholder="Write your other detail.." style="resize: none;">{{ $meal->other_meal_detail }}</textarea>
						              @error('other_meal_detail')
						                <div class="alert alert-danger">{{ $message }}</div>
						              @enderror
						            </div>
        						</div>
        					</div>
        					<div class="separator separator-dashed my-10"></div>
								<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Other Info:</h3>
								<div class="form-group row">
									<label class="col-3">Quantity<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="meal_quantity" value="{{ $meal->meal_quantity }}" placeholder="Enter Quantity"  />
										@error('meal_quantity')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Meal Plan Type <span style="color:red">*</span></label>
									<div class="col-9">
										<label class="radio radio-rounded">
					                    <input type="radio" class="form-control" name="meal_plan_type" value="1" {{ $meal->meal_plan_type == "1" ? 'checked' : '' }}/>
					                    <span></span>
					                   
					                   BREAKFAST
					                    </label>
					                    
					                     <label class="radio radio-rounded">
							                  <input type="radio" name="meal_plan_type" class ="meal_plan_type"   value="2"  {{ $meal->meal_plan_type == "2" ? 'checked' : '' }}/>
							                  <span></span>
							                LUNCH
							              </label>

							             <label class="radio radio-rounded">
							                  <input type="radio" name="meal_plan_type" class ="meal_plan_type"   value="3"  {{ $meal->meal_plan_type == "3" ? 'checked' : '' }}/>
							                  <span></span>
							                DINNER
							              </label>

					                    @error('meal_plan_type')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror
									</div>
								</div>

								<div class="form-group row green-border-focus shadow-textarea">
						            <label class="col-3">Description</label>
						            <div class="col-9">
						              <textarea class="form-control form-control-solid"  name="meal_description" placeholder="Write your description here.." style="resize: none;">{{ $meal->meal_description }}</textarea>
						              @error('meal_description')
						                <div class="alert alert-danger">{{ $message }}</div>
						              @enderror
						            </div>
        						</div>

        						<div class="form-group row">
							      <label class="col-3 col-form-label">Status<span style="color:red">*</span></label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="meal_status" checked="checked"  value="1"  {{ $meal->meal_status == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                  <input type="radio" name="meal_status" value="2" {{ $meal->meal_status == "2" ? 'checked' : '' }} />
							                  <span></span>
							                  Deactive
							              </label>
							          </div>
							           @error('meal_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>

							  	<div class="form-group text-center m-t-20">
		                    		<div class="col-xs-12">
		                        		<button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
		                        		<a class="btn btn-primary" type="button" href="">{{ __('Cancel') }}</a>
		                    		</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?fgf') }}" type="text/javascript"></script>

<script type="text/javascript">


FormValidation.formValidation(
 document.getElementById('edit-meals'),
 {
  fields: {
   meal_title: {
    validators: {
     notEmpty: {
      message: 'Title is required'
     },
    }
   },
   meal_image: {
    validators: {
     notEmpty: {
      message: 'Image is required'
     },
    }
   },
   meal_calories: {
        validators: {
        notEmpty: {
			      message: 'Calories is required'
			     },
         numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: '',
                decimalSeparator: '.'
            }
        }
    },
	meal_protein: {
    validators: {
     notEmpty: {
      message: 'Protein is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: ',',
                decimalSeparator: '.'
            }
        }
    },
   meal_carbs: {
    validators: {
     notEmpty: {
      message: 'Carbs is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: ',',
                decimalSeparator: '.'
            }
        }
   },
   meal_fats: {
    validators: {
     notEmpty: {
      message: 'Fats is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: ',',
                decimalSeparator: '.'
            }
        }
   },
   meal_fiber: {
    validators: {
     notEmpty: {
      message: 'Fiber is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: ',',
                decimalSeparator: '.'
            }
        }
   },
   meal_minerals: {
    validators: {
     notEmpty: {
      message: 'Minerals is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: ',',
                decimalSeparator: '.'
            }
        }
   },
   meal_calcium: {
    validators: {
     notEmpty: {
      message: 'Calcium is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: ',',
                decimalSeparator: '.'
            }
        }
   },
   meal_vitmins: {
    validators: {
     notEmpty: {
      message: 'Vitmins is required'
     }
    }
   },
   meal_quantity: {
    validators: {
     notEmpty: {
      message: 'Quantity is required'
     }
    }
   },
   meal_meal_plan_type: {
    validators: {
     notEmpty: {
      message: 'Meal Plan type is required'
     }
    }
   },
   
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>
@endsection
