 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Customer FoodLog')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

  <div class="row">  
    <div class="col-lg-12">
      <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
          <div class="card-title">
            <h3 class="card-label">
              Customer FoodLog <i class="mr-2"></i>
            </h3>
          </div>
          <div class="card-toolbar">
            <a href="{{route ('cust_food_log.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
              <i class="ki ki-long-arrow-back icon-sm"></i>
              Back
            </a>
          </div> 
        </div>
        <div class="card-body">
          <!--begin::Form-->
          <form class="form" id="add-customer-foodlog" name="add-customer-foodlog" action="{{ route('cust_food_log.save')}}" method="post" accept-charset="utf-8" enctype="multipart/form-data" >
            @csrf
            <div class="row"> 
              <div class="col-xl-12">
                <div class="my-5">
                  <h3 class=" text-dark font-weight-bold mb-10">Customer 3 Days FoodLog Info:</h3>
                  <div class="form-group row">
                    <label class="col-3">Customer Name<span style="color:red">*</span></label>
                    <div class="col-9">
                      <select class="form-control" name="customer_id">
                        <option value= "" >Select Customer</option>
                          @foreach ($customers as $value)
                            <option value="{{$value->id}}">{{$value->name}}</option>
                          @endforeach
                      </select>
                      @error('customer_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-3">1 Day Food Log<span style="color:red">*</span></label>
                    <div class="col-9">
                      <div class="input-group">
                       <input type="text" class="form-control" name = "first_day_foodlog" placeholder="1 Day Food Log" value="{{ old('first_day_foodlog') }}"/>
                      </div>
                        @error('first_day_foodlog')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-3">2 Day Food Log<span style="color:red">*</span></label>
                    <div class="col-9">
                      <div class="input-group">
                       <input type="text" class="form-control" name = "second_day_foodlog" placeholder="2 Day Food Log" value="{{ old('second_day_foodlog') }}"/>
                      </div>
                        @error('second_day_foodlog')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-3">3 Day Food Log<span style="color:red">*</span></label>
                    <div class="col-9">
                      <div class="input-group">
                       <input type="text" class="form-control" name = "third_day_foodlog" placeholder="3 Day Food Log" value="{{ old('third_day_foodlog') }}"/>
                      </div>
                        @error('third_day_foodlog')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                  </div>
                  <!-- END PLANN DETAIL SECTION -->
                  <div class="separator separator-dashed my-10"></div>
                  <div class="form-group text-center m-t-20">
                      <div class="col-xs-12">
                          <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
                           <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <!--end::Form-->
        </div>
      </div>
    </div>  
  </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
 <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
 <script type="text/javascript">
    FormValidation.formValidation(
       document.getElementById('add-customer-foodlog'),
       {
        fields: {
         customer_id: {
          validators: {
           notEmpty: {
            message: 'Customer Name is required'
           }
          }
         },
         first_day_foodlog: {
          validators: {
           notEmpty: {
            message: 'First Day Food Log is required'
           }
          }
         },
         second_day_foodlog: {
          validators: {
           notEmpty: {
            message: 'Second Day Food Log is required'
           }
          }
         },
         third_day_foodlog: {
          validators: {
           notEmpty: {
            message: 'Third Day Food Log is required'
           }
          }
         },

        },

        plugins: {
         trigger: new FormValidation.plugins.Trigger(),
         // Bootstrap Framework Integration
         bootstrap: new FormValidation.plugins.Bootstrap(),
         // Validate fields when clicking the Submit button
         submitButton: new FormValidation.plugins.SubmitButton(),
                  // Submit the form when all fields are valid
         defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        },
      }
    );
  </script>
 @endsection
