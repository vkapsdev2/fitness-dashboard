 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Customer FoodLog')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

<div class="row">
  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    <div class="card-header">
      <div class="card-title">
       <h3 class="card-label">
        Customer FoodLog<i class="mr-2"></i>
       
       </h3>
      </div>
      <div class="card-toolbar">
       <a href="{{route ('cust_food_log.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        Back
       </a>
      </div>
    </div>
    <div class="card-body">
      <!--begin::Form-->
      <form class="form" id="add-customer" name="add-customer" action="{{ route('cust_food_log.save')}}" method="post" accept-charset="utf-8" enctype="multipart/form-data" >
        @csrf
        <div class="row"> 
          <div class="col-xl-12">
            <div class="my-5">
              <h3 class=" text-dark font-weight-bold mb-10">Customer 3 Days FoodLog Info:</h3>
              <div class="form-group row">
                <label class="col-3">Customer Name<span style="color:red">*</span></label>
                <div class="col-9">
                  <select class="form-control" name="customer_id">
                    <option value= "" >Select Customer</option>
                      @foreach ($customers as $value)
                        <option value="{{$value->id}}">{{$value->name}}</option>
                      @endforeach
                  </select>
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-3">1 Day Food Log<span style="color:red">*</span></label>
                <div class="col-9">
                  <div class="input-group">
                   <input type="text" class="form-control" name = "1day_foodlog" placeholder="1 Day Food Log" value="{{ old('1day_foodlog') }}"/>
                  </div>
                    @error('1day_foodlog')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-3">2 Day Food Log<span style="color:red">*</span></label>
                <div class="col-9">
                  <div class="input-group">
                   <input type="text" class="form-control" name = "2day_foodlog" placeholder="2 Day Food Log" value="{{ old('2day_foodlog') }}"/>
                  </div>
                    @error('2day_foodlog')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-3">3 Day Food Log<span style="color:red">*</span></label>
                <div class="col-9">
                  <div class="input-group">
                   <input type="text" class="form-control" name = "3day_foodlog" placeholder="1 Day Food Log" value="{{ old('3day_foodlog') }}"/>
                  </div>
                    @error('3day_foodlog')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
              </div>
              <!-- END PLANN DETAIL SECTION -->
              <div class="separator separator-dashed my-10"></div>
              <div class="form-group text-center m-t-20">
                  <div class="col-xs-12">
                      <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
                       <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      <!--end::Form-->
    </div>
  </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')

 @endsection
