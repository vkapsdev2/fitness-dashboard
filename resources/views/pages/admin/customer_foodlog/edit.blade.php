{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Customer Food Log')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Customer Food Log <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('cust_food_log.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			    <div class="card-body">
					<!--begin::Form-->
					<form class="form" id="edit-customer-foodlog" method="POST" action="{{ route('cust_food_log.update',[$food_log->id]) }}" accept-charset="utf-8" enctype="multipart/form-data">
						  @csrf
						<div class="row">
							<div class="col-xl-2"></div>
							<div class="col-xl-8">
								<div class="my-5">
									<h3 class=" text-dark font-weight-bold mb-10">Customer Food Log Info:</h3>
									<div class="form-group row">
					                    <label class="col-3">Customer Name<span style="color:red">*</span></label>
					                    <div class="col-9">
					                      <select class="form-control" name="customer_id">
					                          @foreach ($customers as $value)
					                            <option value="{{$value->id}}" {{$food_log->customer_id == $value->id  ? 'selected' : ''}} >{{$value->name}}</option>
					                          @endforeach
					                      </select>
					                      @error('name')
					                        <div class="alert alert-danger">{{ $message }}</div>
					                      @enderror
					                    </div>
					                  </div>

					                  <div class="form-group row">
					                    <label class="col-3">1 Day Food Log<span style="color:red">*</span></label>
					                    <div class="col-9">
					                      <div class="input-group">
					                       <input type="text" class="form-control" name = "first_day_foodlog" placeholder="1 Day Food Log" value="{{$food_log->first_day_foodlog}}"/>
					                      </div>
					                        @error('first_day_foodlog')
					                          <div class="alert alert-danger">{{ $message }}</div>
					                        @enderror
					                    </div>
					                  </div>
					                  <div class="form-group row">
					                    <label class="col-3">2 Day Food Log<span style="color:red">*</span></label>
					                    <div class="col-9">
					                      <div class="input-group">
					                       <input type="text" class="form-control" name = "second_day_foodlog" placeholder="2 Day Food Log" value="{{$food_log->second_day_foodlog}}"/>
					                      </div>
					                        @error('second_day_foodlog')
					                          <div class="alert alert-danger">{{ $message }}</div>
					                        @enderror
					                    </div>
					                  </div>
					                  <div class="form-group row">
					                    <label class="col-3">3 Day Food Log<span style="color:red">*</span></label>
					                    <div class="col-9">
					                      <div class="input-group">
					                       <input type="text" class="form-control" name = "third_day_foodlog" placeholder="3 Day Food Log" value="{{$food_log->third_day_foodlog}}"/>
					                      </div>
					                        @error('third_day_foodlog')
					                          <div class="alert alert-danger">{{ $message }}</div>
					                        @enderror
					                    </div>
					                  </div>
								</div>
							</div>
							<div class="col-xl-2"></div>
							<div class="form-group text-center m-t-20">
			                    <div class="col-xs-12">
			                        <button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
			                    </div>
	                		</div>
						</div>
					</form>
					<!--end::Form-->
			   </div>
          </div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
	    FormValidation.formValidation(
	       document.getElementById('edit-customer-foodlog'),
	       {
	        fields: {
	         customer_id: {
	          validators: {
	           notEmpty: {
	            message: 'Customer Name is required'
	           }
	          }
	         },
	         first_day_foodlog: {
	          validators: {
	           notEmpty: {
	            message: 'First Day Food Log is required'
	           }
	          }
	         },
	         second_day_foodlog: {
	          validators: {
	           notEmpty: {
	            message: 'Second Day Food Log is required'
	           }
	          }
	         },
	         third_day_foodlog: {
	          validators: {
	           notEmpty: {
	            message: 'Third Day Food Log is required'
	           }
	          }
	         },

	        },

	        plugins: {
	         trigger: new FormValidation.plugins.Trigger(),
	         // Bootstrap Framework Integration
	         bootstrap: new FormValidation.plugins.Bootstrap(),
	         // Validate fields when clicking the Submit button
	         submitButton: new FormValidation.plugins.SubmitButton(),
	                  // Submit the form when all fields are valid
	         defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
	        },
	      }
	    );
    </script>
@endsection
