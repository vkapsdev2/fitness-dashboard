 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Book')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Edit Book <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('book.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('book.update',[$book->id]) }}" accept-charset="utf-8" enctype="multipart/form-data" id="edit-book" name="edit-book">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Book Info:</h3>
								<div class="form-group row">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="name" value="{{$book->name}}" placeholder="Enter name"/>
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="file" name="book_img" value="{{public_path().'/media/book-banner/'.$book->book_img}}"/>

										<img style="height: 50px;" src="{{ url('/'). '/public/media/book-banner/'.$book->book_img}}" />
								</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Author Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="author_name" value="{{$book->author_name}}" placeholder="Enter author name"/>
										@error('author_name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Book Type<span style="color:red">*</span></label>
									<div class="col-9">
									<label class="checkbox checkbox-success">
                    <input type="checkbox" class="form-control form-control-solid" name="book_type[]" value="Novel" <?php
	                    foreach ($types_arr as $type) {
	                    	if ($type == 'Novel') { echo 'checked';}
	                    	else { echo '';}
	                    }
	                ?>
                     />
                    <span></span> 
                    Novel
                    </label>
                    <label class="checkbox checkbox-success">
                    <input type="checkbox" class="form-control form-control-solid" name="book_type[]" value="Roman" <?php
	                    foreach ($types_arr as $type) {
	                    	if ($type == 'Roman') { echo 'checked';}
	                    	else { echo '';}
	                    }
	                ?>
	                />
                    <span></span>
                    Roman
                    </label>
                    <label class="checkbox checkbox-success">
                    <input type="checkbox" class="form-control form-control-solid" name="book_type[]" value="Poetry" 
                    <?php
	                    foreach ($types_arr as $type) {
	                    	if ($type == 'Poetry') { echo 'checked';}
	                    	else { echo '';}
	                    }
	                ?>
	                />
	                 <span></span>
                    Poetry
                 </label>
										@error('book_type')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
										
									</div>
								</div>
								<div class="form-group row">
							    <label class="col-3">Description</label>
							    
							    <div class="col-9">
							        <textarea name="description" id="kt-ckeditor-3">
							        	{{ $book->description }}
							        </textarea>

							        @error('description')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
							    </div>
							</div>
							</div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">   {{ __('Update') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
		<!--end::Page Vendors-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>

		 <script src="{{ asset('js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	// Class definition

var KTCkeditor = function () {
    // Private functions
    var demos = function () {
        ClassicEditor
            .create( document.querySelector( '#kt-ckeditor-3' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();
 </script>
 <style type="text/css">
 	/*body .error{
 		color: red;
 	}*/
 </style>
 <script type="text/javascript">
	
	FormValidation.formValidation(
 document.getElementById('edit-book'),
 {
  fields: {
   name: {
    validators: {
     notEmpty: {
      message: 'Book name is required'
     },
    }
   },
   author_name: {
    validators: {
     notEmpty: {
      message: 'Author name is required'
     },
    }
   },
   'book_type[]': {
    validators: {
     choice: {
      min:1,
      max:3,
      message: 'Please check at least 1 and maximum 3 options'
     }
    }
   },
   
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>
@endsection
