{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Plan')

{{-- Content --}}
@section('content')

{{-- Dashboard 1 --}}

<div class="row"> 
	<div class="col-lg-12">
		<div class="card card-custom card-sticky" id="kt_page_sticky_card">
			<div class="card-header">
				<div class="card-title">
					<h3 class="card-label">
						Plans <i class="mr-2"></i>
					</h3>
				</div>
				<div class="card-toolbar">
					<a href="{{route ('plans.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
						<i class="ki ki-long-arrow-back icon-sm"></i>
						Back
					</a>
				</div>
			</div>
			<div class="card-body">
				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('plans.update',[$plan->id]) }}" accept-charset="utf-8" enctype="multipart/form-data" id="edit-plans">
					@csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Plan Info:</h3>
								<div class="form-group row">
									<label class="col-3">Title<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="plan_title" value="{{$plan->plan_title}}" placeholder="Enter plan title" />
										@error('plan_title')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="file" name="plan_image" value="{{public_path().'/media/plan/'.$plan->plan_image}}"/>

										<img style="height: 50px;" src="{{ url('/'). '/public/media/plan/'.$plan->plan_image}}" />
										@error('plan_image')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Validity Days<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="validity_days" value="{{$plan->validity_days}}" placeholder="Enter Days" maxlength="2" />
										@error('validity_days')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Plan Type<span style="color:red">*</span></label>
									<div class="col-9">
										<div class="radio-inline">
											<label class="radio">
												<input type="radio" name="plan_type"  id="paid" class="plan_type" value="1"  {{ ($plan->plan_type) == "1" ? 'checked' : '' }}/>
												<span></span>
												Paid
											</label>
											<label class="radio">
												<input type="radio" name="plan_type" id="free" class ="plan_type" value="2" {{ ($plan->plan_type) == "2" ? 'checked' : '' }} />
												<span></span>
												Free
											</label>
										</div>
										@error('plan_type')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
										<span class="form-text text-muted"></span>
									</div>
								</div>
								<div class ="plan_price_box" style="display: none;">
									<div class="form-group row">
										<label class="col-3">Actual Price<span style="color:red">*</span></label>
										<div class="col-9">
											<input class="form-control form-control-solid" type="text" name="actual_price" value="{{$plan->actual_price}}" placeholder="Enter actual price"/>
											@error('actual_price')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>
									<div class="form-group row">
										<label class="col-3">Sales Price<span style="color:red">*</span></label>
										<div class="col-9">
											<input class="form-control form-control-solid" type="text" name="sales_price" value="{{$plan->sales_price}}" placeholder="Enter sales price"/>
											@error('sales_price')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Description</label>

									<div class="col-9">
										<textarea name="plan_description" id="kt-ckeditor-3">
											{{ ($plan->plan_description) }}
										</textarea>
										@error('plan_description')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3 col-form-label">Status<span style="color:red">*</span></label>
									<div class="col-9 col-form-label">
										<div class="radio-inline">
											<label class="radio">
												<input type="radio" name="plan_status" checked="checked"  value="1"  {{ ($plan->plan_status) == "1" ? 'checked' : '' }}/>
												<span></span>
												Activate
											</label>
											<label class="radio">
												<input type="radio" name="plan_status" value="0" {{ ($plan->plan_status) == "0" ? 'checked' : '' }} />
												<span></span>
												Deactive
											</label>
										</div>
										@error('plan_status')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
										<span class="form-text text-muted"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-primary" type="submit">  {{ __('Update') }}</button>
							<a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
	
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom.js?xg') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
	<!--end::Page Vendors-->
	<!--begin::Page Scripts(used by this page)-->
	<script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	var KTCkeditor = function () {
		    // Private functions
		    var demos = function () {
		    	ClassicEditor
		    	.create( document.querySelector( '#kt-ckeditor-3' ) )
		    	.then( editor => {
		    		console.log( editor );
		    	} )
		    	.catch( error => {
		    		console.error( error );
		    	} );
		    }

		    return {
		        // public functions
		        init: function() {
		        	demos();
		        }
		    };
		}();
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			var planTypeValue = $("input[name='plan_type']:checked").val();
			if(planTypeValue == 1)
			{
				$('.plan_price_box').show();
			}
			else
			{
				$('.plan_price_box').hide();
			}
			$(".plan_type").click(function(){
				var selectedValue = $(this).val();

				if(selectedValue == 1)
				{

					$('.plan_price_box').slideDown("slow");
				}
				else
				{
					$('.plan_price_box').slideUp("slow");
				}
			});
		});
	</script>
	<script type="text/javascript">
		FormValidation.formValidation(
			document.getElementById('edit-plans'),
			{
				fields: {
					plan_title: {
						validators: {
							notEmpty: {
								message: 'Plan title is required'
							},
						}
					},
					validity_days: {
						validators: {
							notEmpty: {
								message: 'Validity Days is required'
							},
							digits: {
								message: 'The value is not a valid digit'
							}
						}
					},
					plan_type: {
						validators: {
							notEmpty: {
								message: 'Plan type is required'
							},
						}
					},
					actual_price: {
						validators: {
							digits: {
								message: 'The value is not a valid digit'
							}
						}
					},
					sales_price: {
						validators: {
							digits: {
								message: 'The value is not a valid digit'
							}
						}
					},
					plan_description: {
						validators: {
							notEmpty: {
								message: 'Plan description is required'
							},
						}
					},

				},

				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
            		// Submit the form when all fields are valid
            		defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        		}
    		}
    );
	</script>
@endsection
