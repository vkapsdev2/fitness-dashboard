 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							SPlan <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('plans.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							
								<div class = "detail_img text-center">
									@if ($plan->plan_image)
									<img style= "height:200px;" src="{{ url('/'). '/public/media/plan/'.$plan->plan_image}}" />
									@else
									<img style= "height:200px;" src="{{ url('/'). '/public/media/book-banner/Product-Dummy.jpg'}}"/>
									@endif
								</div>
								<div class ="detail_name text-center detail_txt">

								
								<div class ="row mb-2 plan_title">
									<div class="col plan_title"><span>Title:</span></div>
									<div class="col plan_txt"><span><b>{{$plan->plan_title}}</b></span></div>
								</div>
								<div class ="row mb-2 plan_validity_days">
									<div class="col plan_title"><span>Validity:</span></div>
									<div class="col plan_txt"><span><b>{{$plan->validity_days}}</b></span></div>
								</div>
								<div class ="row mb-2 plan_validity_days">
									<div class="col plan_title"><span>Plan Type:</span></div>
									<div class="col plan_txt">
										@if ($plan->plan_type ==1)
										<span class="label label-lg font-weight-bold label-light-primary label-inline"><b>Paid </b></span>
										@else
										<span class="label label-lg font-weight-bold label-light-danger label-inline"><b>Free</b></span>
										@endif
									</div>
								</div>
								<div class ="row mb-2 plan_actual_price">
									<div class="col plan_title"><span>Actual Price:</span></div>
									<div class="col plan_txt"><span><b>{{$plan->actual_price}}</b></span></div>
								</div>	
								<div class ="row mb-2 plan_sales_price">
									<div class="col plan_title"><span>Sales Price:</span></div>
									<div class="col plan_txt"><span><b>{{$plan->sales_price}}</b></span></div>
								</div>
								<div class ="row mb-2 plan_status_price">
									<div class="col plan_title"><span>Status:</span></div>
									<div class="col plan_txt"><span><b>{{$plan->plan_status}}</b></span></div>
								</div>	
								</div>
									<div class="detail_description text-center>">
									{{ strip_tags($plan->plan_description) }}
								</div>
							</div>
 						</div>
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
.detail_name.text-center.detail_txt {
    display: grid;
    text-align: center;
    text-align: left !important;
    padding: 10px 16% 10px 34%;
    font-size: 14px;
}
p.shake_hd {
    float: left;
}
ul.shake_hd_list {
    list-style: none;
    font-weight: 600;
    float: right;
    padding-inline-start: 15px !important;
}
@media only screen and (max-width: 767px){
.detail_name.text-center.detail_txt {
    padding: 10px 0% 10px 20%;
}
}

</style>