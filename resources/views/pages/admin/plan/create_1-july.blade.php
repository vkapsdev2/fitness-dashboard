 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Article')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Article <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('article.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" id="article-add" method="POST" action="{{ route('article.save') }}" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validateForm('article-add')">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Article Info:</h3>
								<div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
									<label class="col-3">Name</label>
									<div class="col-9">
										<input class="form-control form-control-solid require-field" type="text" name="name" value="{{ old('name') }}" placeholder="Enter name" onkeyup="removeErrors(this)" />
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="file" name="article_img" value="{{ old('article_img') }}"/>
										@error('article_img')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Author Name</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="author_name" value="{{ old('author_name') }}" placeholder="Enter author name"/>
										@error('author_name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="card card-custom form-group row">
							    <label >Description</label>
							    
							    <div class="card-body">
							        <textarea name="description" id="kt-ckeditor-3">
							        	{{ old('description') }}
							        </textarea>
							        @error('description')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
							    </div>
							</div>
							</div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?xg') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/custom/ckeditor/ckeditor-classic.bundle.js?v=7.0.5') }}"></script>
		<!--end::Page Vendors-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{ asset('js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.5') }}"></script>
    <script type="text/javascript">
    	// Class definition

var KTCkeditor = function () {
    // Private functions
    var demos = function () {
        ClassicEditor
            .create( document.querySelector( '#kt-ckeditor-3' ) )
            .then( editor => {
                console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

// Initialization

    </script>
@endsection
