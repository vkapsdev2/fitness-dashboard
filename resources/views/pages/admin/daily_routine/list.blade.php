{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Daily Routine')

{{-- Content --}}
@section('content') 

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
           	<div class="card card-custom gutter-b">
				<div class="card-header flex-wrap border-0 pt-6 pb-0">
					<div class="card-title">
						<h3 class="card-label">Daily Routine List
						<span class="d-block text-muted pt-2 font-size-sm"></span></h3>
					</div>	
				</div>
				@if ($message = Session::get('success'))
			        <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
				<div class="card-body">
					<!--begin: Datatable -->
					<table class="table table-separate table-head-custom table-checkable" id="kt_datatable_daily_routine">
						<thead>
							<tr>
								<th>Customer Name</th>
								<th>Weight</th>
								<th>Hours of Sleep</th>
								<th>Did you eat yet?</th>
								<th>How are you feel?</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							 @foreach ($daily_routines as $daily_routine)
							 	<tr>
									<td>{{$daily_routine->customer->name}}</td>
									<td>{{$daily_routine->weight}}</td>
									<td>{{$daily_routine->sleep}}</td>
									<td>{{$daily_routine->eat}}</td>
									<td>{{$daily_routine->feel}}</td>
									<td>{!! Str::words($daily_routine->description, 2, ' ...') !!}</td>
								</tr>
							 @endforeach
						</tbody>
					</table>
					<!--end: Datatable-->
				</div>
			</div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
  <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/pages/custom.js?vg') }}" type="text/javascript"></script>

@endsection
