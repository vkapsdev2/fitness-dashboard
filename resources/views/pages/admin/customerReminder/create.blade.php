 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Customer Reminder')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

  <div class="row">  
    <div class="col-lg-12">
      <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="card-header">
          <div class="card-title">
            <h3 class="card-label">
              Workout Reminder <i class="mr-2"></i>
            </h3>
          </div>
          <div class="card-toolbar">
            <a href="{{route ('cust_reminder.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
              <i class="ki ki-long-arrow-back icon-sm"></i>
              Back
            </a>
          </div> 
        </div>
        <div class="card-body">
          <!--begin::Form-->
          <form class="form" id="add-customer-reminder" name="add-cust-reminder" action="{{ route('cust_reminder.save')}}" method="post" accept-charset="utf-8" >
            @csrf
            <div class="row"> 
              <div class="col-xl-12">
                <div class="my-5">
                  
                  <div class="form-group row">
                    <label class="col-3">Coach Name<span style="color:red">*</span></label>
                    <div class="col-9">
                      <select class="form-control" name="coach_id">
                        <option value= "" >Select Coach</option>
                          @foreach ($coaches as $value)
                            <option value="{{$value->id}}">{{$value->name}}</option>
                          @endforeach
                      </select>
                      @error('coach_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-3">Customer Name<span style="color:red">*</span></label>
                    <div class="col-9">
                      <select class="form-control" name="customer_id">
                        <option value= "" >Select Customer</option>
                          @foreach ($customers as $value)
                            <option value="{{$value->id}}">{{$value->name}}</option>
                          @endforeach
                      </select>
                      @error('customer_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-3">Title of Reminder<span style="color:red">*</span></label>
                    <div class="col-9">
                      <div class="input-group">
                       <input type="text" class="form-control" name = "title" placeholder="Title of Reminder" value="{{ old('title') }}"/>
                      </div>
                        @error('title')
                          <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                  </div>


                  <div class="form-group row">
                    <label class="col-3">Days <span style="color:red">*</span></label>
                    <div class="col-9">
                     <div class="checkbox-inline">
                      <label class="checkbox">
                       <input type="checkbox" class="form-control" name="days[]" value="Monday" /><span></span> Monday
                      </label>
                      <label class="checkbox">
                        <input type="checkbox" class="form-control" name="days[]" value="Tuesday" /><span></span>Tuesday
                      </label>
                      <label class="checkbox">
                        <input type="checkbox" class="form-control" name="days[]" value="Wednesday" /><span></span>Wednesday
                      </label>
                      <label class="checkbox">
                        <input type="checkbox" class="form-control" name="days[]" value="Thursday" /><span></span>Thursday
                      </label>
                     </div>
                     <div class="checkbox-inline">
                      <label class="checkbox">
                       <input type="checkbox" class="form-control" name="days[]" value="Friday" /><span></span> Friday
                      </label>
                      <label class="checkbox">
                        <input type="checkbox" class="form-control" name="days[]" value="Saturday" /><span></span>Saturday
                      </label>
                      <label class="checkbox">
                        <input type="checkbox" class="form-control" name="days[]" value="Sunday" /><span></span>Sunday
                      </label>
                     </div>
                    </div>
                   </div>

                    <div class="form-group row">
                      <label class="col-3">Reminder Type <span style="color:red">*</span></label>
                      <div class="col-9 col-form-label">
                          <div class="radio-inline">
                              <label class="radio">
                              <input type="radio" name="reminder_name" checked="checked" value="Workout"  {{ old('reminder_name') == "Workout" ? 'checked' : '' }}/>
                              <span></span>
                             Workout
                              </label>
                            <label class="radio">
                              <input type="radio" name="reminder_name" value="Meal" {{ old('reminder_name') == "Meal" ? 'checked' : '' }} />
                              <span></span>
                              Meal
                          </label>
                        </div>
                           @error('reminder_name')
                            <div class="alert alert-danger">{{ $message }}</div>
                           @enderror
                        </div>
                    </div>

                  <div class="form-group row">
                    <label class="col-3">Time <span style="color:red">*</span></label>
                    <div class="col-9">
                     <div class="input-group date">
                      <input class="form-control" id="reminder_time" placeholder="Select time" name="time" type="text"/>
                     </div>
                     <span class="form-text text-muted">Select time</span>
                    </div>
                  </div>

                   <div class="form-group row">
                      <label class="col-3">Repeat Reminder</label>
                      <div class="col-9">
                       <input data-switch="true" type="checkbox" name="repeat_status" id="test" data-on-color="success"/>
                       <span class="form-text text-muted"></span>
                      </div>
                     </div>

                  <!-- END PLANN DETAIL SECTION -->
                  <div class="separator separator-dashed my-10"></div>
                  <div class="form-group text-center m-t-20">
                      <div class="col-xs-12">
                          <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
                           <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <!--end::Form-->
        </div>
      </div>
    </div>  
  </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
 <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
 <script type="text/javascript">
    $('#reminder_time').timepicker({
     minuteStep: 1,
     showSeconds: true,
     showMeridian: true
    });

    // Bootstrap Switch
    $('[data-switch=true]').bootstrapSwitch();
    $('[data-switch=true]').on('switchChange.bootstrapSwitch', function() {
     // Revalidate field
     validator.revalidateField('switch');
    });

 </script>
 <script type="text/javascript">
    FormValidation.formValidation(
       document.getElementById('add-customer-reminder'),
       {
        fields: {
          customer_id: {
            validators: {
             notEmpty: {
              message: 'Customer Name is required'
             }
            }
          }, 
          coach_id: {
            validators: {
             notEmpty: {
              message: 'Coach Name is required'
             }
            }
          },
          time: {
            validators: {
             notEmpty: {
              message: 'Time is required'
             }
            }
          },
          reminder_name: {
            validators: {
             notEmpty: {
              message: 'Reminder type is required'
             }
            }
          },
          'days[]': {
            validators: {
              choice: {
                min:1,
                max:6,
                message: 'Please check at least 1 and maximum 6 options'
              }
          }
         },
         title: {
          validators: {
           notEmpty: {
            message: 'Title is required'
           }
          }
         },

        },

        plugins: {
         trigger: new FormValidation.plugins.Trigger(),
         // Bootstrap Framework Integration
         bootstrap: new FormValidation.plugins.Bootstrap(),
         // Validate fields when clicking the Submit button
         submitButton: new FormValidation.plugins.SubmitButton(),
                  // Submit the form when all fields are valid
         defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        },
      }
    );
  </script>
 @endsection
