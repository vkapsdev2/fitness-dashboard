 <html>
            <body>
               <div class = "enquiry_mail_content">
               	<h4>Dear {{ $data['name'] }} , </h4>
               	<p>Thank you for your interest in our gym. We are excited to hear from you. In response to your enquiry, We will update you soon. {{ $data['message'] }} </p>
               		<p> However, please do not hesitate to contact us for further clarification if need be.
               		</p>
               		<p>We look forward to your patronage. </p>
               		<p>Best wishes,</p>
               		<p> {{ $data['from_name'] }} </p>
               		<p> {{ $data['from_address'] }} </p>

               </div>
            </body>
        </html>