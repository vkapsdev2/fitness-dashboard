 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Contact-Us')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Contact-Us <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('contact.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('contact.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-contact-us">
					@csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Contact-Us Info:</h3>
								<div class="form-group row">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="name" value="{{ old('name') }}"/>
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
							       <label class="col-3">Email Address<span style="color:red">*</span></label>
							       <div class="col-9">
							        <div class="input-group input-group-solid">
							         <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
							         <input type="text" class="form-control form-control-solid" name = "email" placeholder="Email" value="{{ old('email') }}"/>
							        </div>
							         <span class="form-text text-muted">We'll never share your email with anyone else.</span>
							          @error('email')
							      <div class="alert alert-danger">{{ $message }}</div>
							    @enderror
							       </div>
							      </div>
								<div class="form-group row">
				                  <label class="col-3">Query<span style="color:red">*</span></label>
				                  <div class="col-lg-9">
				                    <div class="input-group">
				                      <textarea class="form-control form-control-solid"  name="query" placeholder="Enter your query here.." style="resize: none;">{{old('query')}}</textarea>
				                    </div>
				                      @error('query')
				                      <div class="alert alert-danger">{{ $message }}</div>
				                    @enderror
				                  </div>
				                </div>
							</div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?xg') }}" type="text/javascript"></script>
  
<script type="text/javascript">
	
	FormValidation.formValidation(
 document.getElementById('add-contact-us'),
 {
  fields: {
   name: {
    validators: {
     notEmpty: {
      message: ' Customer name is required'
     },
    }
   },
   email: {
    validators: {
     notEmpty: {
      message: 'Email is required'
     },
     emailAddress: {
      message: 'The value is not a valid email address'
     }
    }
   },
   query: {
    validators: {
     notEmpty: {
      message: 'Query is required'
     },
    }
   },
 },

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>
@endsection
