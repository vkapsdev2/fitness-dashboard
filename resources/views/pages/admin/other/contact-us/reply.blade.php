 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Reply')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Contact-us <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('contact.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>

				@if(Session::has('error'))
							<div class="alert alert-danger alert-block">
		    					<button type="button" class="close" data-dismiss="alert">×</button>    
		    					<strong>{{ session('error') }}</strong>
						 </div> 
					@endif

						@if(Session::has('success'))
							<div class="alert alert-success alert-block">
		    					<button type="button" class="close" data-dismiss="alert">×</button>    
		    					<strong>{{ session('success') }}</strong>
							</div>   
						@endif
				
				<div class="card-body">
						<div class="row">

					
				
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Inquiry Info:</h3>
	   							<div class ="detail_img">
	   								<span><b>Name: {{ $contact->name }} </b></span>
								</div>
								<div class ="detail_name">
									<span><b>Email: {{ $contact->email }} </b></span>
								</div>
								<div class="detail_description>">
									{{ strip_tags($contact->query) }}
								</div>
							</div>
						</div>
						<form class="form" method="POST" action="{{ route('contact.sendReply') }}" accept-charset="utf-8" enctype="multipart/form-data" id="send-contact-us-reply">
					  @csrf
						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Reply:</h3>
	   							
								<div class="form-group row">
							       <label class="col-3">To<span style="color:red">*</span></label>
							       <div class="col-9">
							        <div class="input-group input-group-solid">
							         <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
							         <input type="text" class="form-control form-control-solid" name ="email" placeholder="Email" value="{{ $contact->email }}" disabled />
							         
							         <input type="hidden" class="form-control form-control-solid" name ="to_email" value="{{ $contact->email }}" />

							          <input type="hidden" class="form-control form-control-solid" name ="name" value="{{ $contact->name }}" />

							           <input type="hidden" class="form-control form-control-solid" name ="id" value="{{ $contact->id }}" />

							        </div>
							         @error('to_email')
							      <div class="alert alert-danger">{{ $message }}</div>
							    @enderror
							       </div>
							      </div>
								<div class="form-group row">
				                  <label class="col-3">Answer<span style="color:red">*</span></label>
				                  <div class="col-lg-9">
				                    <div class="input-group">
				                      <textarea class="form-control form-control-solid"  name="query_reply" placeholder="Enter your answer here.." style="resize: none;" rows="6">{{old('query_reply')}}</textarea>
				                    </div>
				                      @error('query_reply')
				                      <div class="alert alert-danger">{{ $message }}</div>
				                    @enderror
				                  </div>
				                </div>
								
							</div>
						</div>
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Send') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</form>
				</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?xg') }}" type="text/javascript"></script>
<script type="text/javascript">
	
	/*FormValidation.formValidation(
 document.getElementById('send-contact-us-reply'),
 {
  fields: {
   
   to_email: {
    validators: {
     notEmpty: {
      message: 'Email is required'
     },
     emailAddress: {
      message: 'The value is not a valid email address'
     }
    }
   },
   query_reply: {
    validators: {
     notEmpty: {
      message: 'Query is required'
     },
    }
   },
 },

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);
*/
</script>   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
</style>