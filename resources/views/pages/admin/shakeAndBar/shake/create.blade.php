 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Shake & Bar')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Shake & Bar <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('shakeBar.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
					@if ($message = Session::get('error'))
			        <div class="alert alert-danger">
			            {{ $message }}
			        </div>
			        @elseif ($message = Session::get('succcess'))
			         <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
				<div class="card-body">
				<!--begin::Form-->
				<form class="form"  method="POST" action="{{ route('shakeBar.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-shakeBar" name="add-shakeBar">
					  @csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">shake & Bar Info:</h3>
								<div class="form-group row">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="shake_name" value="{{ old('shake_name') }}" placeholder="Enter name"/>
										@error('shake_name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
                    				</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Price<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="price" value="{{ old('price') }}" placeholder="Enter Price"/>
										@error('price')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Calories<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="calories" value="{{ old('calories') }}" placeholder="Enter Calories"  maxlength="6"/>
										@error('calories')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Protein<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="protein" value="{{ old('protein') }}" placeholder="Enter Protein"  maxlength="6"/>
										@error('protein')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Carbs<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="carbs" value="{{ old('carbs') }}" placeholder="Enter Carbs"  maxlength="6"/>
										@error('carbs')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>

								<div class="form-group row">
									<label class="col-3">Fats<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="fats" value="{{ old('fats') }}" placeholder="Enter Fats"  maxlength="6"/>
										@error('fats')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Banner<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="file" name="shake_image" value="{{ old('shake_image') }}"/>
										@error('shake_image')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-3">Shake Type<span style="color:red">*</span></label>
									<div class="col-9">
										<label class="checkbox checkbox-success">
					                    <input type="checkbox" class="form-control form-control-solid" name="shake_type[]" value="1" @if(is_array(old('shake_type')) && in_array(1, old('shake_type'))) checked @endif/>
					                    <span></span>
					                    High Protein
					                    </label>
					                    <label class="checkbox checkbox-success">
			                              <input type="checkbox" class="form-control form-control-solid" name="shake_type[]" value="2" @if(is_array(old('shake_type')) && in_array(2, old('shake_type'))) checked @endif />
			                              <span></span>
			                              Low Calorie
			                              </label>
					                    <label class="checkbox checkbox-success">
					                    	<input type="checkbox" class="form-control form-control-solid" name="shake_type[]" value="3" @if(is_array(old('shake_type')) && in_array(3, old('shake_type'))) checked @endif/>
					                    	<span></span>
					                    	Nut Allergy
					                 	</label>
											@error('shake_type')
											<div class="alert alert-danger">{{ $message }}</div>
											@enderror
									</div>
								</div>

								<div class="form-group row">
							      <label class="col-3 col-form-label">Do you want to Customize?<span style="color:red">*</span></label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="is_shake_customize" class ="is_shake_customize"   value="1"  {{ old('is_shake_customize') == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Yes
							              </label>
							              <label class="radio">
							                  <input type="radio" name="is_shake_customize" class ="is_shake_customize" checked="checked" value="2" {{ old('is_shake_customize') == "2" ? 'checked' : '' }} />
							                  <span></span>
							                  No
							              </label>
							          </div>
							           @error('is_shake_customize')
							      <div class="alert alert-danger">{{ $message }}</div>
							    	@enderror
							         <span class="form-text text-muted"></span>
							     	</div>
							  	</div>

								<div class="form-group row shake_ingredients_box" style="display: none;">
									<label class="col-3">Ingredients</label>
									<div class="col-9">
										<select multiple="" class="form-control" name ="shake_ingredients[]">
										     <option value="">Please Select Ingredients</option>
										    @foreach ($ingredients as $ingredient)
										    <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
										    @endforeach
									    </select>
										@error('shake_ingredients')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
									 
							    <div class="form-group row green-border-focus shadow-textarea">
						            <label class="col-3">Description</label>
						            <div class="col-9">
						              <textarea class="form-control form-control-solid"  name="shake_description" placeholder="Write your description here.." style="resize: none;">{{ old('shake_description') }}</textarea>
						              @error('shake_description')
						                <div class="alert alert-danger">{{ $message }}</div>
						              @enderror
						            </div>
        						</div>

        						<div class="form-group row">
							      <label class="col-3 col-form-label">Status<span style="color:red">*</span></label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="shake_status" checked="checked"  value="1"  {{ old('shake_status') == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                  <input type="radio" name="shake_status" value="2" {{ old('shake_status') == "2" ? 'checked' : '' }} />
							                  <span></span>
							                  Deactive
							              </label>
							          </div>
							           @error('shake_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>

							  	<div class="form-group text-center m-t-20">
		                    		<div class="col-xs-12">
		                        		<button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                        		<a class="btn btn-primary" type="button" href="">{{ __('Cancel') }}</a>
		                    		</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?fgf') }}" type="text/javascript"></script>
<script type="text/javascript">
	
      $(document).ready(function(){
        var shakeCustomization = $("input[name='is_shake_customize']:checked").val();
        //alert(physicalActivityValue);
        // false;
        if(shakeCustomization == 1)
        {
          $(".shake_ingredients_box").show();
        }
        $(".is_shake_customize").click(function(){
          var value = $(this).val();
          
          if(value == 1)
          {
              $(".shake_ingredients_box").slideDown("slow");
          }
          else
          {
             $(".shake_ingredients_box").slideUp("slow");
          }
        });
      });
   
</script>
<script type="text/javascript">


FormValidation.formValidation(
 document.getElementById('add-shakeBar'),
 {
  fields: {
   shake_name: {
    validators: {
     notEmpty: {
      message: 'Shake name is required'
     },
    }
   },
   price: {
        validators: {
        notEmpty: {
			      message: 'Price is required'
			     },
         numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: '',
                decimalSeparator: '.'
            }
        }
    },
   calories: {
        validators: {
        notEmpty: {
			      message: 'Calories is required'
			     },
         numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: '',
                decimalSeparator: '.'
            }
        }
    },

   protein: {
    validators: {
     notEmpty: {
      message: 'Protein is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: '',
                decimalSeparator: '.'
            }
        }
    },
   carbs: {
    validators: {
     notEmpty: {
      message: 'Carbs is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: '',
                decimalSeparator: '.'
            }
        }
   },
   fats: {
    validators: {
     notEmpty: {
      message: 'Fats is required'
     },
       numeric: {
                message: 'The value is not a number',
                // The default separators
                thousandsSeparator: '',
                decimalSeparator: '.'
            }
        }
   },
   'shake_type[]': {
    validators: {
	 choice: {
      min:1,
      max:3,
      message: 'Please check at least 1 and maximum 3 options'
     }
    }
   },
   is_shake_customize: 
   {
    validators: {
	 notEmpty: {
    	message: 'Shake customization is required'
  		},
	},
 },
   shake_image:{
   	validators: {
     notEmpty: {
      message: 'Please select Shake image.'
     },
    }
   },
   shake_status: {
    validators: {
     notEmpty: {
      message: 'Status is required'
     },
    }
   },
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>

@endsection
