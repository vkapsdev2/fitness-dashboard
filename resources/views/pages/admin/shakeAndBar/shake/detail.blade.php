 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Shake & Bar <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('shakeBar.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							
								<div class = "detail_img text-center">
									@if ($shakeAndBar->shake_img)
									<img style= "height:200px;" src="{{ url('/'). '/public/media/shake/'.$shakeAndBar->shake_img}}" />
									@else
									<img style= "height:200px;" src="{{ url('/'). '/public/media/book-banner/Product-Dummy.jpg'}}"/>
									@endif
								</div>
								<div class ="detail_name text-center description_txt_box">
								<div class="row">
									<div class="col plan_title"><p>Name: </p></div>
									<div class="col plan_title"><p><b> {{ $shakeAndBar->shake_name }} </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Calories: </p></div>
									<div class="col plan_title"><p><b> {{ $shakeAndBar->calories }} Cal </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Protein: </p></div>
									<div class="col plan_title"><p><b> {{ $shakeAndBar->protein }} gm </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Carbs: </p></div>
									<div class="col plan_title"><p><b> {{ $shakeAndBar->carbs }} gm </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Fats: </p></div>
									<div class="col plan_title"><p><b> {{ $shakeAndBar->fats }} gm</b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Type:</p></div>
									<div class="col plan_title"><p><b>
									<?php 
									$shake_type_arr = array();
										$shake_type = explode(',', $shakeAndBar->shake_type);
										
										for($i=0; $i< count($shake_type); $i++)
										{ 
											if($shake_type[$i] = 1)
											{
												$shake_type_arr[$i] = 'High Protein'; 
											}
											elseif ($shake_type[$i] = 2) 
											{

												$shake_type_arr[$i] = 'Low Calorie';
											}
											else
											{
												$shake_type_arr[$i] = 'Nut Allergy';
											}
										}
										$shake_data = implode(',', $shake_type_arr);
										echo $shake_data;

									 ?>

									</b></p>
									</div>
								</div>
								<div class="row">
									<div class="col plan_title">
										<p>Is shake Customized:</p>
									</div>
									<div class="col plan_title">
									<p><b> 
										<?php
											if($shakeAndBar->shake_type ==1)
											{
												echo 'Yes';
											}
											else
											{
												echo 'No';
											}
										 ?>
									</b></p>
									</div>
								</div>
								<div class="row">
									<div class="col plan_title">
										<p class="shake_hd">Shake Ingredients:</p> 
									</div>
									<div class="col plan_title">
										<ul class="shake_hd_list">
											  @foreach ($ingredients as $ingredient)
											<?php 
											for($i=0; $i<count($ingredient_types_arr); $i++)
											{
												if($ingredient_types_arr[$i] == $ingredient->id)
												{?>
													<li>
										    	{{ $ingredient->name }} 
										       </li>

												<?php }

											}
											?>
										    @endforeach
										</ul>
									</div>
								</div>	 
								</div>
									<div class="detail_description text-center>">
									{{ strip_tags($shakeAndBar->shake_description) }}
								</div>
							</div>
 						</div>
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
.detail_name.text-center.description_txt_box {
    display: grid;
    text-align: center;
    text-align: left !important;
    padding: 10px 15% 10px 30%;
    font-size: 14px;
}
ul.shake_hd_list {
    list-style: none;
    font-weight: 600;
    padding-inline-start: 0px !important;
}
@media only screen and (max-width: 767px){
.detail_name.text-center.description_txt_box {
    padding: 10px 0% 10px 0%;
}
}

</style>