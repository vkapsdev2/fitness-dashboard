 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Detail')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Order <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('order.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
				<div class="card-body">

					
					
 						<div class="row">
 							<div class="my-5 detail_view">
 								<h3 class=" text-dark font-weight-bold mb-10">Detail Info:</h3>
	   							<div class ="detail_name text-center description_txt_box">
								<div class="row">
									<div class="col plan_title"><p>Name: </p></div>
									<div class="col plan_title"><p><b> {{ $order[0]->name }} </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Method: </p></div>
									<div class="col plan_title"><p><b> {{ $order[0]->payment_method }}  </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Applied for coupon: </p></div>
									<div class="col plan_title"><p><b> 
										@if($order[0]->is_applied_coupon ==1)
										 Yes
										@else
										 No
										@endif 
									</b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Coupon: </p></div>
									<div class="col plan_title"><p><b> 
										@if (($order[0]->coupon =='')||($order[0]->coupon ==null)) 
										--------
										@else
										{{ $order[0]->coupon }}

										@endif
										 </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Discount Rate: </p></div>
									<div class="col plan_title"><p><b> {{ $order[0]->discount_rate }}

										
										 </b></p></div>
								</div>
								<div class="row">
									<div class="col plan_title"><p>Total Items: </p></div>
									<div class="col plan_title"><p><b> {{ $order[0]->total_item }}</b></p></div>
								</div>

								<div class="row">
									<div class="col plan_title"><p>Grand Total: </p></div>
									<div class="col plan_title"><p><b> {{ $order[0]->grand_total }} </b></p></div>
								</div>

								<div class="row">
									<div class="col plan_title">
										<p>Payment Status:</p>
									</div>
									<div class="col plan_title">
									<p><b> 
										@if($order[0]->payment_status ==1)
											<span class="label label-lg font-weight-bold label-light-primary label-inline">Paid</span>
										@elseif($order[0]->payment_status ==3)
										<span class="label label-lg font-weight-bold label-light-warning label-inline">Partial</span>
					                     @else
					                      <span class="label label-lg font-weight-bold label-light-danger label-inline">Free</span>
										@endif
										
									</b></p>
									</div>
								</div>

								<div class="row">
									<div class="col plan_title">
										<p class="shake_hd">Order Details:</p> 
									</div>
									<div class="col plan_title">
										<?php 
										
										$orerDetail = json_decode($order[0]->order_detail,true);
										if(!empty($orerDetail))
										{
											$total_items = 0;
										for($i=0; $i<count($orerDetail); $i++)
										{
											$total_items++;

											for($j=0; $j<count($shakes); $j++)
											{
												if($orerDetail[$i]['shake_id'] == $shakes[$j]->id)
													{
														echo '<span><b>'.$total_items.' </b></span><p><span>Name:'.$shakes[$j]['shake_name'].'<span></p>';
													}
													
											}
											echo '<p><span>Quantity:'.$orerDetail[$i]['quantity'].'</span></p>';
											echo '<p><span>Price:'.$orerDetail[$i]['price'].'<span></p>'; 

											
											$extraIndredient = explode(',',$orerDetail[$i]['extraIngredient']);

											//print_r($extraIndredient);die;
											echo '<p><span>Additional Ingredients:</p></span>';
											echo '<div class="col plan_title">
										<ul class="shake_hd_list">';
											for($x = 0; $x < count($extraIndredient); $x++)
											{
												for($y=0; $y<count($ingredients); $y++)
												{
													if($extraIndredient[$x] == $ingredients[$y]->id )
													{
														echo '<li>'.$ingredients[$y]->name.'</li>';
													}
												}
												
											}
											echo '</ul></div>';
											
										}
										//print_r($orerDetail);
										
										}
										?>
									</div>
								</div>
									 
								</div>
									
							</div>
 						</div>
				</div>
			</div>
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
@endsection
<style type="text/css">
	.detail_view {
    padding: 15px;
    width: 70%;
    box-shadow: 0px 0px 7px #ccc;
    margin: 0px auto;
}

.detail_description {
    text-align: justify;
}
.detail_name.text-center.description_txt_box {
    display: grid;
    text-align: center;
    text-align: left !important;
    padding: 10px 15% 10px 30%;
    font-size: 14px;
}
ul.shake_hd_list {
    list-style: none;
    font-weight: 600;
    padding-inline-start: 0px !important;
}
@media only screen and (max-width: 767px){
.detail_name.text-center.description_txt_box {
    padding: 10px 0% 10px 0%;
}
}

</style>