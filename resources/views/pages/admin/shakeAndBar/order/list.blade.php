{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Orders')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-12">
           	<div class="card card-custom gutter-b">
				<div class="card-header flex-wrap border-0 pt-6 pb-0">
					<div class="card-title">
						<h3 class="card-label">Orders List
						<span class="d-block text-muted pt-2 font-size-sm"></span></h3>
					</div>	
					 <div class="card-toolbar">
					 
					 </div>
				</div>
				@if ($message = Session::get('success'))
			        <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
          <div class="delete_message"></div>
				<div class="card-body">
					<!--begin: Datatable -->
					<table class="table table-separate table-head-custom table-checkable" id="orderList">
						<thead>
							<tr>
								<th>Name</th>
								<th>Date</th>
								<th>Method</th>
                <th>Total</th>
                <th>Payment Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							 @foreach ($orders as $order)
							 	<tr>
									<td>{{$order->name}}</td>
									<td>{{$order->date}}</td>
                  <td>
                    @if ($order->payment_method ==1)
                    paypal
                    @elseif ($order->payment_method ==2)
                    Cash On Delivery
                    @elseif ($order->payment_method ==3)
                    Card
                    @endif
                  </td>
                  <td>{{$order->grand_total}}</td>
									<td>
                   @if ($order->payment_status == 1)
                      <span class="label label-lg font-weight-bold label-light-primary label-inline">Paid</span>
                      @elseif ($order->payment_status == 0)
                      <span class="label label-lg font-weight-bold label-light-danger label-inline">Pending</span>
                      @endif
                  </td>
									<td class="text-nowrap">
                   <!--  <a href="{{ route('order.edit',[$order->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a> -->
                    <a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelete({{$order->id}})">  <i class="fa fa-times"></i></a>
                    <a href="{{ route('order.detailView',[$order->id]) }}" data-toggle="tooltip" data-original-title="View All Detail" class="btn btn-primary btn-circle"> <i class="fa fa-eye"></i> </a>

                  </td>
								</tr>
							 @endforeach
						</tbody>
					</table>
					<!--end: Datatable-->
				</div>
			</div>
        </div> 
    </div>

@endsection

{{-- Scripts Section --}}

@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script type="text/javascript">


  function comfrimDelete(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this order!", 
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax('https://vkapsprojects.com/pedro/skeleton/public/shakeBar/order/delete/'+id, {
          method: 'DELETE',
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        }).done(function (r) {
          console.log('result',r); 
          
         if(r.success){
             $("div.delete_message").html('<div class="alert alert-success">' + r.message + '</div>');
            window.setTimeout(function(){location.reload()},2000);
          }else{                
            swal.fire({
            text: 'Order does not exist..',
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
              confirmButton: "btn font-weight-bold btn-light-primary"
            }
          }).then(function () {
            KTUtil.scrollTop();
          });            
          }    
        }).fail(function (jqXHR, textStatus, errorThrown) {             
            swal.fire({
            text: errors,
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
              confirmButton: "btn font-weight-bold btn-light-primary"
            }
          }).then(function () {
            KTUtil.scrollTop();
          });    
        });
      }
    });
  }
 </script>
 <script type="text/javascript">
     $('#orderList').dataTable({
        // "scrollX": true,
        // "scrollY": true, 
    });
  </script>
@endsection
