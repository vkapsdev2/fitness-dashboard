 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Order')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Order <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('order.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
					@if ($message = Session::get('error'))
			        <div class="alert alert-danger">
			            {{ $message }}
			        </div>
			        @elseif ($message = Session::get('succcess'))
			         <div class="alert alert-success">
			            {{ $message }}
			        </div>
			    @endif
				<div class="card-body">
				<!--begin::Form-->
				<form class="form"  method="POST" action="{{ route('order.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-order" name="add-order">
					  @csrf
					<div class="row">
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Order Info:</h3>
								<div class="form-group row">
									<label class="col-3">Customer<span style="color:red">*</span></label>
									<div class="col-9">
														 <select class="form-control" name="customer_id">
				                        <option value= "" >Select Customer</option>
				                          @foreach ($customers as $value)
				                            <option value="{{$value->id}}">{{$value->name}}</option>
				                          @endforeach
				                      </select>
										@error('customer_id')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
                    				</div>
								</div>
								<div class="form-group row">
									<label class="col-3">Payment Method<span style="color:red">*</span></label>
									<div class="col-9">
										<select class="form-control" name="payment_method">
					             <option value= "" >Select Payment</option>
					             <option value ="1" {{ old('payment_method') == "1" ? 'selected' : '' }}>Paypal</option>
					             <option value ="2" {{ old('payment_method') == "2" ? 'selected' : '' }}>Cash On Delivery</option>
								 <option value ="3" {{ old('payment_method') == "3" ? 'selected' : '' }}>Card</option>
				                        
				                      </select>
										@error('payment_method')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
							       <label class="col-3">Coupon<span style="color:red">*</span></label>
							        <div class="col-9">
							        	<select class="form-control" name="coupon">
							        		<option value="">Select Coupon</option>
							          @foreach ($coupons as $value)
				                            <option value="{{$value->id}}">{{$value->name}}</option>
				                          @endforeach
										</select>
							          @error('coupon')
							      <div class="alert alert-danger">{{ $message }}</div>
							    @enderror
							        </div>
							    </div>

							    <div class="separator separator-dashed my-8"></div>

            <div id="kt_repeater_1">
                <div class="form-group row" id="kt_repeater_1">
                    <label class="col-3 col-form-label">Shake Detail:</label>
                    <div data-repeater-list="" class="col-9">
                        <div data-repeater-item class="form-group row align-items-center">
                            <div class="col-md-3">
                                <label>Shake:</label>
                                <select class="form-control" name="order_detail[][shake_id]">
                                 <option value= "" >Select Payment</option>
                                 <option value= "1" >Fruit shake</option>
                                 <option value= "2" >Milk shake</option>
                                 <option value= "3" >Protien shake</option>
                                </select>
                                <div class="d-md-none mb-2"></div>
                            </div>
                            <div class="col-md-3">
                                <label>Ingredients:</label>
                                <select class="form-control" name="order_detail[][extraIngredient]">
                                 <option value= "" >Select Payment</option>
                                 <option value= "1" >A</option>
                                 <option value= "2" >B</option>
                                 <option value= "3" >C</option>
                                </select>
                                <div class="d-md-none mb-2"></div>
                            </div>
                            <div class="col-md-3">
                                <label>Quantity:</label>
                                <input class="form-control" type="number" name="order_detail[][quantity]" value="{{ old('quantity') }}" />
                                <div class="d-md-none mb-2"></div>
                            </div>
                            <div class="col-md-3">
                                <label>Price:</label>
                                <input class="form-control" type="number" name="order_detail[][price]" value="{{ old('price') }}" />
                                <div class="d-md-none mb-2"></div>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                    <i class="la la-trash-o"></i>Delete
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

        						<div class="form-group row">
							      <label class="col-3 col-form-label">Status<span style="color:red">*</span></label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="order_status" checked="checked"  value="1"  {{ old('order_status') == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                  <input type="radio" name="order_status" value="2" {{ old('order_status') == "0" ? 'checked' : '' }} />
							                  <span></span>
							                  Deactive
							              </label>
							          </div>
							           @error('order_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>

							  	
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label text-right"></label>
                    <div class="col-lg-4">
                        <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                            <i class="la la-plus"></i>Add
                        </a>
                    </div>
                </div>
            </div>

							  	<div class="form-group text-center m-t-20">
		                    		<div class="col-xs-12">
		                        		<button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                        		<a class="btn btn-primary" type="button" href="">{{ __('Cancel') }}</a>
		                    		</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?fgf') }}" type="text/javascript"></script>

 <script type="text/javascript">
           // Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormRepeater.init();
});
       </script>

@endsection
