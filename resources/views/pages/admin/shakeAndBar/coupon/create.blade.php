 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Add Coupon')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Coupon <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('coupon.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('coupon.save') }}" accept-charset="utf-8" enctype="multipart/form-data" id="add-coupon">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Coupon Info:</h3>
								<div class="form-group row">
									<label class="col-3">Name<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="name" value="{{ old('name') }}" placeholder="Enter name"  maxlength="32"/>
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
							      <label class="col-3 col-form-label">Method<span style="color:red">*</span></label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="method" checked="checked"  value="1"  {{ old('method') == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Fixed Cost
							              </label>
							              <label class="radio">
							                  <input type="radio" name="method" value="2" {{ old('method') == "2" ? 'checked' : '' }} />
							                  <span></span>
							                  Percent
							              </label>
							          </div>
							           @error('method')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>
							  	<div class="form-group row">
									<label class="col-3">Discount<span style="color:red">*</span></label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="discount_rate" value="{{ old('discount_rate') }}" placeholder="Enter discount" maxlength="10" />
										@error('discount_rate')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div> 
								<div class="form-group row">
									<label class="col-3">Validity<span style="color:red">*</span></label>
									<div class="col-9">
										<!-- <input class="form-control form-control-solid" type="text" name="validity" value="{{ old('validity') }}" placeholder="Enter validity days" maxlength="4"/> -->

										<input type="text" class="form-control  kt_datetimepicker_1" id="validity" name="validity" value="{{ old('validity') }}" placeholder="Coupon expire date" readonly />
										@error('validity')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								<div class="form-group row">
								    <label class="col-3">Generate Code</label>
								    <div class="col-9 col-form-label">
								    	<input type="button" value="Generate" name="promocode_btn" class="btn btn-success font-weight-bold btn-pill" id="g_pcode">
								    </div>
								</div>

							 	<div class="form-group row">
								    <label class="col-3">Generated Code</label>
								    <div class="col-9 col-form-label">
								    	<h4 id='show_p_code'style="color:blue;"></h4>
								    	<input type="hidden" name="code" id="p_code" value="{{ old('code') }}" class="form-control form-control-solid">
								    	@error('code')
											<div class="alert alert-danger">Please Generate coupon code</div>
										@enderror
								    </div>
								</div>


							 	<div class="form-group row">
							      <label class="col-3 col-form-label">Status</label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                  <input type="radio" name="coupon_status" checked="checked"  value="1"  {{ old('coupon_status') == "1" ? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                  <input type="radio" name="coupon_status" value="0" {{ old('coupon_status') == "0" ? 'checked' : '' }} />
							                  <span></span>
							                  Deactive
							              </label>
							          </div>
							           @error('coupon_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    		@enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  	</div>
							</div>
						</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">  {{ __('Create') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div>

        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
     <script src="{{ asset('js/pages/custom.js?xg') }}" type="text/javascript"></script>
 
<script type="text/javascript">
	$(document).ready(function(){
		$('#g_pcode').click(function(){
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			for (var i = 0; i < 5; i++)
			{
				text += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			
			$('#p_code').val(text);
			$('#show_p_code').html(text);
			
		});
	});
	$('.kt_datetimepicker_1').datetimepicker({
			format: "yyyy-mm-dd",
			startDate: new Date(),
			todayHighlight: true,
			autoclose: true,
			startView: 2,
			minView: 2,
			forceParse: 0,
			pickerPosition: 'bottom-right'
		});
</script>
<script type="text/javascript">
	
	FormValidation.formValidation(
 document.getElementById('add-coupon'),
 {
  fields: {
   name: {
    validators: {
     notEmpty: {
      message: 'Coupon name is required'
     },
    }
   },
   method:{
   	validators: {
     notEmpty: {
      message: 'Method is required'
     },
    }
   },
   discount_rate:{
   	validators: {
     notEmpty: {
      message: 'Discount is required'
     },
     // digits: {
     //  message: 'The value is not a valid digit'
     // },
    }
   },
   /*validity: {
    validators: {
     notEmpty: {
      message: 'validity days is required'
     },
     digits: {
      message: 'The value is not a valid digit'
     },
    }
   },*/
   validity: {
    validators: {
	    notEmpty: {
	      message: 'Coupon expire date is required'
	    },
        date: {
            format: 'YYYY-MM-DD',
            message: 'The value is not a valid date'
        }
    }
   },
},

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);

</script>
@endsection
