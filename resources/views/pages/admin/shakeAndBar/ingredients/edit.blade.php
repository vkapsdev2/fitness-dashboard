 {{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Edit Ingredients')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row"> 
        <div class="col-lg-12">
           <div class="card card-custom card-sticky" id="kt_page_sticky_card">
				<div class="card-header">
					<div class="card-title">
						<h3 class="card-label">
							Edit Ingredients <i class="mr-2"></i>
						</h3>
					</div>
					<div class="card-toolbar">
						<a href="{{route ('shakeBarIngredients.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
							<i class="ki ki-long-arrow-back icon-sm"></i>
							Back
						</a>
					</div>
				</div>
			<div class="card-body">

				<!--begin::Form-->
				<form class="form" method="POST" action="{{ route('shakeBarIngredients.update',[$ingredient->id]) }}" accept-charset="utf-8" enctype="multipart/form-data" id="edit-ingredients" name="edit-ingredients">
					  @csrf
					<div class="row">
						
						<div class="col-xl-12">
							<div class="my-5">
								<h3 class=" text-dark font-weight-bold mb-10">Ingredients Info:</h3>
								<div class="form-group row">
									<label class="col-3">Name</label>
									<div class="col-9">
										<input class="form-control form-control-solid" type="text" name="name" value="{{$ingredient->name}}" placeholder="Enter name"/>
										@error('name')
											<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>
								</div>
								
								<div class="form-group row green-border-focus shadow-textarea">
						            <label class="col-3">Price</label>
						            <div class="col-9">
						             <input class="form-control form-control-solid" type="number" name="price" value="{{ old('price') }}" placeholder="Enter price" maxlength="50" />
						              @error('price')
						                <div class="alert alert-danger">{{ $message }}</div>
						              @enderror
						            </div>
        						</div>

        						<div class="form-group row">
							      <label class="col-3 col-form-label">Status</label>
							      <div class="col-9 col-form-label">
							          <div class="radio-inline">
							              <label class="radio">
							                   <input type="radio" name="ingredients_status" value="1"  {{ ($ingredient->ingredients_status == "1")? 'checked' : '' }}/>
							                  <span></span>
							                Activate
							              </label>
							              <label class="radio">
							                   <input type="radio" name="ingredients_status" value="2" {{ ($ingredient->ingredients_status == "2" )? 'checked' : '' }} />
							                  <span></span>
							                  Deactive
							              </label>
							          </div>
							           @error('ingredients_status')
							      <div class="alert alert-danger">{{ $message }}</div>
							    @enderror
							          <span class="form-text text-muted"></span>
							      </div>
							  </div>
							</div>
							</div>
						</div>
						
						<div class="form-group text-center m-t-20">
		                    <div class="col-xs-12">
		                        <button class="btn btn-primary" type="submit">   {{ __('Update') }}</button>
		                         <a class="btn btn-primary" type="button" href="">   {{ __('Cancel') }}</a>
		                    </div>
                		</div>
					</div>

				</form>
				<!--end::Form-->
			</div> 
        </div>

        
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
   
<script type="text/javascript">
	FormValidation.formValidation(
 document.getElementById('edit-ingredients'),
 {
   fields: {
   name: {
    validators: {
     notEmpty: {
      message: 'Ingredients name is required'
     },
    }
   },
   price: {
    validators: {
     notEmpty: {
      message: 'Ingredients price is required'
     },
    }
   },
   ingredients_status: {
    validators: {
     notEmpty: {
      message: 'Status is required'
     },
    }
   },
  },

  plugins: {
   trigger: new FormValidation.plugins.Trigger(),
   // Bootstrap Framework Integration
   bootstrap: new FormValidation.plugins.Bootstrap(),
   // Validate fields when clicking the Submit button
   submitButton: new FormValidation.plugins.SubmitButton(),
            // Submit the form when all fields are valid
   defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
  }
 }
);
</script>
@endsection
