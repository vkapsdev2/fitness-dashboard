{{-- Extends layout --}}
@extends('layout.default')

@section('title', 'Users')


{{-- Content --}}
@section('content')

            <!--begin::Card-->
<div class="card card-custom">
    <!--begin::Header-->
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">
                Users
                <span class="d-block text-muted pt-2 font-size-sm">List of users.</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
<div class="dropdown dropdown-inline mr-2">
    <button style="display: none;" type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/PenAndRuller.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3"/>
        <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000"/>
    </g>
</svg><!--end::Svg Icon--></span>       Export
    </button>

    <!--begin::Dropdown Menu-->
    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
        <!--begin::Navigation-->
        <ul class="navi flex-column navi-hover py-2">
            <li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">
                Choose an option:
            </li>
            <li class="navi-item">
                <a href="#" class="navi-link">
                    <span class="navi-icon"><i class="la la-print"></i></span>
                    <span class="navi-text">Print</span>
                </a>
            </li>
            <li class="navi-item">
                <a href="#" class="navi-link">
                    <span class="navi-icon"><i class="la la-copy"></i></span>
                    <span class="navi-text">Copy</span>
                </a>
            </li>
            <li class="navi-item">
                <a href="#" class="navi-link">
                    <span class="navi-icon"><i class="la la-file-excel-o"></i></span>
                    <span class="navi-text">Excel</span>
                </a>
            </li>
            <li class="navi-item">
                <a href="#" class="navi-link">
                    <span class="navi-icon"><i class="la la-file-text-o"></i></span>
                    <span class="navi-text">CSV</span>
                </a>
            </li>
            <li class="navi-item">
                <a href="#" class="navi-link">
                    <span class="navi-icon"><i class="la la-file-pdf-o"></i></span>
                    <span class="navi-text">PDF</span>
                </a>
            </li>
        </ul>
        <!--end::Navigation-->
    </div>
    <!--end::Dropdown Menu-->
</div>
<!--end::Dropdown-->

<!--begin::Button-->


<a href="javascript:void();" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#addNewUserModal">
    <span class="svg-icon svg-icon-md"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Design/Flatten.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <circle fill="#000000" cx="9" cy="15" r="6"/>
        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
    </g>
</svg><!--end::Svg Icon--></span>   New Record
</a>
<!--end::Button-->
        </div>
    </div>
    <!--end::Header-->

    <!--begin::Body-->
    <div class="card-body">
        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_user_list_datatable"></div>
        <!--end: Datatable-->
    </div>
    <!--end::Body-->
    <!-- Modal Popup start for add New User -->
    <!-- Button trigger modal-->
    <!-- Modal-->
   
    <div class="modal fade" id="addNewUserModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                        <h5 class="modal-title">Create New User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                     <form class="form" id="kt_user_add_form">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Full Name:</label>
                                <input type="text" class="form-control" placeholder="Enter full name" name="name" />
                                <span class="form-text text-muted">Please enter your full name</span>
                            </div>
                            <div class="form-group">
                                <label>Email address:</label>
                                <input type="email" class="form-control" placeholder="Enter email" autocomplete="off" name="email" />
                                 <!-- <input class="form-control form-control-solid h-auto py-4 px-8" type="text" placeholder="Email" name="email" autocomplete="off" /> -->
                                <!-- <span class="form-text text-muted">We'll never share your email with anyone else</span> -->
                            </div>
                             <div class="form-group">
                                <label>Password:</label>
                                <input type="password" class="form-control" placeholder="Enter Password" autocomplete="off" name="password" />
                                 <!-- <input class="form-control form-control-solid h-auto py-4 px-8" type="text" placeholder="Email" name="email" autocomplete="off" /> -->
                                <!-- <span class="form-text text-muted">We'll never share your email with anyone else</span> -->
                            </div>
                             <div class="form-group">
                                <label>Confirm password:</label>
                                <input type="password" class="form-control" placeholder="Confirm your password" autocomplete="off" name="password_confirmation" />
                                 <!-- <input class="form-control form-control-solid h-auto py-4 px-8" type="text" placeholder="Email" name="email" autocomplete="off" /> -->
                                <!-- <span class="form-text text-muted">We'll never share your email with anyone else</span> -->
                            </div>

                            <div class="form-group ">
                            <label>User role</label>
                                <select class="form-control  selectpicker" name="role">
                                     @foreach ($roles as $key => $role)
                                     <option value="{{ $role}}">{{ $role}}</option>
                                     @endforeach;
                                 </select>
                                   
                        </div>
                        </div>
                        <div class="card-footer text-right border-top-0">
                            <button type="reset" class="btn btn-primary" id="kt_user_add_form_submit">Create</button>
                            <span class="mx-2">or</span>
                            <a href="javascript:void()" class="btn btn-link btn-link-primary" data-dismiss="modal">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Popup for New user -->


         <!-- End Popup for edit user -->
       <div class="modal fade" id="editUserModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true" data-user-tobe-edit="0">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                        <h5 class="modal-title" >Edit User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                     <form class="form" id="kt_user_edit_form">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Full Name:</label>
                                <input type="text" class="form-control" placeholder="Enter full name" name="name" />
                            </div>
                            <div class="form-group">
                                <label>Email address:</label>
                                <input type="email" class="form-control" placeholder="Enter email" autocomplete="off" name="email" disabled="disabled" />                                
                            </div>                       

                            <div class="form-group ">
                            <label>User role</label>
                                <select class="form-control  selectpicker" name="role">
                                     @foreach ($roles as $key => $role)
                                     <option value="{{ $role}}">{{ $role}}</option>
                                     @endforeach;
                                 </select>
                                   
                        </div>
                        </div>
                        <div class="card-footer text-right border-top-0">
                            <button type="reset" class="btn btn-primary" id="kt_user_edit_form_submit">Update</button>
                            <span class="mx-2">or</span>
                            <a href="javascript:void()" class="btn btn-link btn-link-primary" data-dismiss="modal">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Popup for edit user -->


</div>
<!--end::Card-->

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

    {{-- page scripts --}}
    <!-- <script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script> -->
    <!-- <script src="{{ asset('js/app.js') }}" type="text/javascript"></script> -->
    <script src="{{ asset('js/pages/custom/user/list-datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/user/add-user.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/user/edit-user.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function KTUserDelete_action(id){
             Swal.fire({
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                  if (result.value) {
                    $.ajax('https://vkapsprojects.com/pedro/skeleton/public/users/ajaxDeleteUser/'+id, {
                      method: 'DELETE',
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      },
                      // data: $( '#kt_user_add_form' ).serialize()
                    }).done(function (r) {
                      console.log('result',r); 
                      
                     if(r.success){
                        window.location.reload();
                      }else{                
                        swal.fire({
                        text: 'User does not exist..',
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                          confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                      }).then(function () {
                        KTUtil.scrollTop();
                      });            
                      }    
                    }).fail(function (jqXHR, textStatus, errorThrown) {             
                        swal.fire({
                        text: errors,
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                          confirmButton: "btn font-weight-bold btn-light-primary"
                        }
                      }).then(function () {
                        KTUtil.scrollTop();
                      });    
                    });
                  }
                });
             

        }

        function KTUserEdit_open_form(id){
            var form = jQuery('#kt_user_edit_form');
            form.find('input[name=name]').val();
            form.find('input[name=email]').val();
            // form.find('#user-tobe-edit').val();

            
              $.ajax('https://vkapsprojects.com/pedro/skeleton/public/users/ajaxGetUser/'+id, {
              method: 'GET',
              headers: {
                  // 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              // data: $( '#kt_user_add_form' ).serialize()
            }).done(function (r) {
              console.log('result',r); 
              
             if(r.success){
                var user = r.user;
                var userRole = r.userRole;
                form.find('input[name=name]').val(user.name);
                form.find('input[name=email]').val(user.email);
                form.data('user-tobe-edit',user.id);

                
                for (let role of Object.values(userRole)) {
                    console.log(role);
                    form.find('input[name=role]').val(role).trigger("change");                  
                }
                jQuery('#editUserModal').modal('show');


              }else{
                
                swal.fire({
                text: 'User does not exist..',
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                  confirmButton: "btn font-weight-bold btn-light-primary"
                }
              }).then(function () {
                KTUtil.scrollTop();
              });            
              }    
            }).fail(function (jqXHR, textStatus, errorThrown) {
              console.log('errorThrown',errorThrown);
              console.log('textStatus',textStatus);
              console.log('jqXHR',jqXHR);

               var errors = '';
                for (let [key, value] of Object.entries(errorThrown.errors)) {
                  errors += key+' : ' + value + '\n'
                }
                /*for(var i=0; i<r.error.length; i++){
                  
                } */ 
                swal.fire({
                text: errors,
                icon: "error",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                  confirmButton: "btn font-weight-bold btn-light-primary"
                }
              }).then(function () {
                KTUtil.scrollTop();
              });    
            });

           
        }
    </script>



@endsection
