<?php
// Aside menu
return [

    'items' => [
        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => true,
            'icon' => 'public/media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/',
            'new-tab' => false,
        ],

        // Custom
        [
            'section' => 'Custom',
        ], 
        
        [
                'title' => 'Coaches',
                'root' => true,
                'icon' => 'public/media/svg/icons/Communication/Group.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/',
                'bullet' => 'dot',
                'root' => true,
                    'submenu' => [
                    [
                        [
                            'title' => 'Coach',
                            'bullet' => 'dot',
                            'page' => '/coach',
                        ],
                        [
                            'title' => 'Testimonial',
                            'bullet' => 'dot',
                            'page' => '/coach/testimonial',
                        ],  
                    ],
                    [
                        'title' => 'Coach assignment to customer',
                        'bullet' => 'dot',
                        'page' => '/coach/customer_assign',
                    ],
                    [
                        'title' => 'Meals',
                        'bullet' => 'dot',
                        'page' => '/coach/meals',
                    ],
                    /*[
                        'title' => 'Suggested Meal to Customer',
                        'bullet' => 'dot',
                        'page' => '/coach/suggest_meals',
                    ],*/
                ]
            
        ],

          /*  udated code by swadha at 2-july-2020*/
          [
            'title' => 'Customers',
            'icon' => 'public/media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                 [
                    'title' => 'Customers',
                    'bullet' => 'dot',
                    'page' => '/customers',
                ],
                [
                    'title' => 'Food Log',
                    'bullet' => 'dot',
                    'page' => '/customers/food_log',
                ],
                [
                    'title' => 'Reminders',
                    'bullet' => 'dot',
                    'page' => '/customers/reminder',
                ],
                [
                    'title' => 'Body Measurement Log',
                    'bullet' => 'dot',
                    'page' => '/customers/body_measurement_log',
                ],
                [
                    'title' => 'Custom Workout',
                    'bullet' => 'dot',
                    'page' => '/customers/custom_workout',
                ],
                [
                    'title' => 'Notification',
                    'bullet' => 'dot',
                    'page' => 'customer/notifications',
                ],
            ],
          ],
          /* End udated code by swadha at 2-july-2020*/
          
          [
                'title' => 'Quote of the Day',
                'root' => true,  
                'icon' => 'public/media/svg/icons/Text/Quote1.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/quote_of_day',
                 'bullet' => 'dot',
                'root' => true,
                    'submenu' => [
                     [
                        'title' => 'Quote',
                        'bullet' => 'dot',
                        'page' => '/quote_of_day',
                    ]
                    
                ]
            
            ],


            [
                'title' => 'Events',
                'root' => true,
                'icon' => 'public/media/svg/icons/General/Notifications1.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/eventsList',
                 'bullet' => 'dot',
                'root' => true,
                    'submenu' => [
                     [
                        'title' => 'Events',
                        'bullet' => 'dot',
                        'page' => '/eventsList',
                    ]
                    
                ]
            
            ],
           
            [
                'title' => 'Plans',
                'root' => true,
                'icon' => 'public/media/svg/icons/Communication/Clipboard-check.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/',
                'bullet' => 'dot',
                'root' => true,
                    'submenu' => [
                     [
                        'title' => 'Plans',
                        'bullet' => 'dot',
                         'page' => '/plans'

                     ],    
                 ]
            ],
            [
                'title' => 'Better Together Schedule',
                'root' => true,
                'icon' => 'public/media/svg/icons/Devices/Server.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/schedule_classes',
                'bullet' => 'dot',
                'root' => true,
                'submenu' => [
                    [
                        'title' => 'Express',
                        'bullet' => 'dot',
                        'page' => '/schedule_classes/express/',
                    ],
                    [
                        'title' => 'CornerStone',
                        'bullet' => 'dot',
                        'page' => '/schedule_classes/corner_stone/',
                    ],
                    [
                        'title' => 'Flex&Flow',
                        'bullet' => 'dot',
                        'page' => '/schedule_classes/flex_flow/',
                    ],
                    [
                        'title' => 'CoreFlex',
                        'bullet' => 'dot',
                        'page' => '/schedule_classes/core_flex/',
                    ],
                ],
            ],
            [
                'title' => 'Resources',
                'root' => true,
                'icon' => 'public/media/svg/icons/Tools/Tools.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/podcasts',
                'bullet' => 'dot',
                'root' => true,
                    'submenu' => [
                     [
                        'title' => 'Podcast',
                        'bullet' => 'dot',
                        'page' => '/podcasts',
                    ],
                    [
                        'title' => 'Articles',
                        'bullet' => 'dot',
                        'page' => '/articles',
                    ],
                    [
                        'title' => 'Book',
                        'bullet' => 'dot',
                        'page' => '/books',
                    ],
                    [
                        'title' => 'Recipes',
                        'bullet' => 'dot',
                        'page' => '/recipies',
                    ],
                ]
            ],

            [
                'title' => 'Shake Bar',
                'root' => true,
                'icon' => 'public/media/svg/icons/Food/Wine.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/shakeBar',
                'bullet' => 'dot',
                'root' => true,
                    'submenu' => [
                     [
                        'title' => 'Shake Bar',
                        'bullet' => 'dot',
                        'page' => '/shakeBar',
                    ],
                    [
                        'title' => 'Order',
                        'bullet' => 'dot',
                        'page' => '/shakeBar/order',
                    ],
                    [
                        'title' => 'Ingredients',
                        'bullet' => 'dot',
                        'page' => '/shakeBar/ingredients',
                    ],
                    [
                        'title' => 'Coupons',
                        'bullet' => 'dot',
                        'page' => '/shakeBar/coupon',
                    ],
                 ]
            ],
              [
                'title' => 'Badges',
                'icon' => 'public/media/svg/icons/Electric/Highvoltage.svg',
                'bullet' => 'dot',
                'root' => true,
                'submenu' => [
                    [
                        'title' => 'Badges',
                        'bullet' => 'dot',
                        'page' => '/badges',
                    ],
                    [
                        'title' => 'Customer Badges',
                        'bullet' => 'dot',
                        'page' => '/customer/badges',
                    ],
                    
                ],
            ],
            
            [
                'title' => 'MindFlex Planner',
                'root' => true,
                'icon' => 'public/media/svg/icons/Devices/Server.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/mindflex_planner/movement/home_workout',
                'bullet' => 'dot',
                'root' => true,
                    'submenu' => [
                    [
                        'title' => 'Movement',
                        'bullet' => 'dot',
                        'page' => '/mindflex_planner/movement/home_workout',
                        'submenu' =>[
                            [
                                'title' => 'Home Workouts',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/movement/home_workout'
                            ],
                            [
                                'title' => 'Gym Workouts',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/movement/gym_workout',
                            ],
                            [
                                'title' => 'HIT',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/movement/hit',
                            ],
                            [
                                'title' => 'Free Weights',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/movement/free_weight',
                            ] //end Free Weights
                        ], 
                    ],//Movement end
                    [
                            'title' => 'MindSet',
                            'bullet' => 'dot',
                            'page' => '/mindflex_planner/mindset/daily_affirmation',
                            'submenu' =>[
                                [
                                    'title' => 'Daily Affirmations',
                                    'bullet' => 'dot',
                                    'page' => '/mindflex_planner/mindset/daily_affirmation',
                                ], //Daily Affirmation end
                                [
                                    'title' => 'Assignments',
                                    'bullet' => 'dot',
                                    'page' => '/mindflex_planner/mindset/assignment',
                                ], //Assignment end
                                [
                                    'title' => 'Questions',
                                    'bullet' => 'dot',
                                    'page' => '/mindflex_planner/mindset/question_to_coaches',
                                ], //question of coaches end
                                [
                                    'title' => 'MindSet Details',
                                    'bullet' => 'dot',
                                    'page' => '/mindflex_planner/mindset/mindset_detail',
                                ], //Mindset detail end
                            ]
                    ], //Mindset end
                    [
                        'title' => 'Nutritions',
                        'root' => true,
                        'icon' => 'public/media/svg/icons/Food/Two-bottles.svg', // or can be 'flaticon-home' or any flaticon-*
                        'page' => '/mindflex_planner/nutrition/food_log',
                         'bullet' => 'dot',
                        'root' => true,
                            'submenu' => [
                             [
                                'title' => 'Food Logs',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/nutrition/food_log',
                            ],
                            [
                                'title' => 'Suggested Meals',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/nutrition/suggested_meal',
                            ],
                            
                            [
                                'title' => 'Tips',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/nutrition/tip',
                            ],
                            [
                                'title' => 'Need Help',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/nutrition/faq/list',
                            ],
                            
                            
                        ]
                    ], //Nutritions end
                    [
                        'title' => 'Supplementation',
                        'root' => true,
                        'icon' => 'public/media/svg/icons/Food/Two-bottles.svg', // or can be 'flaticon-home' or any flaticon-*
                        'page' => '/mindflex_planner/supplementation/reoccur_reminder',
                         'bullet' => 'dot',
                        'root' => true,
                            'submenu' => [
                             [
                                'title' => 'Reoccurring Reminders',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/supplementation/reoccur_reminder',
                            ],
                            [
                                'title' => 'Guides',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/supplementation/guide',
                            ],
                            
                            [
                                'title' => 'Customer Supplements',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/supplementation/supplement',
                            ],
                             [
                                'title' => 'Stores',
                                'bullet' => 'dot',
                                'page' => '/mindflex_planner/supplementation/store',
                            ],
                        ]
                    ], //Supplementation end
                    
                    [
                        'title' => 'Progress Photos',
                        'bullet' => 'dot',
                        'page' => '/mindflex_planner/progress_photos',
                    ], //Progress Photos end
                    [
                        'title' => 'MindFlex RX',
                        'bullet' => 'dot',
                        'page' => '/mindflex_planner/mindflex_rx',
                    ], //MindFlex RX end
                    
                ]
            ],

            [
                'title' => 'MindFlex Kinetics',
                'root' => true,
                'icon' => 'public/media/svg/icons/Devices/Server.svg', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/mindflex_kinetics/daily_stretch',
                'bullet' => 'dot',
                'root' => true, 
                'submenu' => [
                    [
                        'title' => 'Daily Stretches',
                        'bullet' => 'dot',
                        'page' => '/mindflex_kinetics/daily_stretch',
                    ], //Daily Stretch end
                    [
                        'title' => '4 Week Wellness Guide',
                        'root' => true,
                        'icon' => 'public/media/svg/icons/Food/Two-bottles.svg', // or can be 'flaticon-home' or any flaticon-*
                        'page' => 'mindflex_kinetics/wellness_guide/beginner/movement',
                        'bullet' => 'dot',
                        'root' => true,
                        'submenu' => [
                            [
                                'title' => 'Beginner Level',
                                'bullet' => 'dot',
                                'page' => 'mindflex_kinetics/wellness_guide/beginner/movement',
                                'submenu'=>[
                                    [
                                        'title' => 'Movement',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/beginner/movement',
                                    ], //Movement End
                                    [
                                        'title' => 'MindSet',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/beginner/mindset',
                                    ], //MindSet End
                                    [
                                        'title' => 'Nutrition',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/beginner/nutrition',
                                    ], //Nutrition End
                                    [
                                        'title' => 'Supplementation',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/beginner/supplementation',
                                    ], //Supplementation End
                                ],
                            ], //Beginner level end
                            [
                                'title' => 'Intermediate Level',
                                'bullet' => 'dot',
                                'page' => 'mindflex_kinetics/wellness_guide/intermediate/movement',
                                'submenu'=>[
                                    [
                                        'title' => 'Movement',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/intermediate/movement',
                                    ], //Movement End
                                    [
                                        'title' => 'MindSet',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/intermediate/mindset',
                                    ], //MindSet End
                                    [
                                        'title' => 'Nutrition',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/intermediate/nutrition',
                                    ], //Nutrition End
                                    [
                                        'title' => 'Supplementation',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/intermediate/supplementation',
                                    ], //Supplementation End
                                ],
                            ],  //Intermediate level end
                            [
                                'title' => 'Advance Level',
                                'bullet' => 'dot',
                                'page' => 'mindflex_kinetics/wellness_guide/advance/movement',
                                'submenu'=>[
                                    [
                                        'title' => 'Movement',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/advance/movement',
                                    ], //Movement End
                                    [
                                        'title' => 'MindSet',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/advance/mindset',
                                    ], //MindSet End
                                    [
                                        'title' => 'Nutrition',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/advance/nutrition',
                                    ], //Nutrition End
                                    [
                                        'title' => 'Supplementation',
                                        'bullet' => 'dot',
                                        'page' => 'mindflex_kinetics/wellness_guide/advance/supplementation',
                                    ], //Supplementation End
                                ],
                            ]  //Advance level end
                        ], 
                    ], //4 Week wellness Guide end
                    [
                        'title' => 'Breathing',
                        'bullet' => 'dot',
                        'page' => '/mindflex_kinetics/breathing',
                    ], //Breathing end
                    [
                        'title' => 'Daily Affirmation',
                        'bullet' => 'dot',
                        'page' => '/mindflex_kinetics/daily_affirmation',
                    ], // Kinetics Daily Affitmation end
                    [
                        'title' => 'Stores',
                        'bullet' => 'dot',
                        'page' => 'mindflex_kinetics/store',
                    ], //Kinetics Store end
                ]  
            ],
            [
                'title' => 'MindFlex Physio',
                'root' => true,
                'icon' => 'fas fa-running" style="font-size:24px"', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/mindflex_physio/',
                'bullet' => 'dot',
                'root' => true, 
                'submenu' => [
                    [
                        'title' => 'See a Doc',
                        'bullet' => 'dot',
                        'page' => '/mindflex_physio/doc',
                    ], //Doc  end
                    [
                        'title' => 'Exercise Program',
                        'bullet' => 'dot',
                        'page' => '/mindflex_physio/exercise_program',
                    ], //End exercise program
                    [
                        'title' => 'Patient Profile',
                        'root' => true,
                        'icon' => 'public/media/svg/icons/Food/Two-bottles.svg', // or can be 'flaticon-home' or any flaticon-*
                        'page' => '#',
                        'bullet' => 'dot',
                        'root' => true,
                            'submenu' => [
                            [
                                'title' => 'Medical History',
                                'bullet' => 'dot',
                                'page' => '/mindflex_physio/profile/medical_history',
                            ],
                             [
                                'title' => 'Daily Notes',
                                'bullet' => 'dot',
                                'page' => '/mindflex_physio/profile/daily_notes',
                            ],  
                        ]
                    ], //End Profile
                    [
                        'title' => 'Environment Factore',
                        'bullet' => 'dot',
                        'page' => '/mindflex_physio/environment',
                    ], //End Environment factore
                    [
                        'title' => 'Daily Affirmation',
                        'bullet' => 'dot',
                        'page' => '/mindflex_physio/daily_affirmation',
                    ], // Physio Daily Affitmation end
                    [
                        'title' => 'Nutritions',
                        'root' => true,
                        'icon' => 'public/media/svg/icons/Food/Two-bottles.svg', // or can be 'flaticon-home' or any flaticon-*
                        'page' => '/mindflex_physio/nutrition/food_log',
                         'bullet' => 'dot',
                        'root' => true,
                            'submenu' => [
                             [
                                'title' => 'Food Log',
                                'bullet' => 'dot',
                                'page' => '/mindflex_physio/nutrition/food_log',
                            ],
                            [
                                'title' => 'Recommendation Meal',
                                'bullet' => 'dot',
                                'page' => '/mindflex_physio/nutrition/recommendation',
                            ],
                          ]
                    ], //Nutritions end
                    [
                        'title' => 'Store',
                        'bullet' => 'dot',
                        'page' => 'mindflex_physio/store',
                    ], //Physio Store end
                    [
                        'title' => 'Article',
                        'bullet' => 'dot',
                        'page' => '/mindflex_physio/articles',
                    ], //End Articles
                ]  
            ],
            [
                'title' => 'MindFlex cleanse',
                'root' => true,
                'icon' => 'fas fa-running" style="font-size:24px"', // or can be 'flaticon-home' or any flaticon-*
                'page' => '/mindflex_cleanse/weight_loss',
                'bullet' => 'dot',
                'root' => true, 
                'submenu' => [
                    [
                        'title' => 'Weight Loss',
                        'bullet' => 'dot',
                        'page' => '/mindflex_cleanse/weight_loss',
                    ], //weight loss  end
                    [
                        'title' => 'Mind Reset',
                        'bullet' => 'dot',
                        'page' => '/mindflex_cleanse/mind_reset',
                    ], //End mind reset
                    [
                        'title' => 'Nutrition',
                        'root' => true,
                        'icon' => 'public/media/svg/icons/Food/Two-bottles.svg', // or can be 'flaticon-home' or any flaticon-*
                        'page' => '/mindflex_cleanse/nutrition',
                        'bullet' => 'dot',
                        'root' => true,
                            'submenu' => [
                            [
                                'title' => 'Food Log',
                                'bullet' => 'dot',
                                'page' => '/mindflex_cleanse/nutrition/food_log',
                            ],
                            [
                                'title' => 'Suggested Meals',
                                'bullet' => 'dot',
                                'page' => '/mindflex_cleanse/nutrition/suggested_meal',
                            ],
                            [
                                'title' => 'Tips',
                                'bullet' => 'dot',
                                'page' => '/mindflex_cleanse/nutrition/tip',
                            ],  
                        ]
                    ], //End Nutrition
                    [
                        'title' => 'Community',
                        'bullet' => 'dot',
                        'page' => '/mindflex_cleanse/community',
                    ], //End Community
                    [
                        'title' => 'Ranking',
                        'bullet' => 'dot',
                        'page' => '/mindflex_cleanse/ranking',
                    ], // Ranking end
                    
                ]  
            ],
            [
                'title' => 'Payment',
                'icon' => 'public/media/svg/icons/Layout/Layout-4-blocks.svg',
                'bullet' => 'dot',
                'root' => true,
                'submenu' => [
                    [
                    'title' => 'Stripe',
                    'bullet' => 'dot',
                    'page' => '/stripe',
                    ],
                ],
            ],
            [
                'title' => 'Others',
                'icon' => 'public/media/svg/icons/General/Other1.svg',
                'bullet' => 'dot',
                'root' => true,
                'submenu' => [
                    [
                        'title' => 'Contact Us',
                        'bullet' => 'dot',
                        'page' => '/others/contact',
                    ],
                    [
                        'title' => 'Site Links',
                        'bullet' => 'dot',
                        'page' => '/others/social',
                    ],
                    
                ],
            ],
        ],
    ];
